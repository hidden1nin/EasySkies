package me.bukkit.hidden1nin.easySkies;

import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;



public class IslandProtecton implements Listener{
	
	@EventHandler
	public void blockInterract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if(event.getAction()==Action.RIGHT_CLICK_AIR) {return;}
		if(p.getLocation().getWorld().getName().equalsIgnoreCase(firstEnable.config.getString("loc.world"))) {
		 if(IslandInfo.OwnIsland(p)) {
				Enchants.ClickBlockEnchant(event);
				CustomBlocks.ClickBlockCommand(event);
		 }else {
				Enchants.ClickBlockEnchant(event);
				CustomBlocks.ClickBlockCommand(event);
				
			 if(event.getClickedBlock()!=null) {
				 Material b = event.getClickedBlock().getType();
			 if(b==Material.ENDER_CHEST||b==Material.CRAFTING_TABLE||b==Material.ANVIL||b==Material.ENCHANTING_TABLE) {

			 }else {
				 event.setCancelled(true);
			 }
			 }else {
				 event.setCancelled(true);
			 }
		 }
		 }else {
			 
		 }
		
		 }
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		 Player p = event.getPlayer();
		if(IslandInfo.OwnIsland(p)) {
			Enchants.doPlaceEnchant(p, event);
		}else {
			event.setCancelled(true);
		}
		}
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		 Player p = event.getPlayer();
		 if(IslandInfo.OwnIsland(p)) {
			 Enchants.doMineEnchant(p, event);
	    }else{
	    	 //p.sendMessage(server+"You can only do this on your own island!");
	    	event.setCancelled(true);
	    }
	}
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getEntity()instanceof Entity) {
			Entity ent =e.getEntity();
			if(ent.isInvulnerable()) {
				e.setCancelled(true);
				return;
			}
		}
		if(e.getDamager() instanceof  Player && e.getEntity() instanceof Player&&e.getDamager().getLocation().distance(firstEnable.Boss)<25&&e.getEntity().getLocation().distance(firstEnable.Boss)<25) {return;}
		if(e.getDamager() instanceof  Player && e != null) {
	    Entity p = e.getDamager();
	    if(IslandInfo.getIsland(p.getLocation()).equalsIgnoreCase("none")&&IslandInfo.OwnIsland((Player) p)) { 
			Player player =(Player)p;
		Enchants.doPvpEnchant(player,e.getEntity());
		
	    return;}else {
	    if (firstEnable.config.getBoolean("IslandOwnerSettings."+IslandInfo.getIsland(p.getLocation())+".Pvp")) {
	    	if( e.getEntity() instanceof Monster || e.getEntity() instanceof Player) {
	    			Player player =(Player)p;
	    		Enchants.doPvpEnchant(player,e.getEntity());

	    		return;
	    	}else {
	    		if(IslandInfo.OwnIsland((Player)p)&&e.getEntity() instanceof Player==false){
	    			Player player =(Player)p;
	        		Enchants.doPvpEnchant(player,e.getEntity());
	    	    		return;
	    		}else {
	    			e.setCancelled(true);
	    		}
	    		
	    	}
	         
	    }else {
	    	if(e.getEntity() instanceof Monster||e.getEntity() instanceof Slime) {
	    		Player player =(Player)p;
    		Enchants.doPvpEnchant(player,e.getEntity());
	    		return;
	    	}else {
	    		if(IslandInfo.OwnIsland((Player)p)&&e.getEntity() instanceof Player==false){
	    			Player player =(Player)p;
	        		Enchants.doPvpEnchant(player,e.getEntity());
	    	    		return;
	    		}else {
	    			e.setCancelled(true);
	    		}
	    		
	    	}
	    }
	    
	}
		}}
	@EventHandler
	public void onPlayerEntity(PlayerInteractEntityEvent event){
		if(event.getHand()!=EquipmentSlot.HAND) {
			return;
		}
		Player p = event.getPlayer();
		if(!IslandInfo.OwnIsland(event.getPlayer())){
			event.setCancelled(true);
				IslandMobs.clicked(event);
		}else {
			if(event.getRightClicked() instanceof Player) {return;}
			if(p.getInventory().getItemInMainHand()==null) {return;}
			if(p.getInventory().getItemInMainHand().getItemMeta()==null) {return;}
			if(p.getInventory().getItemInMainHand().getItemMeta().getLore()==null) {return;}
			ItemStack cage =Main.createDisplay(Material.IRON_BARS, Main.EasySkyCustomRecipes, -2, ChatColor.GRAY + "Mob Cage",ChatColor.AQUA+ "Can hold up to four mobs, right click to use");
			if (p.getInventory().getItemInMainHand().getAmount()==1&&p.getInventory().getItemInMainHand().isSimilar(cage)&&event.getRightClicked() instanceof Monster == false) {
				String type =ChatColor.BLUE+event.getRightClicked().getType().toString().toLowerCase();
				ItemMeta hand =p.getInventory().getItemInMainHand().getItemMeta();
				List<String> lore = hand.getLore();
				lore.clear();
				lore.add(type);
				hand.setLore(lore);
				hand.setDisplayName(ChatColor.DARK_GRAY + "Used Mob Cage");
				p.getInventory().getItemInMainHand().setItemMeta(hand);
				event.getRightClicked().remove();
				return;
			}
			ItemStack newItem = new ItemStack(Material.IRON_BARS, 1);
			ItemMeta newItemMeta = newItem.getItemMeta();
			newItemMeta.setDisplayName(ChatColor.DARK_GRAY+ "Used Mob Cage");
			newItemMeta.setLore(p.getInventory().getItemInMainHand().getItemMeta().getLore());
			newItem.setItemMeta(newItemMeta);
			if(p.getInventory().getItemInMainHand().isSimilar(newItem)&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)) {
				String type =ChatColor.BLUE+event.getRightClicked().getType().toString().toLowerCase();
				ItemMeta hand =p.getInventory().getItemInMainHand().getItemMeta();
				List<String> lore = hand.getLore();
				if(lore.size()>3) {return;}
				lore.add(type);
				hand.setLore(lore);
				p.getInventory().getItemInMainHand().setItemMeta(hand);
				event.getRightClicked().remove();
				return;
			}
			ItemStack net= Main.createDisplay(Material.VINE, Main.EasySkyCustomRecipes, -2, ChatColor.YELLOW + "Empty Net", ChatColor.AQUA+"can capture mobs when right clicked!");
			if (Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)&&p.getInventory().getItemInMainHand().getAmount()==1&&p.getInventory().getItemInMainHand().isSimilar(net)&&event.getRightClicked() instanceof Monster == false) {
				String type =ChatColor.BLUE+event.getRightClicked().getType().toString().toLowerCase();
				ItemMeta hand =p.getInventory().getItemInMainHand().getItemMeta();
				List<String> lore = hand.getLore();
				lore.clear();
				lore.add(type);
				hand.setLore(lore);
				hand.setDisplayName(ChatColor.GOLD + "Full Net");
				p.getInventory().getItemInMainHand().setItemMeta(hand);
				event.getRightClicked().remove();
			}
		}
		if(event.getRightClicked() instanceof Player) {return;}
		if(p.getInventory().getItemInMainHand()==null) {return;}
		if(p.getInventory().getItemInMainHand().getItemMeta()==null) {return;}
		if(p.getInventory().getItemInMainHand().getItemMeta().getLore()==null) {return;}
		if(p.hasPermission("permission.easyskies.jail")) {if(Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)) {
			ItemStack Jail =Main.createDisplay(Material.IRON_DOOR,null, -2,ChatColor.AQUA+"Portable Jail",ChatColor.RED+"Click on players to jail them, or on mobs to capture them!");
			if (p.getInventory().getItemInMainHand().getAmount()==1&&p.getInventory().getItemInMainHand().isSimilar(Jail)) {
				String type =ChatColor.BLUE+event.getRightClicked().getType().toString().toLowerCase();
				ItemMeta hand =p.getInventory().getItemInMainHand().getItemMeta();
				List<String> lore = hand.getLore();
				lore.clear();
				lore.add(type);
				hand.setLore(lore);
				p.getInventory().getItemInMainHand().setItemMeta(hand);
				if(event.getRightClicked().getType()== EntityType.PLAYER) {
					Cooldowns.isCooldownOver(AntiCheat.Jailed,-1, (Player) event.getRightClicked());
				}else {
				event.getRightClicked().remove();
				}
				return;
				
			}}
			ItemStack newItem = new ItemStack(Material.IRON_DOOR, 1);
			ItemMeta newItemMeta = newItem.getItemMeta();
			newItemMeta.setDisplayName(ChatColor.AQUA+ "Portable Jail");
			newItemMeta.setLore(p.getInventory().getItemInMainHand().getItemMeta().getLore());
			newItem.setItemMeta(newItemMeta);
			if(p.getInventory().getItemInMainHand().isSimilar(newItem)) {
				String type =ChatColor.BLUE+event.getRightClicked().getType().toString().toLowerCase();
				ItemMeta hand =p.getInventory().getItemInMainHand().getItemMeta();
				List<String> lore = hand.getLore();
				lore.add(type);
				hand.setLore(lore);
				p.getInventory().getItemInMainHand().setItemMeta(hand);
				if(event.getRightClicked().getType()== EntityType.PLAYER) {
					Cooldowns.isCooldownOver(AntiCheat.Jailed,-1, (Player) event.getRightClicked());
				}else {
				event.getRightClicked().remove();
				}
			}
		}
		
	}
	@EventHandler
	public void onPlayerArmorStand(PlayerArmorStandManipulateEvent event){
		if(!IslandInfo.OwnIsland(event.getPlayer())){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onPlayerTame(EntityTameEvent event){
		if(!IslandInfo.OwnIsland((Player) event.getOwner())){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if(event.getEntity().getWorld()==firstEnable.Spawn.getWorld()) {
			if(event.getEntity().getLocation().distance(firstEnable.Spawn)<1000&&event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL&&event.getEntity().hasAI()) {
				event.getEntity().remove();
			}
		}
		/*if(event.getEntityType()==EntityType.PHANTOM) {
			if(event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
				event.getEntity().remove();
				}
		}*/
		if(IslandInfo.getIsland(event.getLocation()).equals("none")){return;}
		if(firstEnable.config.getBoolean("IslandOwnerSettings."+IslandInfo.getIsland(event.getLocation())+".MobSpawn")== false) {
		if(event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
		event.getEntity().remove();
		}
		}
		}
}
