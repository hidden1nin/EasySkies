package me.bukkit.hidden1nin.easySkies;


import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.PolarBear;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class MobRiding implements Runnable,Listener{
	@EventHandler
	public void mount(PlayerInteractEntityEvent event) {
	
		if(event.getRightClicked() instanceof Monster ==true) {return;}
		if(Pets.Pets.get(event.getPlayer().getName())!= null) {
		if(event.getRightClicked()instanceof PolarBear&&event.getRightClicked()==Pets.Pets.get(event.getPlayer().getName())&&event.getPlayer().isSneaking()==false) {
			event.setCancelled(true);
			event.getRightClicked().addPassenger(event.getPlayer());
		}
		if(event.getRightClicked()==Pets.Pets.get(event.getPlayer().getName())&&event.getPlayer().isSneaking()&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, event.getPlayer())) {
			event.getPlayer().addPassenger(event.getRightClicked());
		}}
		if(event.getRightClicked() instanceof Player == true&&event.getPlayer().hasPermission("permission.easyskies.pickupplayer")&&event.getPlayer().isSneaking()&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, event.getPlayer())) {event.getPlayer().addPassenger(event.getRightClicked());}
		if(event.getRightClicked() instanceof Player == true&&event.getPlayer().hasPermission("permission.easyskies.rideplayer")&&event.getPlayer().isSprinting()) {event.getRightClicked().addPassenger(event.getPlayer());}
		if (event.getPlayer().getInventory().getItemInMainHand()==null) {return;}
		if(event.getPlayer().getInventory().getItemInMainHand().getItemMeta()==null) {return;}
		if (event.getPlayer().getInventory().getItemInMainHand().getItemMeta()
				.equals(Main.createDisplay(Material.LEAD, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Bare-Back Harness",ChatColor.WHITE+"Can be used to mount:/line/"+ChatColor.GRAY+"Chickens/line/"+ChatColor.GRAY+"Cows/line/"+ChatColor.GRAY+"Pigs/line/"+ChatColor.RED+"5/10 Speed")
						.getItemMeta())
				||event.getPlayer().getInventory().getItemInMainHand().getItemMeta()
						.equals(Main.createDisplay(Material.ELYTRA, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Winged Saddle",ChatColor.WHITE+"Can be used to fly:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GREEN+"15/10 Speed").getItemMeta())||event.getPlayer().getInventory().getItemInMainHand().getItemMeta()
						.equals(Main.createDisplay(Material.SADDLE, Main.EasySkyCustomRecipes, -2, ChatColor.RED + "Master Saddle",ChatColor.WHITE+"Can be used to ride:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GOLD+"10/10 Speed").getItemMeta())) {
			event.setCancelled(true);

		if(event.getRightClicked() instanceof Player == true) {return;}
		if(!IslandInfo.OwnIsland(event.getPlayer())) {return;}
		event.getRightClicked().addPassenger(event.getPlayer());
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void movemob(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if(p.isSneaking()&&p.getPassengers().size()!=0&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, event.getPlayer())) {p.removePassenger(p.getPassenger());
	}}
	@Override
	public void run() {
		if(BossEvents.bossMobs!=null){
			Iterator<LivingEntity> iterator = BossEvents.bossMobs.iterator(); 
		while(iterator.hasNext()){
			Entity mob = iterator.next();
			if(mob ==null||mob.isDead()&&mob!=BossEvents.BossMob) {iterator.remove();}
			if(mob.getLocation().distance(firstEnable.Boss)>25) {mob.setFallDistance(0);mob.teleport(firstEnable.Boss);}
		}
		}
		for(Player p:Bukkit.getOnlinePlayers()) {
			if(Creative.Creative.contains(p.getName())&&p.getGameMode()==GameMode.SURVIVAL) {
				Creative.Creative.remove(p.getName());
			}else {
				if(Creative.Creative.contains(p.getName())==false&&p.getGameMode()==GameMode.CREATIVE&&p.hasPermission("permission.easySkies.freecreative")==false) {
					Creative.Creative.add(p.getName());
				}
			}
			
		if(Cooldowns.silentIsCooldownOverNoUpdate(AntiCheat.moveTime, 600, p)&&Cooldowns.silentIsCooldownOverNoUpdate(AntiCheat.interactTime, 600, p)) {
			AntiCheat.interactTime.put(p.getName(), System.currentTimeMillis());
			AntiCheat.moveTime.put(p.getName(), System.currentTimeMillis());
			Pets.removepet(p);
			p.teleport(firstEnable.Afk);
			Pets.removepet(p);
	}
		
		
		if(p.isInsideVehicle()&&p.getVehicle() instanceof Horse== false) {
			Entity v = p.getVehicle();
			p.setFallDistance(0);
			if(Pets.Pets.get(p.getName())!=null) {
				if(Pets.Pets.get(p.getName())==v) {
					Vector current_velocity = p.getVelocity();
					Vector walk_velocity = p.getLocation().getDirection().multiply(1.15);
					walk_velocity.setY(current_velocity.getY());
					v.setVelocity(current_velocity.add(walk_velocity));
					
				}
			}
			if(v instanceof Player) {return;}
			//v.getLocation().setPitch(p.getLocation().getPitch());
			//v.getLocation().setYaw(p.getLocation().getYaw());
			v.setRotation(p.getLocation().getYaw(),p.getLocation().getPitch());
			if(p.getInventory().getItemInMainHand()==null) {return;}
			if(p.getInventory().getItemInMainHand().getItemMeta()==null) {return;}
			if(p.getInventory().getItemInMainHand().getItemMeta().equals(Main.createDisplay(Material.ELYTRA, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Winged Saddle",ChatColor.WHITE+"Can be used to fly:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GREEN+"15/10 Speed").getItemMeta())) {
			
				Vector current_velocity = p.getVelocity();
				Vector walk_velocity = p.getLocation().getDirection().multiply(1.15);
				v.setVelocity(current_velocity.add(walk_velocity));
			}
			if(p.getInventory().getItemInMainHand().getItemMeta()
						.equals(Main.createDisplay(Material.SADDLE, Main.EasySkyCustomRecipes, -2, ChatColor.RED + "Master Saddle",ChatColor.WHITE+"Can be used to ride:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GOLD+"10/10 Speed").getItemMeta())) {
				if(v instanceof Monster ==false) {
					Vector current_velocity = p.getVelocity();
				Vector walk_velocity = p.getLocation().getDirection().multiply(1.1);
				walk_velocity.setY(current_velocity.getY());
				v.setVelocity(current_velocity.add(walk_velocity));
			}
			if(p.getInventory().getItemInMainHand().getItemMeta()
					.equals(Main.createDisplay(Material.LEAD, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Bare-Back Harness",ChatColor.WHITE+"Can be used to mount:/line/"+ChatColor.GRAY+"Chickens/line/"+ChatColor.GRAY+"Cows/line/"+ChatColor.GRAY+"Pigs/line/"+ChatColor.RED+"5/10 Speed")
							.getItemMeta())) {
			if(v instanceof Pig ||v instanceof Cow ||v instanceof Chicken) {
				Vector current_velocity = p.getVelocity();
			Vector walk_velocity = p.getLocation().getDirection().multiply(1.05);
			walk_velocity.setY(current_velocity.getY());
			v.setVelocity(current_velocity.add(walk_velocity));
			}
			}
		}
		}
	}
		}
	

}
