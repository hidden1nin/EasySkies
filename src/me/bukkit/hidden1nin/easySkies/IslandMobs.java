package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Evoker;
import org.bukkit.entity.Evoker.Spell;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import net.minecraft.server.v1_15_R1.NBTTagCompound;

public class IslandMobs {
@SuppressWarnings("deprecation")
public static void CreateMob(Location l, String name,String mob,String[] args) {
	String mobunclean= new String(mob);
	mob = mob.toUpperCase().split("-")[0];
	if(EntityType.valueOf(mob)!=null) {
	removeNPC(l);
	Location EntityArea = l;
    World world = l.getWorld();
   int clearRadius = 1;
    List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
    for(Entity e : world.getEntities()){
        if(nearbyEntities.contains(e)){
       	 if(e.getType()==EntityType.fromName(mob)) {
            e.remove();
       	 }
        }
    }

	Entity entity = l.getWorld().spawnEntity(l,EntityType.valueOf(mob));
	noAI(entity);
	entity.setCustomName(ChatColor.translateAlternateColorCodes('&', name).replace("/s/", " "));
    entity.setCustomNameVisible(true);
    entity.setInvulnerable(true);
    entity.setSilent(true);
    entity.setPersistent(true);
    entity.setGravity(false);
    if(mobunclean.split("-").length>1) {
    	/*if(mobunclean.toUpperCase().contains("EVOKER")) {
    		Evoker e = (Evoker) entity;
    				
    	}*/
    	if(mobunclean.toUpperCase().contains("VILLAGER")) {
    		Villager v = (Villager) entity;
    		if(v.getLocation().getBlock().getType().toString().contains("BED")) {
    			v.sleep(v.getLocation());
    		}
    		v.setProfession(Profession.valueOf(mobunclean.split("-")[1]));
    	}
    }
    //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoAI set value true ");
    List<String> mobs = Main.islandMobsConfig.getStringList("Npc.List");
    StringBuilder message = new StringBuilder(args[2]);
    for (int arg = 3; arg < args.length; arg++) {
      message.append(" ").append(args[arg]);
      }
    mobs.add(""+l.getX()+":"+l.getY()+":"+l.getZ()+":"+l.getWorld().getName()+":"+l.getPitch()+":"+l.getYaw()+":"+name.replace("/s/"," ")+":"+mobunclean+":"+message+":"+"/none/");
    Main.islandMobsConfig.set("Npc.List", mobs);
    Main.saveMobs();
	}
}

@SuppressWarnings("deprecation")
public static void clicked(PlayerInteractEntityEvent event) {
	EquipmentSlot hand = event.getHand(); // Get the hand of the event and make it the variable hand'.
	if (hand.equals(EquipmentSlot.HAND)) {
	for(String raw:Main.islandMobsConfig.getStringList("Npc.List")){
	String[] args = raw.split(":");
	if(event.getRightClicked().getType()==EntityType.fromName(args[7].toUpperCase().split("-")[0])) {
	if(ChatColor.stripColor(event.getRightClicked().getCustomName()).equals(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', args[6])))) {
		if(args.length==9) {
			if(!args[8].contains("/none/")) {
				event.getPlayer().chat("/"+args[8]);
				//Bukkit.dispatchCommand(event.getPlayer(), args[8]);
			}
			break;
		}else {
		if(!args[8].contains("/none/")||!args[9].contains("/none/")) {
		if(!args[8].contains("/none/")) {
		Bukkit.dispatchCommand(event.getPlayer(), args[8]);
	}
		if(!args[9].contains("/none/")) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), ChatColor.stripColor(args[9].replace("%player%", event.getPlayer().getName())));
		}
		break;
		}
		}
		break;
	}
	}
	}
	}

}

public static void removeNPC(Location l) {
	Location EntityArea = l;
    World world = l.getWorld();
   int clearRadius = 2;
    List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
    for(Entity e : world.getEntities()){
        if(nearbyEntities.contains(e)){
            for(String raw:Main.islandMobsConfig.getStringList("Npc.List")){
            	if(e instanceof Player == false) {e.remove();}
            	String[] args = raw.split(":");
            	if(l.distance(new Location(Bukkit.getWorld(args[3]),Double.parseDouble(args[0]),Double.parseDouble(args[1]),Double.parseDouble(args[2])))<5){
            		List<String> mobs = Main.islandMobsConfig.getStringList("Npc.List");
            	    mobs.remove(raw);
            	    if(e instanceof Player == false&& e instanceof ArmorStand == false) {e.remove();}
            		 Main.islandMobsConfig.set("Npc.List", mobs);
            		    Main.saveMobs();
            	}
            	}
        }
    }

}

@SuppressWarnings("deprecation")
public static void reload() {
	for(String raw:Main.islandMobsConfig.getStringList("Npc.List")){
    	String[] args = raw.split(":");
    	String mobunclean= new String(args[7]);
    	String mob = args[7].toUpperCase().split("-")[0];
    	Location l =new Location(Bukkit.getWorld(args[3]),Double.parseDouble(args[0]),Double.parseDouble(args[1]),Double.parseDouble(args[2]));
    	l.setYaw(Float.parseFloat(args[5]));
    	l.setPitch(Float.parseFloat(args[4]));
    	Location EntityArea = l;
        World world = l.getWorld();
       int clearRadius = 1;
        List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
       
        for(Entity e : world.getEntities()){
            if(nearbyEntities.contains(e)){
       
            	 if(e instanceof Player == false&& e instanceof ArmorStand == false) {e.remove();}
           	 
            }
        }
        
    	Entity entity = l.getWorld().spawnEntity(l, EntityType.valueOf(mob));
    	noAI(entity);
    	entity.setCustomName(ChatColor.translateAlternateColorCodes('&', args[6]));
        entity.setCustomNameVisible(true);
        entity.setInvulnerable(true);
        entity.setSilent(true);
        entity.setPersistent(true);
        entity.setGravity(false);
        if(mobunclean.split("-").length>1) {
        	if(mobunclean.toUpperCase().contains("VILLAGER")) {
        		Villager v = (Villager) entity;
        		if(v.getLocation().getBlock().getType().toString().contains("BED")) {
        			v.sleep(v.getLocation());
        		}
        		v.setProfession(Profession.valueOf(mobunclean.split("-")[1]));
        	}
        }
        //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoAI set value true ");
        
        }
	
}

static void invisible(Entity bukkitEntity) {
    net.minecraft.server.v1_15_R1.Entity nmsEntity = ((CraftEntity) bukkitEntity).getHandle();
   NBTTagCompound tag = null;
   if (tag == null) {
       tag = new NBTTagCompound();
   }
   nmsEntity.c(tag);
   tag.setInt("Invisible", 1);
   nmsEntity.f(tag);
}
static void noAI(Entity bukkitEntity) {
     net.minecraft.server.v1_15_R1.Entity nmsEntity = ((CraftEntity) bukkitEntity).getHandle();
    NBTTagCompound tag = null;
    if (tag == null) {
        tag = new NBTTagCompound();
    }
    nmsEntity.c(tag);
    tag.setInt("NoAI", 1);
    nmsEntity.f(tag);
}
}
