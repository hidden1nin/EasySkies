package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Quests {
	public static FileConfiguration config = Main.islandQuestsConfig;
	public static FileConfiguration playerconfig = Main.getPlugin().getConfig();
	public static void createQuest(Inventory inv) {
		Main.createDisplay(Material.RED_STAINED_GLASS,inv, inv.getSize()-5, ChatColor.WHITE+"Back", "");
		
		for(int i = 0;config.contains("Quests."+i);i++) {
			Material QuestMaterial = Material.getMaterial(config.getString("Quests."+i+".DisplayMaterial").toUpperCase());
			if(Material.getMaterial(config.getString("Quests."+i+".DisplayMaterial").toUpperCase())==null) {
				QuestMaterial = Material.STONE;
			}
		//Main.createDisplay(QuestMaterial, inv, -1, ChatColor.WHITE+config.getString("Quests."+i+".DisplayName"), ChatColor.GRAY+config.getString("Quests."+i+".DisplayLore"));
		ItemStack questItem =Main.createDisplay(QuestMaterial, inv, -2, ChatColor.WHITE+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayName").replace("z", "")),ChatColor.WHITE+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayLore").replace("z", "")));
		ItemMeta questItemMeta =questItem.getItemMeta();
		List<String> lore = questItemMeta.getLore();
		lore.add(" ");
		lore.add(ChatColor.AQUA+"Requirements:");
		for(String required:config.getStringList("Quests."+i+".ItemsRequired")) {
			if(required.contains("none")){
				lore.add(ChatColor.GRAY+ChatColor.translateAlternateColorCodes('&',required.replace("none", "")));
				}else {
					if(required.contains("statistic")) {
						if(required.contains("broken")||required.contains("placed")) {
							
							lore.add(ChatColor.GRAY+required.replace("statistic:", "").split(":")[0].replace("broken","Break").replace("placed","Place").toLowerCase()+" "+required.replace("statistic:", "").split(":")[1]+" blocks!");
							
						}else {
							lore.add(ChatColor.GRAY+required.replace("statistic:", "").split(":")[0].replace("tamed","Tame").replace("bred","Breed").replace("killed","Kill").toLowerCase()+" "+required.replace("statistic:", "").split(":")[2]+" "+required.replace("statistic:", "").split(":")[1]+"s");
							
						}}else {
							if(required.contains("quest:")) {
								lore.add(ChatColor.GRAY+"Complete "+ChatColor.translateAlternateColorCodes('&', required.replace("quest:", "").replace("z", "")));
							}else {
					lore.add(ChatColor.GRAY+required.split(":")[1]+" "+required.split(":")[0].replace("_", " "));}
					}
					}
				}
		lore.add(" ");
		lore.add(ChatColor.AQUA+"Rewards:");
		for(String reward:config.getStringList("Quests."+i+".RewardItems")) {
				if(reward.contains("none")){
				lore.add(ChatColor.GRAY+ChatColor.translateAlternateColorCodes('&',reward.replace("none", "")));
				}else {
					if(reward.contains("hidden")) {
					}else {
			lore.add(ChatColor.GRAY+reward.split(":")[1]+" "+reward.split(":")[0].replace("_", " "));
					}
			}
		}
		questItemMeta.setLore(lore);
		questItem.setItemMeta(questItemMeta);
		inv.addItem(new ItemStack[] { questItem });
		}
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, inv, " ", " ");
		
		}
	public static void rewardQuest(Player player, int i) {
		Bukkit.broadcastMessage(Main.server+player.getName()+" completed the "+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayName")).replace("z","")+ChatColor.GRAY+" quest!!! ");
		player.sendMessage(Main.server+"You completed the "+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayName")).replace("z","")+ChatColor.GRAY+" quest.");
		List<String> Quests =playerconfig.getStringList("IslandOwnerSettings."+player.getName()+".CompletedQuests");

			Quests.add(config.getString("Quests."+i+".DisplayName"));
			playerconfig.set("IslandOwnerSettings."+player.getName()+".CompletedQuests", Quests);
			Main.getPlugin().saveConfig();
			Main.saveQuestsValues();
		
		for(int reward = 0; reward<config.getStringList("Quests."+i+".RewardItems").size();reward++) {
			
			
			String ItemRewards=config.getStringList("Quests."+i+".RewardItems").get(reward).replace("hidden", "").replace(" ", "");
			if(ItemRewards.contains("none")) {}else {
			ItemStack rewards = new ItemStack(Material.getMaterial(ItemRewards.split(":")[0].toUpperCase()),Integer.parseInt(ItemRewards.split(":")[1]));
			player.getInventory().addItem(rewards);
			}
			if(reward ==config.getStringList("Quests."+i+".RewardItems").size()-1&&!config.getStringList("Quests."+i+".RewardCommand").contains("none")) {
				for(String Command:config.getStringList("Quests."+i+".RewardCommand")) {
					if(!Command.equalsIgnoreCase("none")) {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),Command.replace("%player%", player.getName()));
					}
			
				}}
		
		
		}
		
	}
	public static void removeQuest(Player player, int i) {
		Inventory inv=player.getInventory();
		List<ItemStack> stacksToRemove = new ArrayList<ItemStack>();
		int stacksUsed = config.getStringList("Quests."+i+".ItemsRequired").size();
		int passedStacks = 0;
		int times = config.getStringList("Quests."+i+".ItemsRequired").size();
		int loops = 0;
		if(config.getStringList("Quests."+i+".ItemsRequired").contains("none")) {
			rewardQuest(player, i);
			return;
		}
		for(String Item:config.getStringList("Quests."+i+".ItemsRequired")) {
			loops++;
			if(Item.contains("quest:")) {
				if(playerconfig.getList("IslandOwnerSettings."+player.getName()+".CompletedQuests").indexOf(Item.replace("quest:", "")) > -1) {
					 passedStacks++;	
					 
				}
			}
			if(!Item.contains("statistic")) {
			for (ItemStack stack : inv.getContents()) {
			    if (stack!=null&&stack.getType() == Material.getMaterial(Item.split(":")[0].toUpperCase()) && stack.getAmount() >= Integer.parseInt(Item.split(":")[1])) {
			        stacksToRemove.add(stack);
			        passedStacks++;
			        break;
			    	}
			}}
				if(Item.split(":")[1].equalsIgnoreCase("tamed")) {
					if(Integer.parseInt(Item.split(":")[3])<=IslandStatistics.PlayerMobTames(player, Item.split(":")[2])){
						passedStacks++;
					}
				}
				if(Item.split(":")[1].equalsIgnoreCase("bred")) {
					if(Integer.parseInt(Item.split(":")[3])<=IslandStatistics.PlayerMobBreeds(player, Item.split(":")[2])){
						passedStacks++;
					}
				}
				if(Item.split(":")[1].equalsIgnoreCase("killed")) {
					if(Integer.parseInt(Item.split(":")[3])<=IslandStatistics.PlayerMobKills(player, Item.split(":")[2])){
						passedStacks++;
					}
				}
				if(Item.split(":")[1].equalsIgnoreCase("broken")) {
					if(Integer.parseInt(Item.split(":")[2])<=IslandStatistics.PlayerBlocksBroken(player)){
						passedStacks++;
					}
				}
				if(Item.split(":")[1].equalsIgnoreCase("placed")) {
					if(Integer.parseInt(Item.split(":")[2])<=IslandStatistics.PlayerBlocksPlaced(player)){
						passedStacks++;
					}
				
			}
			
			if (passedStacks == stacksUsed) {
				rewardQuest(player, i);
				for(int l = 0;l<stacksToRemove.size();l++) { 
					stacksToRemove.get(l).setAmount(stacksToRemove.get(l).getAmount()-Integer.parseInt(config.getStringList("Quests."+i+".ItemsRequired").get(l).split(":")[1]));
				}
			    return;
			}
			if (passedStacks < stacksUsed && times <= loops) {
				for(String Items:config.getStringList("Quests."+i+".ItemsRequired")) {
					if(Items.contains("statistic")) {
						player.sendMessage(Main.server+"You are missing some "+ChatColor.RED+"statistics");
					}else {
						if(Items.contains("quest")) {
							player.sendMessage(Main.server+"You are missing a "+ChatColor.RED+"quest");
						}else {
				player.sendMessage(Main.server+"You Are Missing "+ChatColor.RED+Items.split(":")[1]+ChatColor.GRAY+" "+Items.split(":")[0].toLowerCase().replace("_", " "));}
				}}
				}
		}
	}
	public static void completeQuest(Player player, ItemStack clicked,Inventory inv) {
        ItemStack filler =Main.createDisplay(Material.BLACK_STAINED_GLASS_PANE,player.getInventory(),-2, " ","");
        ItemStack backButton =Main.createDisplay(Material.RED_STAINED_GLASS, null, -2, ChatColor.WHITE+"Back", "");
    	
		if(clicked.isSimilar(filler)) {
			
			return;
			
		}
		if(clicked.isSimilar(backButton)) {
			if(Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")||Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
				player.openInventory(Main.EasySkyGui);
				return;
				}else {
				Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
				return;
				}
		}
		for(int i = 0;config.contains("Quests."+i);i++) {
			Material QuestMaterial = Material.getMaterial(config.getString("Quests."+i+".DisplayMaterial").toUpperCase());
			if(Material.getMaterial(config.getString("Quests."+i+".DisplayMaterial").toUpperCase())==null) {
				QuestMaterial = Material.STONE;
			}
			ItemStack questItem =Main.createDisplay(QuestMaterial, inv, -2, ChatColor.WHITE+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayName").replace("z", "")),ChatColor.WHITE+ChatColor.translateAlternateColorCodes('&',config.getString("Quests."+i+".DisplayLore").replace("z", "")));
			ItemMeta questItemMeta =questItem.getItemMeta();
			List<String> lore = questItemMeta.getLore();
			lore.add(" ");
			lore.add(ChatColor.AQUA+"Requirements:");
			for(String required:config.getStringList("Quests."+i+".ItemsRequired")) {
				if(required.contains("none")){
					lore.add(ChatColor.GRAY+ChatColor.translateAlternateColorCodes('&',required.replace("none", "")));
					}else {
						if(required.contains("statistic")) {
							if(required.contains("broken")||required.contains("placed")) {
								
								lore.add(ChatColor.GRAY+required.replace("statistic:", "").split(":")[0].replace("broken","Break").replace("placed","Place").toLowerCase()+" "+required.replace("statistic:", "").split(":")[1]+" blocks!");
								
							}else {
								lore.add(ChatColor.GRAY+required.replace("statistic:", "").split(":")[0].replace("tamed","Tame").replace("bred","Breed").replace("killed","Kill").toLowerCase()+" "+required.replace("statistic:", "").split(":")[2]+" "+required.replace("statistic:", "").split(":")[1]+"s");
								
							}}else {
								if(required.contains("quest")) {
								lore.add(ChatColor.GRAY+"Complete "+ChatColor.translateAlternateColorCodes('&', required.replace("quest:", "").replace("z", "")));
							}else {
					lore.add(ChatColor.GRAY+required.split(":")[1]+" "+required.split(":")[0].replace("_", " "));}
						}
						}
					}
			lore.add(" ");
			lore.add(ChatColor.AQUA+"Rewards:");
			for(String reward:config.getStringList("Quests."+i+".RewardItems")) {
					if(reward.contains("none")){
					lore.add(ChatColor.GRAY+ChatColor.translateAlternateColorCodes('&',reward.replace("none", "")));
					}else {
						if(reward.contains("hidden")) {
						}else {
				lore.add(ChatColor.GRAY+reward.split(":")[1]+" "+reward.split(":")[0].replace("_", " "));
						}
				}
			}
			questItemMeta.setLore(lore);
			questItem.setItemMeta(questItemMeta);
			if(player.getInventory().firstEmpty()>-1) {
		if(clicked.equals(questItem)){
			if(!playerconfig.getStringList("IslandOwnerSettings."+player.getName()+".CompletedQuests").contains(config.getString("Quests."+i+".DisplayName"))) {

				removeQuest(player, i);
			}else {
				if(config.getBoolean("Quests."+i+".Redoable")==true) {
					removeQuest(player, i);
				}else {
				player.sendMessage(Main.server+"You have already completed this quest!");
				return;	
				}
			}
		
		}else {
			
		}
			}else {
				player.sendMessage(Main.server+"Full inventory!");
			}
		}
			}
		
	
		
	}


