package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Rewards {
public static void openRewards(Player player) {
	Inventory EasySkyCustomRecipes = Bukkit.createInventory(null,45,ChatColor.GRAY+"Codes");
	createKeyPad(EasySkyCustomRecipes, false, "X X X X");
	player.openInventory(EasySkyCustomRecipes);
}
public static void createKeyPad(Inventory inv,Boolean correct,String code) {
	if(inv.getSize()>=45) {
		if(correct) {
		Main.borderDisplay(Material.LIME_STAINED_GLASS_PANE, inv, " ", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 13," ", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 22," ", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 31," ", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS, inv, 24, ChatColor.GREEN+""+ChatColor.BOLD+code, "");
		}else {
		Main.borderDisplay(Material.RED_STAINED_GLASS_PANE, inv, " ", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 13," ", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 22," ", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 31," ", "");
		Main.createDisplay(Material.RED_STAINED_GLASS, inv, 24, ChatColor.RED+""+ChatColor.BOLD+code, "");
		}
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 10, ChatColor.WHITE+""+ChatColor.BOLD+"1", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 11, ChatColor.WHITE+""+ChatColor.BOLD+"2", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 12, ChatColor.WHITE+""+ChatColor.BOLD+"3", "");
		
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 19, ChatColor.WHITE+""+ChatColor.BOLD+"4", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 20, ChatColor.WHITE+""+ChatColor.BOLD+"5", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 21, ChatColor.WHITE+""+ChatColor.BOLD+"6", "");
		
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 28, ChatColor.WHITE+""+ChatColor.BOLD+"7", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 29, ChatColor.WHITE+""+ChatColor.BOLD+"8", "");
		Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, inv, 30, ChatColor.WHITE+""+ChatColor.BOLD+"9", "");
		
	}
	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, inv, " ", "");
	}
public static void rewardEvent(InventoryClickEvent event) {
	event.setCancelled(true);
	Player player = (Player) event.getWhoClicked();
	ItemStack clicked = event.getCurrentItem();
	String code = event.getClickedInventory().getItem(24).getItemMeta().getDisplayName();
	if(clicked.getType()!=Material.LIGHT_BLUE_STAINED_GLASS_PANE) {
		return;
	}
	if(code.contains("X")) {
		code =ChatColor.stripColor(code).replaceFirst("X",ChatColor.stripColor(clicked.getItemMeta().getDisplayName()));
		Main.createDisplay(Material.RED_STAINED_GLASS, event.getClickedInventory(), 24, ChatColor.RED+""+ChatColor.BOLD+code, "");
	}
		if(code.contains("X")) {
			return;
	}else {
		code =code.replace(" ", "");
		for(int i =0;Main.islandRewardsConfig.contains("Rewards."+i);i++) {
			if(ChatColor.stripColor(code).equalsIgnoreCase(Main.islandRewardsConfig.getString("Rewards."+i+".Code"))) {
				if(!Main.islandRewardsConfig.getStringList("Rewards."+i+".Users").contains(player.getName())&&Main.islandRewardsConfig.getStringList("Rewards."+i+".Users").size()<Main.islandRewardsConfig.getInt("Rewards."+i+".MaxUsers")&&Main.islandRewardsConfig.getBoolean("Rewards."+i+".Enabled")) {
				for(int reward = 0; reward<Main.islandRewardsConfig.getStringList("Rewards."+i+".Items").size();reward++) {
					
					
					String ItemRewards=Main.islandRewardsConfig.getStringList("Rewards."+i+".Items").get(reward);
					if(ItemRewards.contains("none")) {}else {
					ItemStack rewards = new ItemStack(Material.getMaterial(ItemRewards.split(":")[0].toUpperCase()),Integer.parseInt(ItemRewards.split(":")[1]));
					player.getInventory().addItem(rewards);
					}
				}
				for(String Command:Main.islandRewardsConfig.getStringList("Rewards."+i+".Commands")) {
					if(!Command.equalsIgnoreCase("none")) {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),Command.replace("%player%", player.getName()));
					}
			
				}
				List<String> users = Main.islandRewardsConfig.getStringList("Rewards."+i+".Users");
				users.add(player.getName());
				Main.islandRewardsConfig.set("Rewards."+i+".Users", users);
				Main.saveRewards();
				player.sendMessage(Main.server+ChatColor.translateAlternateColorCodes('&', Main.islandRewardsConfig.getString("Rewards."+i+".Message")));
				player.closeInventory();
				return;
				}else {
				player.closeInventory();
				player.sendMessage(Main.server+ChatColor.RED+"This code has already been used!");
				return;
				
			}
			}
		}
		player.closeInventory();
		player.sendMessage(Main.server+ChatColor.RED+"This code is invalid!");
		
	}
	
}
}
