package me.bukkit.hidden1nin.easySkies;



import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public abstract class islandBorder{
	public static FileConfiguration config = Main.getPlugin().getConfig();
	
	public static void onMovement(PlayerMoveEvent e) {
		
		Player p =e.getPlayer();
		if(IslandInfo.getIsland(e.getFrom()).equals("none")||config.contains("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size")==false) {
			return;
		}
		if(p.getLocation().getBlockX()<0&&p.getLocation().getBlockZ()<0||!p.getLocation().getWorld().getName().equalsIgnoreCase(config.getString("loc.world"))) {
			return;
		}
		if(!firstEnable.PlayerHomes.containsKey(IslandInfo.getIsland(e.getFrom()))) {
			return;
		}
	if(!IslandInfo.getIsland(e.getFrom()).equals("none")) {
	if(e.getTo().getBlockX()+1.5>firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size")) {
		
		Location Nloc =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		Location Nloc2 =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		
		drawLines(Nloc,Nloc2,1);
		e.setCancelled(true);
	}
	if(e.getTo().getBlockX()-1.5<firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size")) {
			
		Location Nloc =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		Location Nloc2 =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		
		drawLines(Nloc,Nloc2,1);
		e.setCancelled(true);
		
	}
	if(e.getTo().getBlockZ()+1.5>firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size")) {
		
		Location Nloc =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		Location Nloc2 =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		
		drawLines(Nloc,Nloc2,1);
		e.setCancelled(true);
		
	}
	if(e.getTo().getBlockZ()-1.5<firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size")) {
		
		Location Nloc =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		Location Nloc2 =new Location(e.getTo().getWorld(),firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockX()+config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"),e.getTo().getY()+1,firstEnable.PlayerHomes.get(IslandInfo.getIsland(e.getFrom())).getBlockZ()-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(e.getFrom())+".Size"));
		
		drawLines(Nloc,Nloc2,1);
		e.setCancelled(true);
		
	}
	
	
	}}
	
	public static void drawLines(Location point1, Location point2, double space) {
	    World world = point1.getWorld();
	    if(point2.getWorld().equals(world)) {
	    //Validate.isTrue(point2.getWorld().equals(world), "Lines cannot be in different worlds!");
	    double distance = point1.distance(point2);
	    Vector p1 = point1.toVector();
	    Vector p2 = point2.toVector();
	    Vector vector = p2.clone().subtract(p1).normalize().multiply(space);
	    double length = 0;
	    for (; length < distance; p1.add(vector)) {
	        world.spawnParticle(Particle.REDSTONE,new Location(world,p1.getX(), p1.getY(), p1.getZ()), 1,new Particle.DustOptions(Color.RED,1));
	        length += space;
	    }
	}
	}
}
