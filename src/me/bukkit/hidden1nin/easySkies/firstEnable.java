package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;


public class firstEnable {
	
	
	
public static FileConfiguration config = Main.getPlugin().getConfig();
public static int IslandCountX = config.getInt("IslandCountX");
public static int IslandCountZ = config.getInt("IslandCountZ");
public static List<String> IslandOwners =new ArrayList<String>();
public static List<String> IslandEditors=new ArrayList<String>();
public static List<ItemStack> AdminItems=new ArrayList<ItemStack>();
public static boolean wait = false;
public static boolean tp = false;
static World w = Bukkit.getServer().getWorld(config.getString("loc.world"));
public static World SkyBlockWorld = w;
static double x = config.getDouble("loc.x");
static double y = config.getDouble("loc.y");
static double z = config.getDouble("loc.z");
static double x2 = config.getDouble("loc.x2");
static double y2 = config.getDouble("loc.y2");
static double z2 = config.getDouble("loc.z2");
static double xs = config.getDouble("loc.xspawn");
static double ys = config.getDouble("loc.yspawn");
static double zs = config.getDouble("loc.zspawn");
static double xj = config.getDouble("loc.xjail");
static double yj = config.getDouble("loc.yjail");
static double zj = config.getDouble("loc.zjail");
static double xb = config.getDouble("loc.xboss");
static double yb = config.getDouble("loc.yboss");
static double zb = config.getDouble("loc.zboss");
static double xa = config.getDouble("loc.xafk");
static double ya = config.getDouble("loc.yafk");
static double za = config.getDouble("loc.zafk");
public static Location pos1 = new Location(w, x, y, z);
public static Location pos2 = new Location(w, x2, y2, z2);
public static Location Spawn = new Location(w, xs, ys, zs);
public static Location Jail = new Location(w, xj, yj, zj);
public static Location Boss = new Location(w,xb,yb,zb);
public static Location Afk = new Location(w,xa,ya,za);
public static long startTime = System.currentTimeMillis();
public static HashMap<String, String> PlayerOpenShop = new HashMap<String,String>();
public static HashMap<String, Location> PlayerHomes = new HashMap<String, Location>();{
	
	for(String rawData : config.getStringList("PlayerHomes")) {

        String[] raw = rawData.split(":");
        PlayerHomes.put(raw[0].toString(), new Location(Bukkit.getWorld(raw[1]),Double.valueOf(raw[2]),Double.valueOf(raw[3]),Double.valueOf(raw[4])));

    }
}
public static HashMap<String, Long> PlayerMiners = new HashMap<String, Long>();{
	
	for(String rawData : config.getStringList("PlayerMiners")) {

        String[] raw = rawData.split(":");
        PlayerMiners.put(raw[0].toString(), Long.valueOf(raw[1]));

    }
}
public static void addConfig() {
	Main.playerShopConfig.options().copyDefaults(true);
	config.options().copyDefaults(true);
	World skyWorld;
	skyWorld = Bukkit.getServer().getWorld(config.getString("loc.world"));
	if (skyWorld == null) { // If we don't have a world generated yet
       
            	Bukkit.getServer().createWorld(WorldCreator.name("SkyBlock").type(WorldType.FLAT).generatorSettings("3;minecraft:air;2").generateStructures(false));
            	
            	Location l = new Location(Bukkit.getServer().getWorld("SkyBlock"),0,10,0);
            	l.getBlock().setType(Material.BEDROCK);
       
	}
	if (skyWorld == null) {
	skyWorld = Bukkit.getServer().getWorld("SkyBlock");
	}
	if (SkyBlockWorld == null) {
		World w = Bukkit.getServer().getWorld(config.getString("loc.world"));
		SkyBlockWorld = w;
		pos1 = new Location(w, x, y, z);
		pos2 = new Location(w, x2, y2, z2);
		Spawn = new Location(w, xs, ys, zs);
		Jail = new Location(w, xj, yj, zj);
		Boss = new Location(w,xb,yb,zb);
		Afk = new Location(w,xa,ya,za);
	}else {
		Bukkit.getServer().createWorld(new WorldCreator(config.getString("loc.world")));
		World w = Bukkit.getServer().getWorld(config.getString("loc.world"));
		SkyBlockWorld = w;
		pos1 = new Location(w, x, y, z);
		pos2 = new Location(w, x2, y2, z2);
		Spawn = new Location(w, xs, ys, zs);
		Jail = new Location(w, xj, yj, zj);
		Boss = new Location(w,xb,yb,zb);
		Afk = new Location(w,xa,ya,za);
	}
	String SkyBlockWorld = config.getString("loc.world");
	
	
	
	
    Main.getPlugin().saveConfig(); 
    
	if(!config.contains("IslandCountX")) {
		config.addDefault("IslandCountX", 0); 
		Main.getPlugin().saveConfig();  
		}	
	if(!config.contains("IslandOwnerSettings.DefaultMinerSpeed")) {
		config.addDefault("IslandOwnerSettings.DefaultMinerSpeed",10); 
		Main.getPlugin().saveConfig();  
		}	
	if(!config.contains("IslandCountZ")) {
		config.addDefault("IslandCountZ", 0); 
		Main.getPlugin().saveConfig();  
		}	
	//if (Bukkit.getWorld(SkyBlockWorld) != null || Bukkit.getWorld(SkyBlockWorld) == null) {
		if(!config.contains("loc.world")) {
			config.addDefault("loc.world", Bukkit.getWorld(SkyBlockWorld).getName()); 
			Main.getPlugin().saveConfig();  
			}
	if(!config.contains("loc.x")) {
			config.addDefault("loc.x", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
			Main.getPlugin().saveConfig();  
			}	
	if(!config.contains("loc.y")) {
		config.addDefault("loc.y", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockY()); 
		Main.getPlugin().saveConfig();  
		}
	if(!config.contains("loc.z")) {
		config.addDefault("loc.z", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockZ()); 
		Main.getPlugin().saveConfig();  
		}
	if(!config.contains("loc.x2")) {
		config.addDefault("loc.x2", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
		Main.getPlugin().saveConfig();  
		}	
if(!config.contains("loc.y2")) {
	config.addDefault("loc.y2", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockY()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.z2")) {
	config.addDefault("loc.z2", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockZ()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.xspawn")) {
	config.addDefault("loc.xspawn", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}	
if(!config.contains("loc.yspawn")) {
config.addDefault("loc.yspawn", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockY()); 
Main.getPlugin().saveConfig();  
}
if(!config.contains("loc.zspawn")) {
config.addDefault("loc.zspawn", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockZ()); 
Main.getPlugin().saveConfig();  
}
if(!config.contains("loc.xjail")) {
	config.addDefault("loc.xjail", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}	
if(!config.contains("loc.yjail")) {
config.addDefault("loc.yjail", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockY()); 
Main.getPlugin().saveConfig();  
}
if(!config.contains("loc.zjail")) {
config.addDefault("loc.zjail", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockZ()); 
Main.getPlugin().saveConfig();  
}
if(!config.contains("loc.xboss")) {
	config.addDefault("loc.xboss", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.yboss")) {
	config.addDefault("loc.yboss", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.zboss")) {
	config.addDefault("loc.zboss", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.xafk")) {
	config.addDefault("loc.xafk", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.yafk")) {
	config.addDefault("loc.yafk", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("loc.zafk")) {
	config.addDefault("loc.zafk", new Location(Bukkit.getWorld(SkyBlockWorld),0,0,0).getBlockX()); 
	Main.getPlugin().saveConfig();  
	}
		//getServer().getWorlds().contains(SkyBlockWorld);
		//World Skyblock = Bukkit.getWorld(SkyBlockWorld);
	
	//}else {
		//Bukkit.broadcastMessage(server+ChatColor.RED+"Skyblock Needs a Valid world to make islands in!");
	//}
	
if(!config.contains("IslandOwners")) {
	config.addDefault("IslandOwners", new ArrayList<String>()); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("IslandOwnerSettings.DefaultIslandSize")) {
	config.addDefault("IslandOwnerSettings.DefaultIslandSize", 25); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("IslandOwnerSettings.DefaultUnlockedGen")) {
	config.addDefault("IslandOwnerSettings.DefaultUnlockedGen", new ArrayList<String>());
	List<String> DefaultUnlockedGen = new ArrayList<String>();
	DefaultUnlockedGen.add("0");
	config.set("IslandOwnerSettings.DefaultUnlockedGen",DefaultUnlockedGen);
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("IslandOwnerSettings.DefaultMembersAllowed")) {
	config.addDefault("IslandOwnerSettings.DefaultMembersAllowed", 2); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("IslandOwnerSettings.DefaultResetTokens")) {
	config.addDefault("IslandOwnerSettings.DefaultResetTokens",5); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("PlayerHomes")) {
	config.addDefault("PlayerHomes",new ArrayList<String>() ); 
	Main.getPlugin().saveConfig();  
}
if(!config.contains("PlayerMiners")) {
	config.addDefault("PlayerMiners",new ArrayList<String>() ); 
	Main.getPlugin().saveConfig();  
}
////////////////////////generator/////////////////
if(!Main.islandGeneratorsConfig.contains("GeneratorChances")) {
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"0", new ArrayList<String>()); 
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"0"+".DisplayName", "Ores");
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"0"+".DisplayMaterial","Iron_ore");
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"0"+".DisplayLore","Essential ores");
	List<String> hashmapData = new ArrayList<String>();
    hashmapData.add("coal_ore:20");
    hashmapData.add("IRON_ORE:20");
    hashmapData.add("Gold_ore:20");
    hashmapData.add("DIAMOND_ORE:10");
    hashmapData.add("Lapis_Ore:10");
    hashmapData.add("Redstone_Ore:10");
    hashmapData.add("Emerald_Ore:10");
    hashmapData.add("End_Stone:2");
    Main.islandGeneratorsConfig.set("GeneratorChances.0.Blocks", hashmapData);
	Main.saveGenerators(); 
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"1", new ArrayList<String>()); 
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"1"+".DisplayName", "Stone");
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"1"+".DisplayMaterial","Stone");
	Main.islandGeneratorsConfig.addDefault("GeneratorChances."+"1"+".DisplayLore","Hard and rocky");
	List<String> hashmapData1 = new ArrayList<String>();
	 hashmapData1.add("Stone:100");
	 Main.islandGeneratorsConfig.set("GeneratorChances.1.Blocks", hashmapData1);
	Main.saveGenerators();  
	}
///////////////////////////quests/////////////////////
if(!Main.islandQuestsConfig.contains("Quests")) {
	Main.islandQuestsConfig.addDefault("Quests."+"0", new ArrayList<String>()); 
	List<String> QuestRewards = new ArrayList<String>();
	List<String> QuestRequirements = new ArrayList<String>();
	List<String> QuestCommands = new ArrayList<String>();
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".DisplayName", "Miner");
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".DisplayMaterial","Cobblestone");
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".DisplayLore","Mine 64 cobblestone and 8 Coal to complete");
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".Redoable",true);
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".ItemsRequired",new ArrayList<String>());
	QuestRequirements.add("Coal:8");
	QuestRequirements.add("CobbleStone:64");
	Main.islandQuestsConfig.set("Quests."+"0"+".ItemsRequired", QuestRequirements);
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".RewardItems",new ArrayList<String>());
	QuestRewards.add("WHEAT_SEEDS:4");
	QuestRewards.add("CARROT:4");
	QuestRewards.add("SUGAR_CANE:4");
	Main.islandQuestsConfig.set("Quests."+"0"+".RewardItems", QuestRewards);
	QuestCommands.add("say hi");
	Main.islandQuestsConfig.addDefault("Quests."+"0"+".RewardCommand",QuestCommands);
	Main.saveQuestsValues(); 
	}
//////////////////shops/////////////////
if(!Main.shopConfig.contains("Default."+"Background")) {
	Main.shopConfig.addDefault("Default."+"Background","Black_Stained_Glass_Pane");
	Main.saveAdminShops();
}
if(!Main.shopConfig.contains("Default."+"Border")) {
	Main.shopConfig.addDefault("Default."+"Border","Red_Stained_Glass_Pane");
	Main.saveAdminShops();
}
 ArrayList<String> items = new ArrayList<String>();
 items.add("dirt:1000:100:22");
 items.add("sand:1000:100:23");
 items.add("cobblestone:1000:100:21");
if(!Main.shopConfig.contains("Items")) {
	Main.shopConfig.addDefault("Items",items);
	Main.saveAdminShops();
}
if(!Main.shopConfig.contains("DefaultShop")) {
	Main.shopConfig.addDefault("DefaultShop",items);
	Main.saveAdminShops();
}
if(!Main.shopConfig.contains("Foods")) {
	Main.shopConfig.addDefault("Foods",items);
	Main.saveAdminShops();
}
////////////////playerShopConfig/////////////////
if(!Main.playerShopConfig.contains("Default."+"ShopSize")) {
	Main.playerShopConfig.addDefault("Default."+"ShopSize", 1);
	Main.savePlayerShops();
}
if(!Main.playerShopConfig.contains("Default."+"Balance")) {
	Main.playerShopConfig.addDefault("Default."+"Balance", 100);
	Main.savePlayerShops();
}
if(!Main.playerShopConfig.contains("Default."+"UpgradeSpaceCost")) {
	Main.playerShopConfig.addDefault("Default."+"UpgradeSpaceCost", 1000);
	Main.savePlayerShops();
}
if(!Main.playerShopConfig.contains("Default."+"ShowOfflineShops")) {
	Main.playerShopConfig.addDefault("Default."+"ShowOfflineShops", false);
	Main.savePlayerShops();
}

/////////////////////////BlockValues//////////////////////////

if(!Main.islandBlockValueConfig.contains("Blocks.Values")) {
	Main.islandBlockValueConfig.addDefault("Blocks.Values",new ArrayList<String>());
	List<String> Value = new ArrayList<String>();
	Value.add("Diamond_Block:10");
	Value.add("Gold_Block:5");
	Value.add("Dirt:0");
	Value.add("grass_block:0");
	Main.islandBlockValueConfig.set("Blocks.Values", Value);
	Main.saveBlockValues(); 
}
if(!Main.islandBlockValueConfig.contains("Blocks.DefaultValues")) {
	Main.islandBlockValueConfig.addDefault("Blocks.DefaultValues", 1);
	Main.saveBlockValues(); 
}
if(!Main.islandBlockValueConfig.contains("Blocks.StartingLevel")) {
	Main.islandBlockValueConfig.addDefault("Blocks.StartingLevel", 0);
	Main.saveBlockValues(); 
}
if(!Main.islandBlockValueConfig.contains("Island.Top.Amount")) {
	Main.islandBlockValueConfig.addDefault("Island.Top.Amount", 9);
	Main.saveBlockValues(); 
}
if(!Main.islandBlockValueConfig.contains("Island.Top.Players")) {
	Main.islandBlockValueConfig.addDefault("Island.Top.Players", new ArrayList<String>());
	List<String> Value = new ArrayList<String>();
	Value.add("None1:1");
	Value.add("None2:2");
	Value.add("None3:3");
	Value.add("None4:4");
	Value.add("None5:5");
	Value.add("None6:6");
	Value.add("None7:7");
	Value.add("None8:8");
	Value.add("None9:9");
	Value.add("None10:10");
	Main.islandBlockValueConfig.set("Island.Top.Players", Value);
	Main.saveBlockValues(); 
}
///////////////////////////////holograms//////////
if(!Main.islandHoloConfig.contains("Npc.List")) {
Main.islandHoloConfig.addDefault("Npc.List",  new ArrayList<String>());
Main.saveHolo();
}
Main.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	  public void run() {
		  Holograms.reload();
	  }
	}, 10L);
//////////////////////////////mobs//////////////////
if(!Main.islandMobsConfig.contains("Npc.List")) {
Main.islandMobsConfig.addDefault("Npc.List",  new ArrayList<String>());
Main.saveMobs();
}
Main.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
	  public void run() {
		  IslandMobs.reload();
	  }
	}, 10L);
//////////////////////////////Command Config/////////////////
if(!Main.islandCommandsConfig.contains("Commands.Shop")) {
	Main.islandCommandsConfig.addDefault("Commands.Shop", "shop");
	Main.saveCommandValues();
}

if(!Main.islandCommandsConfig.contains("Commands.Home")) {
	Main.islandCommandsConfig.addDefault("Commands.Home", "is home");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.Spawn")) {
	Main.islandCommandsConfig.addDefault("Commands.Spawn", "is spawn");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.Settings")) {
	Main.islandCommandsConfig.addDefault("Commands.Settings", "is settings");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.Quests")) {
	Main.islandCommandsConfig.addDefault("Commands.Quests", "is quests");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.MenuOpen")) {
	Main.islandCommandsConfig.addDefault("Commands.MenuOpen", "island");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.Team")) {
	Main.islandCommandsConfig.addDefault("Commands.Team", "island members");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.PlayerShops")) {
	Main.islandCommandsConfig.addDefault("Commands.PlayerShops", "ah");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.AdminShops")) {
	Main.islandCommandsConfig.addDefault("Commands.AdminShops", true);
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.Upgrades")) {
	Main.islandCommandsConfig.addDefault("Commands.Upgrades", "island upgrades");
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Commands.VoidFall")) {
	Main.islandCommandsConfig.addDefault("Commands.VoidFall", "loop");
	Main.saveCommandValues();
}
//////////////////////////Settings////////////////////////////
if(!Main.islandCommandsConfig.contains("Settings.PlayerShops.Enabled")) {
	Main.islandCommandsConfig.addDefault("Settings.PlayerShops.Enabled", true);
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Settings.SpawnItemWipe.Enabled")) {
	Main.islandCommandsConfig.addDefault("Settings.SpawnItemWipe.Enabled", false);
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Settings.SpawnItemWipe.Radius")) {
	Main.islandCommandsConfig.addDefault("Settings.SpawnItemWipe.Radius", 15);
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Settings.SpawnItemWipe.Time")) {
	Main.islandCommandsConfig.addDefault("Settings.SpawnItemWipe.Time", 60);
	Main.saveCommandValues();
}
if(!Main.islandCommandsConfig.contains("Settings.Discord.Message")) {
	Main.islandCommandsConfig.addDefault("Settings.Discord.Message", "&l&3Discord:jakdhasj");
	Main.saveCommandValues();
}
/////////////////////////////// upgrades////////////////////
if(!config.contains("Upgrades.IslandSize.Cost")) {
	config.addDefault("Upgrades.IslandSize.Cost", 1000); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.IslandSize.Size")) {
	config.addDefault("Upgrades.IslandSize.Size", 100); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.IslandSize.UpgradeLimit")) {
	config.addDefault("Upgrades.IslandSize.UpgradeLimit", 5); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MemberLimit.Cost")) {
	config.addDefault("Upgrades.MemberLimit.Cost", 1000); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MemberLimit.Amount")) {
	config.addDefault("Upgrades.MemberLimit.Amount", 1); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MemberLimit.UpgradeLimit")) {
	config.addDefault("Upgrades.MemberLimit.UpgradeLimit", 5); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MineSpeed.Cost")) {
	config.addDefault("Upgrades.MineSpeed.Cost", 1000); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MineSpeed.IncreaseAmount")) {
	config.addDefault("Upgrades.MineSpeed.IncreaseAmount", 1); 
	Main.getPlugin().saveConfig();  
	}
if(!config.contains("Upgrades.MineSpeed.UpgradeLimit")) {
	config.addDefault("Upgrades.MineSpeed.UpgradeLimit", 5); 
	Main.getPlugin().saveConfig();  
	}
////////////////////////////Mob Drops/////////////////
if(!Main.customMobDropsConfig.contains("Mobs.List")) {
	Main.customMobDropsConfig.addDefault("Mobs.List.MobName",new ArrayList<String>());
	Main.saveCustomMobDrops();
}
///////////////////////////////Rewards///////////////////////
if(!Main.islandRewardsConfig.contains("Rewards.Enabled")) {
	Main.islandRewardsConfig.addDefault("Rewards.Enabled", true); 
	Main.saveRewards(); 
}
if(!Main.islandRewardsConfig.contains("Rewards.0")) {
	List<String> RewardCommands = new ArrayList<String>();
	RewardCommands.add("eco give %player% 100");
	List<String> RewardItems = new ArrayList<String>();
	RewardItems.add("Dirt:64");
	Main.islandRewardsConfig.addDefault("Rewards.0.Enabled", true); 
	Main.islandRewardsConfig.addDefault("Rewards.0.Code", "1234"); 
	Main.islandRewardsConfig.addDefault("Rewards.0.MaxUsers", 5); 
	Main.islandRewardsConfig.addDefault("Rewards.0.Message", "You claimed 64 dirt and &a100$"); 
	Main.islandRewardsConfig.addDefault("Rewards.0.Commands", RewardCommands); 
	Main.islandRewardsConfig.addDefault("Rewards.0.Items", RewardItems); 
	Main.islandRewardsConfig.addDefault("Rewards.0.Users", new ArrayList<String>());  
	Main.saveRewards();  
	}
//////////////////////////////christmas///////////////
Main.islandHolidayConfig.options().copyDefaults(true);
if(!Main.islandHolidayConfig.contains("Christmas.Presents.OpenMessage")){
	Main.islandHolidayConfig.addDefault("Christmas.Presents.OpenMessage","&6You opened %amount% christmas presents"); 
	Main.saveHolidays();
}
if(!Main.islandHolidayConfig.contains("Christmas.Presents.Cooldown")){
	Main.islandHolidayConfig.addDefault("Christmas.Presents.Cooldown",1); 
	Main.saveHolidays();
}
if(!Main.islandHolidayConfig.contains("Christmas.Presents.Gifts")){
	List<String> Presents = new ArrayList<String>();
	Presents.add("Leather_boots:&eStockings:&dMade with love:1:20");
	Presents.add("Dirt:8:30");
	Presents.add("command:give %player% dirt 8:20");
	Presents.add("cake:&dCake:&cSweet and Tasty/line/&fSnowy 1:1:1");
	Presents.add("cooked_chicken:$6Turkey:&eA good snacc:5:100");
	Presents.add("diamond:&3Diamond:&bGlitters like snow:1:50");
	Main.islandHolidayConfig.addDefault("Christmas.Presents.Gifts",Presents); 
	Main.saveHolidays();
}
/////////////////Holiday Items//////
Main.islandHolidayConfig.options().copyDefaults(true);
if(!Main.islandHolidayConfig.contains("Items.ROTTEN_FLESH.OpenMessage")){
	Main.islandHolidayConfig.addDefault("Items.ROTTEN_FLESH.OpenMessage","&6You opened %amount% Loot Bags"); 
	Main.saveHolidays();
}
if(!Main.islandHolidayConfig.contains("Items.ROTTEN_FLESH.Cooldown")){
	Main.islandHolidayConfig.addDefault("Items.ROTTEN_FLESH.Cooldown",1); 
	Main.saveHolidays();
}
if(!Main.islandHolidayConfig.contains("Items.ROTTEN_FLESH.Loot")){
	List<String> Loot = new ArrayList<String>();
	Loot.add("Leather_boots:&eStockings:&dMade with love:1:20");
	Loot.add("Dirt:8:30");
	Loot.add("command:give %player% dirt 8:20");
	Loot.add("cake:&dCake:&cSweet and Tasty/line/&fSnowy 1:1:1");
	Loot.add("cooked_chicken:$6Turkey:&eA good snacc:5:100");
	Loot.add("diamond:&3Diamond:&bGlitters like snow:1:50");
	Main.islandHolidayConfig.addDefault("Items.ROTTEN_FLESH.Loot",Loot); 
	Main.saveHolidays();
}
///////////////ScoreBoard///////////////
if(!Main.scoreConfig.contains("Titles")) {
	List<String> Titles = new ArrayList<String>();
	Titles.add("&dNitronic");
	Titles.add("&5Nitronic");
	Main.scoreConfig.set("Titles",Titles);
	Main.saveScoreBoard();
}
if(!Main.scoreConfig.contains("UpdateTicks")) {
	Main.scoreConfig.set("UpdateTicks",20);
	Main.saveScoreBoard();
}
//////////////////////////////////Gui///////////////////
Main.customGuiConfig.options().copyDefaults(true);
if(!Main.customGuiConfig.contains("&3Help.Slots")){
	Main.customGuiConfig.addDefault("&3Help.Slots",54); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.Filler")){
	Main.customGuiConfig.addDefault("&3Help.Filler","Black_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.Border")){
	Main.customGuiConfig.addDefault("&3Help.Border","Red_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.Items")){
	List<String> Loot = new ArrayList<String>();
	Loot.add("stone:Super cool name:Super Rad description:23:/none/:give %player% stone 1:permission.easyskies.stone");
	Loot.add("stone_slab:Super cool name:Super Rad description:21:/none/:give %player% stone_slab 1:permission.easyskies.stone");
	Loot.add("oak_planks:Super cool name:Super Rad description:22:/none/:give %player% oak_planks 1:permission.easyskies.stone");

	Main.customGuiConfig.addDefault("&3Help.Items",Loot); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.Skulls")){
	List<String> Loot = new ArrayList<String>();
	Loot.add("hidden1nin:Super cool name:Super Rad description:20:/none/:give %player% stone 1:permission.easyskies.stone");
	
	Main.customGuiConfig.addDefault("&3Help.Skulls",Loot); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.Slots")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.Slots",54); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.Items")){
	List<String> boot = new ArrayList<String>();
	boot.add("stone:Super cool name:Super Rad description:23:/none/:give %player% stone 1:permission.easyskies.stone");
	boot.add("stone_slab:Super duper name:Super Rad description:21:/none/:give %player% stone_slab 1:permission.easyskies.stone");
	boot.add("spruce_planks:Super cool name:Super Rad description:22:/none/:give %player% oak_planks 1:permission.easyskies.stone");

	Main.customGuiConfig.addDefault("&3Help.sub.&c1.Items",boot); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.Filler")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.Filler","Black_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.Border")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.Border","Red_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.sub.&72.Slots")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.sub.&72.Slots",54); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.sub.&72.Items")){
	List<String> boot = new ArrayList<String>();
	boot.add("stone:Super cool name:Super Rad description:23:/none/:give %player% stone 1:permission.easyskies.stone");
	boot.add("stone_slab:Super duper name:Super Rad description:21:/none/:give %player% stone_slab 1:permission.easyskies.stone");
	boot.add("spruce_planks:Super cool name:Super Rad description:22:/none/:give %player% oak_planks 1:permission.easyskies.stone");

	Main.customGuiConfig.addDefault("&3Help.sub.&c1.sub.&72.Items",boot); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.sub.&72.Filler")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.sub.&72.Filler","Black_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
if(!Main.customGuiConfig.contains("&3Help.sub.&c1.sub.&72.Border")){
	Main.customGuiConfig.addDefault("&3Help.sub.&c1.sub.&72.Border","Red_Stained_glass_pane:&7:&l"); 
	Main.saveGuiConfig();
}
//////////////////////////////Items/////////////////////////
AdminItems.add(Main.createDisplay(Material.CHEST, null, -2,ChatColor.AQUA+"Void Collector", ChatColor.GRAY+"Place to collect void items!"));
AdminItems.add(Main.createDisplay(Material.DIAMOND_BOOTS,null, -2,ChatColor.BLUE+"Winged"+ChatColor.WHITE+" Boots",ChatColor.RED+"1400/1400"));
AdminItems.add(Main.createDisplay(Material.PAPER,null, -2,ChatColor.AQUA+"Enchantment slip",ChatColor.LIGHT_PURPLE+"Loved 1"));
AdminItems.add(Main.createDisplay(Material.COOKIE,null, -2,ChatColor.AQUA+"Cookie",ChatColor.LIGHT_PURPLE+"Wow you did something so stupid you/line/"+ChatColor.LIGHT_PURPLE+"got a cookie for it, good job../line/"+ChatColor.RED+"Slowness 10"));
AdminItems.add(Main.createDisplay(Material.CAKE,null, -2,ChatColor.AQUA+"Staff Cake",ChatColor.LIGHT_PURPLE+"Thank you for joining the staff team!"));
AdminItems.add(Main.createDisplay(Material.ALLIUM,null, -2,ChatColor.AQUA+"Flower",ChatColor.LIGHT_PURPLE+"A small token of appreciation"));
AdminItems.add(Main.createDisplay(Material.BLAZE_ROD,null, -2,ChatColor.GOLD+"Mighty Staff",ChatColor.LIGHT_PURPLE+"It's a good pun/line/"+ChatColor.GOLD+"ElectroConductor 9999"));
AdminItems.add(Main.createSkull("CruXXx", null, -2, ChatColor.GREEN+"Small Christmas Gift", ChatColor.WHITE+"Present 1"));
AdminItems.add(Main.createSkull("CruXXx", null, -2, ChatColor.GREEN+"Medium Christmas Gift", ChatColor.WHITE+"Present 5"));
AdminItems.add(Main.createSkull("CruXXx", null, -2, ChatColor.GREEN+"Large Christmas Gift", ChatColor.WHITE+"Present 10"));
AdminItems.add(Main.createDisplay(Material.IRON_DOOR,null, -2,ChatColor.AQUA+"Portable Jail",ChatColor.RED+"Click on players to jail them, or on mobs to capture them!"));
AdminItems.add(Main.createDisplay(Material.ELYTRA, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Winged Saddle",ChatColor.WHITE+"Can be used to fly:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GREEN+"15/10 Speed"));
AdminItems.add(Main.createDisplay(Material.FISHING_ROD, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Grappling Hook",ChatColor.GRAY+"Like batman, but not as cool./line/"+ChatColor.GRAY+"nothings as cool as batman.."+"/line/"+ChatColor.GOLD+"Soft-Landing 1"));
for(ItemStack item:firstEnable.AdminItems) {
	Main.EasySkyAdminItems.addItem(item);
}
Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, Main.EasySkyAdminItems, " ", " ");
Main.lineDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE,Main.EasySkyGui, 0, " ","");
Main.lineDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE,Main.EasySkyGui, 4, " ","");
Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE,Main.EasySkyGui, " ","");
	
Main.createDisplay(Material.RED_BED, Main.EasySkyGui, 9, ChatColor.AQUA+"Go back to "+"Island",ChatColor.GRAY+"This will take you home");
if(Main.islandCommandsConfig.getBoolean("Settings.PlayerShops.Enabled")) {
Main.createDisplay(Material.HOPPER, Main.EasySkyGui, 11, ChatColor.AQUA+"Open your "+"Island"+ChatColor.AQUA+" shop!",ChatColor.GRAY+"This will show your shops stock!");
}else {
Main.createDisplay(Material.HOPPER, Main.EasySkyGui, 11, ChatColor.AQUA+"Open Top Islands",ChatColor.GRAY+"This will show the top island list!");
}
Main.createDisplay(Material.OAK_SIGN, Main.EasySkyGui,12, ChatColor.GRAY+"Island"+ChatColor.GRAY+" Settings",ChatColor.GRAY+"Change your settings");
Main.createDisplay(Material.PAPER, Main.EasySkyGui,13,ChatColor.RED+"Quests", ChatColor.GRAY+"View your quests!");
Main.createDisplay(Material.CRAFTING_TABLE, Main.EasySkyGui,14,ChatColor.LIGHT_PURPLE+"Recipes",ChatColor.GRAY+"View Custom Recipes");
if(Main.islandCommandsConfig.getBoolean("Settings.PlayerShops.Enabled")) {
Main.createDisplay(Material.CHEST, Main.EasySkyGui, 15, ChatColor.AQUA+"Open public "+"Island"+ChatColor.AQUA+" shops!",ChatColor.GRAY+"This will show other players shops!");
}else {
Main.createDisplay(Material.CHEST, Main.EasySkyGui, 15, ChatColor.AQUA+"Open Auction "+"House",ChatColor.GRAY+"This will open the auction house!");
}
Main.createDisplay(Material.ENDER_PEARL, Main.EasySkyGui, 17, ChatColor.AQUA+"Go back to Skyblock"+" Spawn",ChatColor.GRAY+"This will take you to spawn");
Main.createDisplay(Material.GRASS_BLOCK, Main.EasySkyGui, 22, ChatColor.AQUA+"Island "+"Upgrades",ChatColor.GRAY+"This will open island skyblock");
Main.createDisplay(Material.PLAYER_HEAD, Main.EasySkyGui, 21, ChatColor.AQUA+"Island "+"Teammates",ChatColor.GRAY+"This will show island teammates");
Main.createDisplay(Material.DIAMOND_BLOCK, Main.EasySkyGui, 23, ChatColor.AQUA+"Island "+"Level",ChatColor.GRAY+"Calculates island level.");
Main.createDisplay(Material.CAKE, Main.EasySkyGui, 31, ChatColor.GOLD+"Shop",ChatColor.GRAY+"Open Shop.");

//////////////////////////////Initializers//////////////////
if(Main.islandCommandsConfig.getBoolean("Settings.SpawnItemWipe.Enabled")) {
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new SpawnItemRemover(){},1,Main.islandCommandsConfig.getInt("Settings.SpawnItemWipe.Time")*20);
}
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new AntiCheat(),1,300*20);
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new CustomBlocks(),1,20);
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new MobRiding(),1,5);
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new ItemCollector(),1,20);
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new BossEvents(),1,20);
Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(),new Pets(),10,5);
IslandInfo.updateIslandTopMenu();
for(String rawData : config.getStringList("PlayerHomes")) {

    String[] raw = rawData.split(":");
    PlayerHomes.put(raw[0].toString(), new Location(Bukkit.getWorld(raw[1]),Double.valueOf(raw[2]),Double.valueOf(raw[3]),Double.valueOf(raw[4])));

}
for(String rawData : config.getStringList("PlayerMiners")) {

    String[] raw = rawData.split(":");
    PlayerMiners.put(raw[0].toString(), Long.valueOf(raw[1]));

}

IslandOwners =config.getStringList("IslandOwners");




if (!skyVault.setupEconomy() ) {
    skyVault.log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", Main.getPlugin().getDescription().getName()));
    Main.getPlugin().getServer().getPluginManager().disablePlugin(Main.getPlugin());
    return;
}
skyVault.setupPermissions();




}

}
