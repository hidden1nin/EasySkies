package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;


public class Money {
	
	public static FileConfiguration config = Main.getPlugin().getConfig();
	@SuppressWarnings("deprecation")
	public static void displayMoney(Player p,String name) {
		if(hasBalance(name)) {
			p.sendMessage(Main.server+name+"'s balance is : "+ChatColor.GOLD+ skyVault.econ.getBalance(name)+"$");
		}else {
			p.sendMessage(Main.server+"This player doesn't have any money!");
		}
	}
	@SuppressWarnings("deprecation")
	public static void payMoney(Player p, String name, String amount) {
		if(hasBalance(name)) {
			if(!amount.contains(".")&&!amount.contains("-")) {
			if(skyVault.econ.getBalance(p.getName())-Integer.parseInt(amount)>=0){
			skyVault.econ.withdrawPlayer(p.getName(), Double.parseDouble(amount));//set("ShopOwnerData."+p.getName()+".Balance", newbal);
			skyVault.econ.depositPlayer(name, skyVault.econ.getBalance(p.getName())+Double.parseDouble(amount));
			p.sendMessage(Main.server+"You paid "+name+" "+ChatColor.GOLD+amount+ChatColor.GREEN+"$"+ ChatColor.GRAY);
			for(Player player:Bukkit.getOnlinePlayers()) {
				if(player.getName().equals(name)) {
					Bukkit.getPlayer(name).sendMessage(Main.server+"You recieved "+ChatColor.GOLD+amount+ChatColor.WHITE+"$ from "+p.getName());
					}
				if(player.getName().equals(p.getName())) {
					Bukkit.getPlayer(name).sendMessage(Main.server+"You sent "+ChatColor.GOLD+amount+ChatColor.WHITE+ChatColor.GREEN+"$"+ ChatColor.GRAY+" to "+name);
					}
				
				}	
			
			
			}else {
				p.sendMessage(Main.server+"You don't have enough money!");
			}
			}else {
				p.sendMessage(Main.server+"You cannot do that!");
			}
		}
	}
	@SuppressWarnings("deprecation")
	public static void giveMoney(String p, String amount) {
		if(hasBalance(p)) {
			if(!amount.contains(".")&&!amount.contains("-")) {
				for(Player player:Bukkit.getOnlinePlayers()) {
					if(player.getName().equals(p)) {
						Bukkit.getPlayer(p).sendMessage(Main.server+"You have gained "+ChatColor.GOLD+amount+ChatColor.GREEN+"$");
						}
					}	
			
			
				
				skyVault.econ.depositPlayer(p,Double.parseDouble(amount));
				
		}
		}
	}
	@SuppressWarnings("deprecation")
	public static void takeMoney(String p, String amount) {
		if(hasBalance(p)) {
			if(!amount.contains(".")&&!amount.contains("-")) {
				for(Player player:Bukkit.getOnlinePlayers()) {
				if(player.getName().equals(p)) {
					Bukkit.getPlayer(p).sendMessage(Main.server+"You have lost "+ChatColor.GOLD+amount+ChatColor.GREEN+"$");
					}
				}
				
				
				skyVault.econ.withdrawPlayer(p,Double.parseDouble(amount));
						
			}}
		}
		
	
	@SuppressWarnings("deprecation")
	public static void setMoney(String p, String amount) {
		if(hasBalance(p)) {
			if(!amount.contains(".")&&!amount.contains("-")) {
				for(Player player:Bukkit.getOnlinePlayers()) {
					if(player.getName().equals(p)) {
						Bukkit.getPlayer(p).sendMessage(Main.server+"You now have "+ChatColor.GOLD+amount+ChatColor.GREEN+"$");
						}
					}	
			
			
			skyVault.econ.withdrawPlayer(p, Double.parseDouble(String.valueOf(playerBalance(p))));
			skyVault.econ.depositPlayer(p, Double.parseDouble(amount));
			}
			}
		
	}
	
	
	@SuppressWarnings("deprecation")
	public static boolean hasBalance(String name) {
        if(skyVault.econ.hasAccount(name)) {
        	return true;
        }
		return false;
	}
	@SuppressWarnings("deprecation")
	public static int playerBalance(String p) {
		if(hasBalance(p)) {
			return (int) Math.round(skyVault.econ.getBalance(p));
					}
		
		return 0;
	}
	
	

}
