package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityBreedEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTameEvent;

public class IslandStatistics implements Listener{
	public static FileConfiguration config = Main.getPlugin().getConfig();
	public static int PlayerBlocksPlaced(Player player) {
		return config.getInt("IslandOwnerSettings."+player.getName()+".Stats.BlocksPlaced");
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		 Player p = event.getPlayer();
		if(IslandInfo.OwnIsland(p)) {
			 config.set("IslandOwnerSettings."+p.getName()+".Stats.BlocksPlaced",(config.getInt("IslandOwnerSettings."+p.getName()+".Stats.BlocksPlaced")+1));
				Main.getPlugin().saveConfig(); 
		}
		}
	public static int PlayerBlocksBroken(Player player) {
		return config.getInt("IslandOwnerSettings."+player.getName()+".Stats.BlocksBroken");
	}
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		 Player p = event.getPlayer();
		 if(IslandInfo.OwnIsland(p)) {
			 config.set("IslandOwnerSettings."+p.getName()+".Stats.BlocksBroken",(config.getInt("IslandOwnerSettings."+p.getName()+".Stats.BlocksBroken")+1));
				Main.getPlugin().saveConfig(); 
		 }
	}
	public static int PlayerMobKills(Player player, String Mob) {
		 for(String Mobs:config.getStringList("IslandOwnerSettings."+player.getName()+".Stats.MobsKilled")) {
         	String MobName= Mobs.split(":")[0];
     
         			if(MobName.equalsIgnoreCase(Mob)){
         				return Integer.parseInt(Mobs.split(":")[1]);
         			}
         			}
		
		return 0;
	}
	@EventHandler
	public void onPlayerKilledMob(EntityDeathEvent event) {
		Entity dead = event.getEntity();
		Entity killer = event.getEntity().getKiller();
		if (killer instanceof Player) {
			
            Player p = (Player) killer;
             List<String> Mobs = new ArrayList<String>();
             if(config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsKilled").isEmpty()) {
             	Mobs.add(dead.getType().toString()+":"+"1");
             	p.sendMessage(Main.server+"Now collecting mob data!"); 
             	config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsKilled", Mobs);
        		Main.getPlugin().saveConfig(); 
        		return;
             }
             Boolean MobAdded = false;
            for(String Mob:config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsKilled")) {
            	String MobName= Mob.split(":")[0];
        
            			if(MobName.equals(dead.getType().toString())){
            				Mobs.add(MobName+":"+(Integer.parseInt(Mob.split(":")[1])+1));
            				MobAdded = true;
            			}
            			else {
            				Mobs.add(Mob);
            			}
            			
            }
            if(MobAdded == false) {
					Mobs.add(dead.getType().toString()+":"+1);
					p.sendMessage(Main.server+"Now collecting "+dead.getType().toString().toLowerCase().replace("_", " ")); 
    			
            }
            
            
            config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsKilled", Mobs);
    		Main.getPlugin().saveConfig(); 
		}
	}
	public static int PlayerMobTames(Player player, String Mob) {
		 for(String Mobs:config.getStringList("IslandOwnerSettings."+player.getName()+".Stats.MobsTamed")) {
        	String MobName= Mobs.split(":")[0];
    
        			if(MobName.equalsIgnoreCase(Mob)){
        				return Integer.parseInt(Mobs.split(":")[1]);
        			}
        			}
		
		return 0;
	}
	@EventHandler
	public void onPlayerTamedMob(EntityTameEvent event) {
		Entity dead = event.getEntity();
		Entity killer = (Entity) event.getOwner();
		if (killer instanceof Player) {
			
            Player p = (Player) killer;
             List<String> Mobs = new ArrayList<String>();
             if(config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsTamed").isEmpty()) {
             	Mobs.add(dead.getType().toString()+":"+"1");
             	p.sendMessage(Main.server+"Now collecting tame data!"); 
             	config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsTamed", Mobs);
        		Main.getPlugin().saveConfig(); 
        		return;
             }
             Boolean MobAdded = false;
            for(String Mob:config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsTamed")) {
            	String MobName= Mob.split(":")[0];
        
            			if(MobName.equals(dead.getType().toString())){
            				Mobs.add(MobName+":"+(Integer.parseInt(Mob.split(":")[1])+1));
            				MobAdded = true;
            			}
            			else {
            				Mobs.add(Mob);
            			}
            			
            }
            if(MobAdded == false) {
					Mobs.add(dead.getType().toString()+":"+1);
					p.sendMessage(Main.server+"Now collecting tame data for "+dead.getType().toString().toLowerCase().replace("_", " ")); 
    			
            }
            
            
            config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsTamed", Mobs);
    		Main.getPlugin().saveConfig(); 
		}
	}
	public static int PlayerMobBreeds(Player player, String Mob) {
		 for(String Mobs:config.getStringList("IslandOwnerSettings."+player.getName()+".Stats.MobsBred")) {
       	String MobName= Mobs.split(":")[0];
   
       			if(MobName.equalsIgnoreCase(Mob)){
       				return Integer.parseInt(Mobs.split(":")[1]);
       			}
       			}
		
		return 0;
	}
	@EventHandler
	public void onPlayerBreedMob(EntityBreedEvent event) {
		Entity dead = event.getEntity();
		Entity killer = event.getBreeder();
		if (killer instanceof Player) {
			
            Player p = (Player) killer;
             List<String> Mobs = new ArrayList<String>();
             if(config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsBred").isEmpty()) {
             	Mobs.add(dead.getType().toString()+":"+"1");
             	p.sendMessage(Main.server+"Now collecting breed data!"); 
             	config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsBred", Mobs);
        		Main.getPlugin().saveConfig(); 
        		return;
             }
             Boolean MobAdded = false;
            for(String Mob:config.getStringList("IslandOwnerSettings."+p.getName()+".Stats.MobsBred")) {
            	String MobName= Mob.split(":")[0];
        
            			if(MobName.equals(dead.getType().toString())){
            				Mobs.add(MobName+":"+(Integer.parseInt(Mob.split(":")[1])+1));
            				MobAdded = true;
            			}
            			else {
            				Mobs.add(Mob);
            			}
            			
            }
            if(MobAdded == false) {
					Mobs.add(dead.getType().toString()+":"+1);
					p.sendMessage(Main.server+"Now collecting breed data for "+dead.getType().toString().toLowerCase().replace("_", " ")); 
    			
            }
            
            
            config.set("IslandOwnerSettings."+p.getName()+".Stats.MobsBred", Mobs);
    		Main.getPlugin().saveConfig(); 
		}
	}
	
	
	
}
