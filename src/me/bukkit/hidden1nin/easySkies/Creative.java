package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;





public class Creative implements Listener{
static List<String> Creative = new ArrayList<String>();
public static void setVar(String creative){
   Creative.add(creative);
}
public static void delVar(String creative){
    Creative.remove(creative);
}
private static HashMap<String, ItemStack[]> mySavedItems = new HashMap<String, ItemStack[]>();
public static void saveInventory(Player player)
{
mySavedItems.put(player.getName(), copyInventory(player.getInventory()));
}
private static ItemStack[] copyInventory(Inventory inv)
{
ItemStack[] original = inv.getContents();
ItemStack[] copy = new ItemStack[original.length];
for(int i = 0; i < original.length; ++i)
if(original != null)
copy = original ;
return copy;
}
public static boolean restoreInventory(Player player)
{
ItemStack[] savedInventory = mySavedItems.remove(player.getName());
if(savedInventory == null)
return false;
restoreInventory(player, savedInventory);
return true;
}
private static void restoreInventory(Player p, ItemStack[] inventory)
{
p.getInventory().setContents(inventory);
}



@EventHandler
public void onItemDrop(PlayerDropItemEvent e) {
	if(Creative.contains(((Player)e.getPlayer()).getName())) {
		e.getItemDrop().remove();
	}
}
@EventHandler
public void onInventoryOpen(InventoryOpenEvent event) {
	if(Creative.contains(((Player)event.getPlayer()).getName())) {
	event.getPlayer().sendMessage(ChatColor.RED+"Cannot Open " + event.getInventory().getType().toString().toLowerCase()+ " in Creative.");
	   //if (event.getInventory().getType() == InventoryType.CHEST) {
	      event.setCancelled(true);
	      
	      
	      //defines new location
	     
	      event.getPlayer().getWorld().playEffect(event.getPlayer().getLocation(), Effect.SMOKE, 2004);
          event.getPlayer().getWorld().playSound(event.getPlayer().getLocation(), Sound.ENTITY_VILLAGER_NO, 8.0F, 1.0F);
	   }
	   //used to only check for chest, now checks for all.
	}
@EventHandler
public void onTeleport(PlayerTeleportEvent event){
    if (event.getCause() == TeleportCause.ENDER_PEARL){
    	if(Creative.contains(((Player)event.getPlayer()).getName())) {
        event.setCancelled(true);
          event.getPlayer().getWorld().playEffect(event.getPlayer().getLocation(), Effect.SMOKE, 2004);
          event.getPlayer().getWorld().playSound(event.getPlayer().getLocation(), Sound.BLOCK_BAMBOO_BREAK, 8.0F, 1.0F);
       }
    }
   }
@EventHandler
public void onDamage(EntityDamageByEntityEvent e) {
	if(e.getDamager() instanceof  Player && e != null) {
    Entity p = e.getDamager();
    if (Creative.contains(((Player)p).getName())) {
        e.setCancelled(true);
        p.getWorld().playEffect(e.getDamager().getLocation(), Effect.SMOKE, 2004);
        p.getWorld().playSound(p.getLocation(), Sound.BLOCK_CROP_BREAK, 8.0F, 1.0F);
        
    }
}}
@EventHandler
public void FrameRotate(PlayerInteractEntityEvent e) {
	if(e.getPlayer()!=null && e != null &&e.getRightClicked().getType() != null) {
    if (e.getRightClicked().getType().equals(EntityType.ITEM_FRAME)) {
    	if (Creative.contains(((Player)e.getPlayer()).getName())) {
    		e.getPlayer().getWorld().playEffect(e.getRightClicked().getLocation(), Effect.SMOKE, 2004);
    		e.getPlayer().sendMessage(ChatColor.RED+"Cannot Use Item Frame in Creative.");
            e.setCancelled(true);
        }
    }
    }
}
}
