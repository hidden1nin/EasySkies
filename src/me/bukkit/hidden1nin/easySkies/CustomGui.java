package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class CustomGui {
public static Inventory OpenGui(String guiName) {
	if(Main.customGuiConfig.contains(guiName+".Slots")) {
		
		Inventory Gui = Bukkit.createInventory(null,Main.customGuiConfig.getInt(guiName+".Slots"),ChatColor.translateAlternateColorCodes('&',guiName.replace(".sub."," � ")));{
			if(Main.customGuiConfig.contains(guiName+".Items")) {
				for(String i :Main.customGuiConfig.getStringList(guiName+".Items")){
				String[] item = i.split(":");
				Main.createDisplay(Material.getMaterial(item[0].toUpperCase()), Gui,Integer.valueOf(item[3]),ChatColor.translateAlternateColorCodes('&', item[1]),ChatColor.translateAlternateColorCodes('&', item[2]));
				}
				
			}
			if(Main.customGuiConfig.contains(guiName+".Skulls")) {
				for(String i :Main.customGuiConfig.getStringList(guiName+".Skulls")){
				String[] item = i.split(":");
				Main.createSkull(item[0], Gui,Integer.valueOf(item[3]),ChatColor.translateAlternateColorCodes('&', item[1]),ChatColor.translateAlternateColorCodes('&', item[2]));
				}
				
			}
			if(Main.customGuiConfig.contains(guiName+".Border")) {
				Main.borderDisplay(Material.valueOf(Main.customGuiConfig.getString(guiName+".Border").split(":")[0].toUpperCase()), Gui,ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Border").split(":")[1]),ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Border").split(":")[2]));
			}
			if(Main.customGuiConfig.contains(guiName+".Filler")) {
				Main.fillDisplay(Material.valueOf(Main.customGuiConfig.getString(guiName+".Filler").split(":")[0].toUpperCase()), Gui,ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Filler").split(":")[1]),ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Filler").split(":")[2]));
			}
			return Gui;
		}
	}
	
	
	return null;
}
public static void CustomClick(InventoryClickEvent e) {
	if(e.getCurrentItem()==null) {
		return;
	}
	String guiName =e.getView().getTitle().replace("�","&").replace(" � ",".sub.");
	
	if(Main.customGuiConfig.contains(guiName+".Border")) {
		if(e.getCurrentItem().isSimilar(Main.createDisplay(Material.valueOf(Main.customGuiConfig.getString(guiName+".Border").split(":")[0].toUpperCase()),null, -2,ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Border").split(":")[1]),ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Border").split(":")[2])))){
			e.setCancelled(true);
			return;
		}
	}
	if(Main.customGuiConfig.contains(guiName+".Filler")) {
		if(e.getCurrentItem().isSimilar(Main.createDisplay(Material.valueOf(Main.customGuiConfig.getString(guiName+".Filler").split(":")[0].toUpperCase()),null, -2,ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Filler").split(":")[1]),ChatColor.translateAlternateColorCodes('&', Main.customGuiConfig.getString(guiName+".Filler").split(":")[2])))){
			e.setCancelled(true);
			return;
		}	
	}
	if(Main.customGuiConfig.contains(guiName+".Skulls")) {
		for(String i :Main.customGuiConfig.getStringList(guiName+".Skulls")){
		String[] item = i.split(":");
		if(e.getCurrentItem().isSimilar(Main.createSkull(item[0],null,-2,ChatColor.translateAlternateColorCodes('&', item[1]),ChatColor.translateAlternateColorCodes('&', item[2])))){
			if(item.length>3) {
				if(item.length>6) {
					if(e.getWhoClicked().hasPermission(item[6])==false) {
						e.setCancelled(true);
						e.getWhoClicked().sendMessage(Main.server+"You don't have permission for that!");
						return;
					}
				}
				if(item[4].equalsIgnoreCase("/none/")==false) {
				Bukkit.dispatchCommand(e.getWhoClicked(), item[4].replace("%player%", e.getWhoClicked().getName()));}
				if(item[5].equalsIgnoreCase("/none/")==false) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), item[5].replace("%player%", e.getWhoClicked().getName()));}
			}
			
			e.setCancelled(true);
			return;
		}
		}
		
	}
	if(Main.customGuiConfig.contains(guiName+".Items")) {
		for(String i :Main.customGuiConfig.getStringList(guiName+".Items")){
		String[] item = i.split(":");
		if(e.getCurrentItem().isSimilar(Main.createDisplay(Material.getMaterial(item[0].toUpperCase()),null,-2,ChatColor.translateAlternateColorCodes('&', item[1]),ChatColor.translateAlternateColorCodes('&', item[2])))){
			if(item.length>4) {
				if(item.length>6) {
					if(e.getWhoClicked().hasPermission(item[6])==false) {
						e.setCancelled(true);
						e.getWhoClicked().sendMessage(Main.server+"You don't have permission for that!");
						return;
					}
				}
				if(item[4].equalsIgnoreCase("/none/")==false) {
				Bukkit.dispatchCommand(e.getWhoClicked(), item[4].replace("%player%", e.getWhoClicked().getName()));}
				if(item[5].equalsIgnoreCase("/none/")==false) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), item[5].replace("%player%", e.getWhoClicked().getName()));}
			}
			
			e.setCancelled(true);
			return;
		}
		}
		
	}
	e.setCancelled(true);
}
}
