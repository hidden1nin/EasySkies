package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Particles {
	public static void createParticle(Location l,Color c,int size) {
l.getWorld().spawnParticle(Particle.REDSTONE,l, 1,new Particle.DustOptions(c,1));
	}
public static void createParticle(Location l,Particle p,int size) {
	if(p.equals(Particle.REDSTONE)) {
		l.getWorld().spawnParticle(Particle.REDSTONE,l, 1,new Particle.DustOptions(Color.RED,1));
	}else {
	l.getWorld().spawnParticle(p,l,size);
	}
}
public static void createParticleHelix(Location center,Color c,double size,Double density) {
	double scaleX = size;  // use these to tune the size of your circle
	double scaleY = size;
		new BukkitRunnable() {
			double i=0;
			public void run() {
				i +=density;
				double x = Math.cos(i) * scaleX;
			     double z = Math.sin(i) * scaleY;
			     double xt = Math.cos(-i) * scaleX;
			     double zt = Math.sin(-i) * scaleY;
			     double y = x;
			     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY()+y,center.getZ()+z), c, 1);
			     createParticle(new Location(center.getWorld(),center.getX()+xt,center.getY()-y,center.getZ()+zt), c, 1);
				if(i>Math.PI*2) {this.cancel();}
				
			}
		}.runTaskTimer(Main.getPlugin(),0, 1);
}
public static void createParticleRing(Location center,Particle p,double size,Double density,Boolean animated) {
	double scaleX = size;  // use these to tune the size of your circle
	double scaleY = size;
	if(animated) {
		new BukkitRunnable() {
			double i=0;
			public void run() {
				i +=density;
				double x = Math.cos(i) * scaleX;
			     double z = Math.sin(i) * scaleY;
			     
			     
			     double y = i;
			     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY()+y,center.getZ()+z), p, 1);

				if(i>Math.PI*2) {this.cancel();}
				
			}
			
		}.runTaskTimer(Main.getPlugin(),0, 1);
	}else {
	for (double i=0; i < 2 * Math.PI ; i +=density) {
	     double x = Math.cos(i) * scaleX;
	     double z = Math.sin(i) * scaleY;
	     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY(),center.getZ()+z), p, 1);
	}
	}
}
public static void createParticleRing(Location center,Particle p,double size,Double density) {
	double scaleX = size;  // use these to tune the size of your circle
	double scaleY = size;
	
	for (double i=0; i < 2 * Math.PI ; i +=density) {
	     double x = Math.cos(i) * scaleX;
	     double z = Math.sin(i) * scaleY;
	     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY(),center.getZ()+z), p, 1);
	}
}
public static void drawLine(Location point1, Location point2, double space) {
    World world = point1.getWorld();
    if(point2.getWorld().equals(world)) {
    //Validate.isTrue(point2.getWorld().equals(world), "Lines cannot be in different worlds!");
    double distance = point1.distance(point2);
    Vector p1 = point1.toVector();
    Vector p2 = point2.toVector();
    Vector vector = p2.clone().subtract(p1).normalize().multiply(space);
    double length = 0;
    for (; length < distance; p1.add(vector)) {
        world.spawnParticle(Particle.REDSTONE,new Location(world,p1.getX(), p1.getY(), p1.getZ()), 1,new Particle.DustOptions(Color.RED,1));
        length += space;
    }
}
}
public static void createParticleLine(Location point1, Location point2,Particle p, double density) {
	 World world = point1.getWorld();
	    if(point2.getWorld().equals(world)) {
	    //Validate.isTrue(point2.getWorld().equals(world), "Lines cannot be in different worlds!");
	    double distance = point1.distance(point2);
	    Vector p1 = point1.toVector();
	    Vector p2 = point2.toVector();
	    Vector vector = p2.clone().subtract(p1).normalize().multiply(density);
	    double length1 = 0;
	    for (; length1 < distance; p1.add(vector)) {
	    	createParticle(new Location(world,p1.getX(), p1.getY(), p1.getZ()),p,1);
	        //world.spawnParticle(Particle.REDSTONE,new Location(world,p1.getX(), p1.getY(), p1.getZ()), 1,new Particle.DustOptions(Color.RED,1));
	        length1 += density;
	    }
	}
}
public static void createParticleTube(Location center,Particle p,double size, int height,Double density,Boolean animated) {
	if(animated) {
		new BukkitRunnable() {
			double y = 0;
			public void run() {
				double i=0;
				y+=density;
					for (i=0; i < 2 * Math.PI ; i +=density) {
					     double x = Math.cos(i) * size;
					     double z = Math.sin(i) * size;
					     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY()+y,center.getZ()+z), p,1);
					}
					if(y>height) {this.cancel();}
				
					
			
			}
			}.runTaskTimer(Main.getPlugin(),0, 1);
	}else {
		double i=0;
		for(double y = 0;y<height;y+=density) {
			for (i=0; i < 2 * Math.PI ; i +=density) {
			     double x = Math.cos(i) * size;
			     double z = Math.sin(i) * size;
			     createParticle(new Location(center.getWorld(),center.getX()+x,center.getY()+y,center.getZ()+z), p, 1);
			}
			if(i>height) {return;}
		}
			
	}
}
}
