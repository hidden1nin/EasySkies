package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class ItemCollector implements Runnable,Listener{
	

	@EventHandler
	public void onPlacedVoidChest(BlockPlaceEvent event) {
		 Player p = event.getPlayer();
		 if(firstEnable.PlayerHomes.get(IslandInfo.getIsland(p.getLocation()))==IslandInfo.getIslandLocation(p.getName())) {
			if(IslandInfo.getIslandVoidChest(p.getName())== null) {
				if(event.getBlock()!=null) {
		if(event.getBlock().getType()==Material.CHEST) {
			Chest chest = (Chest)event.getBlock().getState();
			
			if(chest.getCustomName().equalsIgnoreCase(ChatColor.AQUA+"Void Collector")) {
				List<String> VoidChestLocation= new ArrayList<String>();
				Location l = event.getBlock().getLocation();
				VoidChestLocation.add(l.getBlockX()+":"+l.getBlockY()+":"+l.getBlockZ());
				firstEnable.config.set("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.VoidCollector",VoidChestLocation);	
				Main.getPlugin().saveConfig();
				p.sendMessage(Main.server+"Now catching void items!");
			}
		}
		}else {
			if(event.getBlock().getType()==Material.CHEST) {
				Chest chest = (Chest)event.getBlock().getState();
				
				if(chest.getCustomName().equalsIgnoreCase(ChatColor.AQUA+"Void Collector")) {
					p.sendMessage(Main.server+"You can only have one Void Collector per island, if yours got destroyed place a chest where it once was!");
				}}
		}
		}
		 }
		}
	@EventHandler
	public void onBreakVoidChest(BlockBreakEvent event) {
		 Player p = event.getPlayer();
		 if(IslandInfo.getIslandVoidChest(event.getPlayer().getName())== null) {
			 return;
		 }
		 if(firstEnable.PlayerHomes.get(IslandInfo.getIsland(p.getLocation()))==IslandInfo.getIslandLocation(p.getName())) {
			if(event.getBlock().getLocation().distance(IslandInfo.getIslandVoidChest(event.getPlayer().getName()))==0){
			p.sendMessage(Main.server+"You will no longer catch void items!");
			firstEnable.config.set("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.VoidCollector",new ArrayList<String>());	
			Main.getPlugin().saveConfig();
			}
	    }
	}
	
	@Override
	public void run() {
		for(Player player:Bukkit.getOnlinePlayers()) {
		if(firstEnable.IslandOwners.contains(player.getName())) {
			
		
		Location EntityArea = firstEnable.PlayerHomes.get(player.getName());
        World world = firstEnable.SkyBlockWorld;
       int IslandRadius = IslandInfo.getIslandSize(player.getName())*2;
       
        List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(new Location(EntityArea.getWorld(),EntityArea.getBlockX(),-40,EntityArea.getBlockZ()), IslandRadius,40, IslandRadius);
       if(nearbyEntities!= null) {
        for(Entity e : world.getEntities()){
            if(nearbyEntities.contains(e)){
           	 if(e instanceof Item) {
               ItemStack item = ((Item) e).getItemStack();
               Location VoidChest =IslandInfo.getIslandVoidChest(player.getName());
              if(VoidChest!=null) {
            	  Chest chest = (Chest) VoidChest.getBlock().getState();
            	  chest.getInventory().addItem(item);
            	  e.remove();
            	  Particles.createParticleRing(new Location(VoidChest.getWorld(),VoidChest.getBlockX()+.5,VoidChest.getBlockY()+1,VoidChest.getBlockZ()+.5), Particle.SLIME, 1, .3);
       	       
              }
           	 }
            }
        }
		}
		}
		}
		
	}

}
