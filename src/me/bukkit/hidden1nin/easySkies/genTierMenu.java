package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class genTierMenu {
public static FileConfiguration config = Main.islandGeneratorsConfig;
public static void addGen(Inventory inv, Player p, String Unlocked) {
	int i = Integer.parseInt(Unlocked);
	Material genMaterial = Material.getMaterial(config.getString("GeneratorChances."+i+".DisplayMaterial").toUpperCase());
	if(Material.getMaterial(config.getString("GeneratorChances."+i+".DisplayMaterial").toUpperCase())==null) {
		genMaterial = Material.STONE;
	}
//Main.createDisplay(GenMaterial, inv, -1, ChatColor.WHITE+config.getString("GeneratorChances."+i+".DisplayName"), ChatColor.GRAY+config.getString("GeneratorChances."+i+".DisplayLore"));
	ItemStack genItem =Main.createDisplay(genMaterial, inv, -2, ChatColor.WHITE+config.getString("GeneratorChances."+i+".DisplayName"), ChatColor.GRAY+config.getString("GeneratorChances."+i+".DisplayLore"));
	ItemMeta genItemMeta =genItem.getItemMeta();
	List<String> lore = genItemMeta.getLore();
	lore.add(ChatColor.AQUA+"Spawns:");
	for(String spawn:config.getStringList("GeneratorChances."+i+".Blocks")) {
		lore.add(ChatColor.GRAY+spawn.split(":")[1]+"% "+spawn.split(":")[0].replace("_", " "));
	}
	genItemMeta.setLore(lore);
	genItem.setItemMeta(genItemMeta);
	inv.addItem(new ItemStack[] { genItem });
}
public static void selectTier(Player player,int i) {
	player.sendMessage(Main.server+"You set "+config.getString("GeneratorChances."+i+".DisplayName")+" as your generator style!");
	Main.getPlugin().getConfig().set("IslandOwnerSettings."+player.getName()+".GenTier",i);
	Main.getPlugin().saveConfig();
}
public static void clickTier(Player player, ItemStack clicked, Inventory inv) {
	
	for(int i = 0;config.contains("GeneratorChances."+i);i++) {
		Material genMaterial = Material.getMaterial(config.getString("GeneratorChances."+i+".DisplayMaterial").toUpperCase());
		if(Material.getMaterial(config.getString("GeneratorChances."+i+".DisplayMaterial").toUpperCase())==null) {
			genMaterial = Material.STONE;
		}
		ItemStack genItem =Main.createDisplay(genMaterial, inv, -2, ChatColor.WHITE+config.getString("GeneratorChances."+i+".DisplayName"), ChatColor.GRAY+config.getString("GeneratorChances."+i+".DisplayLore"));
		ItemMeta genItemMeta =genItem.getItemMeta();
		List<String> lore = genItemMeta.getLore();
		lore.add(ChatColor.AQUA+"Spawns:");
		for(String spawn:config.getStringList("GeneratorChances."+i+".Blocks")) {
			lore.add(ChatColor.GRAY+spawn.split(":")[1]+"% "+spawn.split(":")[0].replace("_", " "));
		}
		genItemMeta.setLore(lore);
		genItem.setItemMeta(genItemMeta);
	if(clicked.equals(genItem)){
		//do action based on click
		selectTier(player,i);
		
	}
		}
	
	
	
}
public static void loadMenu(Inventory i, Player p) {
	
for(String Unlocked :Main.getPlugin().getConfig().getStringList("IslandOwnerSettings."+p.getName()+".UnlockedGenTier")) {
	addGen(i,p,Unlocked);
}
}
}
