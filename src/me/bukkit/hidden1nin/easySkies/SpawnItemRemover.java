package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

public class SpawnItemRemover implements Runnable{
	
	@Override
	public void run() {
		 Location EntityArea = firstEnable.Spawn;
         World world = firstEnable.SkyBlockWorld;
        int clearRadius = Main.islandCommandsConfig.getInt("Settings.SpawnItemWipe.Radius");
         List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
        Boolean cleared = false;
         for(Entity e : world.getEntities()){
             if(nearbyEntities.contains(e)){
            	 if(e instanceof Item) {
                 e.remove();
                 cleared = true;
            	 }
             }
         }
         if(cleared == true) {
         Bukkit.getServer().broadcastMessage(Main.server + ChatColor.RED + "Items near spawn removed!");
         }
     }
	
	
}
