package me.bukkit.hidden1nin.easySkies;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.Inventory;

public class IslandInfo implements Listener{
	public static FileConfiguration config = Main.getPlugin().getConfig();
	public static Inventory EasySkyIslandTop = Bukkit.createInventory(null,36,Main.server+ChatColor.GRAY+"Best Islands!");
	static {
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, EasySkyIslandTop, " ", " ");
		Main.createDisplay(Material.RED_STAINED_GLASS,EasySkyIslandTop, 31, ChatColor.WHITE+"Back", "");
		
	}
	
	public static boolean pvpOn(Player p) {
		if(p.getLocation().distance(firstEnable.Boss)<25) {return true;}
		return OwnIsland(p);
		
	}
	public static boolean pvpOnProjectile(Location p) {
		if(p.distance(firstEnable.Boss)<25){return true;}
		return false;
		}
	public static boolean OwnIsland(Player p) {
		if(p.getLocation().getBlockX()<0&&p.getLocation().getBlockZ()<0||!p.getLocation().getWorld().getName().equalsIgnoreCase(config.getString("loc.world"))) {
			return true;
		}
		if(trusted(p)) {
			return true;
		}
		if(firstEnable.IslandOwners.indexOf(p.getName()) != -1 && p.getLocation().getBlockX()>firstEnable.PlayerHomes.get(p.getName()).getBlockX()-config.getInt("IslandOwnerSettings."+p.getName()+".Size") &&p.getLocation().getBlockX()<firstEnable.PlayerHomes.get(p.getName()).getBlockX()+config.getInt("IslandOwnerSettings."+p.getName()+".Size") && p.getLocation().getBlockZ()>firstEnable.PlayerHomes.get(p.getName()).getBlockZ()-config.getInt("IslandOwnerSettings."+p.getName()+".Size") &&p.getLocation().getBlockZ()<firstEnable.PlayerHomes.get(p.getName()).getBlockZ()+config.getInt("IslandOwnerSettings."+p.getName()+".Size")||Main.HasAccess.indexOf(p.getName()) != -1) {
		
			return true;
		}else {
			return false;}
	}
	public static boolean trusted(Player p) {
		double X = Math.round(p.getLocation().getBlockX()*.0001);
		double Z = Math.round(p.getLocation().getBlockZ()*.0001);
		
			for(String rawData : config.getStringList("PlayerHomes")) {
				
				
		        String[] raw = rawData.split(":");
		        double X2 = Math.round(Double.parseDouble(raw[2])*.0001);
		        double Z2 = Math.round(Double.parseDouble(raw[4])*.0001);
		        if(X2==X&&Z2==Z) {
		        	if(config.getStringList("IslandOwnerSettings."+raw[0]+".Coop").contains(p.getName())&&p.getLocation().getBlockX()>firstEnable.PlayerHomes.get(raw[0]).getBlockX()-config.getInt("IslandOwnerSettings."+raw[0]+".Size") &&p.getLocation().getBlockX()<firstEnable.PlayerHomes.get(raw[0]).getBlockX()+config.getInt("IslandOwnerSettings."+raw[0]+".Size") && p.getLocation().getBlockZ()>firstEnable.PlayerHomes.get(raw[0]).getBlockZ()-config.getInt("IslandOwnerSettings."+raw[0]+".Size") &&p.getLocation().getBlockZ()<firstEnable.PlayerHomes.get(raw[0]).getBlockZ()+config.getInt("IslandOwnerSettings."+raw[0]+".Size") ){
		        		return true;
		        	}
		        }
		        }
		
		return false;
	}
	public static Location getIslandLocation(String playername) {
		if(playername.equalsIgnoreCase("none")== false) {
		return firstEnable.PlayerHomes.get(playername);
		}
		return null;
	}
	public static int getIslandSize(String playername) {
		if(playername.equalsIgnoreCase("none")== false) {
			return config.getInt("IslandOwnerSettings."+playername+".Size");
		}
		
		return 0;
	}
	public static Location getIslandVoidChest(String player) {
		if(!config.getStringList("IslandOwnerSettings."+player+".CustomBlocks.VoidCollector").isEmpty()) {
			List<String> place = config.getStringList("IslandOwnerSettings."+player+".CustomBlocks.VoidCollector");
			return new Location(firstEnable.SkyBlockWorld,Integer.valueOf(place.get(0).split(":")[0]),Integer.valueOf(place.get(0).split(":")[1]),Integer.valueOf(place.get(0).split(":")[2])); 
		}
		
		
		return null;
	}
	public static String getIsland(Location l) {
		double X = Math.round(l.getBlockX()*.0001);
		double Z = Math.round(l.getBlockZ()*.0001);
		
			for(String rawData : config.getStringList("PlayerHomes")) {
				
				
		        String[] raw = rawData.split(":");
		        double X2 = Math.round(Double.parseDouble(raw[2])*.0001);
		        double Z2 = Math.round(Double.parseDouble(raw[4])*.0001);
		        if(X2==X&&Z2==Z) {
		        	
		        		return raw[0];
		        	
		        }
		        }
		
		return "none";
	}
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlacedBlockPlayer(BlockPlaceEvent event) {
	
		 Player p = event.getPlayer();
		 if(IslandInfo.OwnIsland(p)&& !getIsland(p.getLocation()).equalsIgnoreCase("none")) {
			 List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
				
			 int islandValue = 0;
			 if(firstEnable.config.contains("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel")) {
			 islandValue = firstEnable.config.getInt("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel");
	
			 }
			 for(String blockValue:Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
             	if(blockValue.split(":")[0].equalsIgnoreCase(event.getBlock().getType().toString())) {
             		islandValue =islandValue+Integer.valueOf(blockValue.split(":")[1]);
             		break;
             	}
             }
			 if(event.getBlock().getType().isSolid()) {
                 islandValue = islandValue+Main.islandBlockValueConfig.getInt("Blocks.DefaultValues");
                }
			 if(islandValue<=0) {
				 islandValue = 0;
			 }
			
			 for(String player:players) {
					
					if(player.split(":")[0].equals(getIsland(event.getBlock().getLocation()))) {
						players.set(players.indexOf(player), player.split(":")[0]+":"+islandValue);
					break;
					}
				}
				Main.islandBlockValueConfig.set("Island.Top.Players", players);
				Main.saveBlockValues();

			 firstEnable.config.set("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel", islandValue);
			 Main.getPlugin().saveConfig();
			 calculateTopIslands(event.getBlock().getLocation(),islandValue);
	    }
	}
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBrokenBlockPlayer(BlockBreakEvent event) {
		 Player p = event.getPlayer();
		 if(IslandInfo.OwnIsland(p) && !getIsland(p.getLocation()).equalsIgnoreCase("none")) {
			 List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
				
			 int islandValue = 0;
			 if(firstEnable.config.contains("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel")) {

			 islandValue = firstEnable.config.getInt("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel");		 
			 }
		
			 for(String blockValue:Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
             	if(blockValue.split(":")[0].equalsIgnoreCase(event.getBlock().getType().toString())) {
             		islandValue =islandValue-Integer.valueOf(blockValue.split(":")[1]);
             		break;
             	}
             }
			 if(event.getBlock().getType().isSolid()) {
                 islandValue = islandValue-Main.islandBlockValueConfig.getInt("Blocks.DefaultValues");
                }
			 if(islandValue<=0) {
				 islandValue = 0;
			 }
			 for(String player:players) {
					
					if(player.split(":")[0].equals(getIsland(event.getBlock().getLocation()))) {
						players.set(players.indexOf(player), player.split(":")[0]+":"+islandValue);
					break;
					}
				}
				Main.islandBlockValueConfig.set("Island.Top.Players", players);
				Main.saveBlockValues();
			 
			 firstEnable.config.set("IslandOwnerSettings."+getIsland(event.getBlock().getLocation())+".IslandLevel", islandValue);
			 Main.getPlugin().saveConfig();
			 calculateTopIslands(event.getBlock().getLocation(),islandValue);
	    }
	}
	public static int getIslandLevel(Location l) {
		int islandValue = 0;
		if(firstEnable.config.contains("IslandOwnerSettings."+getIsland(l)+".IslandLevel")) {
			 islandValue = firstEnable.config.getInt("IslandOwnerSettings."+getIsland(l)+".IslandLevel");
				
		}
		/*if(!getIsland(l).equals("none")) {
			Location loc1 = new Location(l.getWorld(),firstEnable.PlayerHomes.get(getIsland(l)).getBlockX()-config.getInt("IslandOwnerSettings."+getIsland(l)+".Size"), 1, firstEnable.PlayerHomes.get(getIsland(l)).getBlockZ()-config.getInt("IslandOwnerSettings."+getIsland(l)+".Size"));
			Location loc2 = new Location(l.getWorld(),firstEnable.PlayerHomes.get(getIsland(l)).getBlockX()+config.getInt("IslandOwnerSettings."+getIsland(l)+".Size"), 256, firstEnable.PlayerHomes.get(getIsland(l)).getBlockZ()+config.getInt("IslandOwnerSettings."+getIsland(l)+".Size"));
			int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
	        int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
	        int differenceblockX =topBlockX-bottomBlockX;
	        int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
	        int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
	        int differenceblockY =topBlockY-bottomBlockY;
	        int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
	        int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
	        int differenceblockZ =topBlockZ-bottomBlockZ;
	        
	        for(int x = 0; x <= differenceblockX; x++)
	        {
	            for(int z =0; z <= differenceblockZ; z++)
	            {
	                for(int y = 0; y <= differenceblockY; y++)
	                {
	                	
	                	 
	                    Material Block = loc1.getWorld().getBlockAt(bottomBlockX+x, bottomBlockY+y, bottomBlockZ+z).getType();
	                    
	                    
	                    
	                    for(String blockValue:Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
	                    	if(blockValue.split(":")[0].equalsIgnoreCase(Block.toString())) {
	                    		islandValue =islandValue+Integer.valueOf(blockValue.split(":")[1]);
	                    		break;
	                    	}
	                    }
	                   if(Block.isSolid()) {
	                    islandValue = islandValue+Main.islandBlockValueConfig.getInt("Blocks.DefaultValues");
	                   }
	                    
	                 
	                    }
	                    
	                    
	                }
	            }
	        }
	         
	       
	       */
	        
		
			
			
			
			
			
		
		if(firstEnable.config.contains("IslandOwnerSettings."+getIsland(l)+".IslandLevel")) {
			List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
			
			if(!players.contains(getIsland(l)+":"+firstEnable.config.getInt("IslandOwnerSettings."+getIsland(l)+".IslandLevel"))) {
				players.add(getIsland(l)+":"+islandValue);
			}
			
			for(String player:players) {
			
				if(player.split(":")[0].equals(getIsland(l))) {
					players.set(players.indexOf(player), player.split(":")[0]+":"+islandValue);
				break;
				}
			}
			Main.islandBlockValueConfig.set("Island.Top.Players", players);
			Main.saveBlockValues();
			
		}
		calculateTopIslands(l,islandValue);
		return islandValue;
		
	}
	public static void calculateTopIslands(Location l, int islandValue) {
		Integer place = 0;
		List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
		if(!players.contains(getIsland(l)+":"+firstEnable.config.getInt("IslandOwnerSettings."+getIsland(l)+".IslandLevel"))) {
			players.add(getIsland(l)+":"+islandValue);
		}
		for(String player: players) {
			String name =player.split(":")[0];
			if (getIsland(l).equals(name)) {
				
				place =players.indexOf(player);
				
				while(place+1< players.size()||place-1>=0) {
					if(place-1>=0) {
					if(Integer.parseInt(players.get(place-1).split(":")[1])<islandValue) {
					Collections.swap(players, place, place-1);
					config.set("IslandOwnerSettings."+players.get(place).split(":")[0]+".IslandRank", place+1);
					Main.getPlugin().saveConfig(); 
					place =players.indexOf(player);
					config.set("IslandOwnerSettings."+getIsland(l)+".IslandRank", place+1);
					Main.getPlugin().saveConfig(); 
					}}
						if(place+1< players.size()) {
							if(Integer.parseInt(players.get(place+1).split(":")[1])>islandValue) {
							Collections.swap(players, place+1, place);
							config.set("IslandOwnerSettings."+players.get(place).split(":")[0]+".IslandRank", place+1);
							Main.getPlugin().saveConfig(); 
							place =players.indexOf(player);
							config.set("IslandOwnerSettings."+getIsland(l)+".IslandRank", place+1);
							Main.getPlugin().saveConfig(); 
							}}
					
				
					if(place+1< players.size() == false&&place-1>=0 == false) {
					if(Integer.parseInt(players.get(place-1).split(":")[1])>islandValue&&Integer.parseInt(players.get(place+1).split(":")[1])<islandValue) {
						Main.islandBlockValueConfig.set("Island.Top.Players", players);
						Main.saveBlockValues();
						break;
					}
					}else {
						Main.islandBlockValueConfig.set("Island.Top.Players", players);
						Main.saveBlockValues();
						break;
					}
				}
				break;
			}
		}
		Main.islandBlockValueConfig.set("Island.Top.Players", players);
		Main.saveBlockValues();
		updateIslandTopMenu();
		
	}
	public static Integer islandRank(String name) {
		if(config.contains("IslandOwnerSettings."+name+".IslandRank")) {
		return config.getInt("IslandOwnerSettings."+name+".IslandRank");
		}
		return 0;
	}
	public static void updateIslandTopMenu() {
		Inventory inv =EasySkyIslandTop;
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, inv, " ", " ");
		Main.createDisplay(Material.RED_STAINED_GLASS,EasySkyIslandTop, 31, ChatColor.WHITE+"Back", "");
		
		List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
		for(int i =0; players.size()>=i&& i <= Main.islandBlockValueConfig.getInt("Island.Top.Amount"); i++) {
			
				if(i == 0) {
					Main.createSkull(players.get(i).split(":")[0], inv, 4, ChatColor.AQUA+"1st place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 1) {
					Main.createSkull(players.get(i).split(":")[0], inv, 12, ChatColor.AQUA+"2nd place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 2) {
					Main.createSkull(players.get(i).split(":")[0], inv, 13, ChatColor.AQUA+"3rd place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 3) {
					Main.createSkull(players.get(i).split(":")[0], inv, 14, ChatColor.AQUA+"4th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 4) {
					Main.createSkull(players.get(i).split(":")[0], inv, 20, ChatColor.AQUA+"5th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 5) {
					Main.createSkull(players.get(i).split(":")[0], inv, 21, ChatColor.AQUA+"6th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 6) {
					Main.createSkull(players.get(i).split(":")[0], inv, 22, ChatColor.AQUA+"7th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 7) {
					Main.createSkull(players.get(i).split(":")[0], inv, 23, ChatColor.AQUA+"8th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
				if(i == 8) {
					Main.createSkull(players.get(i).split(":")[0], inv, 24, ChatColor.AQUA+"9th place",ChatColor.GRAY+players.get(i).split(":")[0]+" with an island level of "+players.get(i).split(":")[1]);
				}
			
			
			
			
		}
	}
	public static void loadTeamMates(Inventory inv, Player player) {
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, inv, " ", " ");
		int trusted=0;
		for(String trustedPlayer:config.getStringList("IslandOwnerSettings."+player.getName()+".Coop")) {
			trusted+=1;
			Main.createSkull(trustedPlayer, inv, config.getStringList("IslandOwnerSettings."+player.getName()+".Coop").indexOf(trustedPlayer), ChatColor.AQUA+trustedPlayer, ChatColor.GRAY+"Click to "+ChatColor.RED+"Remove");
		}
		while(trusted<config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")) {
			if(trusted>=inv.getSize()-1) {
				break;
			}
			Main.createDisplay(Material.GLASS_PANE, inv, trusted, ChatColor.WHITE+"Open Slot", "");
			trusted++;
		}
		Main.createDisplay(Material.RED_STAINED_GLASS,inv, inv.getSize()-1, ChatColor.WHITE+"Back", "");
		
	}
	public static ArrayList<Location> getIslandStorage(String name) {
		if(!config.getStringList("IslandOwnerSettings."+name+".CustomBlocks.Storage").isEmpty()) {
			List<String> place = config.getStringList("IslandOwnerSettings."+name+".CustomBlocks.Storage");
			ArrayList<Location> storagesLocations = new ArrayList<Location>();
			for(int i=0;i<place.size();i++) {
			storagesLocations.add(new Location(firstEnable.SkyBlockWorld,Integer.valueOf(place.get(i).split(":")[0]),Integer.valueOf(place.get(i).split(":")[1]),Integer.valueOf(place.get(i).split(":")[2])));
			}
			return storagesLocations;
		}
		
		
		return null;
	}
	public static Boolean IsChestNearIslandStorage(String name,Location l) {
		Boolean b= false;
		if(getIslandStorage(name)!=null) {
		for(Location loc:getIslandStorage(name)) {
		if(Main.getBlockCenter(l.getBlock()).distance(Main.getBlockCenter(loc.getBlock()))<4.5) {
			b=true;
			Particles.drawLine(Main.getBlockCenter(l.getBlock()),Main.getBlockCenter(loc.getBlock()), .5);
		}
		}
		return b;
		}else {
			return false;
		}
	}
	public static Boolean IsNearIslandStorage(String name,Location l) {
		Boolean b= false;
		if(getIslandStorage(name)!=null) {
		for(Location loc:getIslandStorage(name)) {
			if(Main.getBlockCenter(l.getBlock()).distance(Main.getBlockCenter(loc.getBlock()))<9) {
			b=true;
			Particles.drawLine(Main.getBlockCenter(l.getBlock()),Main.getBlockCenter(loc.getBlock()), .5);
		}
		}
		return b;
		}else {
			return false;
		}
	}
	
}
