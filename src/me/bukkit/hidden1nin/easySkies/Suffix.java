package me.bukkit.hidden1nin.easySkies;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class Suffix {
	public static String command = "pex user %player% suffix ";
	public static HashMap<Material, String> clickedperms = new HashMap<Material, String>();
	public static void OpenSuffix(Player p) {
		Inventory EasySkySuffix = Bukkit.createInventory(null, 54, Main.server + "Suffix");
		Main.borderDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE,EasySkySuffix,ChatColor.GREEN+" ","");
		Main.createDisplay(Material.BARRIER, EasySkySuffix, 49,ChatColor.RED+"Remove Suffix", "Click to remove suffix");
		loresetter(p, Material.PIG_SPAWN_EGG, "&dPig",  "Such a radical name!", "permission.easySkies.suffix.pig", EasySkySuffix);
		loresetter(p, Material.GOLDEN_CARROT, "&9S&dw&ea&ag",  "Swagger like mcjagger", "permission.easySkies.suffix.swag", EasySkySuffix);
		loresetter(p, Material.TOTEM_OF_UNDYING, "&eYOLO",  "Wanna go back to 2015?", "permission.easySkies.suffix.yolo", EasySkySuffix);
		loresetter(p, Material.DIAMOND_SWORD, "&dPro",  "Like redbull or something", "permission.easySkies.suffix.pro", EasySkySuffix);
		loresetter(p, Material.ENDER_EYE, "&fNega&0tive",  "Reminds me of Yin and yang", "permission.easySkies.suffix.negative", EasySkySuffix);
		loresetter(p, Material.FEATHER, "&6A&fn&6g&fe&6l",  "sent straight from heaven", "permission.easySkies.suffix.angel", EasySkySuffix);
		loresetter(p, Material.NETHERRACK, "&5D&ce&5m&co&5n",  "where did this come from?", "permission.easySkies.suffix.demon", EasySkySuffix);
		loresetter(p, Material.HONEY_BOTTLE, "&eBuzzy",  "Buzz Buzz Buzz", "permission.easySkies.suffix.buzz", EasySkySuffix);
		loresetter(p, Material.STRING, "&fS&7il&8ky",  "Silky Smooth like butter", "permission.easySkies.suffix.buzz", EasySkySuffix);
		loresetter(p, Material.SUGAR, "&6W&ei&6l&ed",  "Woohoo and Wackadoo!", "permission.easySkies.suffix.wild", EasySkySuffix);
		loresetter(p, Material.SLIME_BALL, "&aS&2l&ai&2m&ae&2y",  "Gooey and Chewy!", "permission.easySkies.suffix.slimy", EasySkySuffix);
		loresetter(p, Material.BELL, "&bP&co&bl&ci&bc&ce",  "WEE WOO WEE WOO", "permission.easySkies.suffix.police", EasySkySuffix);
		p.openInventory(EasySkySuffix);
	}
	public static void loresetter(Player p,Material material,String name,String lore,String permission, Inventory i) {
		clickedperms.put(material, permission);
		if(p.hasPermission(permission)||p.hasPermission("permission.easySkies.suffix.allsuffix")) {
			Main.createDisplay(material, i, -1, ChatColor.translateAlternateColorCodes('&', name), lore+"/line/"+ChatColor.GREEN+"/line/"+ChatColor.GREEN+"Unlocked");
		}else {
			Main.createDisplay(material, i, -1,ChatColor.translateAlternateColorCodes('&', name), lore+"/line/"+"/line/"+ChatColor.RED+"Locked");
		}
	}
	public static void clicksuffix(InventoryClickEvent event) {
		Player p = (Player) event.getWhoClicked();
		if(event.getCurrentItem().getType()==Material.LIGHT_BLUE_STAINED_GLASS_PANE) {return;}
		if(event.getCurrentItem().getType()==Material.BARRIER) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\"\"");return;}
		if(clickedperms.containsKey(event.getCurrentItem().getType())) {
			if(p.hasPermission(clickedperms.get(event.getCurrentItem().getType()))||p.hasPermission("permission.easySkies.suffix.allsuffix")) {
				if(event.getCurrentItem().getType()==Material.PIG_SPAWN_EGG) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &dPig\"");return;}
				if(event.getCurrentItem().getType()==Material.GOLDEN_CARROT) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &9S&dw&ea&ag\"");return;}
				if(event.getCurrentItem().getType()==Material.TOTEM_OF_UNDYING) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &eYOLO\"");return;}
				if(event.getCurrentItem().getType()==Material.DIAMOND_SWORD) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &dPro\"");return;}
				if(event.getCurrentItem().getType()==Material.ENDER_EYE) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &fNega&0tive\"");return;}
				if(event.getCurrentItem().getType()==Material.FEATHER) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &6A&fn&6g&fe&6l\"");return;}
				if(event.getCurrentItem().getType()==Material.NETHERRACK) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &5D&ce&5m&co&5n\"");return;}
				if(event.getCurrentItem().getType()==Material.HONEY_BOTTLE) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &eBuzzy\"");return;}
				if(event.getCurrentItem().getType()==Material.STRING) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &fS&7il&8ky\"");return;}
				if(event.getCurrentItem().getType()==Material.SUGAR) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &6W&ei&6l&ed\"");return;}
				if(event.getCurrentItem().getType()==Material.SLIME_BALL) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &aS&2l&ai&2m&ae&2y\"");return;}
				if(event.getCurrentItem().getType()==Material.BELL) {p.closeInventory(); Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command.replace("%player%",p.getName())+"\" &bP&co&bl&ci&bc&ce\"");return;}
			}
		}
		
	}
}
