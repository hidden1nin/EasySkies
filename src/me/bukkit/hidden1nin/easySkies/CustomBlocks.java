package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
class CommandBlock {
    // Instance Variables
	String name;
    String consoleCommand;
    String playerCommand;
    int delay;
    List<String> players;
    Location location;
    Color particle;
    Double size;
}

public class CustomBlocks implements Runnable{

	public static void CreateBlock(Location l, String[] args) {
		if(Main.customBlocksConfig.contains("Blocks.List."+args[0])==false) {
	    StringBuilder message = new StringBuilder(args[2]);
	    for (int arg = 3; arg < args.length; arg++) {
	      message.append(" ").append(args[arg]);
	      }
	    String loc;
	    	loc =""+l.getX()+":"+l.getY()+":"+l.getZ()+":"+l.getWorld().getName();
	    if(message.toString().split(":").length==2) {
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".ConsoleCommand", message.toString().split(":")[1]);
	    }else {
	    	Main.customBlocksConfig.set("Blocks.List."+args[0]+".ConsoleCommand", "/none/");
	    }
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".PlayerCommand", message.toString().split(":")[0]);
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Location", loc);
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Particle",Color.WHITE);
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Name", args[0]);
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Size", .5);
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Cooldown", Integer.valueOf(args[1]));
	    Main.customBlocksConfig.set("Blocks.List."+args[0]+".Players", new ArrayList<String>());
	    Main.saveCustomBlocks();
		}
	}
	public static ArrayList<CommandBlock> getAllCustomBlocks(){
		
		ArrayList<CommandBlock> all = new ArrayList<CommandBlock>();
		if(Main.customBlocksConfig.contains("Blocks.List")) {
		for (String i : Main.customBlocksConfig.getConfigurationSection("Blocks.List").getKeys(false)) {
			String[] args = Main.customBlocksConfig.getString("Blocks.List." + i + ".Location").split(":");
			CommandBlock NewBlock = new CommandBlock();
			NewBlock.location = new Location(Bukkit.getWorld(args[3]), Double.parseDouble(args[0]),
					Double.parseDouble(args[1]), Double.parseDouble(args[2]));
			NewBlock.consoleCommand = Main.customBlocksConfig.getString("Blocks.List." + i + ".ConsoleCommand");
			NewBlock.playerCommand = Main.customBlocksConfig.getString("Blocks.List." + i + ".PlayerCommand");
			NewBlock.delay = Main.customBlocksConfig.getInt("Blocks.List." + i + ".Cooldown");
			NewBlock.players = Main.customBlocksConfig.getStringList("Blocks.List." + i + ".Players");
			NewBlock.particle = Main.customBlocksConfig.getColor("Blocks.List." + i + ".Particle");
			NewBlock.name = Main.customBlocksConfig.getString("Blocks.List." + i + ".Name");
			NewBlock.size = Main.customBlocksConfig.getDouble("Blocks.List." + i + ".Size");
			all.add(NewBlock);
		}
		}
		return all;
	}
	public static CommandBlock getCustomBlock(Location l) {
		if(l.getWorld().getName().equalsIgnoreCase(firstEnable.config.getString("loc.world"))) {
		if(Main.customBlocksConfig.contains("Blocks.List")) {
		for (String i : Main.customBlocksConfig.getConfigurationSection("Blocks.List").getKeys(false)) {
			String[] args = Main.customBlocksConfig.getString("Blocks.List." + i + ".Location").split(":");
			CommandBlock NewBlock = new CommandBlock();
			NewBlock.location = new Location(Bukkit.getWorld(args[3]), Double.parseDouble(args[0]),
					Double.parseDouble(args[1]), Double.parseDouble(args[2]));
			NewBlock.name =Main.customBlocksConfig.getString("Blocks.List." + i + ".Name");
			NewBlock.consoleCommand = Main.customBlocksConfig.getString("Blocks.List." + i + ".ConsoleCommand");
			NewBlock.playerCommand = Main.customBlocksConfig.getString("Blocks.List." + i + ".PlayerCommand");
			NewBlock.delay = Main.customBlocksConfig.getInt("Blocks.List." + i + ".Cooldown");
			NewBlock.players = Main.customBlocksConfig.getStringList("Blocks.List." + i + ".Players");
			NewBlock.particle = Main.customBlocksConfig.getColor("Blocks.List." + i + ".Particle");
			NewBlock.size = Main.customBlocksConfig.getDouble("Blocks.List." + i + ".Size");
			if (NewBlock.location.distance(l) < 1) {
				return NewBlock;
			}
		}
		}
		}
		return null;
	}

	public static void ClickBlockCommand(PlayerInteractEvent event) {
		if (event.getHand() != null) {
			EquipmentSlot hand = event.getHand(); // Get the hand of the event and make it the variable hand'.
			if (hand.equals(EquipmentSlot.HAND)) { // If the event is fired by HAND (main hand)

				if (event.getClickedBlock() != null) {
					CommandBlock clicked = getCustomBlock(event.getClickedBlock().getLocation());
					if (clicked != null) {
						HashMap<String, Long> cooldown = new HashMap<String, Long>();
						for (String rawData : clicked.players) {
							String[] raw = rawData.split(":");
							cooldown.put(raw[0].toString(), Long.valueOf(raw[1]));
						}
						if (Cooldowns.isCooldownOver(cooldown, clicked.delay, event.getPlayer())) {
							List<String> hashmapData = new ArrayList<String>();
							for (String stuff : cooldown.keySet()) {
								String data = stuff + ":" + cooldown.get(stuff);
								hashmapData.add(data);
							}
							Main.customBlocksConfig.set("Blocks.List." + clicked.name + ".Players", hashmapData);
							Main.saveCustomBlocks();
							if (!clicked.playerCommand.contains("/none/")
									|| !clicked.consoleCommand.contains("/none/")) {
								if (!clicked.playerCommand.contains("/none/")) {
									Bukkit.dispatchCommand(event.getPlayer(), clicked.playerCommand);
								}
								if (!clicked.consoleCommand.contains("/none/")) {
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
											clicked.consoleCommand.replace("%player%", event.getPlayer().getName()));
								}
							}
						}

					}
				}
			}
		}
	}

	@Override
	public void run() {
		for(CommandBlock newblock:getAllCustomBlocks()) {
			if(newblock.particle!=null) {
		Particles.createParticleHelix(Main.getBlockCenter(newblock.location.getBlock()),newblock.particle,newblock.size, .1);
			}
		}
	}
	public static void RemoveBlock(Location location) {
		CommandBlock looked = getCustomBlock(location);
		if(looked!=null) {
		Main.customBlocksConfig.set("Blocks.List."+looked.name,null);
		Main.saveCustomBlocks();
		}
	}

}


