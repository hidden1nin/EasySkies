package me.bukkit.hidden1nin.easySkies;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftEntity;
import org.bukkit.entity.Bee;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.PolarBear;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Slime;
import org.bukkit.entity.TropicalFish;
import org.bukkit.entity.Villager;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_15_R1.EntityInsentient;
import net.minecraft.server.v1_15_R1.PathEntity;

public class Pets implements Listener, Runnable{
public static FileConfiguration config = Main.getPlugin().getConfig();
public static HashMap<String, Entity> Pets = new HashMap<String, Entity>();
public static HashMap<Material, String> clickedperms = new HashMap<Material, String>();
@EventHandler
public void onEntityTarget(EntityTargetEvent event){
        if (event.getEntity() instanceof Monster&&Pets.containsValue(event.getEntity())){
            event.setCancelled(true);
            return;
        }
}

public static void OpenPets(Player p) {
	Inventory EasySkyPets = Bukkit.createInventory(null, 54, Main.server + "Pets");
	Main.borderDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE,EasySkyPets,ChatColor.GREEN+" ","");
	Main.createDisplay(Material.BARRIER, EasySkyPets, 49,ChatColor.RED+"Remove Pet", "Click to remove pet");
	loresetter(p, Material.PIG_SPAWN_EGG, "Piggle!",  "a tiny Cheerful little piggy", "Gives Raw Pork!", "permission.easySkies.pets.babypig", EasySkyPets);
	loresetter(p, Material.COW_SPAWN_EGG, "Coggle!",  "a humble little cow", "Carries a crafting bench!", "permission.easySkies.pets.babycow", EasySkyPets);
	loresetter(p, Material.SHEEP_SPAWN_EGG, "Sheeple <3", " a mature loving sheep", "none", "permission.easySkies.pets.babysheep", EasySkyPets);
	loresetter(p, Material.PINK_WOOL, "Rare Sheeple!", "a majestic creature forged with love", "none", "permission.easySkies.pets.rarebabysheep", EasySkyPets);
	loresetter(p, Material.EGG, "Chiggle", "a small chickadee", "Right click for an egg!", "permission.easySkies.pets.babychicken", EasySkyPets);
	loresetter(p, Material.BOOK, "Villagoo", "a small fan with a big heart!", "Opens shop when clicked!", "permission.easySkies.pets.babyvillager", EasySkyPets);
	loresetter(p, Material.POLAR_BEAR_SPAWN_EGG, "Rare Poggle Bore", "he has a mighty roar!", "Gives piggy back rides!", "permission.easySkies.pets.babypolarbear", EasySkyPets);
	loresetter(p, Material.SOUL_SAND, "Ultra Rare Wittle Skelly", "It must be a ghost!", "Small chance to drop wither skulls!", "permission.easySkies.pets.witherskeleton", EasySkyPets);
	loresetter(p, Material.ZOMBIE_HEAD, "Rare Zobble", "It looks so cute!", "Gives Rotten flesh", "permission.easySkies.pets.babyzombie", EasySkyPets);
	loresetter(p, Material.PURPLE_WOOL, "Endeep", "Really Radical, Totes Nar Nar bro", "Opens enderchest", "permission.easySkies.pets.enderbabysheep", EasySkyPets);
	loresetter(p, Material.HONEYCOMB, "BooBee", "OMG its so tiny!", "restores hunger", "permission.easySkies.pets.babybee", EasySkyPets);
	loresetter(p, Material.SLIME_BALL, "Sliggle Glop!", "Melts your in your mouth but not in your hands!", "gives jump boost!", "permission.easySkies.pets.babyslime", EasySkyPets);
	loresetter(p, Material.COD, "Figgle!", "Flop Flop Flop!!", "Gives Night Vision!", "permission.easySkies.pets.fish", EasySkyPets);

	p.openInventory(EasySkyPets);
}
public static void loresetter(Player p,Material material,String name,String lore,String ability,String permission, Inventory i) {
	clickedperms.put(material, permission);
	if(ability.equalsIgnoreCase("none")) {
		ability="";
	}
	if(p.hasPermission(permission)||p.hasPermission("permission.easySkies.pets.allpets")) {
		Main.createDisplay(material, i, -1, ChatColor.GREEN+name, lore+"/line//line/"+ChatColor.GREEN+ability+"/line/"+ChatColor.GREEN+"Unlocked");
	}else {
		Main.createDisplay(material, i, -1, ChatColor.RED+name, lore+"/line//line/"+ChatColor.GREEN+ability+"/line/"+ChatColor.RED+"Locked");
	}
}
	@Override
	public void run() {

		for(Player p:Bukkit.getOnlinePlayers()) {
			
			if(Pets.containsKey(p.getName())) {
				Entity pet = Pets.get(p.getName());
				if(!pet.isValid()) {
					removepet(p);
				}
				if(pet.getWorld()!=p.getWorld()) {
					removepet(p);
				}
				if(pet.isDead()) {
					removepet(p);
				}
				Object petObject =((CraftEntity) pet).getHandle();
				PathEntity path;
				Location loc = p.getLocation();
				
				path = ((EntityInsentient)petObject).getNavigation().a(loc.getX()+1,loc.getY(),loc.getZ(), 2);
				if(path!=null) {
					((EntityInsentient)petObject).getNavigation().a(path,2.0D);
				}
				double Distance = loc.distance(pet.getLocation());
				if(Distance>10&&!pet.isDead()&& p.isOnGround()) {
					pet.teleport(p);
				}
			}else{
				if(!Cooldowns.silentIsCooldownOver(Cooldowns.petSpawnCoolDown, 5, p)) {return;}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("none")) {return;}else {
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babypig")&&Pets.containsKey(p.getName())==false) {
					Pig pet =(Pig) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.PIG);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.LIGHT_PURPLE+p.getName()+"'s Piggle");
					pet.setInvulnerable(true);
					pet.setAgeLock(true);
					pet.setBreed(false);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("fish")&&Pets.containsKey(p.getName())==false) {
					TropicalFish pet =(TropicalFish) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.TROPICAL_FISH);
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.AQUA+p.getName()+"'s Figgle");
					pet.setInvulnerable(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babycow")&&Pets.containsKey(p.getName())==false) {
					Cow pet =(Cow) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.COW);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.AQUA+p.getName()+"'s Coggle");
					pet.setInvulnerable(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babybee")&&Pets.containsKey(p.getName())==false) {
					Bee pet =(Bee) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.BEE);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.YELLOW+p.getName()+"'s BooBee");
					pet.setInvulnerable(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babychicken")&&Pets.containsKey(p.getName())==false) {
					Chicken pet =(Chicken) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.CHICKEN);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.AQUA+p.getName()+"'s chiggle");
					pet.setInvulnerable(true);
					pet.setAgeLock(true);
					pet.setBreed(false);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babysheep")&&Pets.containsKey(p.getName())==false) {
					Sheep pet =(Sheep) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SHEEP);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					pet.setColor(DyeColor.WHITE);
					pet.setCustomName(ChatColor.BLUE+p.getName()+"'s sheeple");
					pet.setInvulnerable(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babyvillager")&&Pets.containsKey(p.getName())==false) {
					Villager pet =(Villager) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.VILLAGER);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					pet.setCustomName(ChatColor.LIGHT_PURPLE+p.getName()+"'s villagoo");
					pet.setInvulnerable(true);
					pet.getEquipment().setHelmet(p.getInventory().getHelmet());
					pet.getEquipment().setHelmetDropChance(0);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babypolarbear")&&Pets.containsKey(p.getName())==false) {
					PolarBear pet =(PolarBear) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.POLAR_BEAR);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					pet.setCustomName(rarify(ChatColor.YELLOW+p.getName()+"'s poggle bore"));
					pet.setInvulnerable(true);
					pet.getEquipment().setHelmet(p.getInventory().getHelmet());
					pet.getEquipment().setHelmetDropChance(0);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babyslime")&&Pets.containsKey(p.getName())==false) {
					Slime pet =(Slime) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SLIME);
					pet.setSize(1);
					pet.setCustomNameVisible(true);
					pet.setCustomName(rarify(ChatColor.GREEN+p.getName()+"'s Sliggle Glop"));
					pet.setInvulnerable(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("witherskeleton")&&Pets.containsKey(p.getName())==false) {
					WitherSkeleton pet =(WitherSkeleton) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.WITHER_SKELETON);
					pet.setCustomNameVisible(true);
					pet.setCustomName(ChatColor.AQUA+""+ChatColor.MAGIC+"#"+ChatColor.RESET+ChatColor.LIGHT_PURPLE+p.getName()+"'s wittle skelly"+ChatColor.AQUA+ChatColor.MAGIC+"#");
					pet.setInvulnerable(true);
					pet.getEquipment().setHelmet(Main.createSkull(p.getName(), null, -2, "OOOF", "Howd you get this???"));
					pet.getEquipment().setHelmetDropChance(0);
					pet.getEquipment().setChestplate(p.getInventory().getChestplate());
					pet.getEquipment().setChestplateDropChance(0);
					pet.getEquipment().setLeggings(p.getInventory().getLeggings());
					pet.getEquipment().setLeggingsDropChance(0);
					pet.getEquipment().setBoots(p.getInventory().getBoots());
					pet.getEquipment().setBootsDropChance(0);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("babyzombie")&&Pets.containsKey(p.getName())==false) {
					Zombie pet =(Zombie) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.ZOMBIE);
					pet.setCustomNameVisible(true);
					pet.setBaby(true);
					pet.setCustomName(ChatColor.AQUA+""+ChatColor.MAGIC+"#"+ChatColor.RESET+ChatColor.LIGHT_PURPLE+p.getName()+"'s zobble"+ChatColor.AQUA+ChatColor.MAGIC+"#");
					pet.setInvulnerable(true);
					pet.getEquipment().setHelmet(Main.createSkull(p.getName(), null, -2, "OOOF", "Howd you get this???"));
					pet.getEquipment().setHelmetDropChance(0);
					pet.getEquipment().setChestplate(p.getInventory().getChestplate());
					pet.getEquipment().setChestplateDropChance(0);
					pet.getEquipment().setLeggings(p.getInventory().getLeggings());
					pet.getEquipment().setLeggingsDropChance(0);
					pet.getEquipment().setBoots(p.getInventory().getBoots());
					pet.getEquipment().setBootsDropChance(0);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("rarebabysheep")&&Pets.containsKey(p.getName())==false) {
					Sheep pet =(Sheep) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SHEEP);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					pet.setColor(DyeColor.PINK);
					pet.setCustomName(ChatColor.AQUA+""+ChatColor.MAGIC+"#"+ChatColor.RESET+ChatColor.LIGHT_PURPLE+p.getName()+"'s sheeple"+ChatColor.AQUA+ChatColor.MAGIC+"#");
					pet.setInvulnerable(true);
					Pets.put(p.getName(), pet);
				}
				if(firstEnable.config.getString("IslandOwnerSettings."+p.getName()+".Pet").equalsIgnoreCase("enderbabysheep")&&Pets.containsKey(p.getName())==false) {
					Sheep pet =(Sheep) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.SHEEP);
					pet.setBaby();
					pet.setCustomNameVisible(true);
					pet.setBreed(false);
					pet.setAgeLock(true);
					pet.setColor(DyeColor.PURPLE);
					pet.setCustomName(ChatColor.AQUA+""+ChatColor.MAGIC+"#"+ChatColor.RESET+ChatColor.LIGHT_PURPLE+p.getName()+"'s endeep"+ChatColor.AQUA+ChatColor.MAGIC+"#");
					pet.setInvulnerable(true);
					Pets.put(p.getName(), pet);
				}
				p.getLocation().getWorld().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 10);
			}
			}
		}
		
	}
	public static void clickpet(InventoryClickEvent event) {
		Player p = (Player) event.getWhoClicked();
		if(event.getCurrentItem().getType()==Material.LIGHT_BLUE_STAINED_GLASS_PANE) {return;}
		if(event.getCurrentItem().getType()==Material.BARRIER) {config.set("IslandOwnerSettings."+p.getName()+".Pet", "none"); removepet(p); return;}
		if(clickedperms.containsKey(event.getCurrentItem().getType())) {
			if(p.hasPermission(clickedperms.get(event.getCurrentItem().getType()))||p.hasPermission("permission.easySkies.pets.allpets")) {
				config.set("IslandOwnerSettings."+p.getName()+".Pet",clickedperms.get(event.getCurrentItem().getType()).replace("permission.easySkies.pets.", "")); 
				removepet(p);
				Main.getPlugin().saveConfig();
			}
		}
		
	}
	public static void removepet(Player p) {
		if(Pets.get(p.getName())!=null) {
		Pets.get(p.getName()).getLocation().getChunk().setForceLoaded(true);
		Pets.get(p.getName()).remove();
		Pets.get(p.getName()).getLocation().getChunk().setForceLoaded(false);
		Pets.remove(p.getName());
		}
		p.closeInventory();
	}
	@EventHandler
	public static void abilitites(PlayerInteractEntityEvent event) {
		if(event.getHand()==EquipmentSlot.OFF_HAND) {return;}
		Player p = event.getPlayer();
		if(p.isSneaking()) {return;}
				if(Pets.get(p.getName())!=null) {
		if(event.getRightClicked()==Pets.get(p.getName())) {
			Entity e = event.getRightClicked();
			if(e instanceof Slime&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 60, p)) {
				Slime entity=(Slime) e;
				if(entity.getSize()==1) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 10*20, 1));
				}
				}
			if(e instanceof TropicalFish&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 60, p)) {
				p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 30*20, 1));
				}
			if(e instanceof Pig&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 60, p)) {
				p.getInventory().addItem(Main.createDisplay(Material.COOKED_PORKCHOP, null, -2,ChatColor.LIGHT_PURPLE+"Fresh Pork", "Right off the piggle"));
			}
			if(e instanceof Zombie&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 60, p)) {
				p.getInventory().addItem(Main.createDisplay(Material.ROTTEN_FLESH, null, -2,ChatColor.LIGHT_PURPLE+"Rotten Flesh", "Probably useless"));
			}
			if(e instanceof WitherSkeleton&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 600, p)) {
				if(Math.random()*100<=1) {
				p.getInventory().addItem(Main.createDisplay(Material.WITHER_SKELETON_SKULL, null, -2,ChatColor.AQUA+"Wither Skull", ChatColor.RED+"Ultra Rare"));
				}else {
					p.getInventory().addItem(Main.createDisplay(Material.BONE, null, -2,ChatColor.AQUA+"Wither Bone","Try Again..."));	
				}
			}
			if(e instanceof Sheep&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 1, p)) {
				Sheep s =(Sheep) e;
				if(s.getColor()==DyeColor.PURPLE) {
					p.openInventory(p.getEnderChest());
				}
			}
			if(e instanceof Bee&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 60, p)) {
				p.setFoodLevel(20);
			}
			if(e instanceof Chicken&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 120, p)) {
				p.getInventory().addItem(Main.createDisplay(Material.EGG, null, -2,ChatColor.LIGHT_PURPLE+"Dirty Egg", "Where has this been!?"));
			}
			if(e instanceof Cow&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 1, p)) {
				p.openWorkbench(p.getLocation(), true);
			}
			if(e instanceof Villager&&Cooldowns.isCooldownOver(Cooldowns.MobAbilityDown, 1, p)) {
				p.performCommand("auction");
			}
		}
	}
	}

   public static String rarify(String s) {
	   if(s==null) {
	return s;
	   }
	   String newstring= ChatColor.AQUA+""+ChatColor.MAGIC+"#"+ChatColor.RESET+s+ChatColor.AQUA+""+ChatColor.MAGIC+"#";
	   return newstring;
   }

}
