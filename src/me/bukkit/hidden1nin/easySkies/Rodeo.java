package me.bukkit.hidden1nin.easySkies;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Rodeo {

	public static void start(PlayerQueue q) {
    	for (Iterator<String> iter = q.players.iterator(); iter.hasNext();) {
    	    String p = iter.next();
    	   if(Bukkit.getPlayer(p)!=null) {
    		Player pl =   Bukkit.getPlayer(p);
    	   Horse h = (Horse) pl.getWorld().spawnEntity(q.startLocation, EntityType.HORSE);
    	   h.setPassenger(pl);
    	   h.setTamed(true);
    	   h.getInventory().addItem(Main.createDisplay(Material.SADDLE, null, -2,"&a&lRodeo Saddle", "&7"));
    	   }else {
    		   q.players.remove(p);
    		   Queue.QueuePlayers.remove(p);
    	   }   
    	   }
    	   }
		

}
