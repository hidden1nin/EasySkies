package me.bukkit.hidden1nin.easySkies;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Cooldowns {
public static HashMap<String, Long> niceCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> naughtyCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> bandageCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> presentCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> lovedCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> blockCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> mobCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> largebandageCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> PotatoCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> CombatCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> MobAbilityDown = new HashMap<String, Long>();
public static HashMap<String, Long> GrowCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> SuckCoolDown = new HashMap<String, Long>();
public static HashMap<String, Long> petSpawnCoolDown=new HashMap<String, Long>();
public static Boolean isCooldownOver(HashMap<String, Long> cooldown,int time,Player player) {
	if(cooldown.containsKey(player.getName())) {
		if(time == -1) {
			sendActionBarMessage(player,ChatColor.RED+"You cannot do that again!");
			return false;
		}
		long secondsleft = ((cooldown.get(player.getName())/1000)+time)-(System.currentTimeMillis()/1000);
		if(secondsleft>0) {
			sendActionBarMessage(player,ChatColor.RED+"You cannot do that for "+secondsleft+" seconds!");
			return false;
		}else {
			cooldown.remove(player.getName());
		}
	}else {
	cooldown.put(player.getName(), System.currentTimeMillis());
	return true;	
	}
	return true;
	
}
public static Boolean silentIsCooldownOver(HashMap<String, Long> cooldown,int time,Player player) {
	if(cooldown.containsKey(player.getName())) {
		if(time == -1) {
			return false;
		}
		long secondsleft = ((cooldown.get(player.getName())/1000)+time)-(System.currentTimeMillis()/1000);
		if(secondsleft>0) {
			return false;
		}else {
			cooldown.remove(player.getName());
		}
	}else {
	cooldown.put(player.getName(), System.currentTimeMillis());
	return true;	
	}
	return true;
	
}
public static Boolean silentIsCooldownOverNoUpdate(HashMap<String, Long> cooldown,int time,Player player) {
	if(cooldown.containsKey(player.getName())) {
		if(time == -1) {
			return false;
		}
		long secondsleft = ((cooldown.get(player.getName())/1000)+time)-(System.currentTimeMillis()/1000);
		if(secondsleft>0) {
			return false;
		}else {

		}
	}else {
	cooldown.put(player.getName(), System.currentTimeMillis());
	return false;	
	}
	return true;
	
}
public static long SecondsLeftCooldownOver(HashMap<String, Long> cooldown,int time,Player player) {
	if(cooldown.containsKey(player.getName())) {
		if(time == -1) {
			return 100000;
		}
		long secondsleft = ((cooldown.get(player.getName())/1000)+time)-(System.currentTimeMillis()/1000);
		if(secondsleft>0) {
			return secondsleft;
		}else {
			cooldown.remove(player.getName());
		}
	}else {
	cooldown.put(player.getName(), System.currentTimeMillis());
	return 0;	
	}
	return 0;
	
}
public static HashMap<String,Long> isCooldownOverHash(HashMap<String, Long> cooldown,int time,Player player) {
	if(cooldown.containsKey(player.getName())) {
		long secondsleft = ((cooldown.get(player.getName())/1000)+time)-(System.currentTimeMillis()/1000);
		if(secondsleft>0) {
			return cooldown;
		}else {
			cooldown.remove(player.getName());
		}
	}else {
	cooldown.put(player.getName(), System.currentTimeMillis());
	return cooldown;	
	}
	return cooldown;
	
}
public static void sendActionBarMessage(Player player,String message) {
      player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
}
}
