package me.bukkit.hidden1nin.easySkies;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Bounties implements Listener{
	@EventHandler
	public void onKill(PlayerDeathEvent e)
	{
		if(e.getEntity().getKiller() != null) {
		if(e.getEntity()==e.getEntity().getKiller()) {return;}
	String killed = e.getEntity().getName();
	String killer = e.getEntity().getKiller().getName();
	if(Main.PlayerBounties.containsKey(killed) && killer != killed)
	{
	e.setDeathMessage(Main.server+ChatColor.RED + killer + " Has claimed the bounty on "+killed);
	e.getEntity().getKiller().getLocation().getWorld().dropItemNaturally(e.getEntity().getKiller().getLocation(),Main.PlayerBounties.get(killed));
	Main.BountyGui.remove(Main.PlayerBounties.get(killed));
	Main.PlayerBounties.remove(killed);
	}
	}
	}
}
