package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import net.milkbowl.vault.economy.Economy;

public class reset {
public static FileConfiguration config = Main.getPlugin().getConfig();
public static void islandReset(Player p) {
	if(firstEnable.IslandOwners.contains(p.getName())) {
	firstEnable.IslandOwners.remove(p.getName());
	config.set("IslandOwners",firstEnable.IslandOwners );
	setDefaults(p);
	Main.getPlugin().saveConfig();
	Main.savePlayerShops();
	p.getEnderChest().clear();
	p.getInventory().clear();
	firstEnable.PlayerHomes.remove(p.getName());
	List<String> hashmapData = config.getStringList("PlayerHomes");
	  for(String rawData : config.getStringList("PlayerHomes")) {
		  
	        String[] raw = rawData.split(":");
	        if (raw[0].equals(p.getName())) {
	        	hashmapData.remove(rawData);
	        }
	  
	  }
    
    config.set("PlayerHomes", hashmapData);
    Main.getPlugin().saveConfig();
    for(String rawData : config.getStringList("PlayerHomes")) {

        String[] raw = rawData.split(":");
        firstEnable.PlayerHomes.put(raw[0].toString(), new Location(Bukkit.getWorld(raw[1]),Double.valueOf(raw[2]),Double.valueOf(raw[3]),Double.valueOf(raw[4])));

    }

}else {
	p.sendMessage(Main.server+"You don't own an island!");
}
}
public static void setDefaults(Player player) {
	Money.setMoney(player.getName(), String.valueOf(0));
	if (config.contains("IslandOwnerSettings." + player.getName() + ".Home")) {
		config.addDefault("IslandOwnerSettings." + player.getName() + ".Home", "");
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Public")) {
		config.set("IslandOwnerSettings."+player.getName()+".Public",false); 
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".MobSpawn")) {
		config.set("IslandOwnerSettings."+player.getName()+".MobSpawn",true); 
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Pvp")) {
		config.set("IslandOwnerSettings."+player.getName()+".Pvp",false); 
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Coop")) {
		config.set("IslandOwnerSettings."+player.getName()+".Coop", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".CompletedQuests")) {
		config.addDefault("IslandOwnerSettings."+player.getName()+".CompletedQuests",new ArrayList<String>());
		Main.getPlugin().saveConfig();
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".MembersAllowed")) {
		config.set("IslandOwnerSettings."+player.getName()+".MembersAllowed",config.getInt("IslandOwnerSettings.DefaultMembersAllowed"));
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Size")) {
		config.set("IslandOwnerSettings."+player.getName()+".Size", config.getInt("IslandOwnerSettings.DefaultIslandSize"));
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".IslandRank")) {
		config.set("IslandOwnerSettings."+player.getName()+".IslandRank", 0);
		List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
		
		if(!players.contains(player.getName()+":"+IslandInfo.getIslandLevel(firstEnable.PlayerHomes.get(player.getName())))){
			
			}else {
				players.remove(player.getName()+":"+config.getInt("IslandOwnerSettings."+player.getName()+".IslandLevel"));
			}
		Main.saveBlockValues();
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".IslandLevel")) {
		config.set("IslandOwnerSettings."+player.getName()+".IslandLevel", Main.islandBlockValueConfig.getInt("Blocks.StartingLevel"));
		Main.getPlugin().saveConfig(); 
	}
	
	if(config.contains("IslandOwnerSettings."+player.getName()+".GenTier")) {
		config.set("IslandOwnerSettings."+player.getName()+".GenTier",0);
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".MineTier")) {
		config.set("IslandOwnerSettings."+player.getName()+".MineTier",0);
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".UnlockedGenTier")) {
		config.set("IslandOwnerSettings."+player.getName()+".UnlockedGenTier",new ArrayList<String>());
		List<String> Unlocked = config.getStringList("IslandOwnerSettings.DefaultUnlockedGen");
		config.set("IslandOwnerSettings."+player.getName()+".UnlockedGenTier", Unlocked);
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".ResetTokens")) {
		config.set("IslandOwnerSettings."+player.getName()+".ResetTokens",config.getInt("IslandOwnerSettings."+player.getName()+".ResetTokens")-1);
		Main.getPlugin().saveConfig(); 
		player.sendMessage(Main.server+ " you now have "+config.getInt("IslandOwnerSettings."+player.getName()+".ResetTokens")+" Reset Tokens");
		
	}
	////////////shops//////////
	if(Main.playerShopConfig.contains("ShopOwnerData."+player.getName()+".ShopSize")) {
		Main.playerShopConfig.options().copyDefaults(true);
		Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".ShopSize", Main.playerShopConfig.getInt("Default.ShopSize"));
		Main.savePlayerShops();
	}
	if(Main.playerShopConfig.contains("ShopOwnerData."+player.getName()+".Balance")) {
		Main.playerShopConfig.options().copyDefaults(true);
		Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".Balance", Main.playerShopConfig.getInt("Default.Balance"));
		Main.savePlayerShops();
	}
	if(Main.playerShopConfig.contains("ShopOwnerData."+player.getName()+".Selling")) {
		Main.playerShopConfig.options().copyDefaults(true);
		Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".Selling",new ArrayList<String>());
		Main.savePlayerShops();
	}
	/////////////////////statistics////////////////
	if(config.contains("IslandOwnerSettings."+player.getName()+".Stats.BlocksBroken")) {
		config.set("IslandOwnerSettings."+player.getName()+".Stats.BlocksBroken",0);
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Stats.BlocksPlaced")) {
		config.set("IslandOwnerSettings."+player.getName()+".Stats.BlocksPlaced",0);
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Stats.MobsKilled")) {
		config.set("IslandOwnerSettings."+player.getName()+".Stats.MobsKilled", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Stats.MobsTamed")) {
		config.set("IslandOwnerSettings."+player.getName()+".Stats.MobsTamed", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".Stats.MobsBred")) {
		config.set("IslandOwnerSettings."+player.getName()+".Stats.MobsBred", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".CompletedQuests")) {
		config.set("IslandOwnerSettings."+player.getName()+".CompletedQuests", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	///////////////////////////////////custom-blocks///////////////
	if(config.contains("IslandOwnerSettings."+player.getName()+".CustomBlocks.ItemCollectors")) {
		config.set("IslandOwnerSettings."+player.getName()+".CustomBlocks.ItemCollectors", new ArrayList<String>());
		Main.getPlugin().saveConfig();  
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".CustomBlocks.Storage")) {
		config.set("IslandOwnerSettings."+player.getName()+".CustomBlocks.Storage", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".CustomBlocks.VoidCollector")) {
		config.set("IslandOwnerSettings."+player.getName()+".CustomBlocks.VoidCollector", new ArrayList<String>());
		Main.getPlugin().saveConfig(); 
	}
	if(config.contains("IslandOwnerSettings."+player.getName()+".CustomBlocks.MobSpawner")) {
		config.set("IslandOwnerSettings."+player.getName()+".CustomBlocks.MobSpawner", new ArrayList<String>());
		Main.getPlugin().saveConfig();  
	}
	player.sendMessage(Main.server+"Returning to spawn");
	player.teleport(firstEnable.Spawn);
}

}
