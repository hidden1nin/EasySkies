package me.bukkit.hidden1nin.easySkies;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class DeathRespawn implements Listener{
	public HashMap<Player , ItemStack[]> items = new HashMap<Player , ItemStack[]>();
	@EventHandler()
    public void onRespawn(PlayerRespawnEvent event){
		
        if(items.containsKey(event.getPlayer())){
            event.getPlayer().getInventory().clear();
            for(ItemStack stack : items.get(event.getPlayer())){
                if(stack != null ){
                    event.getPlayer().getInventory().addItem(stack);
                }
            }
 
            items.remove(event.getPlayer());
           
        }
    }
 
    @EventHandler()
    public void onDeath(PlayerDeathEvent event){
    	Cooldowns.CombatCoolDown.remove(event.getEntity().getName());
    	ItemStack DeathRune=Main.createDisplay(Material.ENDER_PEARL, Main.EasySkyCustomRecipes, -2,ChatColor.GRAY+"Death"+ChatColor.RED+" Rune", "Keep items after death when in inventory");
    	Boolean KeepItems = false;
    	for(ItemStack Stack:event.getEntity().getInventory().getContents()) {
    		if( Stack!=null) {
    			if(Stack.isSimilar(DeathRune)) {
    			Stack.setAmount(Stack.getAmount()-1);
    			ItemStack[] content = event.getEntity().getInventory().getContents();
    	        items.put(event.getEntity(), content);
    	        KeepItems = true;
    	        event.getEntity().getInventory().clear();
    	        event.getDrops().clear();
    	       
    	        return;
    		}
    			}
    	}
    	if(!KeepItems) {
    		ItemStack[] content = event.getEntity().getInventory().getContents();
    			for(ItemStack i : content) {
    				if( i!=null) {
    				event.getEntity().getLocation().getWorld().dropItemNaturally(event.getEntity().getLocation(),i);
    			}}
    			event.getEntity().getInventory().clear();
    		
    	}
    	
        
    }
}
