package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Shops {
public static void OpenShop(Player player, String args) {
	Inventory EasySkyAdminShop = Bukkit.createInventory(null,54,Main.server+"Admin Shops");
	Main.borderDisplay(Material.getMaterial(Main.shopConfig.getString("Default.Border").toUpperCase()), EasySkyAdminShop, " ", " ");
	Main.fillDisplay(Material.getMaterial(Main.shopConfig.getString("Default.Background").toUpperCase()), EasySkyAdminShop, " ", " ");
	if(Main.shopConfig.contains(args)) {
	for(String item : Main.shopConfig.getStringList(args)) {
		if(Material.getMaterial(item.split(":")[0].toUpperCase())!= null) {
		Main.createDisplay(Material.getMaterial(item.split(":")[0].toUpperCase()),EasySkyAdminShop, Integer.valueOf(item.split(":")[3]), ChatColor.AQUA+""+item.split(":")[0].toLowerCase().replace("_", " ").replace("-", " "), ChatColor.GREEN+"(L) Buy: "+ChatColor.GOLD+item.split(":")[1]+"/line/"+ChatColor.GREEN+"(R) Sell: "+ChatColor.GOLD+item.split(":")[2]);
		}else {
			Main.createDisplay(Material.SPONGE,EasySkyAdminShop, Integer.valueOf(item.split(":")[3]), ChatColor.AQUA+""+item.split(":")[0].toLowerCase().replace("_", " ").replace("-", " ")+" is null", ChatColor.GREEN+"(L) Buy: "+ChatColor.GOLD+item.split(":")[1]+"/line/"+ChatColor.GREEN+"(R) Sell: "+ChatColor.GOLD+item.split(":")[2]);
			
		}
		}
	}else {
		for(String item : Main.shopConfig.getStringList("DefaultShop")) {
			if(Material.getMaterial(item.split(":")[0].toUpperCase())!= null) {
			Main.createDisplay(Material.getMaterial(item.split(":")[0].toUpperCase()),EasySkyAdminShop, Integer.valueOf(item.split(":")[3]), ChatColor.AQUA+""+item.split(":")[0].toLowerCase().replace("_", " ").replace("-", " "), ChatColor.GREEN+"(L) Buy: "+ChatColor.GOLD+item.split(":")[1]+"/line/"+ChatColor.GREEN+"(R) Sell: "+ChatColor.GOLD+item.split(":")[2]);
			}else {
				Main.createDisplay(Material.SPONGE,EasySkyAdminShop, Integer.valueOf(item.split(":")[3]), ChatColor.AQUA+""+item.split(":")[0].toLowerCase().replace("_", " ").replace("-", " ")+" is null", ChatColor.GREEN+"(L) Buy: "+ChatColor.GOLD+item.split(":")[1]+"/line/"+ChatColor.GREEN+"(R) Sell: "+ChatColor.GOLD+item.split(":")[2]);
				
			}
			}	
	}
	player.openInventory(EasySkyAdminShop);
}

public static void ClickEvent(InventoryClickEvent event) {
	event.setCancelled(true);
	Player player =(Player) event.getWhoClicked();
	ItemStack Clicked =event.getCurrentItem();
	if(Clicked.getType()==Material.getMaterial(Main.shopConfig.getString("Default.Background").toUpperCase())||Clicked.getType()==Material.getMaterial(Main.shopConfig.getString("Default.Border").toUpperCase())) {
	return;
	}
	Material mat = Material.AIR;
	String buy = "n/a";
	String sell = "n/a";
	for(String item :Main.shopConfig.getStringList("Items")) {
		 String[] raw = item.split(":");
		 mat = Material.getMaterial(raw[0].toUpperCase());
		 buy = raw[1];
		 sell = raw[2];
		 if(mat ==Clicked.getType()) {break;}
	}

	if(event.isLeftClick()) {
		if(buy.equalsIgnoreCase("n/a")) {
			player.closeInventory();
			player.sendMessage(Main.server+"You can't buy that");
			return;
		}
		if(player.getInventory().firstEmpty()>-1) {
			
				
		if(Money.playerBalance(player.getName())>=Integer.valueOf(buy)) {
			Money.takeMoney(player.getName(), buy);
			player.getInventory().addItem(new ItemStack(mat, 1));
			 player.sendMessage(Main.server+"Bought "+Clicked.getType().toString().toLowerCase().replace("_", " ").replace("-", " "));
		      
		}else {
			player.closeInventory();
			player.sendMessage(Main.server+"That is to expensive!");
		}
		}else {
			player.closeInventory();
			player.sendMessage(Main.server+"Full inventory!");
		}
		return;
	}
	if(event.isRightClick()) {
		if(sell.equalsIgnoreCase("n/a")) {
			player.closeInventory();
			player.sendMessage(Main.server+"You can't sell that");
			return;
		}
		int i = 0;
		for (ItemStack stack : player.getInventory().getContents()) {
			
			if (stack!=null&&stack.getType() == Clicked.getType() && stack.getAmount() >= 0) {
		        Money.giveMoney(player.getName(), sell);
		        ItemStack m = new ItemStack(mat, 1);
		        player.getInventory().removeItem(m);
		        player.updateInventory();
		        player.sendMessage(Main.server+"Sold "+Clicked.getType().toString().toLowerCase().replace("_", " ").replace("-", " "));
		        i = 1;
		        break;
		    	}
		}
		if(i !=1) {
			player.closeInventory();
			player.sendMessage(Main.server+"You don't have any to sell! ");
		}
		return;
	}
}
}
