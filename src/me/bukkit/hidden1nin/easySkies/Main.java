package me.bukkit.hidden1nin.easySkies;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Statistic;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Powerable;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Shulker;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;



public class Main extends JavaPlugin implements Listener {
	Plugin pluginname = Bukkit.getServer().getPluginManager().getPlugin("EasySkies");
	public static File customMobDrops;
	static FileConfiguration customMobDropsConfig;
	public static File customBlocks;
	static FileConfiguration customBlocksConfig;
	public static File shops;
	static FileConfiguration shopConfig;
	public static File scoreBoard;
	static FileConfiguration scoreConfig;
	public static File playerShops;
	static FileConfiguration playerShopConfig;
	public static File islandBlockValue;
	static FileConfiguration islandBlockValueConfig;
	public static File Commands;
	static FileConfiguration islandCommandsConfig;
	public static File QuestsConfig;
	static FileConfiguration islandQuestsConfig;
	public static File GenConfig;
	static FileConfiguration islandGeneratorsConfig;
	public static File RewardConfig;
	static FileConfiguration islandRewardsConfig;
	public static File MobConfig;
	static FileConfiguration islandMobsConfig;
	public static File HoloConfig;
	static FileConfiguration islandHoloConfig;
	public static File holidayConfig;
	static FileConfiguration islandHolidayConfig;
	public static File guiConfig;
	static FileConfiguration customGuiConfig;
	public FileConfiguration config = getConfig();
	private static Plugin plugin;
	public static List<String> HasAccess = new ArrayList<String>();
	public static List<String> editingShop = new ArrayList<String>();
	public static HashMap<Player, ItemStack[]> vanish = new HashMap<Player,ItemStack[]>();
	public static List<Player> freeze = new ArrayList<Player>();
	public List<String> Healed = new ArrayList<String>();
	private static Location ChestPos;
	public static String server = ChatColor.WHITE + "" + ChatColor.GOLD + ChatColor.BOLD + "Nitronic"
			+ ChatColor.RESET + ChatColor.DARK_GRAY + " � " + ChatColor.GRAY + "";
	// public static HashMap<String, Location> firstEnable.PlayerHomes =new
	// HashMap<String, Location>();
	public static HashMap<String, ItemStack> PlayerBounties = new HashMap<String, ItemStack>();{
		
	}
	public static Inventory BountyGui = Bukkit.createInventory(null, 54,server+"Bounties");{
		//createDisplay(Material.OAK_SIGN, BountyGui, 53, ChatColor.BLUE+"Create Bounty", "This will set a bounty");
	}
	public static HashMap<Player, Long> JoinTimes = new HashMap<Player, Long>();
	public static Inventory Reset = Bukkit.createInventory(null, InventoryType.DISPENSER,
			ChatColor.RED + "" + ChatColor.BOLD + "Reset Island");
	static {
		for (int i = 0; i <= 8; i++) {
			if (i != 4) {
				createDisplay(Material.BARRIER, Reset, i, ChatColor.RED + "" + ChatColor.BOLD + "CANCEL", "");
			} else {
				createDisplay(Material.GREEN_CONCRETE, Reset, 4, ChatColor.GREEN + "" + ChatColor.BOLD + "Confirm",
						ChatColor.WHITE + "This will" + ChatColor.RED + "" + ChatColor.BOLD + " RESET "
								+ ChatColor.RESET + ChatColor.WHITE + "your island.");
			}
		}
	}
	public static Inventory Create = Bukkit.createInventory(null, InventoryType.DISPENSER,
			ChatColor.GREEN + "" + ChatColor.BOLD + "Create Island");
	static {
		for (int i = 0; i <= 8; i++) {
			if (i != 4) {
				createDisplay(Material.RED_STAINED_GLASS_PANE, Create, i,
						ChatColor.RED + "" + ChatColor.BOLD + "Cancel", "");
			} else {
				createDisplay(Material.GREEN_STAINED_GLASS_PANE, Create, 4,
						ChatColor.GREEN + "" + ChatColor.BOLD + "Confirm",
						ChatColor.WHITE + "This will" + ChatColor.GREEN + "" + ChatColor.BOLD + " Create "
								+ ChatColor.RESET + ChatColor.WHITE + "your island.");
			}
		}
	}
	public static Inventory EasySkyCustomRecipes = Bukkit.createInventory(null, 54, server + "Custom Recipes");

	public static Inventory EasySkySettingsGui = Bukkit.createInventory(null, 18, server + "Island Settings");
	static {
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, EasySkySettingsGui, " ", "");

		createDisplay(Material.IRON_DOOR, EasySkySettingsGui, 0, ChatColor.AQUA + "Toggle Guests",
				ChatColor.WHITE + "This will toggle the ability to /visit to your island");
		createDisplay(Material.SPAWNER, EasySkySettingsGui, 1, ChatColor.AQUA + "Toggle Mob Spawning",
				ChatColor.WHITE + "This will toggle Mob Spawning on your island");
		createDisplay(Material.DIAMOND_ORE, EasySkySettingsGui, 2, ChatColor.AQUA + "Change Generator Type",
				ChatColor.WHITE + "This will change blocks spawned by Generators");
		createDisplay(Material.IRON_SWORD, EasySkySettingsGui, 3, ChatColor.AQUA + "Toggle pvp",
				ChatColor.WHITE + "This will toggle pvp");

		// The first parameter, is the slot that is assigned to. Starts counting at 0
	}
	public static Inventory EasySkyGui = Bukkit.createInventory(null, 45, server + "Menu");

	public static Inventory EasySkyAdminItems = Bukkit.createInventory(null, 54, server + "Admin Items");

	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if(freeze.contains(e.getPlayer())&&e.getPlayer().getLocation().subtract(0, 1, 0).getBlock().getType()!=Material.AIR) {
			e.setCancelled(true);
			Cooldowns.sendActionBarMessage(e.getPlayer(), ChatColor.BLUE+"Your feet are stuck!");
		}
		if(e.getPlayer().isSneaking()&&Cooldowns.silentIsCooldownOver(Cooldowns.GrowCoolDown, 3, e.getPlayer())&&IslandInfo.OwnIsland(e.getPlayer())) {
			for(BlockFace b :faces) {
				for(Material s:sapling) {
				if(e.getPlayer().getLocation().getBlock().getRelative(b).getType()==s) {
					Particles.createParticle(getBlockCenter(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getBlock()).add(0, 1, 0),Particle.VILLAGER_HAPPY,2);
					if(Math.random()<=.3) {
					if (s == sapling[0]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
						e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.ACACIA);
						return;
					}
					if (s== sapling[1]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
								e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.TREE);
								return;
				
					}
					if (s == sapling[2]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
						e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.BIRCH);
						return;
					}
					if (s == sapling[3]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
								e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.REDWOOD);
								return;
					}
					if (s == sapling[4]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
								e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.DARK_OAK);
								return;
					}
					if (s == sapling[5]) {
						e.getPlayer().getLocation().getBlock().getRelative(b).setType(Material.AIR);
								e.getPlayer().getLocation().getBlock().getRelative(b).getLocation().getWorld().generateTree(e.getPlayer().getLocation().getBlock().getRelative(b).getLocation(),TreeType.SMALL_JUNGLE);
								return;
					}
				}
				}}
			}
		}
		islandBorder.onMovement(e);
		Enchants.onMovementEnchant(e);
		if (e.getTo().getY() < e.getFrom().getY() && e.getPlayer().getLocation().getBlockY() < -1) {
			e.getPlayer().setFallDistance(0);
			if (Main.islandCommandsConfig.getString("Commands.VoidFall").equalsIgnoreCase("loop")) {
				if(e.getPlayer().getPassengers()!=null) {
				e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), e.getPlayer().getLocation().getX(), 256,
						e.getPlayer().getLocation().getZ()));
			} }else {
				if(e.getPlayer().getPassengers()!=null) {
				Bukkit.dispatchCommand(e.getPlayer(), Main.islandCommandsConfig.getString("Commands.VoidFall"));
				}
			}
		}
		ItemStack wingboots = createDisplay(Material.DIAMOND_BOOTS, EasySkyCustomRecipes, -2,
				ChatColor.BLUE + "Winged" + ChatColor.WHITE + " Boots", "");
		if (e.getPlayer().getInventory().getBoots() != null && e.getPlayer().getGameMode() != GameMode.CREATIVE) {
			if (e.getPlayer().getInventory() != null
					&& e.getPlayer().getInventory().getBoots().getItemMeta().getDisplayName()
							.equals(wingboots.getItemMeta().getDisplayName())
					&& e.getPlayer().getInventory().getBoots().getItemMeta() != null
					&& e.getPlayer().getInventory().getBoots() != null) {
				ArrayList<String> Lore = new ArrayList<String>();
				String[] durability = e.getPlayer().getInventory().getBoots().getItemMeta().getLore().get(0)
						.replace("�c", "").replace("�7", "").split("/");
				int lostdurability = Integer.parseInt(durability[0]);
				if (e.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.AIR) {
					lostdurability = Integer.parseInt(durability[0]) - 1;

				}
				if (e.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR) {
					if(Integer.parseInt(durability[0])<1400) {
					lostdurability = Integer.parseInt(durability[0]) + 1;
					}

				}
				if (lostdurability <= 0 || IslandInfo.getIsland(e.getFrom()).equals("none")) {

					e.getPlayer().setAllowFlight(false);
					e.getPlayer().setFlying(false);
					return;
				}
				if (e.getTo().getY() < 1 && e.getPlayer().isFlying() != true && lostdurability > 1
						|| e.getTo().getY() > e.getFrom().getY() && e.getPlayer().isFlying() != true
								&& lostdurability > 1) {
					e.getPlayer().setAllowFlight(true);
					e.getPlayer().setFlying(true);
				}

				if (lostdurability == 5 || lostdurability == 50 || lostdurability == 100 && lostdurability != 0) {
					e.getPlayer().sendMessage(server + "Your Boots will lose flight in: " + ChatColor.RED
							+ Integer.toString(lostdurability) + ChatColor.GREEN + " steps!");
					e.getPlayer().playEffect(e.getPlayer().getLocation(), Effect.BLAZE_SHOOT, 0);
				}

				String NewLore = ChatColor.RED + Integer.toString(lostdurability) + "/" + "1400";
				Particles.createParticle(e.getTo(), Color.RED, 1);
				Particles.createParticle(e.getFrom(), Color.SILVER, 1);
				Lore.add(NewLore);
				if (e.getPlayer().getInventory().getBoots().getItemMeta() != null) {
					ItemMeta meta = e.getPlayer().getInventory().getBoots().getItemMeta();
					meta.setLore(Lore);
					e.getPlayer().getInventory().getBoots().setItemMeta(meta);
				}

			}
		} else {
			if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
				if (e.getPlayer().getAllowFlight() == true) {
					e.getPlayer().setAllowFlight(false);
					e.getPlayer().setFlying(false);
				}
			}

		}
	}
	@EventHandler
	public void fishin(PlayerFishEvent event) {
		
		if(event.getPlayer().getInventory().getItemInMainHand().getItemMeta()==null) {return;}
		if(event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Grappling Hook")){
		event.getHook().setVelocity(event.getHook().getVelocity().multiply(2));
		if(event.getHook().getLocation().distance(event.getPlayer().getLocation())<30) {
			for(BlockFace f: faces) {
				if(event.getHook().getLocation().getBlock().getRelative(f).getType()!=Material.AIR) {
					Vector direction = event.getHook().getLocation().toVector().subtract(event.getPlayer().getLocation().toVector()).normalize();
					event.getPlayer().setVelocity(direction.multiply(2)); 
					event.getHook().remove();
					break;
				}
			}

		}}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if(customGuiConfig.contains(event.getView().getTitle().replace("�","&").replace(" � ",".sub."))) {
			CustomGui.CustomClick(event);
		}
		if(vanish.containsKey(event.getWhoClicked())) {
			event.setCancelled(true);
			return;
		}
		if (event.getCurrentItem() != null) {
			Player player = (Player) event.getWhoClicked(); // The player that clicked the item
			ItemStack clicked = event.getCurrentItem(); // The item that was clicked
			// Inventory inventory = event.getInventory(); // The inventory that was clicked
			// in
			if(clicked.isSimilar(Main.createDisplay(Material.SADDLE, null, -2,"&a&lRodeo Saddle", "&7"))) {
				event.setCancelled(true);
			}
			Inventory Recipes = Bukkit.createInventory(null, InventoryType.DISPENSER, "Recipe");
			Inventory GenTiers = Bukkit.createInventory(null, InventoryType.DISPENSER,
					ChatColor.DARK_GRAY + "GenTiers");
			Inventory EasySkyQuests = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "Quests");
			ItemStack backButton = Main.createDisplay(Material.RED_STAINED_GLASS, null, -2, ChatColor.WHITE + "Back",
					"");
			if (event.getView().getTitle().equalsIgnoreCase(server+"Bounties")) {
				//sets chatmod to correct name
				
			 event.setCancelled(true);

			}
			if (event.getInventory().getType() == InventoryType.ANVIL) {
	            if(event.getSlotType() == InventoryType.SlotType.RESULT) {
	    
	                 ItemMeta anvilmeta =event.getCurrentItem().getItemMeta();
	                 if(anvilmeta.hasDisplayName()) {
	                	 anvilmeta.setDisplayName(anvilmeta.getDisplayName().replace("/", "").replace(":", "").replace("�", ""));
	                 }
	                 event.getCurrentItem().setItemMeta(anvilmeta);
	            }
	        }
			// Inventory userShop =
			// Bukkit.createInventory(null,playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*9,ChatColor.BLUE+player.getName()+ChatColor.DARK_GRAY+"
			// shop items");
			// ChatColor.BLUE+p.getName()+"'s"+ChatColor.DARK_GRAY+" shop"
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + "Island Anvil") && clicked != null
					) {
				if( event.getClickedInventory() instanceof PlayerInventory == true) {
				if(event.getCursor()!=null) {
				if(event.getCursor().getType()==Material.PAPER&&event.getCursor().getItemMeta().hasLore()) {
				if(clicked.getAmount()==1) {
					Enchants.upgrade(event);
				}else {
				event.setCancelled(true);
				}
				}
				}
				}else {
					event.setCancelled(true);
				}
			}
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + "Island Upgrades") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				IslandUpgrades.buyUpgrades(player, clicked);
			}
			if (event.getView().getTitle().equalsIgnoreCase(server+ "Pets")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				Pets.clickpet(event);
			}
			if (event.getView().getTitle().equalsIgnoreCase(server+ "Suffix")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				Suffix.clicksuffix(event);
			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Create Island")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.getType() == Material.GREEN_STAINED_GLASS_PANE) {
					createIsland(player);
					player.closeInventory();
				} else {
					player.closeInventory();
				}
			}
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + "Admin Shops") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				Shops.ClickEvent(event);
			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.GRAY + "Codes") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				Rewards.rewardEvent(event);
			}
			if (event.getView().getTitle()
					.equalsIgnoreCase(ChatColor.BLUE + player.getName() + "'s" + ChatColor.DARK_GRAY + " shop")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				playerShop.clickShopPrice(event);
			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Price") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {

				playerShop.setShopPrice(event);
			}
			if (event.getView().getTitle()
					.equalsIgnoreCase(ChatColor.BLUE + player.getName() + ChatColor.DARK_GRAY + " shop items")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				playerShop.updateShop(event);

			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.DARK_GRAY + "GenTiers") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				player.closeInventory();

				genTierMenu.clickTier(player, clicked, GenTiers);
			}

			if (event.getView().getTitle().equalsIgnoreCase(server + "Admin Items") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				player.closeInventory();
				Bukkit.dispatchCommand(player, "adminitem " + player.getName() + " " + event.getSlot() + " 1");

			}
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + ChatColor.GRAY + "Best Islands!")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.getType() == Material.PLAYER_HEAD) {
					SkullMeta skull = (SkullMeta) clicked.getItemMeta();

					Bukkit.dispatchCommand(player, "visit " + skull.getOwner());

				}
				if (clicked.isSimilar(backButton)) {
					if (Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")
							|| Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
						player.openInventory(EasySkyGui);
					} else {
						Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
					}
				}
			}
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + "Members") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.isSimilar(backButton)) {
					if (Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")
							|| Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
						player.openInventory(EasySkyGui);
					} else {
						Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
					}
				} else {
					if (clicked.getType() == Material.PLAYER_HEAD) {
						SkullMeta skullMeta = (SkullMeta) clicked.getItemMeta();
						String p = skullMeta.getOwner();
						Bukkit.dispatchCommand(player, "i remove " + p);
						Inventory EasySkyTeam = Bukkit.createInventory(null, (Math.round(
								config.getInt("IslandOwnerSettings." + player.getName() + ".MembersAllowed") / 9) + 1)
								* 9, Main.server + "Members");
						player.openInventory(EasySkyTeam);
						IslandInfo.loadTeamMates(EasySkyTeam, player);

					}
				}
			}

			if (event.getView().getTitle().equalsIgnoreCase(server + ChatColor.BLUE + "Player Shops") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				playerShop.openPublicShop(player, clicked);
				event.setCancelled(true);
			}
			if (firstEnable.PlayerOpenShop.get(player.getName()) != null) {
				if (event.getView().getTitle()
						.equalsIgnoreCase(ChatColor.AQUA + firstEnable.PlayerOpenShop.get(player.getName()) + "'s"
								+ ChatColor.GOLD + " Shop")
						& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
					playerShop.playerBuyItem(player, clicked);
					event.setCancelled(true);
				}
			}

			if (!event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA
					+ firstEnable.PlayerOpenShop.get(player.getName()) + "'s" + ChatColor.GOLD + " Shop")) {
				firstEnable.IslandEditors.remove(player.getName());
			}
			if (event.getView().getTitle().equals(ChatColor.RED + "" + ChatColor.BOLD + "Reset Island")
					&& clicked != null && event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.getType() == Material.BARRIER) {
					player.closeInventory();
					player.closeInventory();
				}
				if (clicked.getType() == Material.GREEN_CONCRETE) {
					resetIsland(player);
					player.closeInventory();
				}
			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.DARK_GRAY + "Quests") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				Quests.completeQuest(player, clicked, EasySkyQuests);
			}
			if (event.getView().getTitle().equalsIgnoreCase(server + "Custom Recipes") && clicked != null
					|| event.getView().getTitle().equalsIgnoreCase("Recipe") && clicked != null) {

				event.setCancelled(true);
				player.closeInventory();
				if (clicked.getType() != Material.AIR) {
					player.openInventory(Recipes);
					for (Recipe recipe : Bukkit.getServer().getRecipesFor(clicked)) {
						if (recipe instanceof ShapedRecipe) {
							ShapedRecipe shaped = (ShapedRecipe) recipe;
							int i = 0;
							for (int c = 0; c < shaped.getShape().length; c++) {
								for (char item : shaped.getShape()[c].toCharArray()) {
									if (shaped.getIngredientMap().get(item) != null && item != " ".toCharArray()[0]) {
										Recipes.setItem(i, shaped.getIngredientMap().get(item));
									}
									i++;
								}

							}
							return;
						} else if (recipe instanceof ShapelessRecipe) {

							ShapelessRecipe shapeless = (ShapelessRecipe) recipe;
							for (int c = 0; c < shapeless.getIngredientList().size(); c++) {

								Recipes.setItem(c, shapeless.getIngredientList().get(c));
							}
							return;
						} else if (recipe instanceof FurnaceRecipe) {
							// FurnaceRecipe furnace = (FurnaceRecipe)recipe;
							createDisplay(Material.FURNACE, Recipes, 4, ChatColor.RED + "Item comes from a furnace",
									"");
							player.sendMessage(server + "Can be made in a furnace.");
							return;
						}

					}
					for (int i = 0; i < 9; i++) {
						createDisplay(Material.BLACK_STAINED_GLASS_PANE, Recipes, i,
								ChatColor.RED + "Item cannot be crafted.", "");
					}

				}
			}
			if (event.getView().getTitle().equalsIgnoreCase(server + "Island Settings") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.getType() == Material.IRON_DOOR) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					getConfig().set("IslandOwnerSettings." + player.getName() + ".Public",
							!getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".Public"));
					saveConfig();
					if (getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".Public")) {
						player.sendMessage(server + "Players can now visit your island");
					} else {
						player.sendMessage(server + "Players can no longer visit your island");
					}

				}
				if (clicked.getType() == Material.DIAMOND_ORE) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					player.openInventory(GenTiers);
					genTierMenu.loadMenu(GenTiers, player);
				}

				if (clicked.getType() == Material.SPAWNER) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					getConfig().set("IslandOwnerSettings." + player.getName() + ".MobSpawn",
							!getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".MobSpawn"));
					saveConfig();
					if (getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".MobSpawn")) {
						player.sendMessage(server + "Mobs can now spawn on your island");
					} else {
						player.sendMessage(server + "Mobs can no longer spawn on your island");
					}

				}
				if (clicked.getType() == Material.IRON_SWORD) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					getConfig().set("IslandOwnerSettings." + player.getName() + ".Pvp",
							!getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".Pvp"));
					saveConfig();
					if (getConfig().getBoolean("IslandOwnerSettings." + player.getName() + ".Pvp")) {
						player.sendMessage(server + "Pvp will now be enabled on your island");
					} else {
						player.sendMessage(server + "Pvp is no longer enabled on your island");
					}

				}
			}

			if (event.getView().getTitle().equalsIgnoreCase(server + "Menu") && clicked != null
					&& event.getClickedInventory() instanceof PlayerInventory == false) {
				event.setCancelled(true);
				if (clicked.getType() == Material.PAPER) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Quests"));

				}
				if (clicked.getType() == Material.CHEST) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					if (player.hasPermission("permission.easySkies.auction")) {
						
						
						Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.PlayerShops"));
					}else {
						player.sendMessage(server+"Please wait until you are a member to do that!");
					}

				}
				if (clicked.getType() == Material.HOPPER) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					if (Main.islandCommandsConfig.getBoolean("Settings.PlayerShops.Enabled")) {
						playerShop.loadShopData(player);
					} else {
						Bukkit.dispatchCommand(player, "is top");
					}
				}
				if (clicked.getType() == Material.OAK_SIGN) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Settings"));
				}
				if (clicked.getType() == Material.CRAFTING_TABLE) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					player.openInventory(EasySkyCustomRecipes);

				}
				if (clicked.getType() == Material.CAKE) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Shop"));

				}
				if (clicked.getType() == Material.PLAYER_HEAD) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory();
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Team"));

				}
				if (clicked.getType() == Material.GRASS_BLOCK) { // The item that the player clicked
					event.setCancelled(true);
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Upgrades"));

				}
				if (clicked.getType() == Material.RED_BED) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory(); // Closes there inventory
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Home"));

					// player.getLocation().setWorld(Bukkit.getWorld("world"));
				}
				if (clicked.getType() == Material.DIAMOND_BLOCK) {
					player.closeInventory();
					Bukkit.getServer().dispatchCommand(player, "island level");

				}
				if (clicked.getType() == Material.ENDER_PEARL) { // The item that the player clicked
					event.setCancelled(true); // Make it is back in its original spot
					player.closeInventory(); // Closes there inventory
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.Spawn"));

					// player.getLocation().setWorld(Bukkit.getWorld("world"));
				}
			}
		}
	}

	public Location blockset(Location loc1, Location loc2, Player player) {
		// Location playerpos = player.getLocation();

		int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
		int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
		int differenceblockX = topBlockX - bottomBlockX;
		int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
		int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
		int differenceblockY = topBlockY - bottomBlockY;
		int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
		int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
		int differenceblockZ = topBlockZ - bottomBlockZ;
		int islandValue = 0;
		for (int x = 0; x <= differenceblockX; x++) {
			for (int z = 0; z <= differenceblockZ; z++) {
				for (int y = 0; y <= differenceblockY; y++) {
					// loc1.setWorld(Bukkit.getWorld("EasySkiesIslands"));
					Material setType = loc1.getWorld().getBlockAt(bottomBlockX + x, bottomBlockY + y, bottomBlockZ + z)
							.getType();
					for (String blockValue : Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
						if (blockValue.split(":")[0].equalsIgnoreCase(setType.toString())) {
							islandValue = islandValue + Integer.valueOf(blockValue.split(":")[1]);
							break;
						}
					}
					if (setType.isSolid()) {
						islandValue = islandValue + Main.islandBlockValueConfig.getInt("Blocks.DefaultValues");
					}

					loc1.getWorld().getBlockAt(firstEnable.IslandCountX * 10000 + x, y + 126,
							z + firstEnable.IslandCountZ * 10000).setType(setType);
					if (setType == Material.CHEST) {
						Chest chest = (Chest) loc1.getWorld()
								.getBlockAt(bottomBlockX + x, bottomBlockY + y, bottomBlockZ + z).getState();
						// Inventory inv = chest.getInventory();
						loc1.getWorld().getBlockAt(firstEnable.IslandCountX * 10000 + x, y + 126,
								z + firstEnable.IslandCountZ * 10000).setType(setType);
						// for (int i = 0; i< inv.getSize(); i++){

						ChestPos = loc1.getWorld().getBlockAt(firstEnable.IslandCountX * 10000 + x, y + 127,
								z + firstEnable.IslandCountZ * 10000).getLocation();
						((Chest) loc1.getWorld()
								.getBlockAt(firstEnable.IslandCountX * 10000 + x, y + 126,
										z + firstEnable.IslandCountZ * 10000)
								.getState()).getInventory().setContents(chest.getInventory().getContents());
						firstEnable.IslandCountX++;

						// }

					}

				}
			}
		}
		firstEnable.config.set("IslandOwnerSettings." + player.getName() + ".IslandLevel",
				islandValue + islandBlockValueConfig.getInt("Blocks.StartingLevel"));
		Main.getPlugin().saveConfig();
		player.sendMessage(server + "Finished Island");
		return ChestPos;

	}
	@EventHandler
	public void onEntityTeleport(EntityTeleportEvent e) {
		if(Pets.Pets.containsKey(e.getEntity().getName())) {
			
			Pets.removepet((Player) e.getEntity());
		}
		if (e.getEntity() instanceof Shulker && firstEnable.tp == true) {

			e.setCancelled(true);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		Player p = event.getPlayer();
		if (IslandInfo.OwnIsland(p)) {
			if (event.getBlock().getType() == Material.CARVED_PUMPKIN) {
				if (event.getBlock().getRelative(BlockFace.DOWN).getType() == Material.EMERALD_BLOCK && event.getBlock()
						.getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType() == Material.EMERALD_BLOCK) {
					p.sendMessage(server + "You Summoned a villager!");
					p.playEffect(event.getBlock().getLocation(), Effect.ZOMBIE_CONVERTED_VILLAGER, 0);
					event.getBlock().getRelative(BlockFace.DOWN).setType(Material.AIR);
					event.getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).setType(Material.AIR);
					event.getBlock().setType(Material.AIR);
					event.getBlock().getLocation().getWorld().dropItem(event.getBlock().getLocation(),
							createDisplay(Material.VILLAGER_SPAWN_EGG, EasySkyCustomRecipes, -2,
									ChatColor.BLUE + "Spawn" + ChatColor.WHITE + " Villager", ""));
					return;

				}
				if (event.getBlock().getRelative(BlockFace.DOWN).getType() == Material.HAY_BLOCK) {
					for (BlockFace face : faces) {
						if (event.getBlock().getRelative(BlockFace.DOWN).getRelative(face)
								.getType() == Material.HAY_BLOCK && face != BlockFace.UP && face != BlockFace.SELF
								&& face != BlockFace.DOWN) {
							event.getBlock().getRelative(BlockFace.DOWN).setType(Material.AIR);
							event.getBlock().getRelative(BlockFace.DOWN).getRelative(face).setType(Material.AIR);
							event.getBlock().setType(Material.AIR);
							event.getBlock().getLocation().getWorld().dropItem(event.getBlock().getLocation(),
									createDisplay(Material.COW_SPAWN_EGG, EasySkyCustomRecipes, -1,
											ChatColor.BLUE + "Spawn" + ChatColor.WHITE + " Cow", ""));

							return;
						}
					}
				}

			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void blockInterract(final PlayerInteractEvent event) {
		Player p = event.getPlayer();
		
		ItemStack largeHeal = createDisplay(Material.PAPER,EasySkyCustomRecipes,-2,ChatColor.BLUE+"Large"+ChatColor.WHITE+" Bandage","Heals to full health when right clicked");
		if (p.getInventory().getItemInMainHand().isSimilar(largeHeal)) {
			if (18 > p.getHealth() &&Cooldowns.isCooldownOver(Cooldowns.largebandageCoolDown, 20, event.getPlayer())) {
				p.getInventory().removeItem(largeHeal);
				p.setHealth(p.getMaxHealth());
				p.setAbsorptionAmount(2);
			} else {
				p.sendMessage(server + "You are at max health!");
			}
		}
		ItemStack smallHeal = createDisplay(Material.PAPER, EasySkyCustomRecipes, -2,
				ChatColor.RED + "Small" + ChatColor.WHITE + " Bandage", "Heals 3 hearts when right clicked");
		if (p.getInventory().getItemInMainHand().isSimilar(smallHeal)) {
			if (18 > p.getHealth() &&Cooldowns.isCooldownOver(Cooldowns.bandageCoolDown, 5, event.getPlayer())) {
				p.getInventory().removeItem(smallHeal);
				p.setHealth(p.getHealth() + 6);
			} else {
				p.sendMessage(server + "You are at max health!");
			}
		}
		////////////own island only////////////////
		if (!IslandInfo.OwnIsland(p)) {
			return;
		}
		if(p.getInventory().getItemInMainHand().getType()==Material.IRON_DOOR&&p.getInventory().getItemInMainHand().getAmount()==1) {
			ItemStack newItem = new ItemStack(Material.IRON_DOOR, 1);
			ItemMeta newItemMeta = newItem.getItemMeta();
			newItemMeta.setDisplayName(ChatColor.AQUA + "Portable Jail");
			newItemMeta.setLore(p.getInventory().getItemInMainHand().getItemMeta().getLore());
			newItem.setItemMeta(newItemMeta);
			if(p.getInventory().getItemInMainHand().isSimilar(newItem)&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)) {
				for(String text :p.getInventory().getItemInMainHand().getItemMeta().getLore()) {
					event.getClickedBlock().getLocation().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(0, 1, 0),EntityType.valueOf(ChatColor.stripColor(text).toUpperCase()));	
				}
				p.getInventory().getItemInMainHand().setAmount(0);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					public void run() {

						Player p = event.getPlayer();
						p.getInventory().setItemInMainHand(Main.createDisplay(Material.IRON_DOOR,null, -2,ChatColor.AQUA+"Portable Jail",ChatColor.RED+"Click on players to jail them, or on mobs to capture them!"));
						
					}
				}, 3);
				}
		}
		if(p.getInventory().getItemInMainHand().getType()==Material.IRON_BARS&&p.getInventory().getItemInMainHand().getAmount()==1) {
			ItemStack newItem = new ItemStack(Material.IRON_BARS, 1);
			ItemMeta newItemMeta = newItem.getItemMeta();
			newItemMeta.setDisplayName(ChatColor.DARK_GRAY + "Used Mob Cage");
			newItemMeta.setLore(p.getInventory().getItemInMainHand().getItemMeta().getLore());
			newItem.setItemMeta(newItemMeta);
			if(p.getInventory().getItemInMainHand().isSimilar(newItem)&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)) {
				for(String text :p.getInventory().getItemInMainHand().getItemMeta().getLore()) {
					event.getClickedBlock().getLocation().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(0, 1, 0),EntityType.valueOf(ChatColor.stripColor(text).toUpperCase()));	
				}
				p.getInventory().getItemInMainHand().setAmount(0);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					public void run() {

						Player p = event.getPlayer();
						p.getInventory().setItemInMainHand(createDisplay(Material.IRON_BARS, EasySkyCustomRecipes, -2,
								ChatColor.GRAY + "Mob Cage",ChatColor.AQUA+ "Can hold up to four mobs, right click to use"));
						
					}
				}, 3);
				}
		}
		if(p.getInventory().getItemInMainHand().getType()==Material.VINE&&p.getInventory().getItemInMainHand().getAmount()==1) {
			ItemStack newItem = new ItemStack(Material.VINE, 1);
			ItemMeta newItemMeta = newItem.getItemMeta();
			newItemMeta.setDisplayName(ChatColor.GOLD + "Full Net");
			newItemMeta.setLore(p.getInventory().getItemInMainHand().getItemMeta().getLore());
			newItem.setItemMeta(newItemMeta);
			if(p.getInventory().getItemInMainHand().isSimilar(newItem)&&Cooldowns.isCooldownOver(Cooldowns.mobCoolDown, 1, p)) {
				event.getClickedBlock().getLocation().getWorld().spawnEntity(event.getClickedBlock().getLocation().add(0, 1, 0),EntityType.valueOf(ChatColor.stripColor(p.getInventory().getItemInMainHand().getItemMeta().getLore().get(0).toUpperCase())));
				p.getInventory().getItemInMainHand().setAmount(0);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					public void run() {

						Player p = event.getPlayer();
						p.getInventory().setItemInMainHand(Main.createDisplay(Material.VINE, Main.EasySkyCustomRecipes, -2, ChatColor.YELLOW + "Empty Net", ChatColor.AQUA+"can capture mobs when right clicked!"));
						
					}
				}, 3);
				}
		}
		if (event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.FARMLAND
				&& !IslandInfo.OwnIsland(p)) {
			event.setCancelled(true);
		}
		if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK
				&& IslandInfo.OwnIsland(p) && p.getInventory().getItemInMainHand()!=null) {
			if(event.getClickedBlock().getType()==Material.LEVER) {
				for(BlockFace f:faces) {
				if(event.getClickedBlock().getRelative(f).getType().toString().contains("_LOG")) {
					if(p.getInventory().getItemInMainHand().getType()==Material.GLASS_BOTTLE) {
					p.getInventory().addItem(new ItemStack(Material.HONEY_BOTTLE,1));
					p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount()-1);;
					return;
				}}
				}
			}
		}
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() != null) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK && !IslandInfo.OwnIsland(p)
					&& event.getClickedBlock() != null) {
				event.setCancelled(true);
				// p.sendMessage(server+"You can only do this on your own island!");
			} else {
				if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK
						&& IslandInfo.OwnIsland(p) && p.getInventory().getItemInMainHand().getType() == Material.BUCKET
						&& event.getClickedBlock().getType() == Material.OBSIDIAN) {
					ItemStack m = new ItemStack(Material.BUCKET, 1);

					p.getInventory().removeItem(m);
					p.updateInventory();

					// p.getInventory().remove(Material.BUCKET);
					event.getClickedBlock().setType(Material.AIR);
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
						public void run() {

							Player p = event.getPlayer();
							createDisplay(Material.LAVA_BUCKET, p.getInventory(), p.getInventory().getHeldItemSlot(),
									ChatColor.RED + "" + ChatColor.BOLD + "Resurrected" + ChatColor.RESET
											+ ChatColor.WHITE + " Lava Bucket",
									"Magically Restored");
						}
					}, 3);

				}
				
				ItemStack treegrow = createDisplay(Material.OAK_LEAVES, EasySkyCustomRecipes, -2,
						ChatColor.GREEN + "Tree grow", "Click on a sapling to grow it instantly");

				if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK
						&& IslandInfo.OwnIsland(p) && p.getInventory().getItemInMainHand().isSimilar(treegrow)) {
					event.setCancelled(true);
					for (Material saplings : sapling) {
						if (event.getClickedBlock().getType() == saplings) {
							event.getClickedBlock().setType(Material.AIR);
							if (saplings == sapling[0]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.ACACIA);
								return;
							}
							if (saplings == sapling[1]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.TREE);
								return;
							}
							if (saplings == sapling[2]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.BIRCH);
								return;
							}
							if (saplings == sapling[3]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.REDWOOD);
								return;
							}
							if (saplings == sapling[4]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.DARK_OAK);
								return;
							}
							if (saplings == sapling[5]) {
								p.getInventory().removeItem(treegrow);
								event.getClickedBlock().getWorld().generateTree(event.getClickedBlock().getLocation(),
										TreeType.SMALL_JUNGLE);
								return;
							}
						}
					}
					p.sendMessage(server + "Please use on a sapling.");
				}
				ItemStack SlimeDetect = createDisplay(Material.SLIME_BALL, EasySkyCustomRecipes, -2,
						ChatColor.GREEN + "Slime" + ChatColor.RESET + " Detector",
						"Click on a block to see if its in a slime chunk");
				if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK
						&& IslandInfo.OwnIsland(p) && p.getInventory().getItemInMainHand().isSimilar(SlimeDetect)) {
					if (event.getClickedBlock().getChunk().isSlimeChunk()) {
						event.getPlayer().sendMessage(server + "Slime chunk detected!");
					} else {
						event.getPlayer().sendMessage(server + "Sadly this is not a slime chunk =(");
					}
				}
				ItemStack farmhoe = createDisplay(Material.GOLD_NUGGET, EasySkyCustomRecipes, -2,
						ChatColor.GOLD + "Farmers" + ChatColor.RESET + " Hoe", "Tills the surrounding blocks aswell!");
				if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK
						&& IslandInfo.OwnIsland(p) && p.getInventory().getItemInMainHand().isSimilar(farmhoe)
						&& event.getClickedBlock().getType() == Material.DIRT
						|| event.getClickedBlock().getType() == Material.GRASS_BLOCK
								&& event.getAction() == Action.RIGHT_CLICK_BLOCK && IslandInfo.OwnIsland(p)
								&& p.getInventory().getItemInMainHand().isSimilar(farmhoe)) {
					for (BlockFace face : faces) {
						Block r = event.getClickedBlock().getRelative(face, 1);
						if (face != BlockFace.DOWN && face != BlockFace.UP && r.getType() == Material.DIRT
								|| r.getType() == Material.GRASS_BLOCK || r.getType() == Material.GRASS_PATH) {
							r.setType(Material.FARMLAND);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onFromTo(BlockFromToEvent event) {
		Material type = event.getBlock().getType();
		if (type == Material.WATER || type == Material.WATER || type == Material.LAVA || type == Material.LAVA) {
			Block b = event.getToBlock();
			if (b.getType() == Material.AIR) {
				if (generatesCobble(type, b)) {
					/* DO WHATEVER YOU NEED WITH THE COBBLE */
					event.setCancelled(true);
					Material Materials = RandomMaterial(event.getBlock().getLocation());
					b.setType(Materials);
					for (BlockFace face : faces) {
						BlockData data = b.getRelative(face).getBlockData();
						if (face != BlockFace.SELF) {
							if (data instanceof Powerable) {
								Powerable powerableBlock = (Powerable) data;
								powerableBlock.setPowered(true);
								b.getRelative(face).setBlockData(powerableBlock);
								b.getRelative(face).getState().update();
							}
						}
					}

					if (!IslandInfo.getIsland(b.getLocation()).equalsIgnoreCase("none")) {
						List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");

						int islandValue = 0;
						if (firstEnable.config.contains(
								"IslandOwnerSettings." + IslandInfo.getIsland(b.getLocation()) + ".IslandLevel")) {
							islandValue = firstEnable.config.getInt(
									"IslandOwnerSettings." + IslandInfo.getIsland(b.getLocation()) + ".IslandLevel");
						}
						for (String blockValue : Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
							if (blockValue.split(":")[0].equalsIgnoreCase(Materials.toString())) {
								islandValue = islandValue + Integer.valueOf(blockValue.split(":")[1]);
								break;
							}
						}

						islandValue = islandValue + Main.islandBlockValueConfig.getInt("Blocks.DefaultValues");

						if (islandValue <= 0) {
							islandValue = 0;
						}
						for (String player : players) {

							if (player.split(":")[0].equals(IslandInfo.getIsland(b.getLocation()))) {
								players.set(players.indexOf(player), player.split(":")[0] + ":" + islandValue);
								break;
							}
						}
						Main.islandBlockValueConfig.set("Island.Top.Players", players);
						Main.saveBlockValues();

						firstEnable.config.set("IslandOwnerSettings."
								+ IslandInfo.getIsland(event.getBlock().getLocation()) + ".IslandLevel", islandValue);
						Main.getPlugin().saveConfig();
						IslandInfo.calculateTopIslands(event.getBlock().getLocation(), islandValue);
					}
				}
			}
		}
	}

	private final Material[] sapling = new Material[] { Material.ACACIA_SAPLING, Material.OAK_SAPLING,
			Material.BIRCH_SAPLING, Material.SPRUCE_SAPLING, Material.DARK_OAK_SAPLING, Material.JUNGLE_SAPLING, };
	public final static BlockFace[] faces = new BlockFace[] { BlockFace.SELF, BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH,
			BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };

	public Material RandomMaterial(Location l) {

		int Generator = 0;
		if (IslandInfo.getIsland(l) == "none") {
			Generator = 0;
		} else {
			Generator = config.getInt("IslandOwnerSettings." + IslandInfo.getIsland(l) + ".GenTier");
		}
		List<String> Gen = islandGeneratorsConfig.getStringList("GeneratorChances" + "." + Generator + ".Blocks");
		for (String rawData : Gen) {
			int x = new Random().nextInt(100);
			String[] raw = rawData.split(":");
			if (x <= Integer.valueOf(raw[1])) {
				if (Material.getMaterial(raw[0].toUpperCase()) == null) {
					return Material.SPONGE;
				}
				return Material.getMaterial(raw[0].toUpperCase());
			}
		}
		return Material.COBBLESTONE;

	}

	public boolean generatesCobble(Material type, Block b) {
		Material mirrorID1 = (type == Material.WATER || type == Material.WATER ? Material.LAVA : Material.WATER);
		Material mirrorID2 = (type == Material.WATER || type == Material.WATER ? Material.LAVA : Material.WATER);
		for (BlockFace face : faces) {
			Block r = b.getRelative(face, 1);
			if (r.getType() == mirrorID1 || r.getType() == mirrorID2) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String Island, String[] args) {
		if (cmd.getName().equalsIgnoreCase("doall")&&sender.isOp()) {
			if(args.length>1) {
		      Bukkit.getScheduler().runTask(plugin, new Runnable() {
		          @Override
		          public void run() {
		              StringBuffer sb = new StringBuffer();
		              for(int i = 0; i < args.length; i++) {
		                 sb.append(args[i]+" ");
		              }
		        	  for(Player p : Bukkit.getOnlinePlayers()) {
		        		  
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(),sb.toString().replace("%player%",p.getName()));
						}	
		        	  sender.sendMessage(sb.toString());
		          }
		       });}else {
		    	   sender.sendMessage(server+"Not enough arguements!");
		       }
			
		}
		if (cmd.getName().equalsIgnoreCase("joinqueue")&&sender.hasPermission("permission.easyskies.queue")) {
			if(args.length<2) {
		    	   sender.sendMessage(server+"Not enough arguements!");
			}else {
			if(Bukkit.getPlayer(args[1])!=null) {	
				Queue.joinQueue(Bukkit.getPlayer(args[1]),args[0]);
				}
			}
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("opengui")&&sender.hasPermission("permission.easyskies.gui")) {
			if(args.length<2) {
		    	   sender.sendMessage(server+"Not enough arguements!");
			}else {
			if(Bukkit.getPlayer(args[1])!=null) {	
				Inventory inv = CustomGui.OpenGui(args[0]);
				if (inv!=null) {
				Bukkit.getPlayer(args[1]).openInventory(CustomGui.OpenGui(args[0]));
				}else {
			    	   sender.sendMessage(server+"Not a real gui");
				}
				}
			}
			return true;
		}
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("discord")) {
				Player p = (Player) sender;
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',islandCommandsConfig.getString("Settings.Discord.Message")));
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("auction") && sender instanceof Player&& sender.hasPermission("permission.easySkies.auction")) {
				Player p = (Player) sender;
				playerShop.loadPublicShopsPage(p,0);
				return true;
			}else {
				if(cmd.getName().equalsIgnoreCase("auction") && sender instanceof Player) {
					Player p = (Player) sender;
					p.sendMessage(server+"Please wait until you are a member to do that!");
				}
			}
			if (cmd.getName().equalsIgnoreCase("mode") && sender instanceof Player&& sender.hasPermission("permission.easySkies.lockedcreative")) {
				Player player = (Player) sender;
				if (player.getGameMode().toString()!="CREATIVE" ) {
				Creative.Creative.add(player.getName());
					Creative.saveInventory(player);
					player.getInventory().clear();
					player.setGameMode(GameMode.CREATIVE);
					player.sendMessage(server+"now in creative");
					return true;
					}
				if (player.getGameMode().toString()!="SURVIVAL") {
					Creative.Creative.remove(player.getName());
					player.setGameMode(GameMode.SURVIVAL);
					
					player.getInventory().clear();
					Creative.restoreInventory(player);
					player.sendMessage(server+"now in survival");
					return true;
					//player.teleport(new Location(location.World,location.PositionX,location.PositionY,location.PositionZ));
				}
				return true;
			}else {
				if(cmd.getName().equalsIgnoreCase("mode") && sender instanceof Player) {
					Player p = (Player) sender;
					p.sendMessage(server+"Silly Goose, you can't do that =)");
				}
			}
			if (cmd.getName().equalsIgnoreCase("clist") && sender instanceof Player) {
				Player player = (Player) sender;
				if(player.hasPermission("permission.CreativeLog.List") || player.isOp()) {
					
						String playerlist = server+ChatColor.GOLD +"Players in creative:" + ChatColor.WHITE+" ";
						for(String s :Creative.Creative){
							  playerlist = playerlist.concat(s+", ");
						}
						player.sendMessage(playerlist);
					
					}else {
						
						player.sendMessage(ChatColor.RED +"You don't have permission to do this!");
					}
			
			
			}
			if (cmd.getName().equalsIgnoreCase("freeze") && sender instanceof Player&&sender.hasPermission("permission.easySkies.freeze")&&args.length>=1) {
				
				if(Bukkit.getPlayer(args[0])!=null) {
				Player p = (Player) Bukkit.getPlayer(args[0]);
			if(freeze.contains(p)) {
				freeze.remove(p);
			}
			else {
			freeze.add(p);
			}
			return true;
			}
			}
			if (cmd.getName().equalsIgnoreCase("vanish") && sender instanceof Player&&sender.hasPermission("permission.easySkies.vanish")) {
				Player p = (Player) sender;
			if(vanish.containsKey(p)) {
				p.getInventory().setArmorContents(new ItemStack[4]);
				p.getInventory().setArmorContents(vanish.get(p));
				vanish.remove(p);
				Player playerToHide = p;
				p.removePotionEffect(PotionEffectType.INVISIBILITY);
				p.sendMessage(server+"Now visible to other players");
				p.setGameMode(GameMode.SURVIVAL);
				for(Player allPlayers : getServer().getOnlinePlayers()) {
				    allPlayers.showPlayer(playerToHide);
				}	
			}else {
			vanish.put(p,p.getInventory().getArmorContents());
			p.getInventory().setArmorContents(new ItemStack[4]);
			p.getInventory().setHelmet(createSkull(p.getName(), null, -2,ChatColor.WHITE+"Top Secret Agent "+ p.getName(), ChatColor.RED+"Super Sneaky"));
			Player playerToHide = p;
			p.sendMessage(server+"Now hidden from other players");
			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 200000000, 3));
			p.setGameMode(GameMode.CREATIVE);
			for(Player allPlayers : getServer().getOnlinePlayers()) {
				if(allPlayers.hasPermission("permission.easySkies.viewvanish")== false) {
					allPlayers.hidePlayer(playerToHide);
				}
			}}
			return true;
			}
			if (cmd.getName().equalsIgnoreCase("pets") && sender instanceof Player) {
				Player p = (Player) sender;
				Pets.OpenPets(p);
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("suffix") && sender instanceof Player) {
				Player p = (Player) sender;
				Suffix.OpenSuffix(p);
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("bounty") && sender instanceof Player) {
				Player p = (Player) sender;
				p.openInventory(BountyGui);
				saveConfig();
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("playtime") && sender instanceof Player) {
				if(args.length>=1) {
					Player p = (Player) sender;
					Player s = Bukkit.getPlayer(args[0]);
					if(s==null) {
						p.sendMessage(server+"They have never played!");
						return true;
					}
					long date = (s.getStatistic(Statistic.PLAY_ONE_MINUTE)/1000);
				    long minutes = date% 60;
				    long hours = date / (60) % 24;
				    long days = date / (24 * 60);
					 
					    String fulldate = days+" d "+hours+" h "+minutes+" m ";
					p.sendMessage(server+fulldate);
				}else {
				Player p = (Player) sender;
				long date = (p.getStatistic(Statistic.PLAY_ONE_MINUTE)/1000);
			    long minutes = date% 60;
			    long hours = date / (60) % 24;
			    long days = date / (24 * 60);
				 
				    String fulldate = days+" d "+hours+" h "+minutes+" m ";
				p.sendMessage(server+fulldate);
				return true;
				}
			}
			if (cmd.getName().equalsIgnoreCase("setbounty") && sender instanceof Player) {
				Player p = (Player) sender;
				if(args[0] != null && p.getInventory().getItemInMainHand().getType() != Material.AIR) {
					
				
					if(PlayerBounties.containsKey(args[0])) {
					p.sendMessage(server+"A bounty has already been set for this player!");
						}else {
							if(args[0].equalsIgnoreCase(p.getName())== false&&Bukkit.getPlayer(args[0])!=null) {
								
							PlayerBounties.put(args[0],p.getInventory().getItemInMainHand());
							ItemStack itemToAdd = p.getInventory().getItemInMainHand();
						      p.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
						      Bukkit.broadcastMessage(server+"Bounty set on " + args[0]);
						   
						            
						            ItemMeta meta = itemToAdd.getItemMeta();
						            if (!itemToAdd.getItemMeta().hasDisplayName()) {
						            	meta.setDisplayName(ChatColor.RED +"Kill " +ChatColor.GOLD +args[0] +ChatColor.WHITE +" for "+ itemToAdd.getType().toString().toLowerCase().replaceAll("_", " "));
						            }else {
						        	meta.setDisplayName(ChatColor.RED +"Kill " +ChatColor.GOLD +args[0] +ChatColor.WHITE +" for "+ itemToAdd.getItemMeta().getDisplayName());
						            }
						        	itemToAdd.setItemMeta(meta);
						            BountyGui.addItem(itemToAdd);
						        
							}else {
								p.sendMessage(server+"Invalid bounty!");
							}
						}
					
					
					
				
					return true;
			}else {
				if(sender instanceof Player) {
				Player e = (Player) sender;
				e.sendMessage(server+"Please hold an item to set as a bounty!");
				}
			}
			}}else {
				if(sender instanceof Player) {
				Player p = (Player) sender;
				p.sendMessage(server+"Please specify a player");
				}
			}
		if (cmd.getName().equalsIgnoreCase("islandtoplist")) {
			List<String> players = Main.islandBlockValueConfig.getStringList("Island.Top.Players");
			sender.sendMessage(ChatColor.GOLD+""+ChatColor.BOLD+"Top Islands");
			for(int i =0; players.size()>=i&& i <= Main.islandBlockValueConfig.getInt("Island.Top.Amount"); i++) {
				sender.sendMessage(ChatColor.GOLD+""+ChatColor.BOLD+players.get(i).split(":")[0]+", "+ChatColor.YELLOW+players.get(i).split(":")[1]+" points");
			}
			}
		if (cmd.getName().equalsIgnoreCase("customenchant") 
				&& sender.hasPermission("permission.EasySkies.enchant")) {
			if (args.length > 2) {
				Enchants.giveEnchant(args[0], args[1], args[2]);
			}else {
				if (sender instanceof Player) {
				sender.sendMessage(Enchants.Enchantments.toString());
				}
			}
		}
		if (sender.hasPermission("permission.EasySkies.adminitems") && cmd.getName().equalsIgnoreCase("recipeitem")) {
			if (args.length > 2) {

				if (EasySkyCustomRecipes.getContents().length >= Integer.valueOf(args[1])) {
					for (int i = 0; i < Integer.valueOf(args[2]); i++) {
						Bukkit.getPlayer(args[0]).getInventory()
								.addItem(EasySkyCustomRecipes.getContents()[Integer.valueOf(args[1])]);
					}
				} else {
					if (sender instanceof Player) {
						sender.sendMessage(server + "That is not an item");
					}
				}

			} else {
				if (args.length == 0) {

					if (sender instanceof Player) {
						((Player) sender).openInventory(EasySkyCustomRecipes);
					}
				} else {
					if (sender instanceof Player) {
						sender.sendMessage(server + "Missing arguements!");
					}
				}
			}

		}
		if (sender.hasPermission("permission.EasySkies.adminitems") && cmd.getName().equalsIgnoreCase("adminitem")) {
			if (args.length > 2) {

				if (firstEnable.AdminItems.size() >= Integer.valueOf(args[1])) {
					for (int i = 0; i < Integer.valueOf(args[2]); i++) {
						Bukkit.getPlayer(args[0]).getInventory()
								.addItem(firstEnable.AdminItems.get(Integer.valueOf(args[1])));
					}
				} else {
					if (sender instanceof Player) {
						sender.sendMessage(server + "That is not an admin item");
					}
				}

			} else {
				if (args.length == 0) {

					if (sender instanceof Player) {
						((Player) sender).openInventory(EasySkyAdminItems);
					}
				} else {
					if (sender instanceof Player) {
						sender.sendMessage(server + "Missing arguements!");
					}
				}
			}

		}
		if (sender.hasPermission("permission.EasySkies.etable") && cmd.getName().equalsIgnoreCase("etable")) {
				if(sender instanceof Player) {
				Enchants.openEnchantTable((Player) sender);
				}
			
		}
		if (sender.hasPermission("permission.EasySkies.enchant") && cmd.getName().equalsIgnoreCase("enchanttable")) {
			if (args.length == 1) {
				if(Bukkit.getPlayer(args[0])!=null) {
				Enchants.openEnchantTable(Bukkit.getPlayer(args[0]));
				}
			}
		}
		if (sender.hasPermission("permission.EasySkies.economy") && cmd.getName().equalsIgnoreCase("eco")) {
			if (args.length == 3) {

				if (args[0].equalsIgnoreCase("give")) {
					Money.giveMoney(args[1], args[2]);

				}
				if (args[0].equalsIgnoreCase("take")) {
					Money.takeMoney(args[1], args[2]);
				}
				if (args[0].equalsIgnoreCase("set")) {
					Money.setMoney(args[1], args[2]);
				}

			} else {
				if (sender instanceof Player) {
					sender.sendMessage(server + "Missing arguements!");
				}
			}

		}
		if (sender.hasPermission("permission.EasySkies.islandUpgrades")
				&& cmd.getName().equalsIgnoreCase("resettokens")) {
			if (args.length == 3) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {
					if (args[1].equalsIgnoreCase("set")) {
						config.set("IslandOwnerSettings." + args[0] + ".ResetTokens", Integer.valueOf(args[2]));
						saveConfig();
					}
					if (args[1].equalsIgnoreCase("give")) {
						config.set("IslandOwnerSettings." + args[0] + ".ResetTokens",
								(config.getInt("IslandOwnerSettings." + args[0] + ".ResetTokens")
										+ Integer.valueOf(args[2])));
						saveConfig();
					}
					if (args[1].equalsIgnoreCase("take")) {
						if ((config.getInt("IslandOwnerSettings." + args[0] + ".ResetTokens")
								- Integer.valueOf(args[2])) >= 0) {
							config.set("IslandOwnerSettings." + args[0] + ".ResetTokens",
									(config.getInt("IslandOwnerSettings." + args[0] + ".ResetTokens")
											- Integer.valueOf(args[2])));
							saveConfig();
						}
					}

					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + args[0] + " now has "
								+ config.getInt("IslandOwnerSettings." + args[0] + ".ResetTokens") + " Reset Tokens");
					}

				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}

			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(server + "Missing arguements , /resettokens <player> <give/set/take> <amount>");
				}

			}

		}
		if(sender.hasPermission("permission.EasySkies.shopHelper")&&sender instanceof Player&& cmd.getName().equalsIgnoreCase("shophelper")) {
			Inventory shop = Bukkit.createInventory(null, 54, server + "Shop Creation");
			((Player)sender).openInventory(shop);
			sender.sendMessage("Code will be outputted to chat when closed");
			
		}
		if (sender.hasPermission("permission.EasySkies.islandUpgrades")
				&& cmd.getName().equalsIgnoreCase("islandmembers")) {
			if (args.length == 2) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {

					if (args[1].equalsIgnoreCase("upgrade")) {
						int i = config.getInt("IslandOwnerSettings." + args[0] + ".MembersAllowed")
								+ config.getInt("Upgrades.MemberLimit.Amount");
						if (config.getInt("IslandOwnerSettings.DefaultMembersAllowed")
								+ config.getInt("Upgrades.MemberLimit.Amount")
										* config.getInt("Upgrades.MemberLimit.UpgradeLimit") >= i) {

							config.set("IslandOwnerSettings." + args[0] + ".MembersAllowed", i);
							saveConfig();
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[0] + " Upgraded island Member limit to "
										+ config.getInt("IslandOwnerSettings." + args[0] + ".MembersAllowed"));
							}
						} else {
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + " This player has the largest amount of allowed members!");
							}

						}
					} else {
						if (args[1].equalsIgnoreCase("downgrade")) {
							int i = config.getInt("IslandOwnerSettings." + args[0] + ".MembersAllowed")
									- config.getInt("Upgrades.MemberLimit.Amount");
							if (config.getInt("IslandOwnerSettings.DefaultMembersAllowed") <= i) {

								config.set("IslandOwnerSettings." + args[0] + ".MembersAllowed", i);
								saveConfig();
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[0] + " Downgraded island member count to "
											+ config.getInt("IslandOwnerSettings." + args[0] + ".MembersAllowed"));
								}
							} else {
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(
											server + " This player has the smallest amount of allowed members!");
								}

							}
						} else {
							if (config.getInt("IslandOwnerSettings.DefaultMembersAllowed")
									+ config.getInt("Upgrades.MemberLimit.Amount")
											* config.getInt("Upgrades.MemberLimit.UpgradeLimit") >= Integer
													.valueOf(args[1])
									&& Integer.valueOf(args[1]) >= config
											.getInt("IslandOwnerSettings.DefaultMembersAllowed")) {
								config.set("IslandOwnerSettings." + args[0] + ".MembersAllowed",
										Integer.parseInt(args[1]));
								saveConfig();
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[0] + "'s Island member limit is now " + args[1]);
								}
							} else {
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[1] + " That limit is not a correct limit!");
								}

							}
						}
					}

				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}
			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(server
							+ "Missing arguements , /islandmembers <player> <size> or /islandmembers <player> <upgrade/downgrade>");
				}

			}
		}
		if (sender.hasPermission("permission.EasySkies.islandUpgrades")
				&& cmd.getName().equalsIgnoreCase("islandsize")) {
			if (args.length == 2) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {

					if (args[1].equalsIgnoreCase("upgrade")) {
						int i = config.getInt("IslandOwnerSettings." + args[0] + ".Size")
								+ config.getInt("Upgrades.IslandSize.Size");
						if (config.getInt("IslandOwnerSettings.DefaultIslandSize")
								+ config.getInt("Upgrades.IslandSize.Size")
										* config.getInt("Upgrades.IslandSize.UpgradeLimit") >= i) {

							config.set("IslandOwnerSettings." + args[0] + ".Size", i);
							saveConfig();
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[0] + " Upgraded island size to "
										+ config.getInt("IslandOwnerSettings." + args[0] + ".Size"));
							}
						} else {
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + " This player has the biggest size island!");
							}

						}
					} else {
						if (args[1].equalsIgnoreCase("downgrade")) {
							int i = config.getInt("IslandOwnerSettings." + args[0] + ".Size")
									- config.getInt("Upgrades.IslandSize.Size");
							if (config.getInt("IslandOwnerSettings.DefaultIslandSize") <= i) {

								config.set("IslandOwnerSettings." + args[0] + ".Size", i);
								saveConfig();
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[0] + " Downgraded island size to "
											+ config.getInt("IslandOwnerSettings." + args[0] + ".Size"));
								}
							} else {
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + " This player has the smallest island!");
								}

							}
						} else {
							if (config.getInt("IslandOwnerSettings.DefaultIslandSize")
									+ config.getInt("Upgrades.IslandSize.Size")
											* config.getInt("Upgrades.IslandSize.UpgradeLimit") >= Integer
													.valueOf(args[1])
									&& Integer.valueOf(args[1]) >= config
											.getInt("IslandOwnerSettings.DefaultIslandSize")) {
								config.set("IslandOwnerSettings." + args[0] + ".Size", Integer.parseInt(args[1]));
								saveConfig();
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[0] + "'s Island Size is now " + args[1]);
								}
							} else {
								if (sender instanceof Player) {
									Player player = (Player) sender;
									player.sendMessage(server + args[1] + " That size is not a correct size!");
								}

							}
						}
					}

				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}
			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(server
							+ "Missing arguements , /islandsize <player> <size> or /islandsize <player> <upgrade/downgrade>");
				}

			}
		}
		if (sender.hasPermission("permission.EasySkies.gentier") && cmd.getName().equalsIgnoreCase("gentier")) {
			if (args.length == 2) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {

					if (args[1].equals("promote")) {
						int i = config.getInt("IslandOwnerSettings." + args[0] + ".GenTier") + 1;
						if (config.contains("GeneratorChances." + i)) {

							config.set("IslandOwnerSettings." + args[0] + ".GenTier", i);
							saveConfig();
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[0] + " Promoted to generator tier "
										+ config.getInt("IslandOwnerSettings." + args[0] + ".GenTier"));
							}
						} else {
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + i + " Is not a real generator!");
							}

						}
					} else {
						if (islandGeneratorsConfig.contains("GeneratorChances." + args[1])) {
							config.set("IslandOwnerSettings." + args[0] + ".GenTier", Integer.parseInt(args[1]));
							saveConfig();
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[0] + " Promoted to generator tier " + args[1]);
							}
						} else {
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[1] + " Is not a real generator!");
							}

						}
					}

				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}
			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(
							server + "Missing arguements , /gentier <player> <tier> or /gentier <player> promote");
				}

			}
		}

		if (sender.hasPermission("permission.EasySkies.gentier") && cmd.getName().equalsIgnoreCase("unlockGenTier")) {
			if (args.length == 2) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {

					if (islandGeneratorsConfig.contains("GeneratorChances." + args[1])) {
						List<String> Unlocked = config
								.getStringList("IslandOwnerSettings." + args[0] + ".UnlockedGenTier");
						if (!Unlocked.contains(args[1])) {
							Unlocked.add(args[1]);
						}

						config.set("IslandOwnerSettings." + args[0] + ".UnlockedGenTier", Unlocked);
						saveConfig();
						if (sender instanceof Player) {
							Player player = (Player) sender;
							player.sendMessage(server + args[0] + " unlocked generator tier " + args[1]);
						}
					} else {
						if (sender instanceof Player) {
							Player player = (Player) sender;
							player.sendMessage(server + args[1] + " Is not a real generator!");
						}

					}

				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}
			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(server + "Missing arguements , /unlockGenTier <player> <tier>");
				}

			}
		}
		if (sender.hasPermission("permission.EasySkies.Playerhead") && cmd.getName().equalsIgnoreCase("playerhead")) {
			if (args.length == 1) {
				if (sender instanceof Player) {
					Player player = (Player) sender;
				Main.createSkull(args[0], player.getInventory(), -1, ChatColor.AQUA+args[0]+"'s head", ChatColor.WHITE+"The head of "+args[0]);
				}
			}else {
				if (args.length == 2) {
				Main.createSkull(args[1], Bukkit.getPlayer(args[1]).getInventory(), -1, ChatColor.AQUA+args[1]+"'s head", ChatColor.WHITE+"The head of "+args[1]);
				}else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server+"provide a name!");
					}
				}
			}
			
		}
		if (sender.hasPermission("permission.EasySkies.Lore") && cmd.getName().equalsIgnoreCase("itemlore")) {
			StringBuilder message = new StringBuilder(args[0]);
		    for (int arg = 1; arg < args.length; arg++) {
		      message.append(" ").append(args[arg]);
		      }
		    List<String> lore = new ArrayList<String>();
		    for(String line:message.toString().split("/line/")) {
		    	lore.add(ChatColor.translateAlternateColorCodes('&', line));
		    }
		    if (sender instanceof Player) {
				Player player = (Player) sender;
				ItemMeta meta = player.getInventory().getItemInMainHand().getItemMeta();
				meta.setLore(lore);
				player.getInventory().getItemInMainHand().setItemMeta(meta);
			}
			
		}
		if (sender.hasPermission("permission.EasySkies.minetier") && cmd.getName().equalsIgnoreCase("minetier")) {
			if (args.length == 2) {
				if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {

					if (args[1].equals("promote")) {
						int i = config.getInt("IslandOwnerSettings." + args[0] + ".MineTier") + 1;

						config.set("IslandOwnerSettings." + args[0] + ".MineTier", i);
						saveConfig();
						if (sender instanceof Player) {
							Player player = (Player) sender;
							player.sendMessage(server + args[0] + " Promoted mine to tier "
									+ config.getInt("IslandOwnerSettings." + args[0] + ".MineTier"));
						}

					} else {
						if (Integer.parseInt(args[1]) < config.getInt("IslandOwnerSettings.DefaultMinerSpeed")) {
							config.set("IslandOwnerSettings." + args[0] + ".MineTier", Integer.parseInt(args[1]));
							saveConfig();
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(server + args[0] + " Promoted mine to tier " + args[1]);
							}
						} else {
							if (sender instanceof Player) {
								Player player = (Player) sender;
								player.sendMessage(
										server + " Mine Tier higher than mine speed, cannot do this! " + args[1]);
							}
						}

					}
				} else {
					if (sender instanceof Player) {
						Player player = (Player) sender;
						player.sendMessage(server + "That Player Doesn't own an Island!");
					}
				}
			} else {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					player.sendMessage(
							server + "Missing arguements, /minetier <player> <tier> or /minetier <player> promote");
				}

			}
		}

		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("access") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.Access")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("access")) {

				if (HasAccess.indexOf(player.getName()) == -1) {
					HasAccess.add(player.getName());
					player.sendMessage(server + "Now Bypassing Island Protection");
				} else {
					HasAccess.remove(player.getName());
					player.sendMessage(server + "No Longer Bypassing Island Protection");
				}
			}
			ItemStack pos1 = createDisplay(Material.FEATHER, player.getInventory(), -2,
					ChatColor.GOLD + "Island Corner" + ChatColor.RED + " 1",
					"click this on the first corner of the default island");
			ItemStack pos2 = createDisplay(Material.FEATHER, player.getInventory(), -2,
					ChatColor.GOLD + "Island Corner" + ChatColor.RED + " 2",
					"click this on the second corner of the default island");
			if (cmd.getName().equalsIgnoreCase("reloadskyblock") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.reload")) {
				Bukkit.broadcast(server + "Reloading, hold on tight this is gonna be a wild ride!",
						"permission.EasySkies.reload");
				Bukkit.broadcast(server + "Reloading was blocked... oof",
						"permission.EasySkies.reload");
				Bukkit.broadcast(server + "Finished reloading, that was so radical!",
						"permission.EasySkies.reload");

			}
			if (cmd.getName().equalsIgnoreCase("spawn")&& sender instanceof Player) {
				player.sendMessage(server + "Returning to spawn");
				player.setFallDistance(0);
				Pets.removepet(player);
				player.teleport(firstEnable.Spawn);
				//Pets.removepet(player);
				return true;
			}
			if (cmd.getName().equalsIgnoreCase("removemob") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.mob")) {
				if (player.getTargetBlockExact(10) != null) {
					IslandMobs.removeNPC(player.getTargetBlockExact(10).getLocation());
				} else {
					IslandMobs.removeNPC(player.getLocation());
				}
				player.sendMessage(server + "attempted to remove npc");
			}
			if (cmd.getName().equalsIgnoreCase("removeholo") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.mob")) {
				Holograms.removeHolo(player.getLocation());
				player.sendMessage(server + "Removed hologram");
			}

			if (cmd.getName().equalsIgnoreCase("createholo") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.mob")) {
				Player p = (Player) sender;
				if (args.length > 0) {
					Holograms.CreateHolo(p.getLocation(), args[0], args);

					p.sendMessage(server + "Creating hologram");
				} else {

					p.sendMessage(server + "please provide text");
				}
			}
			if (cmd.getName().equalsIgnoreCase("createblock") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.customblock")) {
				Player p = (Player) sender;
				if(p.getTargetBlockExact(100)!=null) {
				if (args.length > 2) {
					CustomBlocks.CreateBlock(p.getTargetBlockExact(100).getLocation(),args);

					p.sendMessage(server + "Set "+p.getTargetBlockExact(100).getType()+" to run a command");
				} else {

					p.sendMessage(server + "please provide a name, cooldown and a command");
				}
				}else {
					p.sendMessage(server + "please look at a block");
				}
			}
			if (cmd.getName().equalsIgnoreCase("removeblock") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.customblock")) {
				Player p = (Player) sender;
				if(p.getTargetBlockExact(100)!=null) {
				
					CustomBlocks.RemoveBlock(p.getTargetBlockExact(100).getLocation());
					p.sendMessage(server + "Removed "+p.getTargetBlockExact(100).getType());
				}else {
					p.sendMessage(server + "please look at a block");
				}
			}
			if (cmd.getName().equalsIgnoreCase("createmob") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.mob")) {
				Player p = (Player) sender;
				if (args.length > 2) {
					IslandMobs.CreateMob(p.getLocation(), args[0], args[1], args);

					p.sendMessage(server + "Creating Npc");
				} else {

					p.sendMessage(server + "please provide a name and a command");
				}
			}
			if (cmd.getName().equalsIgnoreCase("IslandSetSpawn") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetSpawn")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("IslandSetSpawn")) {
				Player p = (Player) sender;
				Bukkit.dispatchCommand(sender, "worldborder set 30000000");
				getConfig().set("loc.worldspawn", p.getLocation().getWorld().getName());
				getConfig().set("loc.xspawn", p.getLocation().getX());
				getConfig().set("loc.yspawn", p.getLocation().getY());
				getConfig().set("loc.zspawn", p.getLocation().getZ());
				saveConfig();
				p.sendMessage(server+ChatColor.AQUA + "Spawn set");
			}
			if (cmd.getName().equalsIgnoreCase("afkset") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetSpawn")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("afkset")) {
				Player p = (Player) sender;
				getConfig().set("loc.xafk", p.getLocation().getX());
				getConfig().set("loc.yafk", p.getLocation().getY());
				getConfig().set("loc.zafk", p.getLocation().getZ());
				saveConfig();
				p.sendMessage(server+ChatColor.AQUA + "afk set");
			}
			if (cmd.getName().equalsIgnoreCase("SetbossSpawn") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetSpawn")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("SetbossSpawn")) {
				Player p = (Player) sender;
				getConfig().set("loc.xboss", p.getLocation().getX());
				getConfig().set("loc.yboss", p.getLocation().getY());
				getConfig().set("loc.zboss", p.getLocation().getZ());
				saveConfig();
				p.sendMessage(server+ChatColor.AQUA + "boss Spawn set");
			}
			if (cmd.getName().equalsIgnoreCase("seeJail") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetSpawn")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("seejail")) {
				player.teleport(firstEnable.Jail);
				
			}
			if (cmd.getName().equalsIgnoreCase("nJail") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.Jail")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("njail")&&args.length==1) {
				if(Bukkit.getPlayer(args[0])!=null&&player !=Bukkit.getPlayer(args[0])) {
					if(AntiCheat.Jailed.containsKey(Bukkit.getPlayer(args[0]).getName())) {
						AntiCheat.Jailed.remove(Bukkit.getPlayer(args[0]).getName());
						player.sendMessage(Main.server+"UnJailed " + args[0]);
					}else {
					AntiCheat.Jailed.put(Bukkit.getPlayer(args[0]).getName(),System.currentTimeMillis());
					player.sendMessage(Main.server+"Jailed " + args[0]);
					}
				}else {
					player.sendMessage(Main.server+"That player doesn't exist!");
				}
				
			}
			if (cmd.getName().equalsIgnoreCase("IslandSetjail") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetSpawn")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("IslandSetjail")) {
				Player p = (Player) sender;
				getConfig().set("loc.xjail", p.getLocation().getX());
				getConfig().set("loc.yjail", p.getLocation().getY());
				getConfig().set("loc.zjail", p.getLocation().getZ());
				saveConfig();
				firstEnable.Jail=p.getLocation();
				p.sendMessage(ChatColor.AQUA + "Jail set");
			}
			if (cmd.getName().equalsIgnoreCase("code") && sender instanceof Player) {
				if (args.length == 0) {
					if (Main.islandRewardsConfig.getBoolean("Rewards.Enabled")) {
						Rewards.openRewards(player);
					} else {
						player.sendMessage(server + "Codes are disabled!");
					}
				} else {
					if (player.hasPermission("permission.EasySkies.Code") && args[0].equalsIgnoreCase("list")) {
						player.sendMessage(server + ChatColor.WHITE + "" + ChatColor.BOLD + "Codes");
						for (int i = 0; Main.islandRewardsConfig.contains("Rewards." + i); i++) {
							player.sendMessage("                   " + ChatColor.AQUA
									+ Main.islandRewardsConfig.getString("Rewards." + i + ".Code"));
						}
					}
				}
			}

			if (cmd.getName().equalsIgnoreCase("visit") && sender instanceof Player) {
				if (args.length == 1) {
					if (firstEnable.IslandOwners.indexOf(args[0]) != -1) {
						if (config.getBoolean("IslandOwnerSettings." + args[0] + ".Public") || player.isOp() == true
								|| HasAccess.indexOf(player.getName()) != -1) {
							player.sendMessage(server + "Teleporting to " + args[0] + "'s Island!");
							if (firstEnable.PlayerHomes.get(args[0]) == null) {
								player.sendMessage(server + "That player doesn't own an island yet!");
							} else {
								//player.teleport(firstEnable.PlayerHomes.get(args[0]));
								if(Bukkit.getWorld(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[0])== null) {
									player.setFallDistance(0);
									Pets.removepet(player);
									player.teleport(firstEnable.Spawn);
									//Pets.removepet(player);
								}else {
									if(new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[3])).getBlock().getRelative(BlockFace.DOWN).getType().isSolid()== false) {
										new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[3])).getBlock().getRelative(BlockFace.DOWN).setType(Material.GLASS);
									}
									Pets.removepet(player);
									player.setFallDistance(0);
								player.teleport(new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + args[0] + ".Home").split(":")[3])));
								//Pets.removepet(player);
								}
								}
						} else {
							player.sendMessage(server + "That player has island visiting disabled");
						}
					} else {
						player.sendMessage(server + "That player doesn't own an island!");
					}
				} else {
					player.sendMessage(server + "Please specify a player!");
				}
			}
			if (cmd.getName().equalsIgnoreCase("IslandSet") && sender instanceof Player
					&& player.hasPermission("permission.EasySkies.SetIsland")
					|| player.isOp() && cmd.getName().equalsIgnoreCase("IslandSet")) {
				player.getInventory().remove(pos1);
				player.getInventory().remove(pos2);
				if (args.length == 0) {
					createDisplay(Material.FEATHER, player.getInventory(), -1,
							ChatColor.GOLD + "Island Corner" + ChatColor.RED + " 1",
							"click this on the first corner of the default island");
				}
			}

			/*
			 * if (cmd.getName().equalsIgnoreCase("Island") && sender instanceof Player) {
			 * Player player = (Player) sender;
			 * player.sendMessage(server+"Teleporting to Skyblock");
			 * player.teleport(firstEnable.Spawn); }
			 */

			if (cmd.getName().equalsIgnoreCase("island") && sender instanceof Player) {
				if (args.length == 0) {
					if (!firstEnable.PlayerHomes.containsKey(player.getName())) {
						player.openInventory(Create);
						return true;
					}
					player.openInventory(EasySkyGui);
				} else {
					if (args[0].equalsIgnoreCase("settings")) {
						player.openInventory(EasySkySettingsGui);
						return true;
					}
					if (args[0].equalsIgnoreCase("spawn")) {
						player.sendMessage(server + "Returning to spawn");
						player.setFallDistance(0);
						Pets.removepet(player);
						player.teleport(firstEnable.Spawn);
						//Pets.removepet(player);
						return true;
					}
					if (!firstEnable.PlayerHomes.containsKey(player.getName())) {
						player.openInventory(Create);
						return true;
					}
					if (args[0].equalsIgnoreCase("visit")) {
						if (args.length == 1) {
							Bukkit.dispatchCommand(player, "visit ");
						} else {
							Bukkit.dispatchCommand(player, "visit " + args[1]);
						}
					}
					if (args[0].equalsIgnoreCase("shop")
							&& Main.islandCommandsConfig.getBoolean("Commands.AdminShops")) {
						if (args.length == 1) {
							Shops.OpenShop(player, "DefaultShop");
						} else {
							Shops.OpenShop(player, args[1]);
						}
						return true;
					}
					if (args[0].equalsIgnoreCase("quests")) {
						Inventory EasySkyQuests = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "Quests");
						player.openInventory(EasySkyQuests);
						Quests.createQuest(EasySkyQuests);
						return true;
					}
					if (args[0].equalsIgnoreCase("members")) {
						Inventory EasySkyTeam = Bukkit.createInventory(null, (Math.round(
								config.getInt("IslandOwnerSettings." + player.getName() + ".MembersAllowed") / 9) + 1)
								* 9, Main.server + "Members");
						player.openInventory(EasySkyTeam);
						IslandInfo.loadTeamMates(EasySkyTeam, player);
						return true;
					}
					if (args[0].equalsIgnoreCase("upgrades")) {
						IslandUpgrades.loadUpgrades(player);
						return true;
					}
					if (args[0].equalsIgnoreCase("home") || args[0].equalsIgnoreCase("go")) {
						player.sendMessage(server + "Returning home");
						
						if(new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[3])).getBlock().getRelative(BlockFace.DOWN).getType().isSolid()== false) {
							new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[3])).getBlock().getRelative(BlockFace.DOWN).setType(Material.GLASS);
						}
						Pets.removepet(player);
						player.setFallDistance(0);
						player.teleport(new Location(Bukkit.getWorld(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[0]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[1]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[2]),Double.valueOf(config.getString("IslandOwnerSettings." + player.getName() + ".Home").split(":")[3])));
					}
					if (args[0].equalsIgnoreCase("sethome")) {
						if(IslandInfo.getIslandLocation(player.getName()).distance(player.getLocation())<IslandInfo.getIslandSize(player.getName())) {
						if(player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()&&player.getLocation().getBlock().getType() == Material.AIR&&player.getLocation().getBlock().getRelative(BlockFace.UP).getType() == Material.AIR) {
							player.sendMessage(server + "Setting home");
						getConfig().set("IslandOwnerSettings." + player.getName() + ".Home",
								player.getWorld().getName().toString() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":"
										+ player.getLocation().getZ());
						saveConfig();
						}else {
							player.sendMessage(server + "That is not safe!");
						}
						}else {
							player.sendMessage(server + "That is not your island!");
						}
							}
					if (args[0].equalsIgnoreCase("top")) {
						player.openInventory(IslandInfo.EasySkyIslandTop);
					}
					if (args[0].equalsIgnoreCase("level") || args[0].equalsIgnoreCase("lvl")) {
						if (args.length == 1) {
							player.sendMessage(server + ChatColor.GRAY + "This may take a second!");
							if (firstEnable.PlayerHomes.get(player.getName()) != null) {
								player.sendMessage(server + ChatColor.GRAY + "You have a level of " + ChatColor.AQUA
										+ IslandInfo.getIslandLevel(firstEnable.PlayerHomes.get(player.getName()))
										+ ChatColor.GRAY + " Points, and are number " + ChatColor.AQUA
										+ IslandInfo.islandRank(player.getName()) + ChatColor.GRAY + " out of "
										+ ChatColor.AQUA
										+ Main.islandBlockValueConfig.getStringList("Island.Top.Players").size()
										+ ChatColor.GRAY + " players!");
							} else {
								player.sendMessage(server + ChatColor.RED + "You do not own an island!");
							}
							return true;
						}

						if (args.length >= 2) {
							if (args[1].equalsIgnoreCase("add") && player.hasPermission("permission.EasySkies.Level")) {
								String Type = player.getInventory().getItemInMainHand().getType().toString();
								List<String> Value = Main.islandBlockValueConfig.getStringList("Blocks.Values");
								for (String TypeRemove : Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
									if (TypeRemove.split(":")[0].equalsIgnoreCase(Type)) {
										Value.remove(TypeRemove);
									}
								}
								if (args.length == 3) {
									Value.add(Type + ":" + args[2]);
								}
								if (args.length == 2) {
									Value.add(Type + ":" + Main.islandBlockValueConfig.getInt("Blocks.DefaultValues"));
								}
								Main.islandBlockValueConfig.set("Blocks.Values", Value);
								Main.saveBlockValues();
								player.sendMessage(server + "Added " + Type + " to island values!");

							}
							if (args[1].equalsIgnoreCase("remove")
									&& player.hasPermission("permission.EasySkies.Level")) {
								String Type = "none";
								if (args.length == 2) {
									Type = player.getInventory().getItemInMainHand().getType().toString();
								}
								if (args.length == 3) {
									Type = Material.getMaterial(args[2].toUpperCase()).toString();
								}

								List<String> Value = Main.islandBlockValueConfig.getStringList("Blocks.Values");
								for (String TypeRemove : Main.islandBlockValueConfig.getStringList("Blocks.Values")) {
									if (TypeRemove.split(":")[0].equalsIgnoreCase(Type)) {
										Value.remove(TypeRemove);
									}
								}
							}
							if (args.length == 2) {
								player.sendMessage(server + ChatColor.GRAY + "This may take a second!");
								if (firstEnable.PlayerHomes.containsKey(args[1])) {
									player.sendMessage(server + ChatColor.GRAY + "This island has a level of "
											+ ChatColor.AQUA
											+ IslandInfo.getIslandLevel(firstEnable.PlayerHomes.get(args[1]))
											+ ChatColor.GRAY + " points and is in " + ChatColor.AQUA
											+ IslandInfo.islandRank(player.getName()) + ChatColor.GRAY + " Place!");
								} else {
									player.sendMessage(server + ChatColor.RED
											+ "That player may not exist, or the name is not correct!");
								}

							}
							return true;
						}

					}
					if (args[0].equalsIgnoreCase("bal") || args[0].equalsIgnoreCase("balance")) {
						if (args.length == 1) {
							Money.displayMoney(player, player.getName());
						}
						if (args.length == 2) {
							Money.displayMoney(player, args[1]);
						}

						return true;
					}
					if (args[0].equalsIgnoreCase("pay")) {
						if (args.length == 3) {
							Money.payMoney(player, args[1], args[2]);
						} else {
							player.sendMessage(server + "Missing arguements!");
						}

					}

					if (args[0].equalsIgnoreCase("reset")) {
						// player.openInventory(Reset);
						if (config.getInt("IslandOwnerSettings." + player.getName() + ".ResetTokens") > 0) {
							player.openInventory(Reset);
							return true;
						} else {
							player.sendMessage(server + "You have no reset tokens left!");
						}

					}
					if (args[0].equalsIgnoreCase("add")) {
						if (args.length != 2) {
							player.sendMessage(server + "Specify a player to trust");
							return true;
						} else {
							if (Bukkit.getOnlinePlayers().contains(Bukkit.getPlayer(args[1]))
									&& Bukkit.getPlayer(args[1]) != player) {
								if (config.getStringList("IslandOwnerSettings." + player.getName() + ".Coop")
										.size() < config.getInt(
												"IslandOwnerSettings." + player.getName() + ".MembersAllowed")) {
									// "IslandOwnerSettings."+player.getName()+".Coop"
									List<String> Cooped = getConfig()
											.getStringList("IslandOwnerSettings." + player.getName() + ".Coop");
									if (!Cooped.contains(args[1])) {
										Cooped.add(args[1].toString());
										player.sendMessage(server + "Trusted " + args[1]);
									} else {
										player.sendMessage(server + " " + args[1] + " is already trusted!");
									}
									getConfig().set("IslandOwnerSettings." + player.getName() + ".Coop", Cooped);
									saveConfig();
								} else {
									player.sendMessage(server + " Your island is full!");
								}

							} else {
								player.sendMessage(server + " " + args[1] + " isn't online.");
							}
						}
					} else {
						if (args[0].equals("remove")) {
							if (args.length != 2) {
								player.sendMessage(server + "Specify a player to untrust");
								return true;
							} else {
								if (getConfig().getStringList("IslandOwnerSettings." + player.getName() + ".Coop")
										.contains(args[1])) {
									List<String> Cooped = getConfig()
											.getStringList("IslandOwnerSettings." + player.getName() + ".Coop");
									Cooped.remove(args[1].toString());
									getConfig().set("IslandOwnerSettings." + player.getName() + ".Coop", Cooped);
									saveConfig();
									player.sendMessage(server + " " + args[1] + " is no longer trusted on "
											+ player.getName() + "'s island");
								} else {
									player.sendMessage(server + " " + args[1] + " isn't trusted.");
								}

							}

						}

					}
					return true;

				}
			}
		}
		return true;
	}
	public static HashMap<String, String> playerMessages = new HashMap<String,String>();
	 @EventHandler
	    public void onPlayerChat(AsyncPlayerChatEvent e) {
	        Player chatter = e.getPlayer();
	        if(e.getMessage().split("").length<=10) {
	        	playerMessages.put(chatter.getName(),e.getMessage());
	        }else {
	        	String Charactors = (e.getMessage().substring(0, (10))+"...");
	        	 playerMessages.put(chatter.getName(),Charactors);
	        }
	        	
	 }
	@EventHandler
	public void cropgrow(BlockGrowEvent e) {
		if(e.getBlock() instanceof Ageable) {
			Ageable block = (Ageable) e.getBlock();
			block.setAge(block.getMaximumAge());
		}
		
	}
	@EventHandler
	public void onPlayerSneak(PlayerToggleSneakEvent e) {
		Enchants.sneakEnchant(e);
		}
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
	if(Pets.Pets.containsKey(e.getPlayer().getName())) {
		Pets.removepet(e.getPlayer());
	}
	if(Queue.QueuePlayers.containsKey(e.getPlayer().getName())) {
		Queue.Queues.remove(Queue.QueuePlayers.get(e.getPlayer().getName()));
		Queue.QueuePlayers.remove(e.getPlayer().getName());
	}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		addDefaults(e.getPlayer());
		JoinTimes.put(e.getPlayer(), System.currentTimeMillis());
		if(e.getPlayer().getStatistic(Statistic.PLAY_ONE_MINUTE)>(7200*20)&&e.getPlayer().hasPermission("permission.easySkies.member")==false) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex group member user add "+e.getPlayer().getName());
		}
		for (String rawData : getConfig().getStringList("PlayerHomes")) {

			String[] raw = rawData.split(":");
			firstEnable.PlayerHomes.put(raw[0].toString(), new Location(Bukkit.getWorld(raw[1]), Double.valueOf(raw[2]),
					Double.valueOf(raw[3]), Double.valueOf(raw[4])));

		}
	}

	public void addDefaults(Player player) {

		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Home")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Home", "");
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Public")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Public", false);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".MobSpawn")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".MobSpawn", true);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Particle")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Particle", "none");
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Pet")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Pet", "none");
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".PlayTime")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".PlayTime", 0);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Pvp")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Pvp", false);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Coop")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Coop", new ArrayList<String>());
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".Size")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Size",
					config.getInt("IslandOwnerSettings.DefaultIslandSize"));
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".IslandLevel")) {
			config.set("IslandOwnerSettings." + player.getName() + ".IslandLevel",
					islandBlockValueConfig.getInt("Blocks.StartingLevel"));
			Main.getPlugin().saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".IslandRank")) {
			config.set("IslandOwnerSettings." + player.getName() + ".IslandRank", -1);
			Main.getPlugin().saveConfig();
		}

		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".GenTier")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".GenTier", 0);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".MineTier")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".MineTier", 0);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".UnlockedGenTier")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".UnlockedGenTier",
					new ArrayList<String>());
			List<String> Unlocked = config.getStringList("IslandOwnerSettings.DefaultUnlockedGen");
			config.set("IslandOwnerSettings." + player.getName() + ".UnlockedGenTier", Unlocked);
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".CompletedQuests")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".CompletedQuests",
					new ArrayList<String>());
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".MembersAllowed")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".MembersAllowed",
					getConfig().getInt("IslandOwnerSettings.DefaultMembersAllowed"));
			saveConfig();
		}
		if (!getConfig().contains("IslandOwnerSettings." + player.getName() + ".ResetTokens")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".ResetTokens",
					config.getInt("IslandOwnerSettings.DefaultResetTokens"));
			saveConfig();
		}
		////////////////////////////// shops/////////////////////////
		if (!playerShopConfig.contains("ShopOwnerData." + player.getName() + ".ShopSize")) {
			playerShopConfig.options().copyDefaults(true);
			playerShopConfig.addDefault("ShopOwnerData." + player.getName() + ".ShopSize",
					playerShopConfig.getInt("Default.ShopSize"));
			savePlayerShops();
		}
		if (!playerShopConfig.contains("ShopOwnerData." + player.getName() + ".Balance")) {
			playerShopConfig.options().copyDefaults(true);
			playerShopConfig.addDefault("ShopOwnerData." + player.getName() + ".Balance",
					playerShopConfig.getInt("Default.Balance"));
			savePlayerShops();
		}
		if (!playerShopConfig.contains("ShopOwnerData." + player.getName() + ".Selling")) {
			playerShopConfig.options().copyDefaults(true);
			playerShopConfig.addDefault("ShopOwnerData." + player.getName() + ".Selling", new ArrayList<String>());
			savePlayerShops();
		}
		if (!playerShopConfig.contains("ShopOwnerData." + player.getName() + ".Prices")) {
			playerShopConfig.options().copyDefaults(true);
			playerShopConfig.addDefault("ShopOwnerData." + player.getName() + ".Prices", new ArrayList<String>());
			savePlayerShops();
		}
		/////////////////////////////// statistics/////////////////////////
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".Stats.BlocksBroken")) {
			config.addDefault("IslandOwnerSettings." + player.getName() + ".Stats.BlocksBroken", 0);
			Main.getPlugin().saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".Stats.BlocksPlaced")) {
			config.addDefault("IslandOwnerSettings." + player.getName() + ".Stats.BlocksPlaced", 0);
			Main.getPlugin().saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".Stats.MobsKilled")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Stats.MobsKilled",
					new ArrayList<String>());
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".Stats.MobsTamed")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Stats.MobsTamed",
					new ArrayList<String>());
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".Stats.MobsBred")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".Stats.MobsBred",
					new ArrayList<String>());
			saveConfig();
		}
		////////////////////////////////////// Custom-Blocks////////////////////////////////
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".CustomBlocks.Storage")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".CustomBlocks.Storage",
					new ArrayList<String>());
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".CustomBlocks.ItemCollectors")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".CustomBlocks.ItemCollectors",
					new ArrayList<String>());
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".CustomBlocks.VoidCollector")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".CustomBlocks.VoidCollector",
					new ArrayList<String>());
			saveConfig();
		}
		if (!config.contains("IslandOwnerSettings." + player.getName() + ".CustomBlocks.MobSpawner")) {
			getConfig().addDefault("IslandOwnerSettings." + player.getName() + ".CustomBlocks.MobSpawner",
					new ArrayList<String>());
			saveConfig();
		}
	}

	public void createIsland(Player player) {
		if (player.getWorld().getName() == Bukkit.getServer().getWorld(getConfig().getString("loc.world")).getName()) {

			boolean test = false;
			firstEnable.IslandOwners = config.getStringList("IslandOwners");
			if (firstEnable.IslandOwners.indexOf(player.getName()) != -1) {
				test = true;
				if (Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")
						|| Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
					player.openInventory(EasySkyGui);
				} else {
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
				}
				// player.sendMessage(server+"You already own an island!!"
				// +firstEnable.IslandOwners.indexOf(player.getName()));
			}
			if (test != true) {
				test = false;
			}

			if (test == false) {

				addDefaults(player);
				firstEnable.IslandOwners.add(player.getName());
				getConfig().set("IslandOwners", firstEnable.IslandOwners);
				saveConfig();

				player.sendMessage(server + "Creating Island in world: " + firstEnable.SkyBlockWorld.getName());
				blockset(firstEnable.pos1, firstEnable.pos2, player);
				List<String> hashmapData = getConfig().getStringList("PlayerHomes");

				String data = player.getName().toString() + ":" + ChestPos.getWorld().getName().toString() + ":"
						+ ChestPos.getX() + ":" + ChestPos.getY() + ":" + ChestPos.getZ();
				hashmapData.add(data);
				getConfig().set("IslandOwnerSettings." + player.getName() + ".Home",
						ChestPos.getWorld().getName().toString() + ":" + ChestPos.getX() + ":" + ChestPos.getY() + ":"
								+ ChestPos.getZ());
				getConfig().set("PlayerHomes", hashmapData);
				saveConfig();
				for(Entity e:ChestPos.getWorld().getNearbyEntities(firstEnable.pos1, 10, 5, 10)) {
				Entity entity = ChestPos.getWorld().spawnEntity(ChestPos,e.getType());
				entity.setCustomName(e.getCustomName().replace("%player%", player.getName()));
				}
				for (String rawData : getConfig().getStringList("PlayerHomes")) {

					String[] raw = rawData.split(":");
					firstEnable.PlayerHomes.put(raw[0].toString(), new Location(firstEnable.SkyBlockWorld,
							Double.valueOf(raw[2]), Double.valueOf(raw[3]), Double.valueOf(raw[4])));

					if (firstEnable.IslandCountX >= 100) {
						firstEnable.IslandCountX = 0;
						firstEnable.IslandCountZ++;
					}
					getConfig().set("IslandCountZ", firstEnable.IslandCountZ);
					getConfig().set("IslandCountX", firstEnable.IslandCountX);
					saveConfig();

				}
			}
		}
	}

	public void createRecipe(String row1, String row2, String row3, ItemStack Output, String[] materials) {
		@SuppressWarnings("deprecation")
		ShapedRecipe recipe = new ShapedRecipe(Output);
		recipe.shape(row1, row2, row3);
		for (String rawData : materials) {

			String[] raw = rawData.split(":");
			recipe.setIngredient(raw[1].toCharArray()[0], Material.getMaterial(raw[0].toUpperCase()));
		}
		getServer().addRecipe(recipe);

	}

	public static Location getBlockCenter(Block block) {
		return new Location(block.getWorld(), block.getLocation().getBlockX() + .5,
				block.getLocation().getBlockY() + .5, block.getLocation().getBlockZ() + .5);
	}

	public void resetIsland(Player player) {
		player.sendMessage(server + "Reset Island.");
		reset.islandReset(player);

		createIsland(player);
		player.closeInventory();

	}

	public static void fillDisplay(Material material, Inventory inv, String name, String lore) {
		for (int i = 0; inv.getSize() > i; i++) {
			if (inv.getItem(i) == null) {
				createDisplay(material, inv, i, name, lore);
			}
		}
	}

	public static void lineDisplay(Material material, Inventory inv, int line, String name, String lore) {
		if (line > 5 || line * 9 - 1 > inv.getSize()) {
			return;
		}
		for (int i = 0; 9 > i; i++) {

			createDisplay(material, inv, i + line * 9, name, lore);
		}
	}

	public static void borderDisplay(Material material, Inventory inv, String name, String lore) {
		for (int i = 0; inv.getSize() > i; i += 9) {
			lineDisplay(material, inv, 0, name, lore);
			lineDisplay(material, inv, (inv.getSize() / 9) - 1, name, lore);
			if (inv.getItem(i) == null) {
				createDisplay(material, inv, i, name, lore);
			}
			if (inv.getItem(i + 8) == null) {
				createDisplay(material, inv, i + 8, name, lore);
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static ItemStack createSkull(String PlayerName, Inventory inv, int Slot, String name, String lore) {
		ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);

		SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
		skullMeta.setOwner("" + PlayerName);
		skullMeta.setDisplayName(name);
		ArrayList<String> Lore = new ArrayList<String>();
		Lore.add(lore);
		skullMeta.setLore(Lore);
		// meta.addEnchant(enchant, 1, true);
		item.setItemMeta(skullMeta);
		if (Slot > -1) {
			inv.setItem(Slot, item);
		} else {
			if (Slot == -1) {
				inv.addItem(item);
			} else {
				if (Slot == -2) {

					// do nothing
				}
			}
		}
		return item;

	}

	public static ItemStack createDisplay(Material material, Inventory inv, int Slot, String name, String lore) {
		ItemStack item = new ItemStack(material, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);

		ArrayList<String> Lore = new ArrayList<String>();
		int i = 0;
		for (String lores : lore.split("/line/")) {
			Lore.add(ChatColor.GRAY+lores);
			i = 1;
		}
		if (i == 0) {
			Lore.add(lore);
		}
		meta.setLore(Lore);
		// meta.addEnchant(enchant, 1, true);
		item.setItemMeta(meta);
		if (Slot > -1) {
			inv.setItem(Slot, item);
		} else {
			if (Slot == -1) {
				inv.addItem(item);
			} else {
				if (Slot == -2) {

					// do nothing
				}
			}
		}
		return item;

	}
	public static String getRandom(List<String> list) {
	    int rnd = new Random().nextInt(list.size());
	    return (String) list.toArray()[rnd];
	}
	public static void savePlayerShops() {
		try {
			playerShopConfig.options().copyDefaults(true);
			playerShopConfig.save(playerShops);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveBlockValues() {
		try {
			islandBlockValueConfig.options().copyDefaults(true);
			islandBlockValueConfig.save(islandBlockValue);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveCommandValues() {
		try {
			islandCommandsConfig.options().copyDefaults(true);
			islandCommandsConfig.save(Commands);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveQuestsValues() {
		try {
			islandQuestsConfig.options().copyDefaults(true);
			islandQuestsConfig.save(QuestsConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveGenerators() {
		try {
			islandGeneratorsConfig.options().copyDefaults(true);
			islandGeneratorsConfig.save(GenConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveRewards() {
		try {
			islandRewardsConfig.options().copyDefaults(true);
			islandRewardsConfig.save(RewardConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveAdminShops() {
		try {
			shopConfig.options().copyDefaults(true);
			shopConfig.save(shops);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveMobs() {
		try {
			islandMobsConfig.options().copyDefaults(true);
			islandMobsConfig.save(MobConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveHolo() {
		try {
			islandHoloConfig.options().copyDefaults(true);
			islandHoloConfig.save(HoloConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void saveHolidays() {
		try {
			islandHolidayConfig.options().copyDefaults(true);
			islandHolidayConfig.save(holidayConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveCustomBlocks() {
		try {
			customBlocksConfig.options().copyDefaults(true);
			customBlocksConfig.save(customBlocks);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveCustomMobDrops() {
		try {
			customMobDropsConfig.options().copyDefaults(true);
			customMobDropsConfig.save(customMobDrops);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveScoreBoard() {
		try {
			scoreConfig.options().copyDefaults(true);
			scoreConfig.save(scoreBoard);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveGuiConfig() {
		try {
			customGuiConfig.options().copyDefaults(true);
			customGuiConfig.save(guiConfig);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void onEnable() {
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			new easySkiesPlaceHolders().register();
		}
		scoreBoard = new File(getDataFolder(), "ScoreBoard.yml");
		scoreConfig = YamlConfiguration.loadConfiguration(scoreBoard);
		scoreConfig .options().copyDefaults(true);
		saveScoreBoard();
		guiConfig= new File(getDataFolder(), "CustomGui.yml");
		customGuiConfig = YamlConfiguration.loadConfiguration(guiConfig);
		customGuiConfig .options().copyDefaults(true);
		saveGuiConfig();
		customMobDrops = new File(getDataFolder(), "MobDrops.yml");
		customMobDropsConfig = YamlConfiguration.loadConfiguration(customMobDrops);
		customMobDropsConfig.options().copyDefaults(true);
		saveCustomMobDrops();
		playerShops = new File(getDataFolder(), "playerShops.yml");
		playerShopConfig = YamlConfiguration.loadConfiguration(playerShops);
		playerShopConfig.options().copyDefaults(true);
		savePlayerShops();
		islandBlockValue = new File(getDataFolder(), "islandBlockValue.yml");
		islandBlockValueConfig = YamlConfiguration.loadConfiguration(islandBlockValue);
		islandBlockValueConfig.options().copyDefaults(true);
		saveBlockValues();
		Commands = new File(getDataFolder(), "Commands.yml");
		islandCommandsConfig = YamlConfiguration.loadConfiguration(Commands);
		islandCommandsConfig.options().copyDefaults(true);
		QuestsConfig = new File(getDataFolder(), "QuestsConfig.yml");
		islandQuestsConfig = YamlConfiguration.loadConfiguration(QuestsConfig);
		islandQuestsConfig.options().copyDefaults(true);
		saveQuestsValues();
		GenConfig = new File(getDataFolder(), "Generators.yml");
		islandGeneratorsConfig = YamlConfiguration.loadConfiguration(GenConfig);
		islandGeneratorsConfig.options().copyDefaults(true);
		saveGenerators();
		RewardConfig = new File(getDataFolder(), "Rewards.yml");
		islandRewardsConfig = YamlConfiguration.loadConfiguration(RewardConfig);
		islandRewardsConfig.options().copyDefaults(true);
		saveRewards();
		shops = new File(getDataFolder(), "Shops.yml");
		shopConfig = YamlConfiguration.loadConfiguration(shops);
		shopConfig.options().copyDefaults(true);
		saveAdminShops();
		MobConfig = new File(getDataFolder(), "Mobs.yml");
		islandMobsConfig = YamlConfiguration.loadConfiguration(MobConfig);
		islandMobsConfig.options().copyDefaults(true);
		saveMobs();
		HoloConfig = new File(getDataFolder(), "Holograms.yml");
		islandHoloConfig = YamlConfiguration.loadConfiguration(HoloConfig);
		islandHoloConfig.options().copyDefaults(true);
		saveHolo();
		holidayConfig = new File(getDataFolder(), "Holidays.yml");
		islandHolidayConfig = YamlConfiguration.loadConfiguration(holidayConfig);
		islandHolidayConfig.options().copyDefaults(true);
		saveHolidays();
		customBlocks = new File(getDataFolder(), "CustomBlocks.yml");
		customBlocksConfig = YamlConfiguration.loadConfiguration(customBlocks);
		customBlocksConfig.options().copyDefaults(true);
		saveCustomBlocks();
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		plugin = this;
		registerEvents(this, new DeathRespawn(), new Miner(), new playerShop(), new IslandInfo(), new IslandCreation(),
				new IslandProtecton(), new IslandStatistics(), new ItemCollector(), new Enchants(), new MobDrops(), new AntiCheat(),new MobRiding(), new BossEvents(), new Bounties(), new Pets(), new Creative(),new SuperStorage());
		// getCommand("example_command").setExecutor(new Main());
		firstEnable.addConfig();
		getConfig().options().copyDefaults(true);
		 createRecipe("ccc","pfp","ccc",createDisplay(Material.BLAST_FURNACE,EasySkyCustomRecipes, -1, ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest", "Database of any chest within 9 blocks!/line/"+ChatColor.GRAY+"Locked to the island owner!"),new String[] {"piston:p","chest:c","Diamond:f"});
		// createRecipe("iii","ifi","sss",createDisplay(Material.AIR,
		// EasySkyCustomRecipes, -1,"", ""),new String[]
		// {"iron_ingot:i","furnace:f","Smooth_stone:s"});
		
		createRecipe("ccc", "cdc", "ccc",
				createDisplay(Material.OBSIDIAN, EasySkyCustomRecipes, -1, ChatColor.DARK_GRAY + "Forged Obsidian", ""),
				new String[] { "Diamond:d", "Coal_Block:c" });
		createRecipe("dgg", " s ", " s ",
				createDisplay(Material.GOLD_NUGGET, EasySkyCustomRecipes, -1,
						ChatColor.GOLD + "Farmers" + ChatColor.RESET + " Hoe", "Tills the surrounding blocks aswell!"),
				new String[] { "Diamond:d", "Gold_ingot:g", "stick:s" });
		createRecipe("w w", " w ", "w w",
				createDisplay(Material.COBWEB, EasySkyCustomRecipes, -1,
						ChatColor.GOLD + "Cobweb", "Can slow predators"),
				new String[] { "string:w" });
		createRecipe("eie", "idi", "eie",
				createDisplay(Material.IRON_BARS, EasySkyCustomRecipes, -1,
						ChatColor.GRAY + "Mob Cage",ChatColor.AQUA+ "Can hold up to four mobs, right click to use"),
				new String[] { "iron_bars:i","ender_pearl:e","Iron_door:d" });
		createRecipe("www", "wew", "www",
				createDisplay(Material.VINE, EasySkyCustomRecipes, -1,
						ChatColor.YELLOW + "Empty Net", ChatColor.AQUA+"can capture mobs when right clicked!"),
				new String[] { "cobweb:w","ender_pearl:e" });
		createRecipe("r r", " r ", "r r",
				new ItemStack(Material.LEATHER,2),
				new String[] { "rotten_flesh:r",});
		createRecipe("sss", "sws", "sss",
				createDisplay(Material.SLIME_BALL, EasySkyCustomRecipes, -1,
						ChatColor.GREEN + "Slime" + ChatColor.RESET + " Detector",
						"Click on a block to see if its in a slime chunk"),
				new String[] { "stick:w", "slime_block:s" });
		createRecipe(
				" l ", "lsl", " l ", createDisplay(Material.OAK_LEAVES, EasySkyCustomRecipes, -1,
						ChatColor.GREEN + "Tree grow", "Click on a sapling to grow it instantly"),
				new String[] { "OAK_LEAVES:l", "oak_planks:s" });
		// createRecipe("ggg","gag","ggg",createDisplay(Material.ENCHANTED_GOLDEN_APPLE,EasySkyCustomRecipes,
		// -1, ChatColor.GOLD+"Enchanted"+ChatColor.RED+" Apple",""),new String[]
		// {"Gold_block:g","Golden_apple:a"});
		// createRecipe(" s ","lll"," l
		// ",createDisplay(Material.APPLE,EasySkyCustomRecipes,
		// -1,ChatColor.RED+"Apple",""),new String[] {"stick:s","OAK_LEAVES:l"});
		createRecipe("l l", "fbf", "i i",
				createDisplay(Material.DIAMOND_BOOTS, EasySkyCustomRecipes, -1,
						ChatColor.BLUE + "Winged" + ChatColor.WHITE + " Boots", ChatColor.RED + "1400/1400"),
				new String[] { "Lead:l", "feather:f", "Diamond_boots:b", "iron_ingot:i" });
		ItemStack DiamondShears = createDisplay(Material.SHEARS, EasySkyCustomRecipes, -1,
				ChatColor.AQUA + "Diamond" + ChatColor.WHITE + " Shears", "");
		ItemMeta ShearMeta = DiamondShears.getItemMeta();
		ShearMeta.setUnbreakable(true);
		DiamondShears.setItemMeta(ShearMeta);
		ItemStack FortunicHoe = createDisplay(Material.GOLDEN_HOE, EasySkyCustomRecipes, -2,
				ChatColor.GOLD + "Fortunic" + ChatColor.WHITE + " Hoe", "");
		ItemMeta HoeMeta = FortunicHoe.getItemMeta();
		HoeMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 10, true);
		FortunicHoe.setItemMeta(HoeMeta);
		EasySkyCustomRecipes.addItem(FortunicHoe);
		createRecipe(" d ", "d  ", "   ", DiamondShears, new String[] { "Diamond:d" });
		createRecipe(" pp","pwp","pp ",createDisplay(Material.PAPER, EasySkyCustomRecipes, -1,
				ChatColor.RED + "Small" + ChatColor.WHITE + " Bandage", "Heals 3 hearts when right clicked"),new String[] {"Paper:p","WHITE_WOOL:w"});
		 createRecipe(" pp","pip","pp ",createDisplay(Material.PAPER,EasySkyCustomRecipes,-1,ChatColor.BLUE+"Large"+ChatColor.WHITE+" Bandage","Heals to full health when right clicked"),new String[] {"white_wool:p","Iron_Block:i"});
		createRecipe("lgl", "ghg", "lgl", FortunicHoe,
				new String[] { "Gold_Block:g", "Lapis_Block:l", "Golden_Hoe:h" });
		createRecipe("mbp", "bec", "flr",
				createDisplay(Material.ENDER_PEARL, EasySkyCustomRecipes, -1,
						ChatColor.GRAY + "Death" + ChatColor.RED + " Rune", "Keep items after death when in inventory"),
				new String[] { "cooked_mutton:m","Cooked_beef:b","Cooked_porkchop:p","beetroot:b","cooked_chicken:c","cooked_cod:f", "cooked_salmon:l","cooked_rabbit:r", "ender_eye:e" });
		createRecipe(
				"rcr", "cpc", "rcr", createDisplay(Material.CHEST, EasySkyCustomRecipes, -1,
						ChatColor.AQUA + "" + ChatColor.BOLD + "Miner", "Mines Automatically"),
				new String[] { "Chest:c", "Iron_pickaxe:p", "Redstone_block:r" });
		createRecipe("dci", "blr", "iwd",
				createDisplay(Material.LEAD, EasySkyCustomRecipes, -1, ChatColor.GOLD + "Bare-Back Harness",ChatColor.WHITE+"Can be used to mount:/line/"+ChatColor.GRAY+"Chickens/line/"+ChatColor.GRAY+"Cows/line/"+ChatColor.GRAY+"Pigs/line/"+ChatColor.RED+"5/10 Speed"),
				new String[] { "Diamond:d", "Iron_ingot:i","cooked_cod:c","cocoa_beans:b","lead:l","carrot:r","wheat:w" });
		createRecipe("ded", "eye", "ded",
				createDisplay(Material.ELYTRA, EasySkyCustomRecipes, -1, ChatColor.GOLD + "Winged Saddle",ChatColor.WHITE+"Can be used to fly:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GREEN+"15/10 Speed"),
				new String[] { "Diamond_block:d", "elytra:y","end_crystal:e" });
		createRecipe("ded", "bsh", "dfd",
				createDisplay(Material.SADDLE, EasySkyCustomRecipes, -1, ChatColor.RED + "Master Saddle",ChatColor.WHITE+"Can be used to ride:/line/"+ChatColor.GRAY+"Any passive mob/line/"+ChatColor.GOLD+"10/10 Speed"),
				new String[] { "Diamond:d", "ender_eye:e","rabbit_foot:b","saddle:s","hay_block:h","feather:f" });	
		createRecipe(" f ", " f ", " b ",
				createDisplay(Material.GOLDEN_SWORD, EasySkyCustomRecipes, -1, ChatColor.YELLOW+ "Blazing "+ChatColor.RED+"Saber",ChatColor.WHITE+"Shoots fire, thats all"),
				new String[] { "fire_charge:f", "blaze_rod:b"});
		createRecipe("bbb", " s ", "bbb",
				createDisplay(Material.BLAZE_ROD, EasySkyCustomRecipes, -1, ChatColor.YELLOW+ "Blaze "+ChatColor.RED+"Rod",ChatColor.RED+"Semi-Junk"),
				new String[] { "stick:s", "blaze_powder:b"});
		
		createRecipe("geg", "psp", "oto",
				createDisplay(Material.STICK, EasySkyCustomRecipes, -1, ChatColor.GOLD+ "Telekinetic "+ChatColor.BLUE+"QuarterStaff",ChatColor.WHITE+"sucks blocks out of the ground and launches them!"),
				new String[] { "glistering_melon_slice:g", "ender_eye:e", "phantom_membrane:p","stick:s","obsidian:o","end_stone:t"});
		createRecipe("isr", "pep", "rsi",
				createDisplay(Material.IRON_SWORD, EasySkyCustomRecipes, -1, ChatColor.RED+ "Switch "+ChatColor.AQUA+"Blade",ChatColor.GRAY+"Shift Click to swap bullet patterns!"),
				new String[] { "iron_sword:i", "string:s", "redstone:r","ender_eye:e","sticky_piston:p"});
		for (String rawData : config.getStringList("PlayerHomes")) {

			String[] raw = rawData.split(":");
			firstEnable.PlayerHomes.put(raw[0].toString(), new Location(Bukkit.getWorld(raw[1]), Double.valueOf(raw[2]),
					Double.valueOf(raw[3]), Double.valueOf(raw[4])));

		}

	}

	public void onDisable() {
		for(String p:Pets.Pets.keySet()) {
			Pets.Pets.get(p).remove();
		}
		for(LivingEntity ent:BossEvents.bossMobs) {ent.remove();}
		BossEvents.BossMob=null;
		getConfig().set("IslandCountZ", firstEnable.IslandCountZ);
		getConfig().set("IslandCountX", firstEnable.IslandCountX);
		saveConfig();
		plugin = null;

	}

	public static boolean equals(ItemStack a, ItemStack b)
	{
	    if(a == null || b == null)
	        return false;
	    if(a.getType() != b.getType())
	        return false;
	    if(a.hasItemMeta() != b.hasItemMeta())
	        return false;
	    if(a.hasItemMeta() && !a.getItemMeta().equals(b.getItemMeta()))
	        return false;
	    return true;
	}
}
