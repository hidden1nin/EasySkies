package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IslandUpgrades {
	
public static void loadUpgrades(Player player) {
	Inventory EasySkyIslandUpgrades = Bukkit.createInventory(null,36,Main.server+"Island Upgrades");
	Main.lineDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, EasySkyIslandUpgrades, 3, " ", "");
	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE,EasySkyIslandUpgrades , " ", "");
	Main.createDisplay(Material.RED_STAINED_GLASS,EasySkyIslandUpgrades, EasySkyIslandUpgrades.getSize()-5, ChatColor.WHITE+"Back", "");
	Main.createDisplay(Material.NETHER_STAR, EasySkyIslandUpgrades,13 , ChatColor.RED+"Coming Soon!", "");
	addUpgrade(EasySkyIslandUpgrades, player);
	player.openInventory(EasySkyIslandUpgrades);
}
public static void addUpgrade(Inventory easySkyIslandUpgrades, Player player) {
	ItemStack Size = Main.createDisplay(Material.GRASS_BLOCK, easySkyIslandUpgrades, -2, ChatColor.WHITE+"Upgrade Island Size", ChatColor.AQUA+"Current Size: "+ChatColor.GRAY+firstEnable.config.getString("IslandOwnerSettings."+player.getName()+".Size"));
	ItemMeta SizeMeta =Size.getItemMeta();
	List<String> SizeLore = SizeMeta.getLore();
	if(firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")+firstEnable.config.getInt("Upgrades.IslandSize.Size")<=firstEnable.config.getInt("IslandOwnerSettings.DefaultIslandSize")+firstEnable.config.getInt("Upgrades.IslandSize.Size")*firstEnable.config.getInt("Upgrades.IslandSize.UpgradeLimit")) {
		SizeLore.add(ChatColor.AQUA+"Next Island Size: "+ChatColor.WHITE+String.valueOf(firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")+firstEnable.config.getInt("Upgrades.IslandSize.Size")));
		SizeLore.add(ChatColor.AQUA+"Cost: "+ChatColor.GOLD+ String.valueOf(firstEnable.config.getInt("Upgrades.IslandSize.Cost")*((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")-firstEnable.config.getInt("IslandOwnerSettings.DefaultIslandSize"))/firstEnable.config.getInt("Upgrades.IslandSize.Size")+1)));
		
	}else {
		SizeLore.add(ChatColor.RED+""+ChatColor.BOLD+"Max Island Size");
	}
	SizeMeta.setLore(SizeLore);
	Size.setItemMeta(SizeMeta);
	easySkyIslandUpgrades.setItem(11, Size);
	
	
	ItemStack Members = Main.createDisplay(Material.PLAYER_HEAD, easySkyIslandUpgrades, -2, ChatColor.WHITE+"Upgrade Member Limit", ChatColor.AQUA+"Current Member Limit: "+ChatColor.GRAY+firstEnable.config.getString("IslandOwnerSettings."+player.getName()+".MembersAllowed"));
	ItemMeta MemberMeta = Members.getItemMeta();
	List<String> MemberLore = MemberMeta.getLore();
	int i =firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")+firstEnable.config.getInt("Upgrades.MemberLimit.Amount");
	if(firstEnable.config.getInt("IslandOwnerSettings.DefaultMembersAllowed")+firstEnable.config.getInt("Upgrades.MemberLimit.Amount")*firstEnable.config.getInt("Upgrades.MemberLimit.UpgradeLimit")>=i) {
	MemberLore.add(ChatColor.AQUA+"Next Member Limit: "+ChatColor.WHITE+String.valueOf(i));
	MemberLore.add(ChatColor.AQUA+"Cost: "+ChatColor.GOLD+ String.valueOf((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")-firstEnable.config.getInt("IslandOwnerSettings.DefaultMembersAllowed")+1)/firstEnable.config.getInt("Upgrades.MemberLimit.Amount")*firstEnable.config.getInt("Upgrades.MemberLimit.Cost")));
	}else {
		MemberLore.add(ChatColor.RED+""+ChatColor.BOLD+"Max Members");
	}
	
	MemberMeta.setLore(MemberLore);
	Members.setItemMeta(MemberMeta);
	easySkyIslandUpgrades.setItem(15, Members);
	//Main.createDisplay(Material.DIAMOND_PICKAXE, easySkyIslandUpgrades, 6, ChatColor.WHITE+"Upgrade Mine Speed", ChatColor.GOLD+"Current Mine Speed: "+ChatColor.GREEN+firstEnable.config.getString("IslandOwnerSettings."+player.getName()+".MineTier"));
	
}
public static void buyUpgrades(Player player, ItemStack clicked) {
	ItemStack backButton =Main.createDisplay(Material.RED_STAINED_GLASS, null, -2, ChatColor.WHITE+"Back", "");
	ItemStack filler =Main.createDisplay(Material.BLACK_STAINED_GLASS_PANE,player.getInventory(),-2, " ","");
	ItemStack filler2 = Main.createDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE,null,-2, " ", "");
	
	if(clicked.isSimilar(filler)||clicked.isSimilar(filler2)) {
		
		return;
		
	}
	if(clicked.isSimilar(backButton)) {
		if(Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")||Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
			player.openInventory(Main.EasySkyGui);
			}else {
			Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
			}
	}
	if(clicked.getType()==Material.GRASS_BLOCK) {
		if((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")-firstEnable.config.getInt("IslandOwnerSettings.DefaultIslandSize"))/firstEnable.config.getInt("Upgrades.IslandSize.Size")>=firstEnable.config.getInt("Upgrades.IslandSize.UpgradeLimit")) {
			player.sendMessage(Main.server+"You already have the biggest island size!");
			player.closeInventory();
			return;
		}
		if(Money.playerBalance(player.getName())>firstEnable.config.getInt("Upgrades.IslandSize.Cost")*((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")-firstEnable.config.getInt("IslandOwnerSettings.DefaultIslandSize"))/firstEnable.config.getInt("Upgrades.IslandSize.Size")+1)) {
			Money.takeMoney(player.getName(), String.valueOf(firstEnable.config.getInt("Upgrades.IslandSize.Cost")*((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".Size")-firstEnable.config.getInt("IslandOwnerSettings.DefaultIslandSize"))/firstEnable.config.getInt("Upgrades.IslandSize.Size")+1)));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(),"islandsize "+player.getName()+" upgrade");
			player.sendMessage(Main.server+"Island size upgraded!");
			loadUpgrades(player);
		}else {
			player.sendMessage(Main.server+"You need more money for that!");
		}
		
	}
	if(clicked.getType()==Material.PLAYER_HEAD) {
		if((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")-firstEnable.config.getInt("IslandOwnerSettings.DefaultMembersAllowed"))/firstEnable.config.getInt("Upgrades.MemberLimit.Amount")>=firstEnable.config.getInt("Upgrades.MemberLimit.UpgradeLimit")) {
			player.sendMessage(Main.server+"You already have the max amount of players allowed!");
			player.closeInventory();
			return;
		}
		if(Money.playerBalance(player.getName())>(firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")-firstEnable.config.getInt("IslandOwnerSettings.DefaultMembersAllowed")+1)/firstEnable.config.getInt("Upgrades.MemberLimit.Amount")*firstEnable.config.getInt("Upgrades.MemberLimit.Cost")) {
			Money.takeMoney(player.getName(), String.valueOf((firstEnable.config.getInt("IslandOwnerSettings."+player.getName()+".MembersAllowed")-firstEnable.config.getInt("IslandOwnerSettings.DefaultMembersAllowed")+1)/firstEnable.config.getInt("Upgrades.MemberLimit.Amount")*firstEnable.config.getInt("Upgrades.MemberLimit.Cost")));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(),"islandmembers "+player.getName()+" upgrade");
			player.sendMessage(Main.server+"Island member limit increased!");
			loadUpgrades(player);
		}else {
			player.sendMessage(Main.server+"You need more money for that!");
		}
		
	}
	
	
	
	
}
}
