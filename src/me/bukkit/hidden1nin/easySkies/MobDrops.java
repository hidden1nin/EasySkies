package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class MobDrops implements Listener{
	@EventHandler
	public void killedMob(EntityDeathEvent e) {
		Entity dead = e.getEntity();
		if (e.getEntity().getKiller() instanceof Player) {
			if (e.getEntity().getKiller().getInventory().getItemInMainHand() != null) {
				Player player = e.getEntity().getKiller();
				if (Main.customMobDropsConfig.contains("Mobs.List")) {
					for (String i : Main.customMobDropsConfig.getConfigurationSection("Mobs.List").getKeys(false)) {
						if (i.toLowerCase().contains(dead.getType().toString().toLowerCase())) {
							for(int i1 =0;i1<Enchants.getEnchantLevel("Ransack",player.getInventory().getItemInMainHand());i1++) {
								boolean gotsomething = false;
									while (gotsomething  == false) {
								    String item=Main.getRandom(Main.customMobDropsConfig.getStringList("Mobs.List."+i)); 
									String[] args = item.split(":");
									int Random = (int)(Math.random()*100);
									if(args.length==3) {
										if(Random<=Integer.valueOf(args[2])) {
											if(args[0].equalsIgnoreCase("command")) {
												Bukkit.dispatchCommand(Bukkit.getConsoleSender(), args[1].replace("%player%", player.getName()));
												gotsomething = true;
												break;
											}else {
												if(player.getInventory().firstEmpty()>-1) {
													player.getInventory().addItem(new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
												}else {
												player.getWorld().dropItemNaturally(player.getLocation(), new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
												}gotsomething = true;
												break;
											}
										}
									}else {
									if(args.length==5) {
										if(Random<=Integer.valueOf(args[4])) {
											ItemStack gift = Main.createDisplay(Material.getMaterial(args[0].toUpperCase()), null, -2, ChatColor.translateAlternateColorCodes('&', args[1]).replace("%player%", player.getName()), ChatColor.translateAlternateColorCodes('&', args[2]).replace("%player%", player.getName()));
											gift.setAmount(Integer.valueOf(args[3]));
											if(player.getInventory().firstEmpty()>-1) {
												player.getInventory().addItem(gift);
											}else {
											player.getWorld().dropItemNaturally(player.getLocation(),gift);
											}
											//
											gotsomething = true;
											break;
										}
									}
									}
									}
					    	}
							
						}
					}
				}
			}
		}
	}
}
