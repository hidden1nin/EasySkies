package me.bukkit.hidden1nin.easySkies;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class IslandCreation implements Listener{
	@EventHandler
	public void onPlayerUse(PlayerInteractEvent event){
	    Player p = event.getPlayer();
	    
	    
	    
	    ItemStack pos1 = Main.createDisplay(Material.FEATHER, p.getInventory() ,-2, ChatColor.GOLD+"Island Corner"+ChatColor.RED+" 1", "click this on the first corner of the default island");
	    ItemStack pos2 = Main.createDisplay(Material.FEATHER, p.getInventory() ,-2, ChatColor.GOLD+"Island Corner"+ChatColor.RED+" 2", "click this on the second corner of the default island");
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)); {
	    if(p.getInventory().getItemInMainHand().isSimilar(pos1)){
	    	firstEnable.pos1 = event.getClickedBlock().getLocation();
	    	Location loc = event.getClickedBlock().getLocation();
	    	firstEnable.config.set("loc.world", p.getLocation().getWorld().getName());
	    	firstEnable.config.set("loc.x", loc.getX());
	    	firstEnable.config.set("loc.y", loc.getY());
	    	firstEnable.config.set("loc.z", loc.getZ());
	    	Main.getPlugin().saveConfig();
	    	p.getInventory().remove(pos1);
	    	Main.createDisplay(Material.FEATHER, p.getInventory(), -1, ChatColor.GOLD+"Island Corner"+ChatColor.RED+" 2", "click this on the second corner of the default island");
			p.sendMessage(ChatColor.AQUA+"Corner 1 set");
			final Entity shulker =  firstEnable.pos1.getWorld().spawnEntity(firstEnable.pos1,EntityType.SHULKER);
            shulker.setGlowing(true); //Glow
            shulker.setInvulnerable(true);
            firstEnable.wait = true;
            firstEnable.tp = true;
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() { public void run() { shulker.teleport(new Location(shulker.getWorld(),shulker.getLocation().getBlockX(),shulker.getLocation().getBlockY()-260,shulker.getLocation().getBlockZ())); } }, 20 * 9); // 20 (one second in ticks) * 5 (seconds to firstEnable.wait)
            
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() { public void run() { firstEnable.wait = false; } }, 20 * 3); // 20 (one second in ticks) * 5 (seconds to firstEnable.wait)
            //Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() { public void run() { firstEnable.tp = false; } }, 20 * 10);
	    }
	    if(p.getInventory().getItemInMainHand().isSimilar(pos2) && firstEnable.wait == false){
		       
	    	firstEnable.pos2 = event.getClickedBlock().getLocation();
	    	Location loc = event.getClickedBlock().getLocation();
	    	firstEnable.config.set("loc.world2", p.getLocation().getWorld().getName());
	    	firstEnable.config.set("loc.x2", loc.getX());
	    	firstEnable.config.set("loc.y2", loc.getY());
	    	firstEnable.config.set("loc.z2", loc.getZ());
	    	Main.getPlugin().saveConfig();
	    	p.getInventory().remove(pos2);
	    	p.sendMessage(ChatColor.AQUA+"Corner 2 set");
	    	final Entity shulker1 =  firstEnable.pos2.getWorld().spawnEntity(firstEnable.pos2,EntityType.SHULKER);
            shulker1.setGlowing(true); //Glow
            shulker1.setInvulnerable(true);
            firstEnable.wait = true;
            firstEnable.tp = true;
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() { public void run() { shulker1.teleport(new Location(shulker1.getWorld(),shulker1.getLocation().getBlockX(),shulker1.getLocation().getBlockY()-260,shulker1.getLocation().getBlockZ())); } }, 20 * 9); // 20 (one second in ticks) * 5 (seconds to firstEnable.wait)
            
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() { public void run() { firstEnable.tp = false; } }, 20 * 10); // 20 (one second in ticks) * 5 (seconds to firstEnable.wait)
            
	    	
	    }}
	    }
}
