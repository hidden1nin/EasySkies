package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlastFurnace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SuperStorage implements Listener {
public static HashMap<String, ArrayList<ItemStack>> itemDataBase=new HashMap<String, ArrayList<ItemStack>>();
	@EventHandler
	public void onOpenChest(PlayerInteractEvent event) {
		if(event.getAction()==Action.RIGHT_CLICK_BLOCK) {
		if(event.getClickedBlock()!=null) {
			if(event.getClickedBlock().getType()==Material.CHEST) {
			if(IslandInfo.IsChestNearIslandStorage(event.getPlayer().getName(), event.getClickedBlock().getLocation())==true) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(Main.server+"That chest is linked to a database!");
				}
			}
			 if(firstEnable.PlayerHomes.get(IslandInfo.getIsland(event.getPlayer().getLocation()))==IslandInfo.getIslandLocation(event.getPlayer().getName())) {
					
			if(event.getClickedBlock().getType()==Material.BLAST_FURNACE) {
					BlastFurnace chest = (BlastFurnace)event.getClickedBlock().getState();
					if(chest.getCustomName().equalsIgnoreCase(ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest")) {
						event.setCancelled(true);
						SuperStorage.OpenStorage(event.getClickedBlock().getLocation(),event.getPlayer());
					}
				}
			 }else {
				 if(event.getClickedBlock().getType()==Material.BLAST_FURNACE) {
					 BlastFurnace chest = (BlastFurnace)event.getClickedBlock().getState();
						if(chest.getCustomName().equalsIgnoreCase(ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest")) {
							event.getPlayer().sendMessage(Main.server+"You can't open other players databases!");
							event.setCancelled(true);
						}
				 }
			 }
		}
		}
	}

	private static void OpenStorage(Location location, Player player) {
		Inventory storage= Bukkit.createInventory(null, 54,Main.server+"Storage Page 1");{
			Main.lineDisplay(Material.GRAY_STAINED_GLASS_PANE, storage, 5,ChatColor.GRAY+" ",ChatColor.GRAY+" ");
			Main.createDisplay(Material.GREEN_STAINED_GLASS_PANE, storage, 45,ChatColor.GRAY+"Previous Page",ChatColor.GRAY+" ");
			Main.createDisplay(Material.GREEN_STAINED_GLASS_PANE, storage, 53,ChatColor.GRAY+"Next Page",ChatColor.GRAY+" ");
		}
		ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		Location l =location;
		for(int x = l.getBlockX()-4;x<l.getBlockX()+5;x++) {
			for(int y = l.getBlockY()-4;y<l.getBlockY()+5;y++) {
				for(int z = l.getBlockZ()-4;z<l.getBlockZ()+5;z++) {
					Location loc = new Location(l.getWorld(),x,y,z);
					if(loc.getBlock().getType()==Material.CHEST){
						Chest chest = (Chest) loc.getBlock().getState();
						for(ItemStack i:chest.getInventory().getContents()) {
							if(i!=null) {
							items.add(i);
							}
						}
						chest.getInventory().setContents(new ItemStack[1]);
					}
				}
			}
		}
				
		player.openInventory(storage);
		
		itemDataBase.put(player.getName(), items);
	}

	@EventHandler
	public void onPlacedChest(BlockPlaceEvent event) {
		 Player p = event.getPlayer();
		 if(firstEnable.PlayerHomes.get(IslandInfo.getIsland(p.getLocation()))==IslandInfo.getIslandLocation(p.getName())) {
		if(event.getBlock().getType()==Material.BLAST_FURNACE) {
			BlastFurnace chest = (BlastFurnace)event.getBlock().getState();
			if(chest.getCustomName().equalsIgnoreCase(ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest")) {
			if(IslandInfo.IsNearIslandStorage(p.getName(), event.getBlock().getLocation())==false) {
				List<String> ChestLocation= firstEnable.config.getStringList("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.Storage");	
				Location l = event.getBlock().getLocation();
				ChestLocation.add(l.getBlockX()+":"+l.getBlockY()+":"+l.getBlockZ());
				firstEnable.config.set("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.Storage",ChestLocation);	
				Main.getPlugin().saveConfig();
				p.sendMessage(Main.server+"New Storage Registered!");
			}else {
				event.setCancelled(true);
				p.sendMessage(Main.server+"Thats to close to another storage!");
			}
		}
		}
		}else {
			if(event.getBlock().getType()==Material.BLAST_FURNACE) {
				BlastFurnace chest = (BlastFurnace)event.getBlock().getState();
				if(chest.getCustomName().equalsIgnoreCase(ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest")) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(Main.server+"Can't start a database on someone else's island!");
				}}
		}
		}
	@EventHandler
	public void onBreakChest(BlockBreakEvent event) {
		 Player p = event.getPlayer();
		 if(IslandInfo.getIslandStorage(event.getPlayer().getName())== null) {
			 return;
		 }
		 if(firstEnable.PlayerHomes.get(IslandInfo.getIsland(p.getLocation()))==IslandInfo.getIslandLocation(p.getName())) {
			 if(IslandInfo.getIslandStorage(p.getName()).contains(event.getBlock().getLocation())) {
			List<String> ChestLocation= firstEnable.config.getStringList("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.Storage");	
			Location l = event.getBlock().getLocation();
			ChestLocation.remove(l.getBlockX()+":"+l.getBlockY()+":"+l.getBlockZ());
			p.sendMessage(Main.server+"Database Destroyed!");
			firstEnable.config.set("IslandOwnerSettings."+event.getPlayer().getName()+".CustomBlocks.Storage",ChestLocation);	
			Main.getPlugin().saveConfig();
			}
	    
		 }else {
			 if(event.getBlock().getType()==Material.BLAST_FURNACE) {
				 BlastFurnace chest = (BlastFurnace)event.getBlock().getState();
					if(chest.getCustomName().equalsIgnoreCase(ChatColor.YELLOW+""+ChatColor.BOLD+"Super"+ChatColor.GRAY+" Chest")) {
						p.sendMessage(Main.server+"You can't destroy other players databases!");
						event.setCancelled(true);
					}
			 }
		 }
	}
	
}
