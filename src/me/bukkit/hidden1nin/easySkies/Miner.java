package me.bukkit.hidden1nin.easySkies;

import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;

public class Miner implements Listener{
	public static FileConfiguration config = Main.getPlugin().getConfig();
	 @EventHandler
	    public void onInventoryOpenEvent(InventoryOpenEvent e){
		 Player p = (Player) e.getPlayer();
		 Location l = e.getPlayer().getLocation();
		 ItemStack[] Contents = e.getInventory().getContents();
	        if (e.getInventory().getHolder() instanceof Chest && e.getView().getTitle().equals(ChatColor.AQUA+""+ChatColor.BOLD+"Miner") && !IslandInfo.getIsland(l).equals("none")){
	        	if(!firstEnable.PlayerMiners.containsKey(IslandInfo.getIsland(l))) {
	        		List<String> hashmapData = config.getStringList("PlayerMiners");
	    		    
	    		    String data = IslandInfo.getIsland(l) + ":"+String.valueOf(System.currentTimeMillis());
	    		    hashmapData.add(data);
	    		    
	    		    config.set("PlayerMiners", hashmapData);
	    		    Main.getPlugin().saveConfig();
	    		    p.sendMessage(Main.server+"New miner registered!");
	    		    for(String rawData : config.getStringList("PlayerMiners")) {

	    		        String[] raw = rawData.split(":");
	    		        firstEnable.PlayerMiners.put(raw[0], Long.valueOf(raw[1]));
	    		    }
	    		    return;
	        	}
	        	
	        	
	        	if(firstEnable.PlayerMiners.containsKey(IslandInfo.getIsland(l))) {
	        		for(Long i=firstEnable.PlayerMiners.get(IslandInfo.getIsland(l))/1000; i<System.currentTimeMillis()/1000-config.getInt("IslandOwnerSettings.DefaultMinerSpeed")-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(l)+".MineTier");i= config.getInt("IslandOwnerSettings.DefaultMinerSpeed")-config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(l)+".MineTier")+i ) {
	        			e.getInventory().addItem(new ItemStack(RandomMaterial(e.getInventory().getLocation()), 1));
	        			if(Contents.length>e.getInventory().getSize()) {
	      	        	  p.sendMessage(Main.server+"Miner inventory is full!");
	      	        	  break;
	      	           }
	        		}
	        		
	        		
	        		
	        		List<String> hashmapData = config.getStringList("PlayerMiners");
	        		for(String rawData : config.getStringList("PlayerMiners")) {

	    		        String[] raw = rawData.split(":");
	    		        if(raw[0].equals(IslandInfo.getIsland(l))) {
	    		        	hashmapData.remove(rawData);
	    		        	String data = IslandInfo.getIsland(l) + ":"+String.valueOf(System.currentTimeMillis());
	    	    		    hashmapData.add(data);
	    		        	config.set("PlayerMiners", hashmapData);
	    	    		    Main.getPlugin().saveConfig();
	    		        }
	    		    }
	        		 for(String rawData : config.getStringList("PlayerMiners")) {

		    		        String[] raw = rawData.split(":");
		    		        firstEnable.PlayerMiners.put(raw[0], Long.valueOf(raw[1]));
		    		    }
	        		
	        	}
	          
	           
	           
	           
	        }
	    }
	 public Material RandomMaterial(Location l) {
			
			int Generator = 0;
			if(IslandInfo.getIsland(l).equalsIgnoreCase("none")) {
				Generator = 0;
			}else {
			Generator = config.getInt("IslandOwnerSettings."+IslandInfo.getIsland(l)+".GenTier");
			}
			List<String> Gen = Main.islandGeneratorsConfig.getStringList("GeneratorChances" + "." + Generator + ".Blocks");
		for(String rawData : Gen) {
			int x = new Random().nextInt(100);
	        String[] raw = rawData.split(":");
		if(x <= Integer.valueOf(raw[1])) {
			if(Material.getMaterial(raw[0].toUpperCase()) == null) {
				return Material.SPONGE;
			}
			return Material.getMaterial(raw[0].toUpperCase());
		    }
		}
		return Material.COBBLESTONE;
		
	}
}
