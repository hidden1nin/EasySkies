package me.bukkit.hidden1nin.easySkies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;


public class Holograms {
	@SuppressWarnings("deprecation")
	public static void CreateHolo(Location l, String name,String[] args) {
		removeHolo(l);
		Location EntityArea = l;
	    World world = l.getWorld();
	   int clearRadius = 1;
	    List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
	    for(Entity e : world.getEntities()){
	        if(nearbyEntities.contains(e)){
	       	 if(e instanceof ArmorStand) {
	            e.remove();
	       	 }
	        }
	    }

		Entity entity = l.getWorld().spawnEntity(l, EntityType.ARMOR_STAND);
		IslandMobs.noAI(entity);
    	IslandMobs.invisible(entity);
	    entity.setCustomNameVisible(true);
	    entity.setInvulnerable(true);
	    entity.setSilent(true);
	    entity.setPersistent(true);
	    entity.setGravity(false);
	    //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoAI set value true ");
	   // Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" Invisible set value true ");
	   // Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoBasePlate set value true ");
	    List<String> holos = Main.islandHoloConfig.getStringList("Npc.List");
	    StringBuilder message = new StringBuilder(args[0]);
	    for (int arg = 1; arg < args.length; arg++) {
	      message.append(" ").append(args[arg]);
	      }
	    entity.setCustomName(ChatColor.translateAlternateColorCodes('&',message.toString()));
	    holos.add(""+l.getX()+":"+l.getY()+":"+l.getZ()+":"+l.getWorld().getName()+":"+l.getPitch()+":"+l.getYaw()+":"+message);
	    Main.islandHoloConfig.set("Npc.List", holos);
	    Main.saveHolo();
	}

	public static void removeHolo(Location l) {
		Location EntityArea = l;
	    World world = l.getWorld();
	   int clearRadius = 1;
	    List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
	    for(Entity e : world.getEntities()){
	        if(nearbyEntities.contains(e)){
	       	 if(e instanceof ArmorStand) {
	            e.remove();
	            for(String raw:Main.islandHoloConfig.getStringList("Npc.List")){
	            	String[] args = raw.split(":");
	            	if(l.distance(new Location(Bukkit.getWorld(args[3]),Double.parseDouble(args[0]),Double.parseDouble(args[1]),Double.parseDouble(args[2])))<5){
	            		List<String> mobs = Main.islandHoloConfig.getStringList("Npc.List");
	            	    mobs.remove(raw);
	            		 Main.islandHoloConfig.set("Npc.List", mobs);
	            		    Main.saveHolo();
	            	}
	            	}
	       	 }
	        }
	    }

	}

	@SuppressWarnings("deprecation")
	public static void reload() {
		for(String raw:Main.islandHoloConfig.getStringList("Npc.List")){
	    	String[] args = raw.split(":");
	    	
	    	Location l =new Location(Bukkit.getWorld(args[3]),Double.parseDouble(args[0]),Double.parseDouble(args[1]),Double.parseDouble(args[2]));
	    	l.setYaw(Float.parseFloat(args[5]));
	    	l.setPitch(Float.parseFloat(args[4]));
	    	Location EntityArea = l;
	        World world = l.getWorld();
	       int clearRadius = 1;
	        List<Entity> nearbyEntities = (List<Entity>) EntityArea.getWorld().getNearbyEntities(EntityArea, clearRadius, clearRadius, clearRadius);
	       
	        for(Entity e : world.getEntities()){
	            if(nearbyEntities.contains(e)){
	           	 if(e instanceof ArmorStand) {
	                e.remove();
	           	 }
	            }
	        }
	        
	    	Entity entity = l.getWorld().spawnEntity(l, EntityType.ARMOR_STAND);
	    	IslandMobs.noAI(entity);
	    	IslandMobs.invisible(entity);
	    	entity.setCustomName(ChatColor.translateAlternateColorCodes('&', args[6]));
	        entity.setCustomNameVisible(true);
	        entity.setInvulnerable(true);
	        entity.setSilent(true);
	        entity.setPersistent(true);
	        entity.setGravity(false);
	        
	        //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoAI set value true ");
	        //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" Invisible set value true ");
		    //Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "data modify entity "+entity.getUniqueId()+" NoBasePlate set value true ");
		  
	        }
		
	}
	}

