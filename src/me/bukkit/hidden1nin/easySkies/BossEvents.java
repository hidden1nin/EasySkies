
package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftSnowball;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
class BossClass {
    // Instance Variables
	String Name="Pete";
    String MobType="Zombie";
    String AltType="Giant";
    List<ItemStack> Armor = new ArrayList<ItemStack>();
    Boolean DropArmor = false;
    int Hp=1000;
    int Damage=5;
    int AbilityDelay=5;
    int SpawnDelay=60;
    //item then chance out of 100
    HashMap<ItemStack,Integer> BossDrops = new HashMap<ItemStack,Integer>();
    HashMap<ItemStack,Integer> MinionDrops= new HashMap<ItemStack,Integer>();
    String MinionType = "Zombie";
    String MinionName = "Pete's Friends";
    int MinionMaxAmount = 20;
    int MinionHp =10;
    int MinionDamage = 2;
    Boolean MinionDropArmor = false;
    Boolean AbilityActivated = false;
    List<ItemStack> MinionArmor = new ArrayList<ItemStack>();
    List<String> Abilities= new ArrayList<String>();;
    List<String> Weakness =new ArrayList<String>();;


}

public class BossEvents implements Runnable,Listener{
public static BossClass Boss = new BossClass();
public static LivingEntity BossMob = null;
	
	public static ArrayList<Player> bossplayers = new ArrayList<Player>();
	public static ArrayList<LivingEntity> bossMobs = new ArrayList<LivingEntity>();
	
	
///////////////////Pvp////////////////
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getDamager() instanceof  Player) {
			if(Boss!=null) {
				if(Boss.Abilities.contains("split")) {
					if(e.getEntity().isDead()==false&&e.getEntity()!=BossMob&&e.getEntityType()==EntityType.valueOf(Boss.MinionType.toUpperCase())) {
						if(e.getEntity().getName().contains("Clone")== false) {
						LivingEntity mob1 = (LivingEntity) e.getEntity().getWorld().spawnEntity(e.getEntity().getLocation(), e.getEntityType());
						mob1.setMaxHealth(((Damageable) e.getEntity()).getHealth());
						mob1.setCustomName(e.getEntity().getName()+" Clone");
						mob1.setCustomNameVisible(true);
					}}
				}
			}
			Cooldowns.sendActionBarMessage((Player)e.getDamager(),ChatColor.RED+""+ChatColor.BOLD+ Math.round(((Damageable) e.getEntity()).getHealth())+"/"+((Damageable) e.getEntity()).getMaxHealth());
		}
		if(e.getDamager().getWorld()==firstEnable.Boss.getWorld()) {
		if(e.getDamager() instanceof  Player &&e.getDamager().getLocation().distance(firstEnable.Boss)<25) {
			Cooldowns.silentIsCooldownOver(Cooldowns.CombatCoolDown, 20, (Player) e.getDamager());
			}
		if(e.getEntity() instanceof  Player &&e.getEntity().getLocation().distance(firstEnable.Boss)<25) {
			Cooldowns.silentIsCooldownOver(Cooldowns.CombatCoolDown, 20, (Player) e.getEntity());
			}
		}
	}
//////////////////Items//////////////
	
	public static HashMap<Player, ItemStack> projectileMap = new HashMap<Player, ItemStack>();
	@SuppressWarnings("deprecation")
	@EventHandler
	public void bossmovement(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if(p.getWorld()==firstEnable.Boss.getWorld()) {
		if(p.getLocation().distance(firstEnable.Boss)<25&&!bossplayers.contains(p)) {
			p.sendTitle(ChatColor.RED+""+ChatColor.BOLD+"Entering Boss Arena", ChatColor.RED+"Pvp Enabled Ahead!");
			//Cooldowns.silentIsCooldownOver(Cooldowns.CombatCoolDown, 20, p);
			bossplayers.add(p);
		}
		if(p.getLocation().distance(firstEnable.Boss)>25&&bossplayers.contains(p)) {
			long cooldown = Cooldowns.SecondsLeftCooldownOver(Cooldowns.CombatCoolDown, 20, p);
			if(cooldown>0) {p.sendMessage(Main.server+ChatColor.RED+"You cannot flee in combat! " +ChatColor.BOLD+cooldown+ChatColor.RESET+""+ChatColor.RED+" Seconds Left!");p.setFallDistance(0);p.teleport(firstEnable.Boss);}
			p.sendTitle(ChatColor.RED+""+ChatColor.BOLD+"Leaving Boss Arena", "");
			bossplayers.remove(p);
		}
	}}
	
	@EventHandler
	public void onForm(EntityChangeBlockEvent event) {
	    if(event.getEntity().getType() == EntityType.FALLING_BLOCK&&event.getEntity().isSilent()) {
	        event.setCancelled(true);
	}}

	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerUse(PlayerInteractEvent event){
		if(event.getAction()==Action.LEFT_CLICK_AIR||event.getAction()==Action.LEFT_CLICK_BLOCK) {
			if (!IslandInfo.pvpOn(event.getPlayer())) {
				return;
			}
	    Player p = event.getPlayer();
	    projectileMap.put(p,p.getInventory().getItemInMainHand());
		    if(event.getPlayer().getInventory().getItemInMainHand().getType()==Material.AIR) {
	   	    	if(Enchants.fullSet("Webbed", event.getPlayer().getInventory().getArmorContents())) {
	   		    	 Snowball projectile = event.getPlayer().launchProjectile(Snowball.class);
	   		         ((CraftSnowball) projectile).getHandle().setItem(CraftItemStack.asNMSCopy(new ItemStack(Material.STRING)));
	   		         projectile.setVelocity(projectile.getVelocity().multiply(1.5));
	   	    	}
	   	    }
		}
		if(event.getAction()==Action.RIGHT_CLICK_AIR||event.getAction()==Action.RIGHT_CLICK_BLOCK) {
			if (!IslandInfo.pvpOn(event.getPlayer())) {
				return;
			}
	    Player p = event.getPlayer();
	    projectileMap.put(p,p.getInventory().getItemInMainHand());
	    if(p.getInventory().getItemInMainHand().isSimilar(Main.createDisplay(Material.STICK, null, -2, ChatColor.GOLD+ "Telekinetic "+ChatColor.BLUE+"QuarterStaff",ChatColor.WHITE+"sucks blocks out of the ground and launches them!"))&&Cooldowns.silentIsCooldownOver(Cooldowns.PotatoCoolDown, 1, p)) {
	    	Location l = p.getLocation().subtract(0, 1, 0);
	    	Vector v = new Vector();
	    	v.setY(1);
	    	for (BlockFace face : Main.faces) {
	    		if(l.getBlock().getRelative(face).getType()!=Material.AIR&&face!=BlockFace.DOWN&&face!=BlockFace.UP) {
	    		if(Math.random()>.5) {
	    		v.setX(Math.random()*.5);}else {
	    			v.setX(-Math.random()*.5);
	    		}
	    		if(Math.random()>.5) {
	    		v.setZ(Math.random()*.5);}else {
	    			v.setZ(-Math.random()*.5);
	    		}
	    		FallingBlock blockfall = (FallingBlock) l.getWorld().spawnFallingBlock(l.getBlock().getRelative(face).getLocation().add(0, 1, 0),l.getBlock().getRelative(face).getType(),l.getBlock().getRelative(face).getData());
	    		blockfall.setVelocity(v);
	    		blockfall.setDropItem(false);
	    		blockfall.setSilent(true);
	    		}
	    		
	    	}
	    	for(Entity e:p.getNearbyEntities(3, 1, 3)) {
	    		if(e instanceof LivingEntity&&e!=p) {
	    			
	    			((LivingEntity) e).damage(5, p);
	    		}
	    	}
	    	
	    }
	    if(p.getInventory().getItemInMainHand().getType() == Material.POTATO&&Cooldowns.silentIsCooldownOver(Cooldowns.PotatoCoolDown, 1, p)){
	        //Fireball fire = p.getWorld().spawn(event.getPlayer().getLocation(), Fireball.class);
	        //fire.setShooter(p);
	    	 Snowball projectile = p.launchProjectile(Snowball.class);
	         ((CraftSnowball) projectile).getHandle().setItem(CraftItemStack.asNMSCopy(new ItemStack(Material.BAKED_POTATO)));
	         if(p.getVelocity().getY()>-.5) {
	         p.setVelocity(projectile.getVelocity().multiply(-.25));}
	         projectile.setVelocity(projectile.getVelocity().multiply(0.75));
	         p.getInventory().getItemInMainHand().setAmount(p.getInventory().getItemInMainHand().getAmount()-1);
	         //((CraftSnowball) projectile).getHandle().setNoGravity(true);
	    }
	    if(Main.equals(p.getInventory().getItemInMainHand(),Main.createDisplay(Material.IRON_SWORD, Main.EasySkyCustomRecipes, -2, ChatColor.RED+ "Switch "+ChatColor.AQUA+"Blade",ChatColor.GRAY+"Shift Click to swap bullet patterns!"))&&Cooldowns.silentIsCooldownOver(Cooldowns.PotatoCoolDown, 1, p)){
	    	if(p.isSneaking()) {ItemMeta meta =p.getInventory().getItemInMainHand().getItemMeta(); meta.setDisplayName(ChatColor.AQUA+"Switch "+ChatColor.RED+"Blade");p.getInventory().getItemInMainHand().setItemMeta(meta); return;}
	    	 for(int i=0;i<8;i++) {
	    		 Snowball projectile = p.launchProjectile(Snowball.class);
	         ((CraftSnowball) projectile).getHandle().setItem(CraftItemStack.asNMSCopy(new ItemStack(Material.FLINT)));
	         
	         double accuracy = Math.random() * .2f;
	         projectile.setVelocity(projectile.getVelocity().multiply(1.0));
	         projectile.setVelocity(projectile.getVelocity().add(new Vector(Math.random() * accuracy - accuracy,Math.random() * accuracy - accuracy,Math.random() * accuracy - accuracy)));
	    	 }
	    	 p.getInventory().getItemInMainHand().setDurability((short) (p.getInventory().getItemInMainHand().getDurability() - p.getInventory().getItemInMainHand().getType().getMaxDurability()));
	    }
	    if(Main.equals(p.getInventory().getItemInMainHand(),Main.createDisplay(Material.IRON_SWORD, Main.EasySkyCustomRecipes, -2, ChatColor.AQUA+ "Switch "+ChatColor.RED+"Blade",ChatColor.GRAY+"Shift Click to swap bullet patterns!"))){
	    	if(p.isSneaking()) {ItemMeta meta =p.getInventory().getItemInMainHand().getItemMeta(); meta.setDisplayName(ChatColor.RED+"Switch "+ChatColor.AQUA+"Blade");p.getInventory().getItemInMainHand().setItemMeta(meta);  return;}
	    	 for(int i=0;i<3;i++) {
	    		 Snowball projectile = p.launchProjectile(Snowball.class);
	         ((CraftSnowball) projectile).getHandle().setItem(CraftItemStack.asNMSCopy(new ItemStack(Material.FLINT)));
	         projectile.setVelocity(projectile.getVelocity().multiply(1.0));
	        }
	    	 p.getInventory().getItemInMainHand().setDurability((short) (p.getInventory().getItemInMainHand().getDurability() - p.getInventory().getItemInMainHand().getType().getMaxDurability()));
	    }
	    if(Main.equals(p.getInventory().getItemInMainHand(),Main.createDisplay(Material.GOLDEN_SWORD, Main.EasySkyCustomRecipes, -2, ChatColor.YELLOW+ "Blazing "+ChatColor.RED+"Saber",ChatColor.WHITE+"Shoots fire, thats all"))) {
	    	 Snowball projectile = p.launchProjectile(Snowball.class);
	         ((CraftSnowball) projectile).getHandle().setItem(CraftItemStack.asNMSCopy(new ItemStack(Material.FIRE_CHARGE)));
	         if(p.getVelocity().getY()>-.5&&p.getVelocity().getY()<.5) {
	         p.setVelocity(projectile.getVelocity().multiply(-.05));}
	         projectile.setVelocity(projectile.getVelocity().multiply(3.0));
	        p.getInventory().getItemInMainHand().setDurability((short) (p.getInventory().getItemInMainHand().getDurability() - p.getInventory().getItemInMainHand().getType().getMaxDurability()));
	    }
	 
	    }
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onProjectileHit(EntityDamageByEntityEvent event) {

		if(event.getDamager() instanceof  Projectile &&event.getDamager().getLocation().distance(firstEnable.Boss)<25) {
			if(((Projectile) event.getDamager()).getShooter() instanceof Player) {
			Cooldowns.silentIsCooldownOver(Cooldowns.CombatCoolDown, 20,(Player)((Projectile) event.getDamager()).getShooter());
			}}
		if(event.getDamager() instanceof Projectile&&event.getDamager()instanceof Snowball) {
			Snowball projectile = (Snowball) event.getDamager();
			if(projectileMap.get(projectile.getShooter())!=null) {
				if(projectile.getShooter() instanceof  Player) {
					Cooldowns.sendActionBarMessage((Player)projectile.getShooter(),ChatColor.RED+""+ChatColor.BOLD+ Math.round(((Damageable) event.getEntity()).getHealth())+"/"+((Damageable) event.getEntity()).getMaxHealth());
				}
				if(IslandInfo.OwnIsland((Player)projectile.getShooter())||IslandInfo.pvpOnProjectile(projectile.getLocation())) {
				if(projectileMap.get(projectile.getShooter()).getType()==Material.AIR&&Enchants.fullSet("Webbed", ((HumanEntity) projectile.getShooter()).getInventory().getArmorContents())) {
					event.setDamage(4.0);
				}
				if(projectileMap.get(projectile.getShooter()).getType()==Material.POTATO) {
					event.setDamage(1.0);
				}
				if(projectileMap.get(projectile.getShooter()).getType()==Material.GOLDEN_SWORD) {
					event.setDamage(5.0);
					Particles.createParticle(event.getEntity().getLocation(), Particle.EXPLOSION_LARGE, 1);
				}
				if(projectileMap.get(projectile.getShooter()).getType()==Material.IRON_SWORD) {
					event.setDamage(4.0);
					Particles.createParticle(event.getDamager().getLocation(), Particle.CRIT, 1);
				}
			}
			}
			}
		}
	
////////////////bosses///////////////
	public BossClass Pete() {
		BossClass Pete = new BossClass();
			Pete.Name=ChatColor.RED+""+ChatColor.BOLD+"Peter";
		    Pete.MobType="Zombie";
		    Pete.AltType="Giant"; 
		    Pete.Armor.add(Main.createDisplay(Material.LEATHER_BOOTS,Main.EasySkyCustomRecipes, -2, " ", " "));
		   	Pete.Armor.add(Main.createDisplay(Material.LEATHER_LEGGINGS,Main.EasySkyCustomRecipes, -2, " ", " "));
		   	Pete.Armor.add(Main.createDisplay(Material.LEATHER_CHESTPLATE,Main.EasySkyCustomRecipes, -2, " ", " "));
		   	Pete.Armor.add(Main.createDisplay(Material.LEATHER_HELMET, Main.EasySkyCustomRecipes, -2, " ", " "));
		   	Pete.Armor.add(Main.createDisplay(Material.WOODEN_SWORD, Main.EasySkyCustomRecipes, -2, " ", " "));
		    
	    	Pete.DropArmor = false;
	    	Pete.Hp=300;		    	Pete.AbilityDelay=5;
		   	Pete.SpawnDelay=60;
		    //item then chance out of 100
		    Pete.AbilityDelay=15;
		   	Pete.BossDrops.put(Main.createDisplay(Material.ROTTEN_FLESH, Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Small Trash",ChatColor.RED+"Loot-Box 1"),100);
		   	Pete.BossDrops.put(Main.createDisplay(Material.ROTTEN_FLESH,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Large Trash",ChatColor.RED+"Loot-Box 10"),50);
		   	Pete.BossDrops.put(Main.createDisplay(Material.ROTTEN_FLESH,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Large Trash",ChatColor.RED+"Loot-Box 10"),50);
		   	Pete.BossDrops.put(Main.createDisplay(Material.ROTTEN_FLESH,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Large Trash",ChatColor.RED+"Loot-Box 10"),50);
		   	Pete.BossDrops.put(Main.createDisplay(Material.ROTTEN_FLESH,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Large Trash",ChatColor.RED+"Loot-Box 10"),50);
		   	Pete.Damage =10;
	    	Pete.MinionDrops.put(Main.createDisplay(Material.ROTTEN_FLESH,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Small Trash",ChatColor.RED+"Loot-Box 1"),25);
		    Pete.MinionType = "Zombie";
		    Pete.MinionName = ChatColor.LIGHT_PURPLE+"Peter's Yeeter";
		    Pete.MinionMaxAmount = 10;
		    Pete.MinionHp =25;
		    Pete.MinionDamage = 2;
		    Pete.MinionArmor.add(Main.createDisplay(Material.LEATHER_BOOTS,Main.EasySkyCustomRecipes, -2, " ",ChatColor.RED+"Junk"));
		   	Pete.MinionArmor.add(Main.createDisplay(Material.LEATHER_LEGGINGS,Main.EasySkyCustomRecipes, -2, " ",ChatColor.RED+"Junk"));
		   	Pete.MinionArmor.add(Main.createDisplay(Material.LEATHER_CHESTPLATE,Main.EasySkyCustomRecipes, -2, " ", ChatColor.RED+"Junk"));
		   	Pete.MinionArmor.add(Main.createDisplay(Material.END_STONE, Main.EasySkyCustomRecipes, -2,ChatColor.RESET+ "End Stone",ChatColor.RED+"Junk"));
		   	Pete.MinionArmor.add(Main.createDisplay(Material.WOODEN_SWORD, Main.EasySkyCustomRecipes, -2, " ",ChatColor.RED+"Junk"));
		    Pete.Abilities.add("Grow");
		    Pete.Abilities.add("speed");
		    Pete.Weakness.add("None");
		    Pete.AbilityActivated = false;
		    Pete.MinionDropArmor = false;
		return Pete;
	}
	public BossClass Chonk() {
		BossClass Chonk = new BossClass();
			Chonk.Name=ChatColor.AQUA+""+ChatColor.BOLD+"C h o n k";
		    Chonk.MobType="Slime";
		    Chonk.AltType="none"; 
		    
	    	Chonk.DropArmor = false;
	    	Chonk.Hp=200;
		   	Chonk.SpawnDelay=60;
		    //item then chance out of 100
		    Chonk.AbilityDelay=10;
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Sticky Arcane Slip",ChatColor.RED+"Sticky 5"),100);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Sticky Arcane Slip",ChatColor.RED+"Sticky 5"),100);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Slimy Arcane Slip",ChatColor.RED+"Slimy 5"),100);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Slimy Arcane Slip",ChatColor.RED+"Slimy 5"),100);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Moist Paper!",ChatColor.RED+"Edible 10"),100);
		   	Chonk.Damage =10;
		   	Chonk.BossDrops.put(Main.createDisplay(Material.SLIME_BALL, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Lucky Slime Chunk",ChatColor.RED+"Loot-Box 1"),33);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.PAPER, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Moist Paper!",ChatColor.RED+"Edible 10"),100);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.SLIME_BALL, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Lucky Slime Chunk",ChatColor.RED+"Loot-Box 1"),33);
		   	Chonk.BossDrops.put(Main.createDisplay(Material.SLIME_BALL, Main.EasySkyCustomRecipes, -2, ChatColor.BLUE+"Lucky Slime Chunk",ChatColor.RED+"Loot-Box 1"),33);
		   	Chonk.MinionType = "Slime";
		    Chonk.MinionName = ChatColor.LIGHT_PURPLE+"Chonk's Babies";
		    Chonk.MinionMaxAmount = 30;
		    Chonk.MinionHp =25;
		    Chonk.MinionDamage = 5;
		    Chonk.Abilities.add("Suck");
		    Chonk.Abilities.add("speed");
		    Chonk.Abilities.add("slow");
		    Chonk.Abilities.add("crush");
		    Chonk.Abilities.add("nausea");
		    Chonk.Abilities.add("Warp");
		    Chonk.Weakness.add("None");
		    Chonk.AbilityActivated = false;
		    Chonk.MinionDropArmor = false;
		return Chonk;
	}
	@EventHandler
	public static void split(SlimeSplitEvent e) {
		if(e.getEntity().getLocation().distance(firstEnable.Boss)<25) {
			e.setCancelled(true);
			e.setCount(0);
		}
	}
	public BossClass SpiderQueen() {
		BossClass Queen = new BossClass();
			Queen.Name=ChatColor.GREEN+""+ChatColor.BOLD+"Queen Lucille";
		    Queen.MobType="cave_spider";
		    Queen.AltType="none"; 

	    	Queen.DropArmor = false;
	    	Queen.Hp=400;
		   	Queen.SpawnDelay=60;
		    //item then chance out of 100
		    Queen.AbilityDelay=15;
		   	Queen.BossDrops.put(Main.createDisplay(Material.COBWEB, Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Sticky Web",ChatColor.RED+"Junk"),100);
		   	Queen.BossDrops.put(Main.createDisplay(Material.FISHING_ROD, Main.EasySkyCustomRecipes, -2, ChatColor.GOLD + "Grappling Hook",ChatColor.GRAY+"Like batman, but not as cool./line/"+ChatColor.GRAY+"nothings as cool as batman.."+"/line/"+ChatColor.GOLD+"Soft-Landing 1"),50);
			Queen.BossDrops.put(Main.createDisplay(Material.VINE, Main.EasySkyCustomRecipes, -2,
					ChatColor.YELLOW + "Empty Net", ChatColor.AQUA+"can capture mobs when right clicked!"),50);
			Queen.BossDrops.put(Main.createDisplay(Material.DIAMOND_BOOTS, Main.EasySkyCustomRecipes, -2,
					ChatColor.GOLD + "Spider Joints", ChatColor.AQUA+"Soft Landing 1/line/"+ChatColor.GREEN+"Webbed 1"),10);
			Queen.BossDrops.put(Main.createDisplay(Material.DIAMOND_LEGGINGS, Main.EasySkyCustomRecipes, -2,
					ChatColor.GOLD + "Spider Legs", ChatColor.AQUA+"High-Jump 1/line/"+ChatColor.GREEN+"Webbed 1"),10);
			Queen.BossDrops.put(Main.createDisplay(Material.DIAMOND_CHESTPLATE, Main.EasySkyCustomRecipes, -2,
					ChatColor.GOLD + "Spider Thorax", ChatColor.AQUA+"Weaken 1/line/"+ChatColor.GREEN+"Webbed 1"),10);
			Queen.BossDrops.put(Main.createDisplay(Material.DIAMOND_HELMET, Main.EasySkyCustomRecipes, -2,
					ChatColor.GOLD + "Spider Crown", ChatColor.AQUA+"Poison 1/line/"+ChatColor.GREEN+"Webbed 1"),10);
		   	Queen.Damage =15;
	    	Queen.MinionDrops.put(Main.createDisplay(Material.STRING,Main.EasySkyCustomRecipes, -2, ChatColor.GRAY+"Loose Silk",ChatColor.RED+"Junk"),50);
		    Queen.MinionType = "Spider";
		    Queen.MinionName = ChatColor.LIGHT_PURPLE+"Queen's Guard";
		    Queen.MinionMaxAmount = 20;
		    Queen.MinionHp =8;
		    Queen.MinionDamage = 10;
		    Queen.Abilities.add("poison");
		    Queen.Abilities.add("suck");
		    Queen.Abilities.add("blind");
		    Queen.Abilities.add("slow");
		    Queen.Abilities.add("speed");
		    Queen.Abilities.add("split");
		    Queen.Weakness.add("Fire");
		    Queen.AbilityActivated = false;
		    Queen.MinionDropArmor = false;
		return Queen;
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	public void killedMob(EntityDeathEvent e) {
		Boolean BossDead = false;
		if(BossMob!=null){
			if(BossEvents.BossMob.isDead()&&BossMob==e.getEntity()) {Bukkit.broadcastMessage(Main.server+"The Boss has been Slain by "+ChatColor.GOLD+e.getEntity().getKiller().getName().toString()); 
			BossDead=true;}
		}
		Entity dead = e.getEntity();
		if (e.getEntity().getKiller() instanceof Player&&e.getEntity()instanceof Player== false) {
				if(bossMobs.contains(dead)) {
					if(BossDead) {
						if(!Boss.DropArmor) {e.getDrops().clear();}

						Boss.BossDrops.forEach((k,v) -> {
							if((Math.random()*100)<=v) {
						dead.getLocation().getWorld().dropItem(dead.getLocation(), k);
						for(int i =0;i<100;i++) {
							int o=1;
							if(Math.random()>.5) {
							o=-1;	
							}
						dead.getLocation().getWorld().spawnEntity(dead.getLocation().add(o*Math.random()*5, 5,o*Math.random()*5), EntityType.EXPERIENCE_ORB);
						}
							}
					});
						BossMob=null;
						for(LivingEntity ent:bossMobs) {ent.setHealth(0);Particles.createParticle(ent.getLocation(), Particle.EXPLOSION_LARGE, 1);}
						
				}else {
					if(!Boss.MinionDropArmor) {
						e.getDrops().clear();}
					for(ItemStack i:Boss.MinionDrops.keySet()) {
						if((Math.random()*100)<=Boss.MinionDrops.get(i)) {
					dead.getLocation().getWorld().dropItem(dead.getLocation(), i);
						}
				}
				
				}
			}}
		}
@EventHandler
public void mobSpawn(EntitySpawnEvent event) {
	if(event.getEntity().getWorld()==firstEnable.Boss.getWorld()) {
	if(event.getEntity().getLocation().distance(firstEnable.Boss)<25&&event.getEntity() instanceof LivingEntity) {
		bossMobs.add((LivingEntity)event.getEntity());
	}
}}

private Random random = new Random();
private int index;
private int index2;
public BossEvents() {
    setRandom();
}
@Override
public void run() {
	if(BossMob!=null&&index2>=Boss.AbilityDelay) {
		setRandom2();
		BossAbility();
	}else {
		index2++;
	}
    if (index >= 60*60) {//60 minutes
        //firstEnable.Boss.getWorld().spawnEntity(firstEnable.Boss, EntityType.SKELETON);
        spawnBoss();
        setRandom();
    } else {
        index++;
    }
}

@SuppressWarnings("deprecation")
private void BossAbility() {
	if(Boss.AbilityActivated) {
		
		for(String A:Boss.Abilities) {

			if(A.equalsIgnoreCase("grow")) {

				double Health = BossMob.getHealth();
				LivingEntity PrevBoss = BossMob;
				Collection<PotionEffect> effects = PrevBoss.getActivePotionEffects();
				BossMob = (LivingEntity) BossMob.getWorld().spawnEntity(BossMob.getLocation(),EntityType.GIANT);
				BossMob.addPotionEffects(effects);
				PrevBoss.remove();
				BossMob.setCustomName(Boss.Name);
				BossMob.setMaxHealth(Boss.Hp);
				BossMob.setHealth(Health);
				BossMob.setCustomNameVisible(true);
				ItemStack[] a = new ItemStack[5];
				BossMob.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(Boss.Damage);
				BossMob.getEquipment().setArmorContents(Boss.Armor.toArray(a));
				for(Entity ent:BossMob.getNearbyEntities(3, 3, 3)) {
					if(ent instanceof Player) {
						Particles.createParticle(ent.getLocation(), Particle.EXPLOSION_LARGE, 1);
						((Player) ent).damage(5, BossMob);
						ent.setVelocity(ent.getVelocity().add(new Vector(0,ent.getVelocity().getY()+.3,0)));
					}
				}
			}
			if(A.equalsIgnoreCase("Speed")) {
				BossMob.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 10*20, 3));
			}
			if(A.equalsIgnoreCase("blind")) {
				for(Player p:bossplayers) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10*20, 3));
				}
			}
			if(A.equalsIgnoreCase("poison")) {
				for(Player p:bossplayers) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 10*20, 2));
				}
			}
			if(A.equalsIgnoreCase("slow")) {
				for(Player p:bossplayers) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10*20, 2));
				}
			}
			if(A.equalsIgnoreCase("nausea")) {
				for(Player p:bossplayers) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 4*20, 4));
				}
			}
			if(A.equalsIgnoreCase("suck")) {
				for(Player p:bossplayers) {
					Vector v = BossMob.getLocation().toVector().subtract(p.getLocation().toVector());
					p.setVelocity(v.multiply(1.5));
					p.damage(1,BossMob);
				}
			}
		}
		Boss.AbilityActivated = false;
	}else {
		for(String A:Boss.Abilities) {
			if(A.equalsIgnoreCase("grow")) {
		double Health = BossMob.getHealth();
		LivingEntity PrevBoss = BossMob;
		Collection<PotionEffect> effects = PrevBoss.getActivePotionEffects();
		BossMob = (LivingEntity) firstEnable.Boss.getWorld().spawnEntity(BossMob.getLocation(),EntityType.valueOf(Boss.MobType.toUpperCase()));
		BossMob.setMaxHealth(Boss.Hp);
		BossMob.addPotionEffects(effects);
		PrevBoss.remove();
		BossMob.setCustomName(Boss.Name);
		BossMob.setHealth(Health);
		BossMob.setCustomNameVisible(true);
		ItemStack[] a = new ItemStack[4];
		BossMob.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(Boss.Damage);
		BossMob.getEquipment().setArmorContents(Boss.Armor.toArray(a));
		}

			if(A.equalsIgnoreCase("Speed")) {
				BossMob.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 30, 3));
			}
			if(A.equalsIgnoreCase("Warp")) {
				for(Player p:BossEvents.bossplayers) {
				p.teleport(BossMob.getLocation());
				}
			}
		
		}
		Boss.AbilityActivated = true;
	}
	
}


@SuppressWarnings("deprecation")
private void spawnBoss() {
	if(BossMob==null) {
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
			public void run() {
				for(LivingEntity ent:bossMobs) {ent.remove();Particles.createParticle(ent.getLocation(), Particle.EXPLOSION_LARGE, 1);}
				BossMob=null;
				Bukkit.broadcastMessage(Main.server+Boss.Name+" wasn't killed in time, and escaped!");
			}
		}, 19200);
	double rand =Math.random()*100;
	if(rand<25&&rand>0) {
	Boss = Pete();
	}
	if(rand<50&&rand>25) {
	Boss = SpiderQueen();
	firstEnable.Boss.getWorld().setTime(14000);
	}
	if(rand<100&&rand>50) {
	Boss = Chonk();
	}
	Bukkit.broadcastMessage(Main.server+Boss.Name+""+ChatColor.GRAY+" Randomly Spawned!");
	BossMob = (LivingEntity) firstEnable.Boss.getWorld().spawnEntity(firstEnable.Boss,EntityType.valueOf(Boss.MobType.toUpperCase()));
	if(BossMob instanceof Slime) {
		Slime slime =(Slime) BossMob;
		slime.setSize(20);
	}
	BossMob.getWorld().playEffect(BossMob.getLocation(), Effect.END_GATEWAY_SPAWN, 20);
	BossMob.setCustomName(Boss.Name);
	BossMob.playEffect(EntityEffect.FIREWORK_EXPLODE);
	BossMob.setRemoveWhenFarAway(false);
	BossMob.setCustomNameVisible(true);
	ItemStack[] a = new ItemStack[5];
	BossMob.getEquipment().setArmorContents(Boss.Armor.toArray(a));
	BossMob.setMaxHealth(Boss.Hp);
	BossMob.setHealth(Boss.Hp);
	BossMob.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(Boss.Damage);
	//BossMob.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(20);
	for(int i=0;i<Boss.MinionMaxAmount;i++) {
		LivingEntity minion = (LivingEntity) firstEnable.Boss.getWorld().spawnEntity(firstEnable.Boss,EntityType.valueOf(Boss.MinionType.toUpperCase()));
		if(minion instanceof Slime) {
			Slime slime =(Slime) minion;
			slime.setSize(1);
		}
		minion.setRemoveWhenFarAway(false);
		minion.setCustomNameVisible(true);
		minion.setCustomName(Boss.MinionName);
		minion.setMaxHealth(Boss.MinionHp);
		minion.setHealth(Boss.MinionHp);
		minion.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(Boss.MinionDamage);
		minion.getEquipment().setArmorContents(Boss.MinionArmor.toArray(a));
	}
	}
}
private void setRandom() {
    index = random.nextInt(30*60) +1;
}
private void setRandom2() {
    index2 = random.nextInt(10) +1;
}
////////////////events///////////////



}
