package me.bukkit.hidden1nin.easySkies;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
class PlayerQueue {
	String name;
	int maxPlayers;
	int countDown;
	int playersNeeded;
    int delay;
    long timerStart;
    List<String> players;
    Location startLocation;
}
public class Queue {
public static HashMap<String,PlayerQueue> Queues = new HashMap<String,PlayerQueue>();
public static HashMap<String,String> QueuePlayers = new HashMap<String,String>();
public static void joinQueue(Player p,String name) {
	if(Queues.containsKey(name)) {
		PlayerQueue q =Queues.get(name);
		if(q.players.contains(p.getName())||QueuePlayers.containsKey(p.getName())) {
			p.sendMessage(Main.server+"Your Already in the queue");
		}else {	
		if(q.maxPlayers>q.players.size()) {
		q.players.add(p.getName());
		QueuePlayers.put(p.getName(), name);
		p.sendMessage(Main.server+"Joined the Queue for "+name+" ("+q.players.size()+"/"+q.maxPlayers+")");
		}else {
			p.sendMessage(Main.server+"That Queue Is Full!");
		}}
	}else {
		PlayerQueue q =createStoredQueue(name,p);
		Queues.put(name, q);
		if(q!=null) {

		if(q.players.contains(p.getName())||QueuePlayers.containsKey(p.getName())) {
			p.sendMessage(Main.server+"Your Already in the queue");
		}else {	
		if(q.maxPlayers>q.players.size()) {
		
		q.players.add(p.getName());
		QueuePlayers.put(p.getName(), name);
		startTimer(q);
		p.sendMessage(Main.server+"Joined the Queue for "+name+" ("+q.players.size()+"/"+q.maxPlayers+")");
		}else {
			p.sendMessage(Main.server+"That Queue Is Full!");
		}}}else {
			p.sendMessage(Main.server+"That Queue Is Full!");
		}
}}
public static void startTimer(PlayerQueue q) {
	new BukkitRunnable() {
		@Override
	    public void run() {
	    	q.countDown-=1;
	    	if(q.players.size()<1) {
	    	this.cancel();
	    	}
	    	for (Iterator<String> iter = q.players.iterator(); iter.hasNext();) {
	    	    String p = iter.next();
	    	   if(Bukkit.getPlayer(p)!=null) {
	   	    	if(q.countDown==0) {
	   	    		if(q.playersNeeded<=q.players.size()) {
	   	    		Bukkit.getPlayer(p).sendMessage(Main.server+"Starting event soon!");
	   	    		Bukkit.getPlayer(p).teleport(q.startLocation);
		   		    this.cancel();
		   		    QueuePlayers.remove(p);
		   		    Queues.remove(q.name);
	   	    		if(q.name.equals("Rodeo")){Rodeo.start(q);}
	   	    		}else {
	 	    		   iter.remove();
		   	    		Bukkit.getPlayer(p).sendMessage(Main.server+"Not Enough Players!");	
		   		    	this.cancel();
		   		    QueuePlayers.remove(p);
		   		    Queues.remove(q.name);
	   	    		}
		    	}
	    		   Cooldowns.sendActionBarMessage(Bukkit.getPlayer(p),ChatColor.LIGHT_PURPLE+""+q.countDown+" Seconds Left "+" ("+q.players.size()+"/"+q.maxPlayers+")");
	    	   }else {
	    		   q.players.remove(p);
	    		   QueuePlayers.remove(p);
	    	   }
	       }
	    }
	}.runTaskTimer(Main.getPlugin(),20,20);
}
public static PlayerQueue createStoredQueue(String name,Player p) {
	if(name!=null) {
	if(name.equalsIgnoreCase("Rodeo")) {
		return createQueue("Rodeo",60,4,1,new Location(p.getWorld(), -296, 46, 366));
	}}
	return null;
}
public static PlayerQueue createQueue(String name,int Delay,int maxPlayers,int playersNeeded,Location l) {
	if(Queues.containsKey(name)) {
		return Queues.get(name);
	}else {
		PlayerQueue q = new PlayerQueue();
		q.delay=Delay;
		q.countDown=Delay;
		q.name=name;
		q.maxPlayers=maxPlayers;
		q.playersNeeded=playersNeeded;
		q.players =new ArrayList<String>();
		q.startLocation=l;
		q.timerStart =System.currentTimeMillis();
		return q;
	}
}
	
}
