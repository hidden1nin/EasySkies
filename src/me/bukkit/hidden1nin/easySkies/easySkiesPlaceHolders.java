package me.bukkit.hidden1nin.easySkies;




import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;


import me.clip.placeholderapi.expansion.PlaceholderExpansion;


public class easySkiesPlaceHolders extends PlaceholderExpansion {
    
    // Getting our main class with the stuff here
    private Main Plugin;
    
    public String getIdentifier() {
        return "easySkiesApi";
    }

    public String getPlugin() {
        return null;
    }

    /*
     * The author of the Placeholder
     * This cannot be null
     */
    @Override
    public String getAuthor(){
        return Main.getPlugin().getDescription().getAuthors().toString();
    }

    @Override
    public String getVersion(){
        return Main.getPlugin().getDescription().getVersion();
    }
    @Override
    public boolean persist(){
        return true;
    }
    @Override
    public boolean canRegister(){
        return true;
    }
    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
    	 if(player == null) {
             return "";
         }
        if(identifier.equals("island_size")) {
        	if(Plugin.config.contains("IslandOwnerSettings."+player.getName()+".Size")) {
        		return String.valueOf(Plugin.config.getInt("IslandOwnerSettings."+player.getName()+".Size"));
        		
        	}
            return "No island";
        }
        if(identifier.equals("uptime")) {
        	long milliseconds = System.currentTimeMillis()-firstEnable.startTime;
        	//int seconds = (int) (milliseconds / 1000) % 60 ;
        	int minutes = (int) ((milliseconds / (1000*60)) % 60);
        	int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
        	/*return String.format("%02d hrs,%02d min,%02d sec", 
        			TimeUnit.SECONDS.toHours(player.getFirstPlayed()-Bukkit.getServer()./20),
        		    TimeUnit.SECONDS.toMinutes(player.getLastPlayed()/20) - 
        		    TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(player.getLastPlayed()/20)),
        		    TimeUnit.SECONDS.toSeconds(player.getLastPlayed()/20) - 
        		    (TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(player.getLastPlayed()/20))-
        		    TimeUnit.HOURS.toSeconds(TimeUnit.SECONDS.toHours(player.getLastPlayed()/20)))
        		);*/
        	return hours+" hrs, "+(minutes-(hours/60))+" min";//+(seconds-(((minutes-(hours/60))/60)));
            
        }
        if(identifier.equals("message")) {
            return Main.playerMessages.getOrDefault(player.getName(), "None");
        }
        if(identifier.equals("online")) {
            return String.valueOf(Bukkit.getOnlinePlayers().size());
        }
        if(identifier.equals("island_members")) {
            return String.valueOf(firstEnable.config.getStringList("IslandOwnerSettings." + player.getName() + ".Coop").size());
        }
        if(identifier.equals("island_balance")) {
            return String.valueOf(Math.round(Money.playerBalance(player.getName())));
        }
        if(identifier.equals("island_rank")) {
            return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', player.getPlayerListName().split(" ")[0]));
        }
        if(identifier.equals("island_lvl_top")) {
        	if(Main.islandBlockValueConfig.contains("Island.Top.Players")) {
        		return Main.islandBlockValueConfig.getStringList("Island.Top.Players").get(0).split(":")[0];
        		
        	}
            return "No Top Player!";
        }
        if(identifier.equals("current_island")) {
        	if(!IslandInfo.getIsland(player.getLocation()).equalsIgnoreCase("none")) {
        		return IslandInfo.getIsland(player.getLocation());
        		
        	}
            return "Not on an island";
        }
       
        
        // Placeholder: %exampleplugin_is_staff%
        if(identifier.equals("island_level")) {
        		return String.valueOf(IslandInfo.getIslandLevel(firstEnable.PlayerHomes.get(player.getName())));
        }
      
 
        
        // We return null if any other identifier was provided
        return null;
    }
}