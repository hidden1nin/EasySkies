package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;




public class playerShop implements Listener{
	
	public static FileConfiguration config = Main.getPlugin().getConfig();
	public static void loadShopPrice(Player p) {
		
		Inventory playerShopPrices = Bukkit.createInventory(null,Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9,ChatColor.BLUE+p.getName()+"'s"+ChatColor.DARK_GRAY+" shop");
		playerShopPrices.clear();
		p.openInventory(playerShopPrices);
		Main.createDisplay(Material.PAPER, playerShopPrices, Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9-2, ChatColor.GREEN+"Disable prices mode", "");
		Main.createDisplay(Material.RED_STAINED_GLASS, playerShopPrices, Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9-1,ChatColor.RED+"Cannot Upgrade Shop", ChatColor.WHITE+"Switch back to item mode to upgrade");
		for(String item: Main.playerShopConfig.getStringList("ShopOwnerData."+p.getName()+".Selling")) {
		for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+p.getName()+".Prices")) {
			
				if(item.split(":")[0].equals(itemPrice.split(":")[0])) {
					playerShopPrices.addItem(priceUnSerialize(item,itemPrice));
			} 
			
			
		}
		}
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE,playerShopPrices, " ","");
	}
	public static void loadShopData(Player p) {
		Inventory playerShop = Bukkit.createInventory(null,Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9,ChatColor.BLUE+p.getName()+ChatColor.DARK_GRAY+" shop items");
	
		for(String item: Main.playerShopConfig.getStringList("ShopOwnerData."+p.getName()+".Selling")) {
			playerShop.addItem(UnSerialize(item));
		}
		 Main.createDisplay(Material.LIME_STAINED_GLASS, playerShop, Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9-1,ChatColor.GREEN+"Upgrade Shop", ChatColor.WHITE+"Costs: "+ChatColor.GOLD+Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost"));
		 Main.createDisplay(Material.PAPER, playerShop,  Main.playerShopConfig.getInt("ShopOwnerData."+p.getName()+".ShopSize")*9-2, ChatColor.GREEN+"Prices mode", ChatColor.WHITE+"Change item prices!");
				
		 p.openInventory(playerShop);
	}
	public static void updateShop(InventoryClickEvent event) {
		
		Player player = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inv = event.getInventory();
		ItemStack UpgradeButton =Main.createDisplay(Material.LIME_STAINED_GLASS, inv, -2, ChatColor.GREEN+"Upgrade Shop", ChatColor.WHITE+"Costs: "+ChatColor.GOLD+Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost"));
		ItemStack priceButton =Main.createDisplay(Material.PAPER, inv, -2, ChatColor.GREEN+"Prices mode", ChatColor.WHITE+"Change item prices!");
		 
		 if(clicked.isSimilar(UpgradeButton)||clicked.isSimilar(priceButton)) {
			 if(clicked.isSimilar(priceButton)) {
				 event.setCancelled(true);
				 loadShopPrice(player);
			 }
			 if(clicked.isSimilar(UpgradeButton)) {
				 event.setCancelled(true);
				 player.closeInventory();
				 upgradeShop(player);
			 }
			 event.setCancelled(true);
		 }
	}
	public static void upgradeShop(Player player) {
		if(Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")<6) {
			if(Money.playerBalance(player.getName())>=Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost")) {
				Money.takeMoney(player.getName(),String.valueOf(Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost")));
				Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".ShopSize",Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")+1);
				Main.savePlayerShops();
				
				loadShopData(player);
			}else {
				player.sendMessage(Main.server+"You do not have enough to do this!");
			}
		}else {
			player.sendMessage(Main.server+"You already own the biggest shop size!");
		
		}
		
	}
	public static ItemStack priceUnSerialize(String item,String itemPrice) {
		ItemStack MainItem = UnSerialize(item);
		ItemMeta meta=MainItem.getItemMeta();
		List<String> lore = new ArrayList<String>();
		if(meta.getLore()!=null) {
			lore=meta.getLore();
		}
		lore.add(ChatColor.GOLD+itemPrice.split(":")[1]+"$"+ChatColor.WHITE+" for "+ChatColor.GREEN+itemPrice.split(":")[2]);
		meta.setLore(lore);
		MainItem.setItemMeta(meta);
		return MainItem;
		
	}
	@SuppressWarnings("deprecation")
	public static ItemStack UnSerialize(String item) {
		ItemStack itemtoadd = new ItemStack(Material.getMaterial(item.split(":")[0].toUpperCase()),Integer.parseInt(item.split(":")[1]));
		ItemMeta itemsmeta = itemtoadd.getItemMeta();
		itemtoadd.setAmount(Integer.parseInt(item.split(":")[1]));
		if(item.split(":").length>2) {
			if(!item.split(":")[2].equalsIgnoreCase("none")) {itemsmeta.setDisplayName(item.split(":")[2]);}
			List<String> lore = new ArrayList<String>();
				if(!item.split(":")[3].equalsIgnoreCase("none")) {
					for(String line:item.split(":")[3].split("/line/")) {
						if(line!=null) {lore.add(line);}
				}
					itemsmeta.setLore(lore);
			}
				if(item.split(":")[4]!="none") {
					for(int i=0;i<item.split(":")[4].split(" ").length;i++) {
						if(Enchantment.getByName(item.split(":")[4].split(" ")[i].split("=")[0])!=null){
					itemsmeta.addEnchant(Enchantment.getByName(item.split(":")[4].split(" ")[i].replace(" ", "").split("=")[0]),Integer.valueOf(item.split(":")[4].split(" ")[i].split("=")[1]), true);
						}}
					}
				itemtoadd.setDurability(Integer.valueOf(item.split(":")[5]).shortValue());
		}
		itemtoadd.setItemMeta(itemsmeta);
		return itemtoadd;
	
		
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	 public void leave(InventoryCloseEvent event) {
			
	        Player player = (Player)event.getPlayer();
			Inventory inv = event.getInventory();
			ItemStack UpgradeButton =Main.createDisplay(Material.LIME_STAINED_GLASS, inv, -2, ChatColor.GREEN+"Upgrade Shop",ChatColor.WHITE+"Costs: "+ChatColor.GOLD+Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost"));
			ItemStack priceButton =Main.createDisplay(Material.PAPER, inv, -2, ChatColor.GREEN+"Prices mode", ChatColor.WHITE+"Change item prices!");
			ItemStack UpgradeButton1 =Main.createDisplay(Material.LIME_STAINED_GLASS, inv, -2, ChatColor.GREEN+"Upgrade Shop",ChatColor.WHITE+"Costs: "+ChatColor.GOLD+Main.playerShopConfig.getInt(("ShopOwnerData."+player.getName()+".ShopSize"))+1*Main.playerShopConfig.getInt("Default.UpgradeSpaceCost"));
			
			List<String> shopContents = new ArrayList<String>();
			List<String> shopPrices =Main.playerShopConfig.getStringList("ShopOwnerData."+player.getName()+".Prices");
			List<String> shopMaterials =  new ArrayList<String>();
			if(firstEnable.PlayerOpenShop.get(player.getName())!= null) {
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA+firstEnable.PlayerOpenShop.get(player.getName())+"'s"+ChatColor.GOLD+" Shop")){
				firstEnable.PlayerOpenShop.remove(player.getName());
			}
			}
			if (event.getView().getTitle().equalsIgnoreCase(Main.server + "Shop Creation")){
				for(int i= 0; i<54;i++) {
					if(event.getInventory().getItem(i)!=null) {
					Bukkit.getConsoleSender().sendMessage("- "+event.getInventory().getItem(i).getType().toString().toLowerCase()+":100:10:"+i);
					event.getPlayer().sendMessage("- "+event.getInventory().getItem(i).getType().toString().toLowerCase()+":100:10:"+i);
				}}
					
				return;
			}
			if (event.getView().getTitle().equalsIgnoreCase(ChatColor.BLUE+player.getName()+ChatColor.DARK_GRAY+" shop items")){
				Main.editingShop.remove(player.getName());
				 for(ItemStack item:inv.getContents()) {
					 if(item !=null) {
					 if(!item.isSimilar(UpgradeButton)&&!item.isSimilar(priceButton)&&!item.isSimilar(UpgradeButton1)) {
					int amount = item.getAmount();
					item.setAmount(1);
					String iteminfo =item.getType()+":"+amount+":"+item.getItemMeta().getDisplayName();
					if(item.getItemMeta().hasDisplayName()==false) {
						iteminfo+="none";
					}
					iteminfo+=":";
					if(item.getItemMeta().getLore()!=null) {
					for(String itemlores:item.getItemMeta().getLore()) {
						iteminfo+=itemlores+"/line/";
					}
					}else {iteminfo+="none";}
					iteminfo+=":";
					if(item.getItemMeta().hasEnchants()) {
						for(Entry<Enchantment, Integer> key:item.getItemMeta().getEnchants().entrySet()) {
							iteminfo+=key.toString().replace("Enchantment[minecraft:", "").split(",")[1].replace("]","");
						}
					}else {
						iteminfo+="none";
					}
					iteminfo+=":";
					iteminfo+=item.getDurability();
					 shopContents.add(iteminfo);
					 for(String items:Main.playerShopConfig.getStringList("ShopOwnerData."+player.getName()+".Prices")) {
						 String itemmaterial = items.split(":")[0];
						 
						 shopMaterials.add(itemmaterial);
					 }
					 if(!shopMaterials.contains(item.getType().toString())){
						 shopPrices.add(item.getType()+":"+100+":"+item.getMaxStackSize());
					 }
					 }
					 }
				 }
				 Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".Selling", shopContents);
				 Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".Prices", shopPrices);
				 Main.savePlayerShops();
			}
	 
	 
	
	}
	public static void clickShopPrice(InventoryClickEvent event) {
		Inventory inv = event.getInventory();
		ItemStack clicked = event.getCurrentItem();
		Player player = (Player) event.getWhoClicked();
		ItemStack upgradeButton =Main.createDisplay(Material.PAPER, inv, Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*9-2, ChatColor.GREEN+"Disable prices mode", "");
		ItemStack priceButton=Main.createDisplay(Material.RED_STAINED_GLASS, inv, Main.playerShopConfig.getInt("ShopOwnerData."+player.getName()+".ShopSize")*9-1,ChatColor.RED+"Cannot Upgrade Shop", ChatColor.WHITE+"Switch back to item mode to upgrade");
		ItemStack filler =Main.createDisplay(Material.BLACK_STAINED_GLASS_PANE, inv,-2, " ","");
    	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, inv, " ","");
		
		if(clicked.isSimilar(upgradeButton)||clicked.isSimilar(priceButton)||clicked.isSimilar(filler)) {
			 if(clicked.isSimilar(priceButton)) {
				 event.setCancelled(true);
				 loadShopData(player);
			 }
			 if(clicked.isSimilar(upgradeButton)) {
				 event.setCancelled(true);
				 loadShopData(player);
			 }
			 if(clicked.isSimilar(filler)) {
				 event.setCancelled(true);
				 loadShopData(player);
			 }
			 event.setCancelled(true);
		 }else {
			 event.setCancelled(true);
			if(clicked != null) {
			 loadShopPrice(player, clicked);
			}
			 
		 }
		
		
	}
    private static void loadShopPrice(Player player, ItemStack clicked) {
    	Main.editingShop.add(player.getName());
    	Inventory itemPriceInv = Bukkit.createInventory(null,54,ChatColor.AQUA+"Price");
    	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, itemPriceInv, " ","");
    	for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+player.getName()+".Prices")) {
			if(clicked.getType().toString().equals(itemPrice.split(":")[0])) {
				
					Main.createDisplay(clicked.getType(), itemPriceInv, 13, ChatColor.WHITE+clicked.getType().toString().toLowerCase().replace("_"," "),ChatColor.GOLD+itemPrice.split(":")[1]+"$"+ChatColor.WHITE+" for "+ChatColor.GREEN+itemPrice.split(":")[2]);
					
				
				}
			
		}
    	Inventory inv = itemPriceInv;
    	Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 19, ChatColor.RED+"-1$", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 20, ChatColor.RED+"-10$", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 21, ChatColor.RED+"-100$", "");
		Main.createDisplay(Material.BLUE_STAINED_GLASS_PANE, inv, 22, ChatColor.GREEN+"Set to 100$", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 23, ChatColor.GREEN+"+100$", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 24, ChatColor.GREEN+"+10$", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 25, ChatColor.GREEN+"+1$", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 38, ChatColor.RED+"-1", "");
		Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 39, ChatColor.RED+"-10", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 41, ChatColor.GREEN+"+10", "");
		Main.createDisplay(Material.BLUE_STAINED_GLASS_PANE, inv, 40, ChatColor.GREEN+"Set to 1 item", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 41, ChatColor.GREEN+"+10", "");
		Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 42, ChatColor.GREEN+"+1", "");
		Main.createDisplay(Material.RED_STAINED_GLASS, inv, 49, ChatColor.WHITE+"Back", "");
		player.openInventory(itemPriceInv);
	}
	public static void setShopPrice(InventoryClickEvent event){
    	Inventory inv = event.getInventory();
		ItemStack clicked = event.getCurrentItem();
		Player player = (Player) event.getWhoClicked();
		List<String> shopPrices =Main.playerShopConfig.getStringList("ShopOwnerData."+player.getName()+".Prices");
		String Item = null;
		Integer price = 0;
		Integer amount = 0;
		Integer maxStack = 0;
		for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+player.getName()+".Prices")) {
			
				if(event.getInventory().getItem(13).getType().toString().equals(itemPrice.split(":")[0])) {
					price = Integer.parseInt(itemPrice.split(":")[1]);
					amount = Integer.parseInt(itemPrice.split(":")[2]);
					Item = event.getInventory().getItem(13).getType().toString()+":"+shopPrices.indexOf(itemPrice);
					ItemStack ItemStackSize = new ItemStack(Material.getMaterial(Item.split(":")[0].toUpperCase()));
					maxStack = ItemStackSize.getMaxStackSize()+1;
				}
			
		}
		ItemStack filler =Main.createDisplay(Material.BLACK_STAINED_GLASS_PANE, inv,-2, " ","");
    	
		ItemStack priceDown1Button =Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 19, ChatColor.RED+"-1$", "");
		ItemStack priceDown10Button =Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 20, ChatColor.RED+"-10$", "");
		ItemStack priceDown100Button =Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 21, ChatColor.RED+"-100$", "");
		ItemStack priceReset =Main.createDisplay(Material.BLUE_STAINED_GLASS_PANE, inv, 22, ChatColor.GREEN+"Set to 100$", "");
		ItemStack priceUp100Button =Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 23, ChatColor.GREEN+"+100$", "");
		ItemStack priceUp10Button =Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 24, ChatColor.GREEN+"+10$", "");
		ItemStack priceUp1Button =Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 25, ChatColor.GREEN+"+1$", "");
		ItemStack amountDown1Button =Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 38, ChatColor.RED+"-1", "");
		ItemStack amountDown10Button =Main.createDisplay(Material.RED_STAINED_GLASS_PANE, inv, 39, ChatColor.RED+"-10", "");
		ItemStack amountReset =Main.createDisplay(Material.BLUE_STAINED_GLASS_PANE, inv, 40, ChatColor.GREEN+"Set to 1 item", "");
		ItemStack amountUp10Button =Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 41, ChatColor.GREEN+"+10", "");
		ItemStack amountUp1Button =Main.createDisplay(Material.LIME_STAINED_GLASS_PANE, inv, 42, ChatColor.GREEN+"+1", "");
		ItemStack backButton =Main.createDisplay(Material.RED_STAINED_GLASS, inv, 49, ChatColor.WHITE+"Back", "");
		if(clicked.isSimilar(priceDown1Button)) {
			if(price-1 >-1) {
				price--;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(priceDown10Button)) {
			if(price-10 >-1) {
				price-= 10;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
		
		}
		if(clicked.isSimilar(priceDown100Button)) {
			if(price-100 >-1) {
				price-= 100;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(priceUp100Button)) {
			if(price+100 >-1) {
				price+= 100;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(priceUp10Button)) {
			if(price+10 >-1) {
				price+= 10;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(priceUp1Button)) {
			if(price+1 >-1) {
				price+= 1;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(amountDown1Button)) {
			if(amount-1 >0) {
				amount-= 1;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(amountDown10Button)) {
			if(amount-10 >0) {
				amount-= 10;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(amountUp10Button)) {
			if(amount+10 <maxStack) {
				amount+= 10;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(amountUp1Button)) {
			if(amount+1 < maxStack) {
				amount+= 1;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			}
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(amountReset)) {
			
				amount= 1;
			shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
			
			event.setCancelled(true);
			
		}
		if(clicked.isSimilar(priceReset)) {
			
			price= 100;
		shopPrices.set(Integer.parseInt(Item.split(":")[1]),Item.split(":")[0]+":"+price+":"+amount);
		
		event.setCancelled(true);
		
	}
		if(clicked.isSimilar(backButton)) {
			
		loadShopData(player);
		event.setCancelled(true);
		
	}
		if(clicked.isSimilar(filler)) {
			event.setCancelled(true);
			
		}
		
			Main.createDisplay(Material.getMaterial(Item.split(":")[0].toString().toUpperCase()), inv, 13, ChatColor.WHITE+Item.split(":")[0].toString().toLowerCase().replace("_"," "),ChatColor.GOLD+String.valueOf(price)+"$"+ChatColor.WHITE+" for "+ChatColor.GREEN+String.valueOf(amount));
		 Main.playerShopConfig.set("ShopOwnerData."+player.getName()+".Prices", shopPrices);
		 Main.savePlayerShops();
		 event.setCancelled(true);
		
		
		
    	
		
	}
	@SuppressWarnings("deprecation")
	public static void loadPublicShopsPage(Player playerOpen, int page) {
		Inventory EasySkyPlayerShopList = Bukkit.createInventory(null,54,Main.server+ChatColor.BLUE+"Player Shops");
		EasySkyPlayerShopList.clear();
		Main.lineDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, EasySkyPlayerShopList, 5, " ", "");
    	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, EasySkyPlayerShopList, " ","");
    	Main.createDisplay(Material.RED_STAINED_GLASS, EasySkyPlayerShopList, 49, ChatColor.WHITE+"Back", "");
    	Main.createDisplay(Material.LIME_STAINED_GLASS, EasySkyPlayerShopList, 50, ChatColor.WHITE+"Page:"+String.valueOf(page+1), "");
    	if(page<=1) {
    		Main.createDisplay(Material.LIME_STAINED_GLASS, EasySkyPlayerShopList, 48, ChatColor.WHITE+"Page:0", "");
    			
    	}else {
    	Main.createDisplay(Material.LIME_STAINED_GLASS, EasySkyPlayerShopList, 48, ChatColor.WHITE+"Page:"+String.valueOf(page-1), "");
    	}

    	int i = 0;
		if(Main.playerShopConfig.getBoolean("Default."+"ShowOfflineShops")) {
		for(int A = 45*page;firstEnable.IslandOwners.size()>A&& i<45;A++) {
			if(firstEnable.IslandOwners.get(A) != null) {
				String player = firstEnable.IslandOwners.get(A);
			if(!Main.editingShop.contains(player)&&Main.playerShopConfig.contains("ShopOwnerData."+player)) {
				
				ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
                SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
                skullMeta.setDisplayName(ChatColor.AQUA+player+"'s "+ChatColor.WHITE+"shop");
                skullMeta.setOwner("" + player);
                skull.setItemMeta(skullMeta);
		EasySkyPlayerShopList.setItem(i,skull);
		i++;
		}
		}}
		}else {for(int A = 45*page;Bukkit.getOnlinePlayers().size()>A&& i<45;A++) {
			if(Bukkit.getOnlinePlayers().toArray()[A]!= null) {
			Player player = (Player) Bukkit.getOnlinePlayers().toArray()[A];
			String p = player.getName();
			if(!Main.editingShop.contains(p)&&Main.playerShopConfig.contains("ShopOwnerData."+p)) {
				
				ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
                SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
                skullMeta.setDisplayName(ChatColor.AQUA+p+"'s "+ChatColor.WHITE+"shop");
                skullMeta.setOwner("" + p);
                skull.setItemMeta(skullMeta);
		EasySkyPlayerShopList.setItem(i,skull);
		i++;
		}
		}
			
			
		}
			
		}
		playerOpen.openInventory(EasySkyPlayerShopList);
	}
	@SuppressWarnings("deprecation")
	public static void loadPublicShops(Player playerOpen) {
		Inventory EasySkyPlayerShopList = Bukkit.createInventory(null,54,Main.server+ChatColor.BLUE+"Player Shops");
		EasySkyPlayerShopList.clear();
		Main.lineDisplay(Material.LIGHT_BLUE_STAINED_GLASS_PANE, EasySkyPlayerShopList, 5, " ", "");
    	Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE, EasySkyPlayerShopList, " ","");
    	Main.createDisplay(Material.RED_STAINED_GLASS, EasySkyPlayerShopList, 49, ChatColor.WHITE+"Back", "");
    	Main.createDisplay(Material.LIME_STAINED_GLASS, EasySkyPlayerShopList, 50, ChatColor.WHITE+"Page:1", "");
    	Main.createDisplay(Material.LIME_STAINED_GLASS, EasySkyPlayerShopList, 48, ChatColor.WHITE+"Page:0", "");
		
    	
		int i = 0;
		if(Main.playerShopConfig.getBoolean("Default."+"ShowOfflineShops")) {
		for(String player:firstEnable.IslandOwners) {
			if(!Main.editingShop.contains(player)&&Main.playerShopConfig.contains("ShopOwnerData."+player)) {
				
				ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
                SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
                skullMeta.setDisplayName(ChatColor.AQUA+player+"'s "+ChatColor.WHITE+"shop");
                skullMeta.setOwner("" + player);
                skull.setItemMeta(skullMeta);
		EasySkyPlayerShopList.setItem(i,skull);
		i++;
		}
		}
		}else {for(Player player:Bukkit.getOnlinePlayers()) {
			String p = player.getName();
			if(!Main.editingShop.contains(p)&&Main.playerShopConfig.contains("ShopOwnerData."+p)) {
				
				ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
                SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
                skullMeta.setDisplayName(ChatColor.AQUA+p+"'s "+ChatColor.WHITE+"shop");
                skullMeta.setOwner("" + p);
                skull.setItemMeta(skullMeta);
		EasySkyPlayerShopList.setItem(i,skull);
		i++;
		}
			
			
		}
			
		}
		playerOpen.openInventory(EasySkyPlayerShopList);
	}
	@SuppressWarnings("deprecation")
	public static void openPublicShop(Player player, ItemStack clicked) {
		
		//ItemStack filler =Main.createDisplay(Material.BLACK_STAINED_GLASS_PANE,player.getInventory(),-2, " ","");
		ItemStack backButton =Main.createDisplay(Material.RED_STAINED_GLASS, null, -2, ChatColor.WHITE+"Back", "");
		
		if(clicked.getType()!= Material.PLAYER_HEAD) {
			if(clicked.isSimilar(backButton)) {
				if(Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("island")||Main.islandCommandsConfig.getString("Commands.MenuOpen").equalsIgnoreCase("is")) {
					player.openInventory(Main.EasySkyGui);
					return;
					}else {
					Bukkit.dispatchCommand(player, Main.islandCommandsConfig.getString("Commands.MenuOpen"));
					return;
					}
			}
			if(clicked.getType()==Material.LIME_STAINED_GLASS&&clicked.getItemMeta().getDisplayName().split(":")[0]!= null) {
				if(clicked.getItemMeta().getDisplayName().split(":")[0].equalsIgnoreCase("Page")) {
					if(clicked.getItemMeta().getDisplayName().split(":")[1].equalsIgnoreCase("0")) {
						playerShop.loadPublicShopsPage(player,0);
					}
					playerShop.loadPublicShopsPage(player, Integer.valueOf(clicked.getItemMeta().getDisplayName().split(":")[1]));
				}
			}
			return;
			
		}
		
		SkullMeta skullMeta = (SkullMeta) clicked.getItemMeta();
		String p = skullMeta.getOwner();
		Inventory itemPriceInv = Bukkit.createInventory(null,Main.playerShopConfig.getInt("ShopOwnerData."+p+".ShopSize")*9,ChatColor.AQUA+skullMeta.getOwner()+"'s"+ChatColor.GOLD+" Shop");
		
		
		firstEnable.PlayerOpenShop.put(player.getName(), p);
		int i = 0;
		for(String item: Main.playerShopConfig.getStringList("ShopOwnerData."+p+".Selling")) {
			for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+p+".Prices")) {
				
					if(item.split(":")[0].equals(itemPrice.split(":")[0])) {
						if(item.split(":").length>=3) {
							itemPriceInv.addItem(priceUnSerialize(item,itemPrice));}else {
						Main.createDisplay(Material.getMaterial(itemPrice.split(":")[0].toUpperCase()), itemPriceInv, i,ChatColor.AQUA+itemPrice.split(":")[1].toLowerCase().replace("_"," "),ChatColor.GOLD+itemPrice.split(":")[1]+"$"+ChatColor.WHITE+" for "+ChatColor.GREEN+itemPrice.split(":")[2]);
						}
						i++;
				} 
				
				
			}
			}
		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE,itemPriceInv, " ","");
		Main.createDisplay(Material.RED_STAINED_GLASS, itemPriceInv, Main.playerShopConfig.getInt("ShopOwnerData."+p+".ShopSize")*9-1, ChatColor.WHITE+"Back", "");
		
		player.openInventory(itemPriceInv);
		return;
	}
	@SuppressWarnings("deprecation")
	public static void playerBuyItem(Player player, ItemStack clicked) {
		if(Main.editingShop.contains(firstEnable.PlayerOpenShop.get(player.getName()))) {
			player.closeInventory();
			player.sendMessage(Main.server+"Shop Owner is editing shop");
			return;
		}
		String Materials = "none";
		Integer price =0;
		Integer amount = 0;
		boolean foundItem =false;
		List<String> shopStock =new ArrayList<String>();
		shopStock.clear();
		ItemStack backButton =Main.createDisplay(Material.RED_STAINED_GLASS, player.getInventory(), -2, ChatColor.WHITE+"Back", "");
		if(clicked.isSimilar(backButton)) {
			loadPublicShops(player);
			return;
		}
		for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+firstEnable.PlayerOpenShop.get(player.getName())+".Prices")) {
			
			if(clicked.getType().toString().equals(itemPrice.split(":")[0])) {
				
				 Materials = itemPrice.split(":")[0].toUpperCase();
				 price =Integer.parseInt(itemPrice.split(":")[1]);
				 amount = Integer.parseInt(itemPrice.split(":")[2]);
				
		} 
		
	}
		if(player.getInventory().firstEmpty()>-1) {
		if(Money.playerBalance(player.getName())-price>=0) {
			//if(player.getInventory().firstEmpty() ==-1) {
			for(String item: Main.playerShopConfig.getStringList("ShopOwnerData."+firstEnable.PlayerOpenShop.get(player.getName())+".Selling")) {
					
						if(item.split(":")[0].equals(Materials)) {
							if(Integer.parseInt(item.split(":")[1])-amount>=0&& foundItem == false) {
								foundItem =true;
							Money.takeMoney(player.getName(), String.valueOf(price));
							Money.giveMoney(firstEnable.PlayerOpenShop.get(player.getName()), String.valueOf(price));
							ItemStack bought = UnSerialize(item);
							bought.setAmount(amount);
							player.getInventory().addItem(bought);
							if(Integer.parseInt(item.split(":")[1])-amount>=1) {
							shopStock.add(item.split(":")[0]+":"+String.valueOf(Integer.parseInt(item.split(":")[1])-amount)+":"+item.split(":")[2]+":"+item.split(":")[3]+":"+item.split(":")[4]+":"+item.split(":")[5]);
							
							}
							
							}else {
								shopStock.add(item.split(":")[0]+":"+item.split(":")[1]+":"+item.split(":")[2]+":"+item.split(":")[3]+":"+item.split(":")[4]+":"+item.split(":")[5]);
							}
							
							
							}else{
								shopStock.add(item.split(":")[0]+":"+item.split(":")[1]+":"+item.split(":")[2]+":"+item.split(":")[3]+":"+item.split(":")[4]+":"+item.split(":")[5]);
								}
							
					} 
					
				
				Main.playerShopConfig.set("ShopOwnerData."+firstEnable.PlayerOpenShop.get(player.getName())+".Selling", shopStock);
				Main.savePlayerShops();
				
				
				
				
			//}else {
				//player.sendMessage(Main.server+"You have a full inventory");
			//}
				ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
                SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
                skullMeta.setDisplayName(ChatColor.AQUA+firstEnable.PlayerOpenShop.get(player.getName())+"'s "+ChatColor.WHITE+"shop");
                skullMeta.setOwner(firstEnable.PlayerOpenShop.get(player.getName()));
                skull.setItemMeta(skullMeta);
                String p =firstEnable.PlayerOpenShop.get(player.getName());
        		for(String item: Main.playerShopConfig.getStringList("ShopOwnerData."+p+".Selling")) {
        			for(String itemPrice:Main.playerShopConfig.getStringList("ShopOwnerData."+p+".Prices")) {
        				
        					if(item.split(":")[0].equals(itemPrice.split(":")[0])) {
        						ItemStack toadd = priceUnSerialize(item,itemPrice);
        						toadd.setAmount(-1);
        						player.getOpenInventory().getTopInventory().addItem(toadd);
        				} 
        				
        				
        			}
        			}
        		Main.fillDisplay(Material.BLACK_STAINED_GLASS_PANE,player.getOpenInventory().getTopInventory(), " ","");
        		Main.createDisplay(Material.RED_STAINED_GLASS, player.getOpenInventory().getTopInventory(), Main.playerShopConfig.getInt("ShopOwnerData."+p+".ShopSize")*9-1, ChatColor.WHITE+"Back", "");
        		}else {
			player.sendMessage(Main.server+"You need more money to buy that!");
		}
		}else {
			player.sendMessage(Main.server+"You have a full inventory!");
		}
		
	}
}
