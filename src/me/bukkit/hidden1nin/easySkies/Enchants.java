package me.bukkit.hidden1nin.easySkies;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class Enchants implements Listener {
	public static ArrayList<String> Enchantments = new ArrayList<String>();
	static {
		//Enchantments.add("Blind");
		Enchantments.add("Exploding");
		Enchantments.add("Flinging");
		Enchantments.add("Healing");
		Enchantments.add("Magnet");
		Enchantments.add("Loved");
		Enchantments.add("Blaze");
		Enchantments.add("Slowness");
		Enchantments.add("Snowy");
		Enchantments.add("Present");
		Enchantments.add("Nausea");
		Enchantments.add("Quick");
		Enchantments.add("Ogre");
		Enchantments.add("Nice");
		Enchantments.add("Naughty");
		Enchantments.add("Ransack");
		Enchantments.add("Glimmer");
		Enchantments.add("Rainbow");
		Enchantments.add("ElectroConductor");
		Enchantments.add("Soft-Landing");
		Enchantments.add("High-Jump");
		Enchantments.add("Weaken");
		Enchantments.add("Poison");
		Enchantments.add("Webbed");
		Enchantments.add("Loot-Box");
		Enchantments.add("Edible");
		Enchantments.add("Sticky");
		Enchantments.add("Slimy");
	}
	public static int getEnchantLevel(String enchant, ItemStack item) {
		if(!hasCustomEnchant(enchant, item)) {return 0;}
		if (item.hasItemMeta()) {
		    if (item.getItemMeta().hasLore()) {
		        for (int i = 0;i<item.getItemMeta().getLore().size();i++) {
		            String[] fLore = ChatColor.stripColor(item.getItemMeta().getLore().get(i)).split(" ");
		            String testLore = fLore[0];
		            if (testLore.equals(enchant)) {
		                return Integer.parseInt(fLore[1]);
		            }
		        }
		    }
		}
		return 0;
	}
	public static boolean hasCustomEnchant(String enchant, ItemStack item) {
		if(item!=null) {
		if (item.hasItemMeta()) {
		    if (item.getItemMeta().hasLore()) {
		    	if (item.getItemMeta().getLore()!=null) {
		        for (int i = 0;i<item.getItemMeta().getLore().size();i++) {
		            String[] fLore = ChatColor.stripColor(item.getItemMeta().getLore().get(i)).split(" ");
		            String testLore = fLore[0];
		            if(enchant.equalsIgnoreCase("any")) {
		            	if(Enchantments.contains(testLore)) {
		            		return true;
		            		
		            	}
		            }
		            	 if (testLore.equals(enchant)) {
				                return true;
				            }
		            
		        }
		    }}
		}
		}
		return false;
	}
	
	@EventHandler
	public void onEnchantEvent (EnchantItemEvent event) {
	    //Player player = event.getEnchanter();
	    ItemStack item = event.getItem();
	    Material mat = item.getType();
	    if (mat.equals(Material.BOW)) {
	    	item.setItemMeta (addEnchantment (item, "&7Warping", event.getExpLevelCost(), 10, 1));
	    }
	    if (mat.toString().toLowerCase().contains("_shovel")) {
	    	if(!item.containsEnchantment(Enchantment.SILK_TOUCH)) {
		    	item.setItemMeta (addEnchantment (item, "&7Magnet", event.getExpLevelCost(), 5, 1));
		    	}
	    }
	    if (mat.toString().toLowerCase().contains("_hoe")) {
	    	if(!item.containsEnchantment(Enchantment.SILK_TOUCH)) {
		    	item.setItemMeta (addEnchantment (item, "&7Magnet", event.getExpLevelCost(), 5, 1));
		    	}
	    }
	    if (mat.toString().toLowerCase().contains("_axe")) {
	      	item.setItemMeta (addEnchantment (item, "&7Blind", event.getExpLevelCost(), 10, 5));
	    	item.setItemMeta (addEnchantment (item, "&7Exploding", event.getExpLevelCost(), 10, 5));
	    	if(!item.containsEnchantment(Enchantment.SILK_TOUCH)) {
		    	item.setItemMeta (addEnchantment (item, "&7Magnet", event.getExpLevelCost(), 5, 1));
		    	}
	    }
	    if (mat.toString().toLowerCase().contains("boots")||mat.toString().toLowerCase().contains("helmet")||mat.toString().toLowerCase().contains("chestplate")||mat.toString().toLowerCase().contains("leggings")) {
	    	item.setItemMeta (addEnchantment (item, "&7Healing", event.getExpLevelCost(), 1, 3));
	    	item.setItemMeta (addEnchantment (item, "&7Quick", event.getExpLevelCost(), 1, 3));
	    	item.setItemMeta (addEnchantment (item, "&7Soft Landing", event.getExpLevelCost(), 1, 1));
	    	item.setItemMeta (addEnchantment (item, "&7Blaze", event.getExpLevelCost(), 1, 1));
	    	item.setItemMeta (addEnchantment (item, "&7Glimmer", event.getExpLevelCost(), 1, 1));
	    	item.setItemMeta (addEnchantment (item, "&cR&6a&ei&an&bb&do&5w", event.getExpLevelCost(), 1, 1));
	    }
	    if (mat.toString().toLowerCase().contains("pickaxe")) {
	    	if(!item.containsEnchantment(Enchantment.SILK_TOUCH)) {
	    	item.setItemMeta (addEnchantment (item, "&7Magnet", event.getExpLevelCost(), 5, 1));
	    	}
	    }
	    if (mat.toString().toLowerCase().contains("book")) {
	    	if(!item.containsEnchantment(Enchantment.LOOT_BONUS_MOBS)) {
	    	item.setItemMeta (addEnchantment (item, "&7Ransack", event.getExpLevelCost(), 4, 3));
	    	}
	    }
	    if (mat.toString().toLowerCase().contains("sword")) {
	    	item.setItemMeta (addEnchantment (item, "&7Flinging", event.getExpLevelCost(), 10, 5));
	    	if(!item.containsEnchantment(Enchantment.LOOT_BONUS_MOBS)) {
	    	item.setItemMeta (addEnchantment (item, "&7Ransack", event.getExpLevelCost(), 4, 3));
	    	}
	    	//item.setItemMeta (addEnchantment (item, "&7Blind", event.getExpLevelCost(), 10, 5));
	    	item.setItemMeta (addEnchantment (item, "&7Exploding", event.getExpLevelCost(), 10, 5));
	    }
	    
	}
	public ItemMeta addEnchantment (ItemStack item, String enchantmentName, int xp, int chance, int maxLevel) {
		if(!Enchantments.contains(enchantmentName)) {Enchantments.add(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', enchantmentName)));}
		if (item==null || enchantmentName==null) {
		    return null;
		}
		int successRate = (int) Math.round(Math.random()*100);
		if (successRate<=chance+xp) {

		int level = Math.round(xp/Math.round(30/maxLevel));
		
		ArrayList<String> newLores = new ArrayList<String>();
		if (item.hasItemMeta()) {
		    if (item.getItemMeta().hasLore()) {
		        newLores.addAll(item.getItemMeta().getLore());
		    }
		}
		if(level != 0) {newLores.add(ChatColor.translateAlternateColorCodes('&', enchantmentName) + " " + level);}
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setLore(newLores);
		item.setItemMeta(itemMeta);
		}
		return item.getItemMeta();
	}

	@EventHandler
	public static void onInventoryEnchant(PrepareAnvilEvent e) {


		Inventory inv = e.getInventory();

// see if we are talking about an anvil here
		if (inv instanceof AnvilInventory) {
			AnvilInventory anvil = (AnvilInventory) inv;



			// all three items in the anvil inventory
			ItemStack[] items = anvil.getContents();

// item in the left slot
			ItemStack item1 = items[0];

// item in the right slot
			ItemStack item2 = items[1];
			
			// item in the result slot
			ItemStack item3 = e.getResult();

// check if there is an item in the result slot
			
			if (item3 != null) {
				ItemMeta meta = item3.getItemMeta();

// meta data could be null
				if (meta != null) {	

					if (hasCustomEnchant("any", item2)) {

						ArrayList<String> newLores = new ArrayList<String>();
						for (int b = 0; b < item2.getItemMeta().getLore().size(); b++) {
							String[] fLore2 = ChatColor.stripColor(item2.getItemMeta().getLore().get(b)).split(" ");
							String testLore2 = fLore2[0];
							int level2 = Integer.parseInt(fLore2[1]);
			
							for (int i = 0; i < item1.getItemMeta().getLore().size(); i++) {
								String[] fLore = ChatColor.stripColor(item1.getItemMeta().getLore().get(i)).split(" ");
								String testLore = fLore[0];
								String[] cLore = item1.getItemMeta().getLore().get(i).split(" ");
								int level = Integer.parseInt(fLore[1]);
								if (testLore.equals(testLore2)) {

									if (level2 == level) {
										newLores.add(ChatColor.GRAY+cLore[0] + " " + (level2 + 1));
									} else {
										newLores.add(ChatColor.GRAY+cLore[0] + " " + level);
									}
								} 
								
							}
							if(!hasCustomEnchant(testLore2, item1)) {
								newLores.add(item2.getItemMeta().getLore().get(b));
								}
							
						}
						meta.setLore(newLores);
						item3.setItemMeta(meta);
						e.setResult(item3);

					}

				}
			}
		}
	}
	public static void doMineEnchant(Player player , BlockBreakEvent event) {
		if(player.getInventory().getItemInMainHand()!=null) {
		
		if(Enchants.hasCustomEnchant("Magnet", player.getInventory().getItemInMainHand())) {
			int successRate = (int) Math.round(Math.random()*100);
			if (successRate<=100) {
			event.setDropItems(false);
			for(ItemStack item:event.getBlock().getDrops()) {
				if(player.getInventory().firstEmpty()== -1) {
					player.getWorld().dropItemNaturally(player.getLocation(),item);
				}else {
			player.getInventory().addItem(item);
				}
			}
			player.getWorld().spawnParticle(Particle.REDSTONE,Main.getBlockCenter(event.getBlock()), 10,new Particle.DustOptions(Color.PURPLE,1));
			}
			}
		}
	}	
	public static void doPlaceEnchant(Player player , BlockPlaceEvent event) {
		//if (successRate<=100) {} example chance
		for(ItemStack item:player.getInventory().getArmorContents()) {
			if(item != null) {
			if(Enchants.hasCustomEnchant("Healing", item)) {
				int successRate = (int) Math.round(Math.random()*100);
				if(player.getHealth()+(.5*getEnchantLevel("Healing",item))<=20&&successRate<=3*getEnchantLevel("Healing",item)) {
				player.setHealth(player.getHealth()+.5*getEnchantLevel("Healing",item));
				Particles.createParticleTube(player.getLocation(), Particle.HEART, 1, 2, .5,true);
				}
				}
			}
		}
	}
	public static void doPvpEnchant(Player player, Entity e) {
		
		if(Enchants.hasCustomEnchant("Flinging", player.getInventory().getItemInMainHand())) {
			int successRate = (int) Math.round(Math.random()*100);
			if(successRate<=5*getEnchantLevel("Flinging",player.getInventory().getItemInMainHand())) {
		e.setVelocity(player.getLocation().getDirection().multiply(-1.5*getEnchantLevel("Flinging",player.getInventory().getItemInMainHand())));
		Particles.createParticleLine(player.getLocation(), e.getLocation(), Particle.DRIP_LAVA, .1);
			}
		}
		if(Enchants.hasCustomEnchant("Exploding", player.getInventory().getItemInMainHand())) {
			int successRate = (int) Math.round(Math.random()*100);
			if(((Damageable) e).getHealth()-1>0&&successRate<=10*getEnchantLevel("Exploding",player.getInventory().getItemInMainHand())) {
			((Damageable) e).setHealth(((Damageable) e).getHealth()-1);
			e.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, e.getLocation(), 4);
			}
			}
			
		if(Enchants.hasCustomEnchant("Blind", player.getInventory().getItemInMainHand())) {
			int successRate = (int) Math.round(Math.random()*100);
			if (successRate<=5*getEnchantLevel("Blind",player.getInventory().getItemInMainHand())) {
			((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1*getEnchantLevel("Blind",player.getInventory().getItemInMainHand())*20, 1));
			}
			}
		for(ItemStack item:player.getInventory().getArmorContents()) {
			if(item != null) {
		if(Enchants.hasCustomEnchant("Poison", item)) {
			((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.POISON,1000, 1), true);
			}
		if(Enchants.hasCustomEnchant("Weaken", item)) {
			int successRate = (int) Math.round(Math.random()*100);
			if (successRate<=20*getEnchantLevel("Weaken",item)) {
			((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5*getEnchantLevel("Weaken",player.getInventory().getItemInMainHand())*20, 1));
			}
			}
			}
			}
		
		
		}
	
	public static void giveEnchant(String player, String name, String enchant) {
		if(Enchantments.contains(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&',name)))) {
    Bukkit.getPlayer(player).getInventory().addItem(Main.createDisplay(Material.PAPER, null, -2, ChatColor.AQUA+"Enchantment slip",ChatColor.GRAY+ChatColor.translateAlternateColorCodes('&', name)+" "+enchant));
		}
		}
	public static void openEnchantTable(Player p) {
		Inventory EasySkyEnchantTable = Bukkit.createInventory(null,9,Main.server + "Island Anvil");
		Main.fillDisplay(Material.PURPLE_STAINED_GLASS_PANE,EasySkyEnchantTable, " ",ChatColor.LIGHT_PURPLE+"Combine Enchant Slips with items");
		p.openInventory(EasySkyEnchantTable);
	}
	@SuppressWarnings("deprecation")
	public static void upgrade(InventoryClickEvent event) {
		ItemStack item2 = event.getCurrentItem();
		ItemStack item1 = event.getCursor();
		ItemStack item3 = item2;
		ItemMeta meta = item3.getItemMeta();
		if(event.getCursor().getAmount()>1) {
			return;
		}
		if(event.getCurrentItem().getItemMeta().hasLore()== false) {
			ArrayList<String> newLores = new ArrayList<String>();
			newLores.addAll(item1.getItemMeta().getLore());
			meta.setLore(newLores);
			item3.setItemMeta(meta);
			event.setCurrentItem(item3);
			event.setCursor(null);
			Particles.createParticleTube(event.getWhoClicked().getLocation(), Particle.VILLAGER_HAPPY, .5, 2, .2, true);
			return;
		}
		if (hasCustomEnchant("any", item2)||hasCustomEnchant("any", item1)) {

			ArrayList<String> newLores = new ArrayList<String>();
			ArrayList<String> enchants = new ArrayList<String>();
			for (int b = 0; b < item2.getItemMeta().getLore().size(); b++) {
				enchants.add(item2.getItemMeta().getLore().get(b).split(" ")[0]);
				if(item2.getItemMeta().getLore().get(b).startsWith(item1.getItemMeta().getLore().get(0))) {
				newLores.add(item1.getItemMeta().getLore().get(0).split(" ")[0]+" "+(Integer.valueOf(item1.getItemMeta().getLore().get(0).split(" ")[1])+1));
				}else {
				newLores.add(item2.getItemMeta().getLore().get(b));
				}
			}
			if(!enchants.contains(item1.getItemMeta().getLore().get(0).split(" ")[0])) {
				newLores.add(item1.getItemMeta().getLore().get(0));
			}
				
			/*for (int b = 0; b < item2.getItemMeta().getLore().size(); b++) {
				String[] fLore2 = ChatColor.stripColor(item2.getItemMeta().getLore().get(b)).split(" ");
				String testLore2 = fLore2[0];
				int level2 = Integer.parseInt(fLore2[1]);

				for (int i = 0; i < item1.getItemMeta().getLore().size(); i++) {
					String[] fLore = ChatColor.stripColor(item1.getItemMeta().getLore().get(i)).split(" ");
					String[] cLore = item1.getItemMeta().getLore().get(i).split(" ");
					String testLore = fLore[0];
					int level = Integer.parseInt(fLore[1]);
					if (testLore.equals(testLore2)) {

						if (level2 == level) {
							newLores.add(ChatColor.GRAY+cLore[0] + " " + (level2 + 1));
						} else {
							newLores.add(ChatColor.GRAY+cLore[0] + " " + level);
						}
					} 
					
				}
				if(!hasCustomEnchant(testLore2, item1)) {
					newLores.add(item2.getItemMeta().getLore().get(b));
					}
				
			}*/
			meta.setLore(newLores);
			item3.setItemMeta(meta);
			event.setCurrentItem(item3);
			event.getCursor().setAmount(event.getCursor().getAmount()-1);
			Particles.createParticleTube(event.getWhoClicked().getLocation(), Particle.VILLAGER_HAPPY, .5, 2, .2, true);
		}

			
		
	}
	public static void onMovementEnchant(PlayerMoveEvent e) {
		Player player =e.getPlayer();
		/////////Loved//
		if(player.getInventory().getItemInMainHand()!=null) {
		if(Enchants.hasCustomEnchant("Rainbow", player.getInventory().getItemInMainHand())) {
			Particles.createParticle(e.getTo(),Color.fromRGB((int)(Math.random() * 0x100),(int)(Math.random() * 0x100), (int)(Math.random() * 0x100)),1);
			}
		if(Enchants.hasCustomEnchant("Glimmer", player.getInventory().getItemInMainHand())) {
			Particles.createParticle(e.getTo(),Color.fromRGB(255, 215, 0),1);
			}
		if(Enchants.hasCustomEnchant("Ogre", player.getInventory().getItemInMainHand())) {
			Particles.createParticle(e.getTo(),Color.fromRGB(34, 139, 34),1);
			Particles.createParticle(e.getTo(),Color.SILVER,1);
			 }
		if(Enchants.hasCustomEnchant("Loved", player.getInventory().getItemInMainHand())) {
			Particles.createParticle(e.getTo(), Color.FUCHSIA, 1*getEnchantLevel("Loved",player.getInventory().getItemInMainHand()));
		}
		if(Enchants.hasCustomEnchant("Snowy", player.getInventory().getItemInMainHand())) {
			Particles.createParticle(e.getTo(), Color.WHITE, 1*getEnchantLevel("Snowy",player.getInventory().getItemInMainHand()));
		}
		if(Enchants.hasCustomEnchant("Slowness", player.getInventory().getItemInMainHand())) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10*20, 100));
		}
		if(Enchants.hasCustomEnchant("Nausea", player.getInventory().getItemInMainHand())) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10*20, 10));
		}
		if(Enchants.hasCustomEnchant("Soft-Landing",player.getInventory().getItemInMainHand())) {
			player.setFallDistance(0);
		}
		if(Enchants.hasCustomEnchant("Quick", player.getInventory().getItemInMainHand())) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20*getEnchantLevel("Quick",player.getInventory().getItemInMainHand()), 1*getEnchantLevel("Quick",player.getInventory().getItemInMainHand())));
			Particles.createParticle(e.getTo(), Color.SILVER, 1);
		}
		if(Enchants.fullSet("Sticky",player.getInventory().getArmorContents())&&player.isSneaking()&&IslandInfo.pvpOn(player)) {
			for(Entity entity:player.getNearbyEntities(6, 3, 6)) {
				if(entity instanceof ArmorStand==false&&entity instanceof Player==false&&entity instanceof Item== false) {
				Vector to =player.getLocation().toVector().subtract(entity.getLocation().toVector());
				entity.setVelocity(to.multiply(.2));
				entity.setFallDistance(0);
				entity.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 20);
			}}}
		if(Enchants.fullSet("Slimy",player.getInventory().getArmorContents())&&player.isSneaking()&&IslandInfo.pvpOn(player)) {
			for(Entity entity:player.getNearbyEntities(3, 2, 3)) {
				if(entity instanceof ArmorStand==false&&entity instanceof Player==false&&entity instanceof Item== false) {
				Vector to =entity.getLocation().toVector().subtract(player.getLocation().toVector());
				entity.setVelocity(to.multiply(.2));
				entity.setFallDistance(0);
				entity.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 20);
			}}}
		}
		for(ItemStack item:player.getInventory().getArmorContents()) {
			if(item != null) {
				if(Enchants.hasCustomEnchant("Slowness",item)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10*20, 100));
				}
				if(Enchants.hasCustomEnchant("High-Jump",item)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20*getEnchantLevel("High-Jump",item), getEnchantLevel("High-Jump",item)));
					Particles.createParticle(e.getTo(), Color.WHITE, 1);
				}
				if(Enchants.hasCustomEnchant("Soft-Landing",item)) {
					player.setFallDistance(0);
				}
				if(Enchants.hasCustomEnchant("Quick", item)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20*getEnchantLevel("Quick",item), getEnchantLevel("Quick",item)));
					Particles.createParticle(e.getTo(), Color.SILVER, 1);
				}
				if(Enchants.hasCustomEnchant("Nausea",item)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10*20, 10));
				}
				if(Enchants.hasCustomEnchant("Blaze", item)) {
					Particles.createParticle(e.getTo(),Particle.LAVA, 1*getEnchantLevel("Blaze",item));
				}
				if(Enchants.hasCustomEnchant("Snowy", item)) {
					Particles.createParticle(e.getTo(), Color.WHITE, 1*getEnchantLevel("Snowy",item));
				}
				if(Enchants.hasCustomEnchant("Rainbow", item)) {
					Particles.createParticle(e.getTo(),Color.fromRGB((int)(Math.random() * 0x100),(int)(Math.random() * 0x100), (int)(Math.random() * 0x100)),1);
					}
				if(Enchants.hasCustomEnchant("Glimmer", item)) {
					Particles.createParticle(e.getTo(),Color.fromRGB(255, 215, 0),1);
					}
			if(Enchants.hasCustomEnchant("Loved", item)) {
				Particles.createParticle(e.getTo(), Color.FUCHSIA, 1*getEnchantLevel("Loved",item));
			}
			if(Enchants.hasCustomEnchant("Ogre", item)) {
				Particles.createParticle(e.getTo(), Color.fromRGB(34, 139, 34), 1*getEnchantLevel("Ogre",item));
				Particles.createParticle(e.getTo(),Color.SILVER,1);
			}
			}
		}
		/////////////other enchant////////
	}
	public static void ClickBlockEnchant(PlayerInteractEvent event) {
		if(event.getAction()==Action.RIGHT_CLICK_AIR||event.getAction()==Action.RIGHT_CLICK_BLOCK) {
		if(event.getPlayer().getInventory().getItemInMainHand()!=null) {
			if(Enchants.hasCustomEnchant("Edible", event.getPlayer().getInventory().getItemInMainHand())&&Cooldowns.silentIsCooldownOver(Cooldowns.PotatoCoolDown, 1, event.getPlayer())) {
				if(event.getPlayer().getFoodLevel()<20) {
				event.setCancelled(true);
				event.getPlayer().setSaturation(event.getPlayer().getSaturation()+Enchants.getEnchantLevel("Edible", event.getPlayer().getInventory().getItemInMainHand()));
				event.getPlayer().setFoodLevel(event.getPlayer().getFoodLevel()+Enchants.getEnchantLevel("Edible", event.getPlayer().getInventory().getItemInMainHand()));
				event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount()-1);
				event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_GENERIC_EAT, 1000, 1);
				}
			}
		if(Enchants.hasCustomEnchant("ElectroConductor", event.getPlayer().getInventory().getItemInMainHand())) {
				event.setCancelled(true);
				if(event.getPlayer().getTargetBlockExact(100)!=null) {
				if((int)(Math.random()*100)<Enchants.getEnchantLevel("ElectroConductor", event.getPlayer().getInventory().getItemInMainHand())*2)
				event.getPlayer().getLocation().getWorld().strikeLightning(event.getPlayer().getTargetBlockExact(100).getLocation());
				}else {event.getPlayer().sendMessage(Main.server+"That block is out of range!");}
				}
		if(Enchants.hasCustomEnchant("Loot-Box", event.getPlayer().getInventory().getItemInMainHand())) {
			if(Main.islandHolidayConfig.contains("Items."+event.getPlayer().getInventory().getItemInMainHand().getType())) {
			event.setCancelled(true);
			if(Cooldowns.isCooldownOver(Cooldowns.presentCoolDown, Main.islandHolidayConfig.getInt("Items."+event.getPlayer().getInventory().getItemInMainHand().getType()+".Cooldown"), event.getPlayer())) {
	    	for(int i =0;i<getEnchantLevel("Loot-Box",event.getPlayer().getInventory().getItemInMainHand());i++) {
				boolean gotsomething = false;
					while (gotsomething  == false) {
				    String item=Main.getRandom(Main.islandHolidayConfig.getStringList("Items."+event.getPlayer().getInventory().getItemInMainHand().getType()+".Loot")); 
					String[] args = item.split(":");
					int Random = (int)(Math.random()*100);
					if(args.length==3) {
						if(Random<=Integer.valueOf(args[2])) {
							if(args[0].equalsIgnoreCase("command")) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), args[1].replace("%player%", event.getPlayer().getName()));
								gotsomething = true;
								break;
							}else {
								if(event.getPlayer().getInventory().firstEmpty()>-1) {
									event.getPlayer().getInventory().addItem(new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
								}else {
								event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
								}gotsomething = true;
								break;
							}
						}
					}else {
					if(args.length==5) {
						if(Random<=Integer.valueOf(args[4])) {
							ItemStack gift = Main.createDisplay(Material.getMaterial(args[0].toUpperCase()), null, -2, ChatColor.translateAlternateColorCodes('&', args[1]).replace("%player%", event.getPlayer().getName()), ChatColor.translateAlternateColorCodes('&', args[2]).replace("%player%", event.getPlayer().getName()));
							gift.setAmount(Integer.valueOf(args[3]));
							if(event.getPlayer().getInventory().firstEmpty()>-1) {
								event.getPlayer().getInventory().addItem(gift);
							}else {
							event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(),gift);
							}
							//
							gotsomething = true;
							break;
						}
					}
					}
					}
	    	}
	    	}
	    	if(Main.islandHolidayConfig.getString("Items."+event.getPlayer().getInventory().getItemInMainHand().getType()+".OpenMessage")!=null) {
	    		Cooldowns.sendActionBarMessage(event.getPlayer(),ChatColor.translateAlternateColorCodes('&',Main.islandHolidayConfig.getString("Items."+event.getPlayer().getInventory().getItemInMainHand().getType()+".OpenMessage")).replace("%amount%", String.valueOf(getEnchantLevel("Loot-Box",event.getPlayer().getInventory().getItemInMainHand()))));
	    		event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_BAMBOO_HIT, 1000, 1);
	    	}
	    	event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount()-1);
	    	
	    	return;
		}}
		if(Enchants.hasCustomEnchant("Present", event.getPlayer().getInventory().getItemInMainHand())) {
			event.setCancelled(true);
			if(Cooldowns.isCooldownOver(Cooldowns.presentCoolDown, Main.islandHolidayConfig.getInt("Christmas.Presents.Cooldown"), event.getPlayer())) {
	    	for(int i =0;i<getEnchantLevel("Present",event.getPlayer().getInventory().getItemInMainHand());i++) {
				boolean gotsomething = false;
					while (gotsomething  == false) {
				    String item=Main.getRandom(Main.islandHolidayConfig.getStringList("Christmas.Presents.Gifts")); 
					String[] args = item.split(":");
					int Random = (int)(Math.random()*100);
					if(args.length==3) {
						if(Random<=Integer.valueOf(args[2])) {
							if(args[0].equalsIgnoreCase("command")) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), args[1].replace("%player%", event.getPlayer().getName()));
								gotsomething = true;
								break;
							}else {
								if(event.getPlayer().getInventory().firstEmpty()>-1) {
									event.getPlayer().getInventory().addItem(new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
								}else {
								event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), new ItemStack(Material.getMaterial(args[0].toUpperCase()),Integer.valueOf(args[1])));
								}gotsomething = true;
								break;
							}
						}
					}else {
					if(args.length==5) {
						if(Random<=Integer.valueOf(args[4])) {
							ItemStack gift = Main.createDisplay(Material.getMaterial(args[0].toUpperCase()), null, -2, ChatColor.translateAlternateColorCodes('&', args[1]).replace("%player%", event.getPlayer().getName()), ChatColor.translateAlternateColorCodes('&', args[2]).replace("%player%", event.getPlayer().getName()));
							gift.setAmount(Integer.valueOf(args[3]));
							if(event.getPlayer().getInventory().firstEmpty()>-1) {
								event.getPlayer().getInventory().addItem(gift);
							}else {
							event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(),gift);
							}
							//
							gotsomething = true;
							break;
						}
					}
					}
					}
	    	}
	    	}
	    	if(Main.islandHolidayConfig.getString("Christmas.Presents.OpenMessage")!=null) {
	    		Cooldowns.sendActionBarMessage(event.getPlayer(),ChatColor.translateAlternateColorCodes('&',Main.islandHolidayConfig.getString("Christmas.Presents.OpenMessage")).replace("%amount%", String.valueOf(getEnchantLevel("Present",event.getPlayer().getInventory().getItemInMainHand()))));
	    	}
	    	event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount()-1);
	    	
	    	return;
		}
		}
		}
	}
	public static Boolean fullSet(String string, ItemStack[] itemStacks) {
		boolean fullset = true;
		int helmet = 1;
		int boots = 2;
		int chestplate = 3;
		int pants = 4;
		if(itemStacks[0] != null) {
			if(!Enchants.hasCustomEnchant(string, itemStacks[0])) {
				fullset = false;
			}else {
				helmet = getEnchantLevel(string,itemStacks[0]);
			}
		}else{
			fullset = false;
		}
		if(itemStacks[1] != null) {
			if(!Enchants.hasCustomEnchant(string, itemStacks[1])) {
				fullset = false;
			}else {
				chestplate = getEnchantLevel(string,itemStacks[1]);
			}
		}else{
			fullset = false;
		}
		if(itemStacks[2] != null) {
			if(!Enchants.hasCustomEnchant(string, itemStacks[2])) {
				fullset = false;
			}else {
				pants = getEnchantLevel(string,itemStacks[2]);
			}
		}else{
			fullset = false;
		}
		if(itemStacks[3] != null) {
			if(!Enchants.hasCustomEnchant(string, itemStacks[3])) {
				fullset = false;
			}else {
				boots = getEnchantLevel(string,itemStacks[3]);
			}
		}else{
			fullset = false;
		}
		if(boots!=pants||boots!=chestplate||boots!=helmet) {
			fullset = false;
		}
		return fullset;
	}
	public static void sneakEnchant(PlayerToggleSneakEvent e) {
		Player player = e.getPlayer();
		if(fullSet("Naughty",player.getInventory().getArmorContents())){
			if(Cooldowns.isCooldownOver(Cooldowns.naughtyCoolDown,300/getEnchantLevel("Naughty", player.getInventory().getArmorContents()[0]), player)){
			int Random = (int)(Math.random()*64);
			player.getInventory().addItem(new ItemStack(Material.COAL,Random));
			player.sendMessage(Main.server+"you recieved "+Random+" coal!");
			return;
			}
		}
		if(fullSet("Loved",player.getInventory().getArmorContents())){
			if(Cooldowns.isCooldownOver(Cooldowns.lovedCoolDown,30, player)){
				Particles.createParticleTube(player.getLocation(), Particle.HEART, .5, 2, .3, true);
				return;
			}
		}
		if(fullSet("Nice",player.getInventory().getArmorContents())){
			if(Cooldowns.isCooldownOver(Cooldowns.niceCoolDown,300/getEnchantLevel("Nice", player.getInventory().getArmorContents()[0]), player)){
				int Random = (int)(Math.random()*100);
				Money.giveMoney(player.getName(),String.valueOf(Random));
				return;
			}
		}
	}	
}
